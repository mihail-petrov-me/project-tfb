(function () {

    var Controller = function (Translation, PubSub, UserModel, BundleModel, SubjectModel) {

        this.COLLECTION = {
            subject : []
        };


        this.FACTORY = {
            SUBJECT : function($title) {
                return {
                    'teacher_id'    : UserModel.profile.my("user_id"),
                    'bundle_id'     : UserModel.state('__bundle_id__'),
                    'subject_title' : $title
                }
            }
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en' : 'Assign existent class',
                    'bg' : 'Добави предметите, които ще преподаваш'
                })
            };
        });

        /**
         *
         */
        this.on_process= () => {
            PubSub.form.init("FORM_ASSIGN_TEACHER_SUBJECT");
        };

        /**
         *
         * @param $collection
         * @returns {Array}
         */
        this.subjectFactory = ($collection) => {

            let $processCollection = [];
            for(let index in $collection) {
                $processCollection.push(this.FACTORY.SUBJECT($collection[index]));
            }

            return $processCollection;
        };


        /**
         *
         * @param $collection
         */
        this.request_FETCH_subjectCollection = ($bundleId) => {

            SubjectModel.api.COLLECTION().fetch.mySubjects(UserModel.profile.my("user_id"), $bundleId)
                .then(this.handle_FETCH_subjectCollection_success)
                .catch(this.handle_FETCH_subjectCollection_error);
        };

        /**
         *
         */
        this.handle_FETCH_subjectCollection_success = ($response) => {
            this.COLLECTION.subject = $response.data;
        };

        /**
         *
         */
        this.handle_FETCH_subjectCollection_error = () => {
            console.log("@Exception: Cannot fetch subjects")
        };


        /**
         *
         * @param $collection
         */
        this.request_CREATE_subjectCollection = ($collection) => {

            SubjectModel.api.create.subject($collection)
                .then(this.handle_CREATE_subjectCollection_success)
                .catch(this.handle_CREATE_subjectCollection_error);
        };

        /**
         *
         */
        this.handle_CREATE_subjectCollection_success = () => {
            PubSub.api.publish("WIZARD::PROCESS");
        };

        /**
         *
         */
        this.handle_CREATE_subjectCollection_error = () => {
            console.log("@Exception: Cannot add subject")
        };


        /**
         * @type
         * Lifecycle event : INIT
         */
        this.$onInit = () => {

            if(this.uiBundle) {
                this.request_FETCH_subjectCollection(this.uiBundle);
            }

            PubSub.form.end("FORM_ASSIGN_TEACHER_SUBJECT", ($collection) => {
                this.request_CREATE_subjectCollection(this.subjectFactory($collection['subject']));
            });
        };
    };

    Controller.$inject  = ['Translation', 'PubSub', 'UserModel', 'BundleModel', 'SubjectModel'];
    let Component       = {
        templateUrl     : './add-subject.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            $router     : '<',
            uiBundle    : '<'
        }
    };

    angular.module('application.components.page').component('assignSubjects', Component);
})();
