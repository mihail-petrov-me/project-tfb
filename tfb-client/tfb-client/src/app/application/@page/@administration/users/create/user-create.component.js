(function () {

    var Controller = function (Translation, PubSub, UserModel, CampaignModel) {

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en' : '@Education campaign',
                    'bg' : '@Образователна кампания'
                })
            };
        });

        /**
         * @type
         * Event
         */
        this.on_create = () => {
            PubSub.form.init("FORM_CREATE_USER");
        };

        /**
         * @type
         * Request : FETCH : userfeed
         */
        this.request_CREATE_user = ($collection) => {

            CampaignModel.api.create.campaign($collection)
                .then(this.handle_CREATE_user_success)
                .catch(this.handle_CREATE_user_error);
        };

        /**
         * @type
         * Handler : FECH : SUCCESS
         */
        this.handle_CREATE_user_success = ($response) => {
            //this.$router.navigate(['/Dashboard/Administration/CampaignSingle', {'id' : $response.data.id}]);
        };

        /**
         * @type
         * Handler : FECH : ERROR
         */
        this.handle_CREATE_user_error = () => {
            console.log("@Exception: Cannot create new user");
        };

        /**
         * @type
         * Lifecicle Event : $INIT
         */
        this.$onInit = () => {

            PubSub.form.end('FORM_CREATE_USER', ($response) => {
                this.request_CREATE_user($response);
            });
        }
    };

    Controller.$inject  = ['Translation', 'PubSub', 'UserModel', 'CampaignModel'];
    var Component       = {
        templateUrl     : './user-create.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            $router     : '<'
        }
    };

    angular.module('application.components.page').component('usersCreate', Component);
})();
