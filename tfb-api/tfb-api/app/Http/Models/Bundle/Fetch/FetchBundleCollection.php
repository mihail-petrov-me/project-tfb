<?php
namespace App\Http\Models\Bundle\Fetch;

use App\Http\Models\Bundle\Bundle;
use App\Http\Models\Repository;
use DB;

/**
 * Class FetchBundleCollection
 * @package App\Http\Models\Bundle\Fetch
 */
class FetchBundleCollection extends Repository
{

    public $filter = [
        'teacher_id', 'school_id'
    ];

    public $filterTransform = [
        'teacher_id'          => 'teacher__bundle.teacher_id',
        'school_id'           => 'bundles.school_id'
    ];

    /**
     * @param $query
     *
     * @return mixed
     */
    public function execute($query)
    {
        return DB::table(Bundle::TABLE)
            ->leftJoin(
                Bundle::TABLE_TEACHERS,
                $this->table(Bundle::TABLE_TEACHERS, Bundle::ID),
                '=',
                $this->table(Bundle::TABLE, Bundle::ID)
            )
            ->whereNull('teacher__bundle.teacher_id')
            ->orderBy('bundles.position')
            ->orderBy('bundles.label')
            ->get();
    }
}