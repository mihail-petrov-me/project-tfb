(function () {

    var Controller = function (Translation, PubSub, UserModel, BundleModel, SubjectModel) {

        this.PROPERTY = {
            BUNDLE_ID       : null,
            SUBJECT_ID      : null,
            GOAL_ID         : null
        };

        this.COLLECTION = {
            subject         : [],
            primaryGoal     : {},
            secondaryGoal   : {}
        };

        this.STATE = {
            SUBJECT_ID          : null,
            GOAL_ID             : null,
            MUTATED_OBJECT_ID   : null,

            DELETE              : false,
            EDIT                : false
        };

        this.FACTORY = {

            GOAL : function($parentId, $subjectId, $title, $rigar) {
                return  {
                    'teacher_id' : UserModel.profile.my('id'),
                    'parent_id'  : $parentId,
                    'subject_id' : $subjectId,
                    'title'      : $title,
                    'rigar'      : $rigar
                };
            }
        };

        this.PARENT_ID = null;

        this.collection = [];

        this.collectionObject = {

        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en' : '@New bundle',
                    'bg' : '@Нов клас'
                })
            };
        });

        /**
         *
         * @param $id
         * @returns {*}
         */
        this.findCollection = ($id) => {

            for(let i = 0; i < this.collection.length; i++) {
                if(this.collection[i].id === $id) return this.collection[i];
            }
        };


        this.on_form_primaryGola = ($id) => {

            this.PROPERTY.SUBJECT_ID = $id;
            this.STATE.SUBJECT_ID    = $id;
        }

        this.on_form_secondaryGola = ($id, $subjectId) => {

            this.PROPERTY.GOAL_ID   = $id;
            this.STATE.GOAL_ID      = $id;
            this.STATE.SUBJECT_ID   = $subjectId;
        };

        this.show_form_primaryGoal = ($id) => {
            return (this.STATE.SUBJECT_ID == $id && this.STATE.GOAL_ID == null);
        };

        this.show_form_secondaryGoal = ($id) => {
            return (this.STATE.GOAL_ID == $id)
        };

        this.on_edit = ($id, $subjectId) => {

            this.STATE.EDIT                 = true;

            this.PROPERTY.SUBJECT_ID        = $subjectId;
            this.STATE.MUTATED_OBJECT_ID    = $id;
        };

        this.on_delete = ($id, $subjectId) => {

            this.STATE.DELETE               = true;

            this.PROPERTY.SUBJECT_ID        = $subjectId;
            this.STATE.MUTATED_OBJECT_ID    = $id;
        };

        this.on_delete_process = () => {
            this.request_ARCHIVE_goal();
        };

        this.on_cancel = () => {

            this.STATE.DELETE = false;
            this.STATE.MUTATED_OBJECT_ID = null;
        };


        this.show_form_interactive = () => {
            return ((this.STATE.DELETE == false) && (this.STATE.EDIT == false));
        };

        this.show_form_edit = ($id) => {
            return (this.STATE.EDIT == true && this.STATE.MUTATED_OBJECT_ID == $id);
        };

        this.show_form_delete = ($id) => {
            return (this.STATE.DELETE == true && this.STATE.MUTATED_OBJECT_ID == $id);
        };

        this.show_row = () => {
            return (this.STATE.EDIT == false);
        };


        /**
         *
         */
        this.request_FETCH_goal = () => {

            console.log(this.uiBundle);
            BundleModel.api.COLLECTION().fetch
                .goalCollection(this.uiBundle)
                .then(this.handle_FETCH_goal_success)
                .catch(this.handle_FETCH_goal_error);
        };

        /**
         *
         * @param $response
         */
        this.handle_FETCH_goal_success  = ($response) => {

            var map = {};
            for(var i = 0; i < $response.data.length; i++) {
                map[$response.data[i].subject_id] = $response.data[i];
            }

            for(var index in map) {
                this.COLLECTION.subject.push(map[index]);
            }


            for(var i = 0; i < $response.data.length; i++) {

                if(this.COLLECTION.primaryGoal[$response.data[i].subject_id]) {
                    this.COLLECTION.primaryGoal[$response.data[i].subject_id].push($response.data[i]);
                }
                else {
                    this.COLLECTION.primaryGoal[$response.data[i].subject_id] = [];
                    this.COLLECTION.primaryGoal[$response.data[i].subject_id].push($response.data[i]);
                }
            }
        };


        /**
         *
         */
        this.handle_FETCH_goal_error    = () => {
            console.log("@Exception: Could not fetch collection of goals");
        };



        /**
         *
         * @param $request
         * @param $collection
         */
        this.request_UPDATE_goal = ($id, $request) => {

            let $query = {
                'goal_id'    : $request.goal_id,
                'teacher_id' : UserModel.profile.my('id'),
                'parent_id'  : this.PROPERTY.GOAL_ID,
                'subject_id' : this.PROPERTY.SUBJECT_ID,
                'goal_title' : $request.goal_title,
                'rigar'      : $request.goal_rigar,
                'type'       : $request.goal_type
            };

            BundleModel.api.update.goal($id, $request.id, $query)
                .then(this.handle_UPDATE_goal_success)
                .catch(this.handle_UPDATE_goal_error)
        };

        /**
         *
         * @param $response
         */
        this.handle_UPDATE_goal_success = ($response) => {
            console.log($response);
        };

        /**
         *
         */
        this.handle_UPDATE_goal_error   = () => {

            console.log("@Exception: Response");
            this.PROPERTY.GOAL_ID = null;
        };


        /**
         *
         * @param $request
         * @param $collection
         */
        this.request_CREATE_goal = ($id, $request) => {

            let $query = {
                'teacher_id' : UserModel.profile.my('id'),
                'parent_id'  : this.PROPERTY.GOAL_ID,
                'subject_id' : this.PROPERTY.SUBJECT_ID,
                'goal_title' : $request.goal_title,
                'rigar'      : $request.goal_rigar,
                'type'       : $request.goal_type
            };


            BundleModel.api.create.goal($id, $query)
                .then(this.handle_CREATE_goal_success)
                .catch(this.handle_CREATE_goal_error)
        };

        /**
         *
         * @param $response
         */
        this.handle_CREATE_goal_success = ($response) => {

            // get request back
            var element = $response.data;

            if(element.parent_id) {

                if(!this.COLLECTION.secondaryGoal[this.PROPERTY.GOAL_ID]) {
                    this.COLLECTION.secondaryGoal[this.PROPERTY.GOAL_ID] = [];
                }

                this.COLLECTION.secondaryGoal[this.PROPERTY.GOAL_ID].push(element);
                this.PROPERTY.GOAL_ID   = null;
                this.STATE.GOAL_ID      = null;
            }
            else {

                if(!this.COLLECTION.primaryGoal[this.PROPERTY.SUBJECT_ID]) {
                    this.COLLECTION.primaryGoal[this.PROPERTY.SUBJECT_ID] = [];
                }

                this.COLLECTION.primaryGoal[this.PROPERTY.SUBJECT_ID].push(element);
                this.STATE.SUBJECT_ID = null;
            }

            this.STATE.SUBJECT_ID = null;
        };

        /**
         *
         */
        this.handle_CREATE_goal_error   = () => {

            console.log("@Exception: Response");
            this.PROPERTY.GOAL_ID = null;
        };


        this.request_ARCHIVE_goal = () => {

            BundleModel.api.archive
                .goal(this.PROPERTY.BUNDLE_ID, this.STATE.MUTATED_OBJECT_ID)
                .then(this.handle_ARCHIVE_goal_success)
                .catch(this.handle_ARCHIVE_goal_error);
        };

        this.handle_ARCHIVE_goal_success = () => {


            console.log(this.COLLECTION.primaryGoal);
            console.log(this.PROPERTY.SUBJECT_ID);


            // for(var $i = 0; $i < this.COLLECTION.primaryGoal[this.PROPERTY.SUBJECT_ID].length; $i++) {

            //     console.log(this.COLLECTION.primaryGoal[this.PROPERTY.SUBJECT_ID][$i].goal_id);
            //     console.log(this.STATE.MUTATED_OBJECT_ID);
            //     console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@==");

            //     if(this.COLLECTION.primaryGoal[this.PROPERTY.SUBJECT_ID][$i].goal_id == this.STATE.MUTATED_OBJECT_ID) {
            //        this.COLLECTION.primaryGoal[this.PROPERTY.SUBJECT_ID].splice($i, 1);
            //     }
            // }

            // // clean application state
            // this.STATE.MUTATED_OBJECT_ID    = null;
            // this.STATE.DELETE               = false;
        };

        this.handle_ARCHIVE_goal_error = () => {
            console.log("@Exception: This operation is not posible you can not archive this element");
        };


        this.on_next = () => {
            PubSub.api.publish("WIZARD::PROCESS");
        };

        /**
         * @type
         * Lifecycle event : INIT
         */
        this.$onInit = () => {

            if(this.uiBundle) {
                this.PROPERTY.BUNDLE_ID = this.uiBundle;
                this.request_FETCH_goal();
            }
            else {
                this.PROPERTY.BUNDLE_ID = UserModel.state('__bundle_id__');

                var subjectCollection = SubjectModel.api.fetch.mySubjects(UserModel.profile.my("user_id"), this.PROPERTY.BUNDLE_ID);

                // #
                subjectCollection.then(($response) => {
                    this.COLLECTION.subject = $response.data;
                });


                // #
                subjectCollection.catch(() => {
                    console.log("@Exception: Cannot get my subject collection");
                });
            }

            /**
             *
             */
            PubSub.form.end("FORM_CREATE_GOAL", ($response) => {
                this.request_CREATE_goal(this.PROPERTY.BUNDLE_ID, $response);
            });

            /**
             *
             */
            PubSub.form.endUpdate("FORM_CREATE_GOAL", ($response) => {
                this.request_UPDATE_goal(this.PROPERTY.BUNDLE_ID, $response);
            });
        };
    };

    Controller.$inject  = ['Translation', 'PubSub', 'UserModel', 'BundleModel', 'SubjectModel'];
    var Component       = {
        templateUrl     : './add-goals.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            $router     : '<',
            uiBundle    : '<'
        }
    };

    angular.module('application.components.page').component('assignGoals', Component);
})();
