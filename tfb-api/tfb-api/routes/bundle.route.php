<?php

// GET
// =====================================================================================================================
$app->get('bundles'                                , 'Tracker\BundleController@fetchBundleCollection'              );
$app->get('bundlesspecial'                         , 'Tracker\BundleController@fetchBundleSpecialCollection'       );
$app->get('bundles/{id}'                           , 'Tracker\BundleController@fetchBundleSingle'                  );
$app->get('bundles/{id}/student'                   , 'Tracker\BundleController@fetchStudentCollection'             );
$app->get('bundles/{id}/goal'                      , 'Tracker\BundleController@fetchGoalCollection'                );


// POST
// =====================================================================================================================
$app->post('bundles'                               , 'Tracker\BundleController@createBundle'                       );
$app->post('bundles/{id}/goal'                     , 'Tracker\BundleController@createGoal'                         );
$app->post('bundles/{id}/student'                  , 'Tracker\BundleController@createStudent'                      );

// PUT
// =====================================================================================================================
$app->put('bundles/{id}'                           , 'Tracker\BundleController@updateBundle'                       );
$app->put('bundles/{id}/add'                       , 'Tracker\BundleController@addBundleToTeacher'                 );
$app->put('bundles/{id}/goal/{goal_id}'            , 'Tracker\BundleController@updateGoal'                         );
$app->put('bundles/{id}/student/{student_id}'      , 'Tracker\BundleController@updateStudent'                      );

// DELETE
// =====================================================================================================================
$app->delete('bundles/{id}'                        , 'Tracker\BundleController@deleteBundle'                       );
$app->delete('bundles/{id}/student/{student_id}'   , 'Tracker\BundleController@deleteStudent'                      );
$app->delete('bundles/{id}/goal/{goal_id}'         , 'Tracker\BundleController@archiveGoal'                        );