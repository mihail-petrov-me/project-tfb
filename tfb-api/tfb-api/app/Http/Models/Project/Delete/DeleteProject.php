<?php
namespace App\Http\Models\Project\Delete;


use App\Http\Models\Project\Project;
use \App\Http\Models\Repository;

/**
 * Class DeleteProject
 */
class DeleteProject extends Repository
{
    /**
     * @param $id
     *
     * @return mixed
     */
    public function execute($id)
    {
        return DB::table(Project::TABLE)->where([
            Project::ID => $id
        ])->update([
            Project::DELETED_AT     => time() // Carbon::now()
        ])
        ->get();
    }
}