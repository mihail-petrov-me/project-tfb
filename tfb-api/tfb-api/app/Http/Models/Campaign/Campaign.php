<?php

namespace App\Http\Models\Campaign;

use App\Http\Models\Model;
use App\Http\Models\Campaign\Create\CreateCampaignTeacher;
use App\Http\Models\Campaign\Create\CreateCampaign;
use App\Http\Models\Campaign\Fetch\FetchCampaignCollection;
use App\Http\Models\Campaign\Fetch\FetchCampaignSingle;
use App\Http\Models\Campaign\Fetch\FetchCampaignTeacher;

/**
 * Class Campaign
 * @package App\Http\Models\Campaign
 */
class Campaign extends Model
{
    // Public fields
    const ID            = 'campaign_id';
    const TYPE          = 'campaign_type';
    const TITLE         = 'campaign_title';
    const IS_ACTIVE     = 'campaign_is_active';

    // Meta fields
    const CREATED_AT    = 'created_at';
    const UPDATED_AT    = 'updated_at';
    const DELETED_AT    = 'deleted_at';

    // Tables
    const TABLE         = 'campaigns';
    const TABLE_TEACHER = 'campaign__teacher';

    /**
     * @param $bundle
     * @return mixed
     */
    public function single($bundle)
    {
        return (new FetchCampaignSingle())->execute($bundle['id']);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function collection($query)
    {
        return (new FetchCampaignCollection())->execute($query);
    }

    /**
     * @param $collection
     * @param null $query
     * @return array
     */
    public function store($collection, $query = null)
    {
        return (new CreateCampaign())->execute($collection, $query);
    }

    /**
     * @return mixed
     */
    public function fetch_campaign_teacher()
    {
        return (new FetchCampaignTeacher())->execute();
    }


    /**
     * @param $id
     * @param $collection
     * @param null $query
     * @return array
     */
    public function create_campaign_teacher($id, $collection, $query = null)
    {
        return (new CreateCampaignTeacher())->execute($id, $collection);
    }

    /**
     * @param $bundle
     * @param $collection
     * @param null $query
     */
    public function update($bundle, $collection, $query = null)
    {

    }

    /**
     * @param $bundle
     * @param $collection
     * @param null $query
     */
    public function delete($bundle, $collection, $query = null)
    {

    }
}