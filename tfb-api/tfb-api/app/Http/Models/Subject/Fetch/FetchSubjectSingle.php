<?php

namespace App\Http\Models\Subject\Fetch;
use App\Http\Models\Repository;
use DB;

class FetchSubjectSingle extends Repository
{
    public function execute($id)
    {
        return DB::table('goals')->where(array(
            'id'    => $id
        ))->get();
    }
}