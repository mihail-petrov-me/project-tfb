<?php

// GET
// =====================================================================================================================
$app->get('/users'                     , ['uses'  => 'Application\UserController@index'                                         ]);
$app->get('/users/teachers'            , ['uses'  => 'Application\UserController@fetchTeachers'                                 ]);
$app->get('/users/alumnies'            , ['uses'  => 'Application\UserController@index'                                         ]);
$app->get('/users/employes'            , ['uses'  => 'Application\UserController@index'                                         ]);
$app->get('/users/employes'            , ['uses'  => 'Application\UserController@index'                                         ]);

$app->get('/users/{id}/contact'        , ['uses'  => 'Application\UserController@showContact'                                   ]);
$app->get('/users/{id}/education'      , ['uses'  => 'Application\UserController@showEducation'                                 ]);
$app->get('/users/{id}/employment'     , ['uses'  => 'Application\UserController@showEmployment'                                ]);
$app->get('/users/{id}/language'       , ['uses'  => 'Application\UserController@showLanguage'                                  ]);

// POST routes
// =====================================================================================================================
$app->post('/users'                    , ['uses'  => 'Application\UserController@store'                                         ]);
$app->post('/users/auth/{provider}'    , ['uses'  => 'Application\UserController@auth'                                          ]);
$app->post('/users/{id}/picture'       , ['uses'  => 'Application\UserController@updatePicture'                                 ]);
$app->post('/users/{id}/education'     , ['uses'  => 'Application\UserController@storeUserEducation'                            ]);
$app->post('/users/{id}/employment'    , ['uses'  => 'Application\UserController@storeUserEmployment'                           ]);

// PUT routes
// =====================================================================================================================
$app->put('/users/{id}'                , ['uses'  => 'Application\UserController@update'                                        ]);
$app->put('/users/{id}/contact'        , ['uses'  => 'Application\UserController@updateContact'                                 ]);
$app->put('/users/{id}/education'      , ['uses'  => 'Application\UserController@updateEducation'                               ]);
$app->put('/users/{id}/employment'     , ['uses'  => 'Application\UserController@updateEmployment'                              ]);
$app->put('/users/{id}/language'       , ['uses'  => 'Application\UserController@updateLanguage'                                ]);

// DELETE routes
// =====================================================================================================================
$app->delete('/users/{id}'             , ['uses'  => 'Application\UserController@delete'                   , 'middleware' => 'jwt']);