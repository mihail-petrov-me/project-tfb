(function () {

    /**
     *
     * @param {*} Translation
     * @param {*} ProjectModel
     * @param {*} PubSub
     */
    var Controller = function (Translation, ProjectModel, UserModel, PubSub, Form) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;

        /**
         * @type
         * State collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.STATE = {
            IS_LOADING: false
        };


        /**
         * @type
         * Event Publish collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.PUBLISH = {

            PAGE_LOAD           : PubSub.events.page.LOAD,

            GROUP_CREATE_INIT   : PubSub.events.page.GROUP.CREATE.INIT,
            GROUP_CREATE_DESTROY: PubSub.events.page.GROUP.CREATE.DESTROY,

            FORM_PROCESS_INIT   : PubSub.events.form.PROCESS.INIT,
        };

        /**
         * @type
         * Event Subscribe collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.SUBSCRIBE = {
            FORM_PROCESS_END: PubSub.events.form.PROCESS.END
        };


        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(function ($provider) {

            model.LABEL = {

                PAGE_TITLE: $provider({
                    'en': 'Create New Group',
                    'bg': 'Нов проект'
                }),

                BUTTON_CREATE: $provider({
                    'en': 'Create New Group',
                    'bg': 'Нов проект'
                })
            };
        });




        /**
         * @type
         * Event : ON
         */
        model.on_process = function () {

            model.STATE.IS_LOADING = true;
            PubSub.api.publish(model.PUBLISH.FORM_PROCESS_INIT);
        };


        /**
         *
         */
        model.handlerSuccess = function (response) {

            model.STATE.IS_LOADING = false;
            UserModel.profile.setSession(response.tokken);

            window.location.href = "#!/dashboard/projects/" + response.data.id;
        };

        /**
         *
         */
        model.handlerError = function () {
            model.STATE.IS_LOADING = false;
        };

        /**
         *
         */
        model.on_close = function() {
            PubSub.api.publish('close_model_window');
        };


        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        model.$onInit = function () {

            /**
             * @type
             * Publisher
             */
            PubSub.api.publish(model.PUBLISH.GROUP_CREATE_INIT);


            /**
             * @type
             * Subscriber
             */
            PubSub.api.subscribe(model.SUBSCRIBE.FORM_PROCESS_END, function (data) {

                console.log(data);
                console.log("***");


                // data.user_id = UserModel.profile.my('user_id');
                // data.is_owner = 1;

                ProjectModel.api.create
                    .project(data)
                        .then(model.handlerSuccess)
                        .catch(model.handlerError);
            });

        };

        /**
         * @type
         * Lifecycle event
         */
        model.$onDestroy = function () {

            /**
             * @type
             * Publisher
             */
            PubSub.api.publish(model.PUBLISH.GROUP_CREATE_DESTROY);

            /**
             * @type
             * Unsubscribe
             */
            PubSub.api.unsubscribe(model.SUBSCRIBE.FORM_PROCESS_END);
        };

        /**
         * @type
         * Lifecycle event
         */
        model.$postLink = function () {
            PubSub.api.publish(model.PUBLISH.PAGE_LOAD);
        };

    };

    /**
     * @type
     * Component injector
     */
    Controller.$inject = ['Translation', 'ProjectModel', 'UserModel', 'PubSub', 'Form'];

    var Component = {
        templateUrl: './project-create.template.html',
        controller: Controller,
        controllerAs: 'model'
    };

    angular.module('application.components.page').component('projectModify', Component);

})();
