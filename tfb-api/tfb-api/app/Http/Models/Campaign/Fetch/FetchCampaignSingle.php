<?php

namespace App\Http\Models\Campaign\Fetch;
use DB;

class FetchCampaignSingle
{
    public function execute($id)
    {
        return DB::table('tt_campaigns')->where(array(
            'id' => $id
        ))->get();
    }


    public function build()
    {
        return array('id', 'title');
    }    
}
