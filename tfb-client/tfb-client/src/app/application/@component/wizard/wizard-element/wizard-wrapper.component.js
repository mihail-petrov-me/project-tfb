(function () {

    /**
     *
     * @param {*} Translation
     * @param {*} PubSub
     * @param {*} Form
     */
    var Controller = function(Translation, PubSub) {

        this.PROPERTY = {
            COLLECTION        : [],
            POINTER           : 0
        };

        this.STATE = {
            IS_HEADER_VISIBLE   : true,
            SELECTED_TAB        : null
        };


        this.hide = () => {
            for(let i = 0; i < this.PROPERTY.COLLECTION.length; i++) {
                this.PROPERTY.COLLECTION[i].isVisible = false;
            }
        };


        this.on_select = (element, $index) => {

            if(!this.isStep) {
                this.hide();
                element.isVisible = true;
            }

            if(this.isStep) {
                this.STATE.SELECTED_TAB = $index;

                if(this.PROPERTY.POINTER > $index) {
                    this.hide();
                    element.isVisible = true;
                }
            }
        };


        this.is_selected = ($index) => {


            if(this.PROPERTY.POINTER == $index) {
                return 'tab-selected';
            }

            if(this.PROPERTY.POINTER > $index) {
                return 'tab-visited';
            }

            return '';
        };


        this.$onInit = () => {

            PubSub.api.subscribe('WIZARD::PROCESS', () => {

                this.PROPERTY.POINTER++;

                this.hide();
                this.PROPERTY.COLLECTION[this.PROPERTY.POINTER].isVisible = true;

                if(this.PROPERTY.COLLECTION[this.PROPERTY.POINTER].uiFinish) {
                    this.STATE.IS_HEADER_VISIBLE = false;
                }
            });
        };




        this.$on_process = () => {

        }
    };


    var Component = {
        templateUrl     : './wizard-wrapper.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        transclude      : true,
        bindings        : {
            isStep      : '@'
        }
    };

    angular.module('application.components.visual').component('wizardWrapper', Component);

})();
