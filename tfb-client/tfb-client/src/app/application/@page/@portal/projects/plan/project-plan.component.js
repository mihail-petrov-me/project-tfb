(function () {

    /**
     *
     * @param {*} Translation
     * @param {*} ProjectModel
     * @param {*} PubSub
     */
    var Controller = function (Translation, ProjectModel, UserModel, PubSub, Form) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;

        model.STATE = {
            IS_ACTIVE : false
        }

        model.on_show = function() {
            model.STATE.IS_ACTIVE = true;
        };


        model.collection =[
            {
                title : 'A'
            },

            {
                title : 'B'
            },

            {
                title : 'C'
            },

            {
                title : 'D'
            },

            {
                title : 'E'
            },

            {
                title : 'F'
            }


        ]

    };

    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['Translation', 'ProjectModel', 'UserModel', 'PubSub', 'Form'];

    /**
     *
     */
    var Component = {
        templateUrl: './project-plan.template.html',
        controller: Controller,
        controllerAs: 'model'
    };

    angular.module('application.components.page').component('projectPlan', Component);

})();
