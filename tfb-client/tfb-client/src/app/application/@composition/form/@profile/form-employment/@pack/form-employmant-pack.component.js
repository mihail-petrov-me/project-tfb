(function () {

    /**
     *
     */
    var Controller = function(PubSub, UserModel) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;

        model.on_submit = function() {
            PubSub.api.publish(PubSub.events.form.PROCESS.INIT, 'employment');
        };


        PubSub.api.subscribe(PubSub.events.form.PROCESS.END, function($response) {

            console.log($response);

            UserModel.api.create
                .employment(UserModel.profile.my("user_id"), $response)
                    .then(function() {
                        model.parent.on_close();
                        console.log("Close window");
                    })
                    .catch(function() {

                    });
        });


        model.$onDestroy = function() {
            PubSub.api.unsubscribe(PubSub.events.form.PROCESS.END);
        };
    };


    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['PubSub', 'UserModel'];

    /**
     *
     */
    var Component = {
        templateUrl     : './form-employmant-pack.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        require         : {
            parent : '^formDialog'
        }
    };

    /**
     *
     */
    angular.module('application.components.page').component('formEmploymentPack', Component);
})();
