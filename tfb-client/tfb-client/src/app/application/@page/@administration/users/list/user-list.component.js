(function () {

    var Controller = function (Translation, PubSub, UserModel, CampaignModel) {

        /**
         *
         */
        this.STATE = {
            IS_SIDEBAR_VISIBLE : false
        };

        /**
         *
         */
        this.COLLECTION = {
            user : []
        };

        /**
         *
         */
        this.ENTITY = {
            user_name        : null,
            user_permissions : null,
            user_contact     : null,
            user_education   : null
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en' : '@Education campaign',
                    'bg' : '@Образователна кампания'
                })
            };
        });

        /**
         * @type
         * Event: ON
         */
        this.on_select = (element) => {

            console.log("@@@@")
            console.log(element);
            console.log("@@@@")

            this.STATE.IS_SIDEBAR_VISIBLE = true;


            this.ENTITY.user_name                       = element.user_name;
            this.ENTITY.user_signature                  = element.user_signature;
            this.ENTITY.user_permissions                = JSON.parse(element.user_permissions);

            //
            this.ENTITY.user_education                  = element.user_education;

            this.ENTITY.user_contact_phone              = element.user_contact_phone;
            this.ENTITY.user_contact_birth_date         = element.user_contact_birth_date;
            this.ENTITY.user_contact_birth_place        = element.user_contact_birth_place;
            this.ENTITY.user_contact_address            = element.user_contact_address;

            // this.ENTITY.user_education_institution      = element.user_education_institution;
            // this.ENTITY.user_education_date_start       = element.user_education_date_start;
            // this.ENTITY.user_education_date_end         = element.user_education_date_end;
            // this.ENTITY.user_education_degree           = element.user_education_degree;
            // this.ENTITY.user_education_field_of_study   = element.user_education_field_of_study;
        };


        this.permition = (permitionIndex) => {

            switch(permitionIndex) {
                case 1 :  return 'учител';
                case 2 :  return 'алумни';
                case 3 :  return 'служител';
                case 4 :  return 'координатор';
                case 5 :  return 'администратор';
            }
        };

        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {

            let handler = UserModel.api.fetch.users();

            handler.then(($request) => {
                this.COLLECTION.user = $request.data;
            });

            handler.catch(() => {
                console.log("@Exception: Could not fetch user collection");
            });
        };

        /**
         * @type
         * Lifecycle event
         */
        this.$onDestroy = function() {

        };
    };

    Controller.$inject  = ['Translation', 'PubSub', 'UserModel', 'CampaignModel'];
    var Component       = {
        templateUrl     : './user-list.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('usersList', Component);
})();
