<?php
use \App\Http\Models\Group\Group;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration
{
    private $table = Group::TABLE;

    public function up()
    {
        Schema::create($this->table, function (Blueprint $builder)
        {
            //# Primary Key
            $builder->char(Group::ID, 13)->primary();

            //# Table Entity Collection
            $builder->string(Group::TITLE, 100);
            $builder->text(Group::DESCRIPTION);
            $builder->text(Group::CAUSES)->nullable();
            $builder->TEXT(Group::PICTURE)->nullable();

            //# Meta
            $builder->timestamps();
            $builder->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop($this->table);
    }
}
