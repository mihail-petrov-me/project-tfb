(function () {

    var Controller = function(Translation) {

        this.name = "FORM_CREATE_NOTE";

        /**
         * @component
         * Fields collection
         *
         */
        this.FIELD = {

            "note": {
                name            : "note",
                type            : 'textarea',
                value           : null,
                show_errors     : false,
                isRequired      : true
            }
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {
                INPUT_NOTE: $provider({
                    "bg"    : "Бележка за ученика",
                    "en"    : "Note for this particular student"
                })
            };
        });


        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {
            this.parent.FIELD  = this.FIELD;
            this.formContact   = this.parent.formContact;
        }
    };


    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['Translation', 'PubSub', 'Form'];

    var Component = {
        templateUrl     : './form-create-note.template.html',
        controller      : Controller,
        controllerAs    : 'model',

        require             : {
            parent          : '^formWrapper'
        },

        bindings        : {
            uiCollection    : '<',
            uiIsDisabled    : '<',
            uiId            : '<',
        }
    };
    angular.module('application.components.page').component('formCreateNote', Component);

})();
