<?php
namespace App\Http\Models\Bundle\Delete;

use App\Http\Models\Bundle\Bundle;
use App\Http\Models\Repository;
use App\Http\Models\Student\Student;

class DeleteStudent extends Repository
{
    public function execute($bundleId, $studentId)
    {
        $this->delete(Student::TABLE, [
            Bundle::ID  => $bundleId,
            Student::ID => $studentId
        ]);
    }
}