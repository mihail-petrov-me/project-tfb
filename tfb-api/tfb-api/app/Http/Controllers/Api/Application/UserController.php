<?php
namespace App\Http\Controllers\Api\Application;

use App\Http\Models\User\User;
use App\Services\Authentication\AuthenticationService;
use \App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use \Illuminate\Http\Request;

use \App\Services\Token\TokenProvider;
use App\Http\Models\User\Create\CreateUserEducation;
use App\Http\Models\User\Create\CreateUserEmployment;

use DB;
use Image;

/**
 * Class UserController
 * @package App\Http\Controllers\Api\Application
 */
class UserController extends Controller
{
    /**
     *
     * @var array
     */
    private $API_MESSAGES = [
        "USER_AUTH"     => "Success user authentication",
        "USER_CREATED"  => "User created successfuly",
        "USER_UPDATED"  => "User udated successfully",
        "USER_DELETE"   => "User deleted successfully"
    ];
    
    /**
     *
     * @var array
     */
    private $API_ERROR_MESSAGES = [
        "USER_AUTH"     => "Unnable to authenticate the user, usermail or password does not match",
        "USER_CREATED"  => "Something whent wrong during user initialisation",
        "USER_UPDATED"  => "The requested resourse could not be updated"
    ];

    /**
     * Contains the service provider with witch the user can authenticate 
     * it self in front of the system
     * 
     * @var AuthenticationService 
     */
    private $authentication;
    
    /**
     * @var AuthenticationService 
     */
    private $tokenProvider;
    
    /**
     * 
     * @var type 
     */
    private $model;

    /**
     *
     * @param AuthenticationService $provider
     * @param TokenProvider $tokenProvider
     * @param User $user
     */
    public function __construct(AuthenticationService $provider, TokenProvider $tokenProvider, User $user)
    {
        $this->authentication    = $provider;
        $this->model             = $user;
        $this->tokenProvider     = $tokenProvider;
    }

    /**
     * @method  GET
     * @path    users
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $collection = $this->model->all($request->query());
        return $this->responseSuccess($collection, $this->model->getMeta());
    }

    /**
     * GET : users/{id}/contact
     * @param $id
     *
     * @return JsonResponse
     */
    public function showSignature($id): JsonResponse
    {
        $entity = $this->model->get_userSignature($id);
        return $this->responseSuccess($entity);
    }

    /**
     * @method  GET
     * @path    users/{id}/contact
     *
     * @param   $id
     * @return JsonResponse
     */
    public function showContact($id): JsonResponse
    {
        $collection = $this->model->get_userContact($id);
        return $this->responseSuccess($collection);
    }

    /**
     * GET : users/{id}/education
     * @param $id
     *
     * @return JsonResponse
     */
    public function showEducation($id): JsonResponse
    {
        $collection = $this->model->get_userEducation($id);
        return $this->responseSuccess($collection);
    }


    /**
     * @return JsonResponse
     */
    public function fetchTeachers(): JsonResponse
    {
        $collection = $this->model->get_teachers();
        return $this->responseSuccess($collection);
    }

    /**
     *  GET : users/{id}/employment
     * @param $id
     *
     * @return JsonResponse
     */
    public function showEmployment($id): JsonResponse
    {
        $collection = $this->model->get_userEmployment($id);
        return $this->responseSuccess($collection);
    }

    /**
     *  GET : users/{id}/language
     * @param $id
     *
     * @return JsonResponse
     */
    public function showLanguage($id): JsonResponse
    {
        $collection = $this->model->get_userLanguage($id);
        return $this->responseSuccess($collection);
    }

    /**
     * @param         $provider
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function auth($provider, Request $request)
    {
        try {
            $collection                 = $request->all();
            $authenticationReference    = $this->authentication->authenticate($provider, $collection);
            $this->tokenProvider->push('data', $authenticationReference);

            return $this->responseSuccess($authenticationReference, [
                'token' => $this->tokenProvider->build()
            ]);
        }
        catch(\Exception $exception)
        {
            return $this->responseNotFound($this->API_ERROR_MESSAGES['USER_AUTH']);
        }
    }

    /**
     *
     * @param type $collection
     * @return type
     */
    private function store_success($id, $collection): JsonResponse
    {
        $transfromator = $this->model->transform($id, $collection);
        $this->tokenProvider->set(TokenProvider::PROFILE, $transfromator);

        return $this->responseSuccess([
            'token'    => $this->tokenProvider->build()
        ]);
    }

    // POST : /users
    // ##
    public function store(Request $request)
    {
        $collection = $request->json()->all();

        $requestIdentificator = $this->model->store_user($collection);
        if($requestIdentificator) {
            return $this->store_success($requestIdentificator, $collection);
        }

        return $this->responseNotFound($this->API_MESSAGES['USER_CREATED']);
    }

    /**
     *
     * @param type $id
     * @param Request $request
     * @return type
     */
    public function updatePicture($id, Request $request)
    {
        if( $request->hasFile('file') ) {

            $image = $request->file('file');
            $pictureIdentificator = uniqid();
            $path = 'http://api.zaednovchas.bg/uploads/' . $pictureIdentificator;

            // create an image thumbnail
            $img = Image::make($image->getRealPath());
            $img->resize(66, 66, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads').'/'. $pictureIdentificator . '.' . $image->getClientOriginalExtension());

            DB::table('users')->where('id', $id)->update(array(
                'picture' => $path,
            ));

            // Compse new token
            // #
            $tokken = $request->headers->get('x-authorize');
            $this->tokkenProvider->decompose($tokken);
            $this->tokkenProvider->update(TokenProvider::PROFILE, 'picture', $path);

            // Response
            // #
            return $this->responseSuccess($this->API_MESSAGES['USER_CREATED'], array(
                'tokken'    => $this->tokkenProvider->build()
            ));
        }
    }

   /**
     *
     * @param type $id
     * @return type
     */
    public function updateContact($id, Request $request)
    {
        $result = $request->json()->all();
        $this->model->update_userContact($id, $result);
        return $this->responseSuccess($this->API_MESSAGES['USER_UPDATED']);
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function updateEducation($id, Request $request)
    {
        $result = $request->json()->all();
        $this->model->update_userEducation($id, $result);
        return $this->responseSuccess($this->API_MESSAGES['USER_UPDATED']);
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function updateEmployment($id, Request $request)
    {
        $result = $request->json()->all();
        $this->model->update_userEmployment($id, $result);
        return $this->responseSuccess($this->API_MESSAGES['USER_UPDATED']);
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function updateLanguage($id, Request $request)
    {
        $result = $request->json()->all();
        $this->model->update_userLanguage($id, $result);
        return $this->responseSuccess($this->API_MESSAGES['USER_UPDATED']);
    }


    /**
     *
     * @param type $id
     * @param Request $request
     */
    public function storeUserEducation($id, Request $request)
    {
        (new CreateUserEducation())->execute($id, $request->json()->all());
    }

    /**
     *
     * @param type $id
     * @param Request $request
     */
    public function storeUserEmployment($id, Request $request)
    {
        (new CreateUserEmployment())->execute($id, $request->json()->all());
    }
}