(function () {

    var Controller = FormController (function(Translation) {

        /**
         * @type
         * State provider
         */
        var model = this;


        /**
         * @component
         */
        model.FIELD = {

            "project_title"     : {
                name            : "project_title",
                type            : 'text',
                value           : null,
                show_errors     : false,
                error           : 'Field is required'
            },

            "project_description": {
                name            : "project_description",
                type            : 'textarea',
                value           : null,
                show_errors     : false,
                error           : 'Field is required'
            },

            "project_date_start"    : {
                name            : "project_date_start",
                type            : 'date',
                value           : null,
                show_errors     : false,
                error           : 'Field is required'
            },


            "project_date_end"    : {
                name            : "project_date_end",
                type            : 'date',
                value           : null,
                show_errors     : false,
                error           : 'Field is required'
            },
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(function($provider) {

            model.LABEL = {

                PAGE_TITLE          :  $provider({
                    'en' : 'Create New Project',
                    'bg' : 'Нов проект'
                }),


                BUTTON_CREATE       :  $provider({
                    'en' : 'Create New Project',
                    'bg' : 'нов проект'
                }),

                INPUT_TITLE         :  $provider({
                    'en' : 'Project title',
                    'bg' : 'Наименование'
                }),

                INPUT_DESCRIPTION   :  $provider({
                    'en' : 'Project description',
                    'bg' : 'Описание'
                }),

                INPUT_DATE_START        :  $provider({
                    'en' : 'Project date start',
                    'bg' : 'Начало на проекта'
                }),

                INPUT_DATE_END        :  $provider({
                    'en' : 'Project date end',
                    'bg' : 'Край на проекта'
                }),

            };
        });
    });

    /**
     * @type
     * Component injector
     */
    Controller.$inject = ['Translation', 'PubSub', 'Form'];

    /**
     *
     */
    var Component = {
        templateUrl     : './form-create.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('formProjectCreate', Component);

})();
