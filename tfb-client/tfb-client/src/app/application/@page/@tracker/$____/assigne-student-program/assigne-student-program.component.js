(function () {

    /**
     *
     * @param {*} UserModel
     * @param {*} Translation
     * @param {*} State
     * @param {*} PubSub
     */
    var Controller = function (UserModel, Translation, State, PubSub) {

        this.STATE = {
            EVENT_VIEW : false
        };


        this.on_process = () => {

        };


        /**
         * @type
         * EVENT : request
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         */
        this.request_FETCH_user = () => {

            UserModel.api.SINGLE().fetch
                .experiance(UserModel.profile.my("user_id"))
                    .then(this.handler_experiance_success)
                    .catch(this.handler_experiance_error);
        };

        /**
         * @type
         * HANDLER :  success
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        this.handler_experiance_success = ($response) => {
            this.STATE.MESSAGE_NO_SCHOOL_AVAILABLE = ($response.data.length == 0);
        };

        /**
         * @type
         * HANDLER :  error
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        this.handler_experiance_error = () => {
            console.log("@Exception: When we try to get user experiance");
        };

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        this.$onInit = () => {
            this.request_FETCH_user();
        };

        this.add_event = () => {
            this.STATE.EVENT_VIEW = true;
        };

        this.on_close = () => {
            this.STATE.EVENT_VIEW = false;
        }
    };

    Controller.$inject = ['UserModel', 'Translation', 'State', 'PubSub'];
    var Component = {
        templateUrl     : './assigne-student-program.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('assigneStudentProgram', Component);

})();
