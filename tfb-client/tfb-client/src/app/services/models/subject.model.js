(function () {

    var BASE_URL = "subjects";

    /**
     * @type
     * Class creation services
     *
     * @description
     * Contains collection of methods for creating an Group related items
     */
    var create = function(request) {

        return {
            subject   : ($collection)   => request.post([BASE_URL], $collection),
            // goal   : ($id, $collection)   => request.post([BASE_URL, $id, 'goal'], $collection),
            // bundle   : ($id, $collection)   => request.post([BASE_URL, 'teacher', $id, 'bundle'], $collection),
            // studеnt  : ($id, $collection)   => request.post([BASE_URL, 'bundles', $id, 'student'], $collection),
            // goal     : ($collection)        => request.post([BASE_URL, 'goals'], $collection),
            // note     : ($teacherId, $studentId, $collection)        => request.post([BASE_URL,'teachers', $teacherId, 'student', $studentId, 'note'], $collection),
        };
    };

    /**
     * @type
     * Group fetch services
     *
     * @description
     * Contains collection of methods for fetching an Group related items
     */
    var fetch = function(request, requestType) {

        return {
                mySubjects : ($teacherId, $bundleId)     => request.beffore(requestType).get([BASE_URL, 'teacher', $teacherId, 'bundle', $bundleId]),
            // bundleSingle        : ($id)     => request.beffore(requestType).get([BASE_URL, $id]),
            // bundleCollection    : ($query)  => request.beffore(requestType).get([BASE_URL], $query),

            // bundles     : ($id) => request.beffore(requestType).get([BASE_URL, 'teacher', $id, 'bundles']),
            // teachers    : ($id) => request.beffore(requestType).get([BASE_URL, 'teacher', $id]),
            // students    : ($id, $bundleId)  => request.beffore(requestType).get([BASE_URL, 'teacher', $id, 'bundles', $bundleId]),
        };
    }

    /**
     * @type
     * Class update services
     *
     * @description
     * Contains collection of methods for updating an Group related items
     */
    var update = function(request) {

        return {
            assign   : ($id, $collection) => request.put([BASE_URL, $id, 'add'], $collection)
        };
    };

    /**
     * @type
     * Group fetch services
     *
     * @description
     * Contains collection of methods for archiving an Group related items
     */
    var archive = function(request) {

        return {
            // student : ($id) => request.delete([BASE_URL,'student', $id])
        };
    };

    /**
     *
     * @param {*} request
     * @param {*} base
     * @param {*} provider
     */
    var ApiProvider = function(request) {

        var requestType = {
            type : 'collection'
        };

        return {

            PAGINATE : function() {
                requestType.type = 'pagination';
                return this;
            },

            REFRESH : function() {
                requestType.type = 'refresh';
                return this;
            },

            COLLECTION : function() {
                requestType.type = 'collection';
                return this;
            },

            SINGLE : function() {
                requestType.type = 'single';
                return this;
            },

            create  : create(request),
            fetch   : fetch(request, requestType),
            update  : update(request),
            archive : archive(request)
        };
    };

    /**
     *
     * @param {*} http
     * @param {*} q
     * @param {*} AppManager
     */
    var ServiceHandler = function (Request) {

        return { api: ApiProvider(Request) };
    };

    ServiceHandler.$inject = ['Request'];
    angular.module('application.models').factory('SubjectModel', ServiceHandler);
})();
