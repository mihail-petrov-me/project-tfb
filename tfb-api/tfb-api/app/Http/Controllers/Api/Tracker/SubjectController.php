<?php
namespace App\Http\Controllers\Api\Tracker;

use App\Http\Models\Subject\Subject;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use \Illuminate\Http\Request;
use \App\Http\Controllers\Controller;
use \App\Services\TokenProvider;

/**
 * Class SubjectController
 * @package App\Http\Controllers\Api\Tracker
 */
class SubjectController extends Controller
{

    /**
     * @var array
     */
    private $API_ERROR_MESSAGES = [
        'FETCH' => 'Requested entity was not found'
    ];

    /**
     * @var Subject
     */
    private $model;

    /**
     * SubjectController constructor.
     *
     * @param Subject $model
     */
    public function __construct(Subject $model)
    {
        $this->model = $model;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function collection(Request $request): JsonResponse
    {
        $collection = $this->model->collection($request->query());
        return $this->responseSuccess($collection);
    }

    /**
     * @param $teacherId
     * @param $bundleId
     *
     * @return JsonResponse
     */
    public function mySubjects($teacherId, $bundleId): JsonResponse
    {
        $collection = $this->model->mySubjects($teacherId, $bundleId);
        return $this->responseSuccess($collection);
    }


    /**
     * @param $id
     *
     * @return JsonResponse
     */
    public function single($id): JsonResponse
    {
        $collection = $this->model->single($id);
        return $this->responseSuccess($collection);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $collection = $this->model->create($request->json()->all());
        return $this->responseSuccess($collection);
    }

    /**
     * @param         $id
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function update($id, Request $request): JsonResponse
    {
        $collection = $this->model->update($id, $request->json()->all());
        return $this->responseSuccess( $collection);
    }

    /**
     * @param $id
     *
     * @return JsonResponse
     */
    public function delete($id): JsonResponse
    {
        $collection = $this->model->delete($id);
        return $this->responseSuccess($collection);
    }
}