(function () {

    /**
     *
     */
    var BASE_URL = "projects";

    /**
     * @type
     * Project creation services
     */
    var create = function(request) {

        /**
         * @endpoint
         * POST : /projects
         */
        function create_new_project(collection) {
            return request.post([BASE_URL], collection);
        }

        /**
         * @endpoint
         * POST : /projects/$project_id/tasks
         */
        function create_new_task($project_id, collection) {
            return request.post([BASE_URL, $project_id, 'tasks' ], collection);
        }

        /**
         * @endpoint
         * POST : /projects/$project_id/tasks/$task_id
         */
        function create_new_comment($project_id, $task_id, $collection) {
            return request.post([BASE_URL, $project_id, 'tasks', $task_id, 'comments' ], $collection);
        }

        /**
         * @endpoint
         * POST : /projects/$project_id/tasks/$task_id
         */
        function create_new_group($project_id, $collection) {
            return request.post([BASE_URL, $project_id, 'groups'], $collection);
        }

        /**
         * @type
         * Api
         */
        return {
            project     : create_new_project,
            task        : create_new_task,
            comment     : create_new_comment,
            group       : create_new_group
        };
    };


    /**
     * @type
     * Project update services
     */
    var update = function(request) {

        /**
         * @endpoint
         * PUT : /projects/$id/tasks/$task_id
         */
        function update_task($id, $task_id, $collection) {
            return request.put([BASE_URL, $id, 'tasks', $task_id ], $collection);
        }

        return {
            task : update_task
        }
    };


    /**
     * @type
     * Group fetch services
     *
     * @description
     * Contains collection of methods for fetching an Group related items
     */
    /**
     *
     * @param {*} request
     * @param {*} base
     * @param {*} provided
     */
    var fetch = function(request, requestType) {

        /**
         * @endpoint
         * GET : /projects
         */
        function projects($id) {
            return request.beffore(requestType).get([BASE_URL, $id]);
        }

        /**
         * @endpoint
         * GET : /projects/$id/tasks
         */
        function fetch_task_collection($id) {
            return request.beffore(requestType).get([BASE_URL, $id, 'tasks']);
        }

        /**
         * @endpoint
         * GET : /projects/$project_id/tasks/$task_id
         */
        function fetch_comment_collection($project_id, $task_id) {
            return request.beffore(requestType).get([BASE_URL, $project_id, 'tasks', $task_id]);
        }

        /**
         * @type
         * GET : Api
         */
        return {
            projects    : projects,
            tasks       : fetch_task_collection,
            comments    : fetch_comment_collection
        };
    }


    /**
     * @type
     * Group fetch services
     */
    var archive = function(request) {

        /**
         * @endpoint
         * DELETE : /projects/$id/tasks
         */
        function delete_task($id, collection) {
            return request.delete([BASE_URL, $id, 'tasks' ], collection);
        }

        /**
         * @type
         * GET : Api
         */
        return {
            task: delete_task
        };

    };


    /**
     *
     * @param {*} request
     * @param {*} base
     * @param {*} provider
     */
    var ApiProvider = function(request) {

        var requestType = {
            type : 'collection'
        };

        return {

            PAGINATE : function() {
                requestType.type = 'pagination';
                return this;
            },

            REFRESH : function() {
                requestType.type = 'refresh';
                return this;
            },

            COLLECTION : function() {
                requestType.type = 'collection';
                return this;
            },

            SINGLE : function() {
                requestType.type = 'single';
                return this;
            },


            create  : create(request),
            fetch   : fetch(request, requestType),
            update  : update(request),
            archive : archive(request)
        };
    };

    /**
     *
     * @param {*} http
     * @param {*} q
     * @param {*} AppManager
     */
    var ServiceHandler = function (Request) {
        return { api : ApiProvider(Request) };
    };

    ServiceHandler.$inject = ['Request'];
    angular.module('application.models').factory('ProjectModel', ServiceHandler);

})();
