((() => {

    let LanguageObserver = {
        domainCollection    : ['en', 'bg'],
        domain              : 'bg'
    };

    var Service = (PubSub) => {

        /**
         * @description
         * Change between two diferent language domains
         *
         * @parameters
         * @param {string} key
         *
         * @return
         * void
         */
        function domain() {
            return LanguageObserver.domain;
        }


        /**
         * @description
         * Change between two diferent language domains
         *
         * @parameters
         * @param {string} key
         *
         * @return
         * void
         */
        function toggle($id) {

            if($id) {
                if($id == 2) {
                    LanguageObserver.domain = 'bg';
                }

                if($id == 1) {
                    LanguageObserver.domain = 'en';
                }
            }
            else {
                LanguageObserver.domain = (LanguageObserver.domain == 'bg') ? 'en' : 'bg';
            }

            //PubSub.api.publish('event:tranlation:domain:change');
        }


        /**
         * @description
         * Change between two diferent language domains
         *
         * @parameters
         * @param {string} key
         *
         * @return
         * void
         */
        function $translate(callback) {

            var provider = function(languageCollection) {
                return languageCollection[LanguageObserver.domain];
            };

            // PubSub.api.subscribe('event:tranlation:domain:change', () => {
            //     callback(provider);
            // });

            callback(provider);
        }

        return { toggle, domain, $translate }
    };

    Service.$inject = ['PubSub'];
    angular.module('application.service').factory('Translation', Service);

})());
