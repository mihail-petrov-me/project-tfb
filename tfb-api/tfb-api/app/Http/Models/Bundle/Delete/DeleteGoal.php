<?php
namespace App\Http\Models\Bundle\Delete;

use App\Http\Models\Bundle\Bundle;
use App\Http\Models\Goal\Goal;
use App\Http\Models\Repository;

class DeleteGoal extends Repository
{
    public function execute($bundleId, $goalId)
    {
        $this->delete(Bundle::TABLE_GOALS, [
            Bundle::ID => $bundleId,
            Goal::ID   => $goalId
        ]);
    }
}