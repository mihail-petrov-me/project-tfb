<?php
namespace App\Http\Models\Bundle\Fetch;

use App\Http\Models\Repository;
use DB;

class FetchGoalCollection extends Repository
{
    public function execute($id)
    {
        return DB::table('bundle__goal')
            ->select(array(
                'teacher__subject.subject_id',
                'teacher__subject.subject_title',
                'bundle__goal.goal_id',
                'bundle__goal.parent_id',
                'bundle__goal.goal_title',
                'bundle__goal.rigar',
                'bundle__goal.type'
            ))
            ->join('teacher__subject', function ($join) {
                $join->on('teacher__subject.bundle_id', '=', 'bundle__goal.bundle_id');
                $join->on('teacher__subject.subject_id', '=', 'bundle__goal.subject_id');
            })
            //->join('teacher__subject', 'teacher__subject.bundle_id', '=', 'bundle__goal.bundle_id')
            //->join('teacher__subject', 'teacher__subject.subject_id', '=', 'bundle__goal.subject_id')
            ->where('bundle__goal.bundle_id', $id)->get();
    }
}