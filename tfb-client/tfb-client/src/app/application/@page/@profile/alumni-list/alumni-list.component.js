(function () {

    /**
     *
     * @param {*} PubSub
     * @param {*} UserModel
     */
    var Controller = function(Translation, PubSub, UserModel) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;


        /**
         * @type
         * State collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.STATE = {
            IS_LOADING : false
        };


        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(function($provider) {

            model.LABEL = {

                BUTTON_ACTION : $provider({
                    'en' : 'Update',
                    'bg' : 'Актуализирай'
                })
            };
        });



        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        model.$onInit = function() {

        };

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component destroy
         */
        model.$onDestroy = function() {

        };
    };


    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['Translation','PubSub', 'UserModel'];



    /**
     *
     */
    var Component = {
        templateUrl     : './alumni-list.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    /**
     *
     */
    angular.module('application.components.page').component('alumniList', Component);

})();
