<?php
namespace App\Http\Controllers\Api\Portal;

use App\Http\Models\Project\Project;
use DB;

use \App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use \Illuminate\Http\Request;
use \App\Http\Transformars\ProjectTransformer;

/**
 * Class ProjectController
 * @package App\Http\Controllers\Api\Portal
 */
class ProjectController extends Controller
{
    /**
     * @var Project
     */
    private $model;

    /**
     * ProjectController constructor.
     *
     * @param Project $model
     */
    public function __construct(Project $model)
    {
        $this->model = $model;
    }
    

    /**
     * GET : projects
     * =====================================
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $collection = $this->model->all($request->query());
        $meta       = $this->model->getMeta();

        return $this->responseSuccess($collection, $meta);
    }

    /**
     * POST : projects
     * =====================================
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $entity = $this->model->create_project($request->json()->all());
        return $this->responseSuccess($entity);
    }

    /**
     * GET : projects/{id}
     * =====================================
     * @param $id
     *
     * @return JsonResponse
     */
    public function show($id): JsonResponse
    {
        $collection = $this->model->get($id);
        return $this->responseSuccess($collection);
    }

    /**
     * @param $id
     *
     * @return JsonResponse
     */
    public function fetchProjectMembers($id): JsonResponse
    {
        $collection = $this->model->fetch_project_members($id);
        return $this->responseSuccess($collection);
    }

    /**
     * @param         $id
     * @param Request $request
     *
     * @return JsonResponse
     * @internal param $collection
     *
     */
    public function addMemberToProject($id, Request $request): JsonResponse
    {
        try
        {
            $collection = $this->model->add_member_to_project($id, $request->json()->all());
            return $this->responseSuccess($collection);
        }
        catch(\Exception $e)
        {
            return $this->responseNotFound('Member already added to this project');
        }
    }

    /**
     * PUT : projects/{id}
     * =====================================
     *
     * @param         $project_id
     * @param Request $request
     *
     * @return JsonResponse
     * @internal param $id
     */
    public function update($project_id, Request $request): JsonResponse
    {
        $collection = $request->json()->all();
        $this->model->update($project_id, $collection);

        return $this->responseSuccess();
    }

    /**
     * DELETE : projects/{id}
     * =====================================
     * @param $id
     *
     * @return JsonResponse
     */
    public function delete($id): JsonResponse
    {
        $dbResult = DB::table('projects')->where([
            'id'    => $id
        ])->update([
            'status'    => 3
        ]);
        
        if($dbResult) {
            return $this->responseSuccess();
        }
        
        return $this->responseNotFound('The specifide item could not be archived');
    }


    /**
     * @param         $id
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addTaskToProject($id, Request $request): JsonResponse
    {
        try
        {
            $collection = $this->model->add_task_to_project($id, $request->json()->all());
            return $this->responseSuccess($collection);
        }
        catch(\Exception $e)
        {
            return $this->responseNotFound('Member already added to this project');
        }
    }

    /**
     * @param         $id
     *
     * @return JsonResponse
     */
    public function fetchProjectTasks($id): JsonResponse
    {
        try
        {
            $collection = $this->model->fetch_task_collection($id);
            return $this->responseSuccess($collection);
        }
        catch(\Exception $e)
        {
            return $this->responseNotFound('@Error');
        }
    }

    /**
     * @param         $project_id
     * @param         $task_id
     * @param Request $request
     *
     * @return JsonResponse
     * @internal param $projectId
     * @internal param $taskId
     */
    public function updateProjectTask($project_id, $task_id, Request $request): JsonResponse
    {
        try
        {
            $collection = $this->model->update_task($project_id, $task_id, $request->json()->all());
            return $this->responseSuccess($collection);
        }
        catch(\Exception $e)
        {
            return $this->responseNotFound('@Error');
        }
    }

    /**
     * @param         $id
     *
     * @return JsonResponse
     */
    public function deleteProjectTask($id): JsonResponse
    {
        try
        {
            $collection = $this->model->delete_task($id);
            return $this->responseSuccess($collection);
        }
        catch(\Exception $e)
        {
            return $this->responseNotFound('@Error');
        }
    }


    /**
     * @param         $task_id
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addCommentToProjectTask($task_id, Request $request)
    {
        try
        {
            $collection = $this->model->add_comment_to_task($task_id, $request->json()->all());
            return $this->responseSuccess($collection);
        }
        catch(\Exception $e)
        {
            return $this->responseNotFound('Comment cannot be added to this task at the moment');
        }
    }


    /**
     * @param $task_id
     *
     * @return JsonResponse
     */
    public function fetchProjectTaskComments($task_id)
    {
        try
        {
            $collection = $this->model->fetch_task_comment_collection($task_id);
            return $this->responseSuccess($collection);
        }
        catch(\Exception $e)
        {
            return $this->responseNotFound('Comment cannot be added to this task at the moment');
        }
    }

    /**
     * @param         $project_id
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addGroupToProject($project_id, Request $request)
    {
        try
        {
            $collection = $this->model->add_group_to_project($project_id, $request->json()->all());
            return $this->responseSuccess($collection);
        }
        catch(\Exception $e)
        {
            return $this->responseNotFound('Comment cannot be added to this task at the moment');
        }
    }
}
