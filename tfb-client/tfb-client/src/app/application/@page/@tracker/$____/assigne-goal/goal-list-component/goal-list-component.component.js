(function () {

    /**
     *
     * @param {*} UserModel
     * @param {*} Translation
     */
    var Controller = function (PubSub) {


        var model = this;

        /**
         *
         */
        model.inputText = null;


        model.count = {
            a : true
        };

        model.get_data = function() {
            return model.count.a;
        }

        /**
         *
         */
        model.on_showAdd = function() {
            model.count.a = false;
        };

        model.on_close = function() {

        };

        /**
         *
         */
        model.on_add = function() {

            PubSub.api.publish('input:dropdown:goal_primary', model.inputText);
            model.inputText             = null;
            model.count.a               = true;
        };

    };

    Controller.$inject  = ['PubSub'];
    var Component       = {
        templateUrl     : './goal-list-component.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('goalListComponent', Component);

})();
