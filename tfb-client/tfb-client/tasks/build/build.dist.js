//
var fs          = require('fs');
var uglify      = require('uglify-js');

// Build Tasks
var clean       = require('../common/clean');
var copyApp     = require('../common/copy.app');
// var copyVendor  = require('../common/copy.vendor');
var copyFont    = require('../common/copy.fonts');

//
var util        = require('../helpers/util');

//
var style       = require('../common/style');
var serve       = require('../common/serve');

//
var injectTemplate  = require('../injectors/inject.template');
var injectScript    = require('../injectors/inject.script');
var injectStyle     = require('../injectors/inject.style');
var injectVendor    = require('../injectors/inject.vendor');

//
var lint            = require('../common/lint');


// Configuration
// #
var settings            = require('../../settings');
var scriptCollection    = require((settings.app + 'modules.js'));



var bundle          = require('../bundle/bundle');
var bundleVendor    = require('../bundle/bundle.vendor');
var INJECTOR        = require('../../injector.json');

// Scripts
// #
var moduleReference = scriptCollection.vendors;

// Styles
// #
var styleReference = scriptCollection.cssVendors;


var fontsReference = scriptCollection.fontVendors;


// # @1. Clear working directory
// #
clean.exec(settings.prod);


// # @2. Create dist folder
// #
fs.mkdirSync(settings.prod);

copyApp.exec({
    from    : [(settings.app + 'assets')],
    to      : [(settings.prod + 'assets')]
});

// # @3.
// #
bundle.exec({
    from    : settings.dev,
    to      : settings.prod,
    pipe    : INJECTOR.pipe
});

// # @3.
// #
bundleVendor.exec({
    from    : settings.dev,
    to      : settings.prod,
    pipe    : INJECTOR.pipe
});

// @4 Build and inject style
// #
style.exec({
    from    : (settings.app + 'styles/style.scss'),
    to      : (settings.prod + 'style.css'),
    vendor  : INJECTOR.transformers.style
});


injectVendor.exec({
    from    : settings.app,
    to      : settings.prod,
    pipe    : [
        'vendor.js', 'application.js'
    ]
});


injectStyle.exec(settings.prod, 'style.css');



// # @3. Bundle application files
// #
// util.traverse(settings.dev, function(file, directory, filePath) {

    // console.log(filePath);

    // 1. Read file content

    // 2. Parse file template
    // var result = replace(filePath, directory);
    // fs.appendFileSync(settings.prod + 'application.js', fs.readFileSync(filePath).toString());
// });


// util.traverse(settings.app + 'app/', function (file, directory, filePath) {

//     // 1.Read file content
//     // 2. Parse file template
//     var result = replace(filePath, directory);

//     // read file content
//     //var fileReference = uglify.minify(fs.readFileSync(filePath).toString(), { fromString: true });
//     // var fileReference = uglify.minify(result, { fromString: true });
//     //fs.appendFileSync(settings.prod + 'application.js', fileReference.code);
//     fs.appendFileSync(settings.prod + 'application.js', fs.readFileSync(filePath).toString());
// });


// get application referenece pointer
// function replace(file, dir) {

//     var reference = fs.readFileSync(file).toString();
//     var referenceCollection = reference.match(/(templateUrl) *: *'([a-z\.\/]+)'/);
//     if (referenceCollection) {

//         // replace file path
//         var pathReference = referenceCollection[2].replace('./', dir);

//         // Get template content
//         var template = fs.readFileSync(pathReference).toString().replace(/(\r\n|\n|\r)/gm, "");
//         return reference.replace(referenceCollection[1], 'template').replace(referenceCollection[2], template);
//     }
//     return reference;
// }







// Core cmponents
// var fs      = require('fs.extra');
// var util    = require('../helpers/util');
// var setting = require('../../settings');
// var uglify  = require('uglify-js');
// var sass    = require('npm-sass');
// var chalk   = require('chalk');
// var style   = require('./style');


// Build tasks
// var clean             = require('./clean');
// var bundleApplication = require('./bundle.app');
// var bundleVendor      = require('./bundle.vendor');


// Include njectors
// var injectStyle = require('./inject.style');
// var injectScript = require('./inject.script');

// var scriptCollection = require((setting.app + 'modules.js')).vendors;

// Clear working directory
// #
// clean.exec(setting.prod);


// Create dist folder
// #
// fs.mkdirSync(setting.prod);



// // Bundle appplication files
// // #
// util.traverse(setting.app + 'app/', function(file, directory, filePath) {

//   // 1.Read file content
//   // 2. Parse file template
//   var result = replace(filePath, directory);

//   // read file content
//   //var fileReference = uglify.minify(fs.readFileSync(filePath).toString(), { fromString: true });
//   var fileReference = uglify.minify(result, { fromString: true });
//   fs.appendFileSync(setting.prod + 'application.js', fileReference.code);
// });

// 1. Bundle application files and modify templates
// #

// bundleApplication.exec({
//   input  : (setting.app + 'app/'),
//   output : (setting.prod + 'application.js')
// });



// 1. Bundle vendor applicatin directory
// #
// bundleVendor.exec(scriptCollection, {
//   output : (setting.prod + 'vendor.js')
// });




// // Bundle vendor file
// // #
// for(var i = 0; i < scriptCollection.length; i++) {

//   var path = (setting.root +scriptCollection[i].path);
//   var fileReference = uglify.minify(fs.readFileSync(path).toString(), { fromString: true });
//   fs.appendFileSync(setting.prod + 'vendor.js', fileReference.code);
// }

  // # Bundle style file
  // #
  // style.exec({
  //   from  : (setting.app + 'styles/style.scss'),
  //   to    : (setting.prod + 'style.css')
  // });


// Create index.html file will final application code
// #

// var scriptCollection = [
//   '<script src="/dist/application.js"></script>',
//   '<script src="/dist/vendor.js"></script>',
// ];

// injectScript.exec(setting.app, setting.prod, scriptCollection);
// injectStyle.exec(setting.prod);


// // get application referenece pointer
// function replace(file, dir) {

//   var reference = fs.readFileSync(file).toString();
//   var referenceCollection = reference.match(/(templateUrl) *: *'([a-z\.\/]+)'/);
//   if(referenceCollection) {

//     // replace file path
//     var pathReference = referenceCollection[2].replace('./', dir);

//     // Get template content
//     var template = fs.readFileSync(pathReference).toString().replace(/(\r\n|\n|\r)/gm,"");;
//     var d = reference.replace(referenceCollection[1], 'template').replace(referenceCollection[2], template);
//     return d;
//   }
//     return reference;
// }
