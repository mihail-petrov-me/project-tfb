(function () {

    var Controller = function(Translation, ClassModel) {

        var model = this;

        /**
         *
         */
        this.name = "form-schedule";

        /**
         * @component
         * Fields collection
         */
        this.FIELD = {

            "class": {

                name            : "class",
                type            : "dropdown",
                title           : 'Клас',
                value           : null,
                show_errors     : false,
                isRequired      : true,
                provider        : () => ClassModel.api.COLLECTION().fetch.classes(this.teacherId),
            },

            "subject": {

                name            : "subject",
                type            : "dropdown",
                title           : 'Предмет',
                value           : null,
                show_errors     : false,
                isRequired      : true,
                provider        : () => ClassModel.api.COLLECTION().fetch.subjects(this.teacherId),
            },

            "half": {

                name            : "half",
                type            : "dropdown",
                title           : 'Смяна',
                value           : null,
                show_errors     : false,
                isRequired      : true
            },

            "period": {

                name            : "period",
                type            : "dropdown",
                title           : 'Час',
                value           : null,
                show_errors     : false,
                isRequired      : true
            }
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                OPTIONS_HALF : $provider({
                    "bg"    : [
                        { 'id': 1 , 'title': 'Първа' },
                        { 'id': 2 , 'title': 'Втора' },
                        { 'id': 3 , 'title': 'Занималня' },
                    ],
                    "en"    : [
                        { 'id': 1 , 'title': 'Първа' },
                        { 'id': 2 , 'title': 'Втора' },
                        { 'id': 3 , 'title': 'Занималня' },
                    ]
                }),

                OPTIONS_PERIOD : $provider({
                    "bg"    : [
                        { 'id': 1 , 'title': 'Първи' },
                        { 'id': 2 , 'title': 'Втори' },
                        { 'id': 3 , 'title': 'Трети' },
                        { 'id': 4 , 'title': 'Четвърти' },
                        { 'id': 5 , 'title': 'Пети' },
                        { 'id': 6 , 'title': 'Шести' },
                    ],
                    "en"    : [
                        { 'id': 1 , 'title': 'Първи' },
                        { 'id': 2 , 'title': 'Втори' },
                        { 'id': 3 , 'title': 'Трети' },
                        { 'id': 4 , 'title': 'Четвърти' },
                        { 'id': 5 , 'title': 'Пети' },
                        { 'id': 6 , 'title': 'Шести' },
                    ]
                }),

                LABEL_CLASS :    $provider({
                    "bg"    : "Класове",
                    "en"    : "Subject"
                }),


                LABEL_SUBJECT :    $provider({
                    "bg"    : "Предмет",
                    "en"    : "Subject"
                }),

                LABEL_HALF :    $provider({
                    "bg"    : "Смяна",
                    "en"    : "Half"
                }),

                LABEL_PERIOD :    $provider({
                    "bg"    : "Учебен час",
                    "en"    : "Period"
                })
            };
        });

        /**
         *
         */
        this.on_edit = () => { this.uiIsDisabled = false; }

        /**
         *
         */
        this.$onInit = () => {

            this.parent.FIELD  = this.FIELD;
            this.formContact   = this.parent.formContact;
        }
    };


    Controller.$inject  = ['Translation', 'ClassModel'];
    var Component       = {
        templateUrl     : './form-program.template.html',
        controller      : Controller,
        controllerAs    : 'model',

        transclude: {
            'afterFormSlot' : '?afterFormSlot'
        },

        require             : {
            parent          : '^formWrapper'
        },

        bindings        : {
            uiCollection    : '<',
            uiIsDisabled    : '<',
            uiPresentation  : '<',
            uiId            : '<',
            teacherId       : '<'
        }
    };

    /**
     *
     */
    angular.module('application.components.page').component('formProgram', Component);
})();
