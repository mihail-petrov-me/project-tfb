(function () {

    /**
     *
     * @param {*} PubSub
     * @param {*} UserModel
     */
    var Controller = function(Translation, PubSub, UserModel) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;

        model.ACTIVE_ELEMENT = 'contact';

        model.$userProfile = null;

        model.FIELD = {
            "message"   : {
                name            : "message",
                type            : 'textarea',
                value           : null,
                show_errors     : false,
                error           : 'Field is required'
            }
        };


        /**
         * @type
         * Request : FETCH : signature
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        model.request_FETCH_signature = function($id) {

            UserModel.api.SINGLE().fetch
            .signature($id)
                .then(model.handle_FETCH_signature_success)
                .catch(model.handle_FETCH_signature_error);
        };

        /**
         * @type
         * Request : FETCH : contact
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        model.request_FETCH_contact = function($id) {

            UserModel.api.SINGLE().fetch
            .contact($id)
                .then(model.handle_FETCH_collection_success)
                .catch(model.handle_FETCH_collection_error);
        };

        /**
         * @type
         * Request : FETCH : education
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        model.request_FETCH_education = function($id) {

            UserModel.api.COLLECTION().fetch
            .education($id)
                .then(model.handle_FETCH_collection_success)
                .catch(model.handle_FETCH_collection_error);
        };

        /**
         * @type
         * Request : FETCH : employment
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        model.request_FETCH_employment = function($id) {

            UserModel.api.COLLECTION().fetch
            .employment($id)
                .then(model.handle_FETCH_collection_success)
                .catch(model.handle_FETCH_collection_error);
        };


        /**
         * @type
         * Handler : FECH : SUCCESS
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        model.handle_FETCH_collection_success = function(response) {
            model.fieldCollection = response.data;
        };

        /**
         * @type
         * Handler : FECH : ERROR
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        model.handle_FETCH_collection_error = function() {
            console.error("@Exception : cant fetch collection of user profile elements");
        };

        /**
         * @type
         * Handler : FECH : SUCCESS
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        model.handle_FETCH_signature_success = function($response) {
            model.$userProfile = $response.data;
        };

        /**
         * @type
         * Handler : FECH : ERROR
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        model.handle_FETCH_signature_error = function() {
            console.error("@Exception : cant fetch user signature");
        };


        /**
         *
         */
        model.isActive = function($type) {
            return model.ACTIVE_ELEMENT == $type;
        }

        /**
         *
         */
        model.on_sendMessage = function() {

            UserModel.api.create.messages({
                'sender_id'     : UserModel.profile.my("user_id"),
                'receiver_id'   : UserModel.state("id"),
                'content'       : model.textbinding
            });
        };


        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        this.$onInit = function() {
            PubSub.api.publish('api:publish:user-profile');

            PubSub.api.subscribe('users:filter:user_profile', function($subscribe) {

                model.ACTIVE_ELEMENT = $subscribe.filter_user_profile;

                if(model.ACTIVE_ELEMENT == 'message') {

                    var handler = UserModel.api.fetch.messages(UserModel.profile.my("user_id"), UserModel.state("id"));

                    handler.then(function(response) {
                        model.messageCollection = response.data;
                    });


                    handler.catch(function() {

                    });
                }
            });

            PubSub.api.subscribe('__filter_by_type', function($type) {
                model.ACTIVE_ELEMENT = $type;

                console.log($type);

                if($type == 'contact') {
                    model.request_FETCH_contact(UserModel.state("id"));
                }

                if($type == 'education') {
                    model.request_FETCH_education(UserModel.state("id"));
                }

                if($type == 'employment') {
                    model.request_FETCH_employment(UserModel.state("id"));
                }
            });
        }


        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component destroy
         */
        model.$onDestroy = function() {
            PubSub.api.unsubscribe('users:filter:user_profile');
        };


        /**
         * @type
         * Route event
         *
         * @description
         * This method is executed on component route init
         */
        model.$routerOnActivate = function(a, b, c) {

            UserModel.state("id", a.urlPath);
            model.request_FETCH_signature(UserModel.state("id"));
            model.request_FETCH_contact(UserModel.state("id"));
        };
    };

    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['Translation','PubSub', 'UserModel'];

    /**
     *
     */
    var Component = {
        templateUrl     : './user-profile.template.html',
        controller      : Controller,
        controllerAs    : 'model',
    };

    angular.module('application.components.page').component('userProfile', Component);

})();
