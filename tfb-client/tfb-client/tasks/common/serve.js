var path    = require('path');
var open    = require('open');

var express = require('express');
var server  = express();

var host    = 'http://localhost';

var handler = null;

exports.exec = function(directory, port, message) {

  var port = port || '8080';
  var message = message || 'Initiate build';

  server.use(express.static(directory));
  server.get('/', function(request, response) {
    response.sendFile(path.join(directory, 'index.html'))
  });

  handler = server.listen(port, function() {
    console.log(message);
    console.log("Serving folder: " + directory);
    console.log("Serving on port: " + port);
    open((host + ':' + port));
  });
};


exports.close = function() {
  handler.close();
};
