<?php

use \App\Http\Models\Goal\Goal;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoalTable extends Migration
{
    private $table = Goal::TABLE;

    public function up()
    {
        Schema::create($this->table, function(Blueprint $builder)
        {
            //# Primary Key
            $builder->char(Goal::ID, 13);
            $builder->char(Goal::TEACHER_ID, 13);
            $builder->char(Goal::SUBJECT_ID, 13);
            $builder->char(Goal::BUNDLE_ID, 13);

            //# Table Entity Collection
            $builder->char(Goal::PARENT_ID, 13);
            $builder->tinyInteger(Goal::RIGAR);
            $builder->tinyInteger(Goal::TYPE);
            $builder->string(Goal::TITLE, 200);

            //# Meta
            $builder->timestamps();
            $builder->softDeletes();

            //# Primary Key : indexes
            $builder->primary([
                Goal::ID,
                Goal::TEACHER_ID,
                Goal::SUBJECT_ID,
                Goal::BUNDLE_ID
            ]);
        });
    }

    public function down()
    {
        Schema::drop($this->table);
    }
}