(function () {

    /**
     *
     * @param {*} Component
     * @param {*} PubSub
     * @param {*} Translation
     * @param {*} $sce
     */
    var Controller = function (Component, PubSub, Translation, $sce) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;


        /**
         * @type
         * Publish collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.PUBLISH = {
            article   : PubSub.events.component.ARTICLE.FINISH,
            finish    : PubSub.events.component.FINISH
        };


        /**
         * @type
         * Publish collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.SUBSCRIBE = {
            article   : PubSub.events.component.ARTICLE.PROCESS
        };

        /**
         * @type
         * Field collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.FIELD = {
            TITLE       : null,
            CONTENT     : null
        };

        /**
         *
         */
        model.STATE = {
            IS_MODE_INTERACTIVE   : false,
            IS_MODE_PRESENTATION  : true
        };


        /**
         * @type
         * Event : init
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.init_process = function () {

            PubSub.api.publish(model.PUBLISH.article, {
                title   : model.FIELD.TITLE,
                content : model.FIELD.CONTENT
            });

            PubSub.api.publish(model.PUBLISH.finish);

            model.FIELD.TITLE   = null;
            model.FIELD.CONTENT = null;
        };

        /**
         * @type
         * Event : init
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.$init_interactive = function () {

            Component.$interactive(model.uiMode, function () {

                model.STATE.IS_MODE_INTERACTIVE     = true;
                model.STATE.IS_MODE_PRESENTATION    = false;

                PubSub.api.subscribe(model.SUBSCRIBE.article, model.init_process);
            });
        };

        /**
         * @type
         * Event : init
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.$init_presentation = function () {

            Component.$presentation(model.uiMode, function () {

                model.STATE.IS_MODE_PRESENTATION    = true;
                model.STATE.IS_MODE_INTERACTIVE     = false;


                model.FIELD.TITLE   = model.uiTitle;
                model.FIELD.CONTENT = model.uiContent;
            });
        };

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        model.$onInit = function () {
            model.$init_interactive();
        };

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        model.$onChanges = function () {
            model.$init_presentation();
        }

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component destroy
         */
        model.$onDestroy = function () {
            PubSub.api.unsubscribe(model.SUBSCRIBE.article);
        };
    };


    Controller.$inject = ['Component', 'PubSub', 'Translation', '$sce'];

    var Component = {
        templateUrl     : './ui-block-article.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            uiTitle     : '<',
            uiContent   : '<',
            uiMode      : '<'
        }
    };

    angular.module('application.components.visual').component('uiBlockArticle', Component);

})();
