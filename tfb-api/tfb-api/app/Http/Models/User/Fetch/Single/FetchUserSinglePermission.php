<?php
namespace App\Http\Models\User\Fetch\Single;

use App\Http\Models\Repository;
use App\Http\Models\User\User;
use App\Http\Models\User\UserType;
use DB;

/**
 * Class FetchUserSinglePermission
 * @package App\Http\Models\User\Fetch\Single
 */
class FetchUserSinglePermission extends Repository
{
    /**
     * @var array
     */
    public $fieldCollection = [
        UserType::ID
    ];

    /**
     * @param $id
     *
     * @return mixed
     * @internal param $bundle
     *
     */
    public function execute($id)
    {
        $collection = $this->fetch(User::TABLE_USER_TYPES, [ User::ID => $id ], $this->fieldCollection);

        return array_map(function($element) {
            return $element[UserType::ID]; },
        $collection);
    }
}