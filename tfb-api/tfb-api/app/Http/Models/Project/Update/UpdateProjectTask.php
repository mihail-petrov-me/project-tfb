<?php
namespace App\Http\Models\Project\Update;

use App\Http\Models\Project\ProjectGroup;
use App\Http\Models\Project\ProjectTask;
use Carbon\Carbon;
use DB;

use App\Http\Models\Project\Project;
use App\Http\Models\Repository;

/**
 * Class FetchProjectCollection
 */
class UpdateProjectTask extends Repository
{
    /**
     * @param $projectId
     * @param $taskId
     * @param $collection
     *
     * @return mixed
     * @internal param $id
     *
     */
    public function execute($projectId, $taskId, $collection)
    {
        DB::table(Project::TABLE_TASK)->where([
            ProjectTask::ID         => $taskId
        ])
        ->update([
            ProjectGroup::ID            => $this->process($collection, ProjectGroup::ID),
            ProjectTask::TITLE          => $this->process($collection, ProjectTask::TITLE),
            ProjectTask::DESCRIPTION    => $this->process($collection, ProjectTask::DESCRIPTION),
            ProjectTask::PARENT         => $this->process($collection, ProjectTask::PARENT),
            ProjectTask::IS_COMPLETED   => $this->process($collection, ProjectTask::IS_COMPLETED),
            ProjectTask::IS_COMPLETED   => $this->process($collection, ProjectTask::IS_COMPLETED),
            ProjectTask::USERS          => $this->process($collection, ProjectTask::USERS),
        ]);
    }
}