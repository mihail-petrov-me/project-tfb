(function () {


    var Component = {
        templateUrl     : './ui-list-campaign.template.html',
        controllerAs    : 'model',
        transclude      : true,
        bindings        : {
            collection    : '<',
            uiLoadMore    : '<'
        }
    };

    angular.module('application.components.visual').component('uiListCampaign', Component);

})();
