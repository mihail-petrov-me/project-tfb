(function () {

    /**
     *
     * @param {*} Translation
     * @param {*} GroupModel
     * @param {*} PubSub
     */
    var Controller = FormController (function(Translation) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;


        /**
         * @component
         *
         * @behaivior
         * This property contains all the fields of
         * the form controller. Every single field contain value and mutator properties
         * in order to check if this field is required or not you must provide
         *
         * @properties
         * isRequired : true | false
         * value : String
         */
        model.FIELD = {

            "project_title": {
                name            : "project_title",
                type            : 'text',
                value           : null,
                show_errors     : false,
                error           : 'Field is required'
            },

            "project_description": {
                name            : "project_description",
                type            : 'textarea',
                value           : null,
                show_errors     : false,
                error           : 'Field is required'
            },

            "project_budget": {
                name            : "project_budget",
                type            : 'text',
                value           : null,
                show_errors     : false,
                error           : 'Field is required'
            }
        };

        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(function($provider) {

            model.LABEL = {

                PAGE_TITLE          :  $provider({
                    'en' : 'Create New Group',
                    'bg' : 'Нова група'
                }),

                BUTTON_CREATE       :  $provider({
                    'en' : 'Create New Group',
                    'bg' : 'Нова група'
                }),

                INPUT_TITLE         :  $provider({
                    'en' : 'Group name',
                    'bg' : 'Наименование'
                }),

                INPUT_DESCRIPTION   :  $provider({
                    'en' : 'Group description',
                    'bg' : 'Описание'
                }),

                INPUT_COUSES        :  $provider({
                    'en' : 'Group couses',
                    'bg' : 'Каузи'
                })
            };
        });
    });

    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['Translation', 'PubSub', 'Form'];

    /**
     *
     */
    var Component = {
        templateUrl     : './form-create.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('formInitiativeCreate', Component);

})();
