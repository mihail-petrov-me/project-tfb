(function () {

    let Controller = function (Translation, PubSub, UserModel, BundleModel, CampaignModel) {

        /**
         *
         */
        this.PROPERTY = {
            TITLE       : null,
            COLLECTION  : []
        };

        /**
         *
         */
        this.STATE = {
            IS_SIDEBAR_VISIBLE : false
        };


        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en' : '@Education campaign',
                    'bg' : '@Образователна кампания'
                }),

                SUBJECT : $provider({
                    'en' : 'Subject',
                    'bg' : 'Предмет'
                })
            };
        });


        /**
         *
         * @param element
         */
        this.navigate_config = (element) => {
            this.$router.navigate(['/Dashboard/Bundle/EditBundle', { id : element.id}]);
        };


        /**
         *
         * @param element
         */
        this.navigate_classroom = (element) => {

            UserModel.state("__class_signature_classroom", element.signature);
            this.$router.navigate(['/Dashboard/Classroom/ClassroomSingle/AttendanceClassroom', { id : element.id}]);
        };

        /**
         *
         */
        this.on_select = () => {
            this.STATE.IS_SIDEBAR_VISIBLE = true;

            // send a request for all bundles for this school
        };


        /**
         * @type
         * EVENT : request
         */
        this.request_FETCH_user = () => {

            let handler = CampaignModel.api.fetch.campaignCollection({
                'teacher_id': UserModel.profile.my("user_id")
            });

            handler.then(($response) => {

                if($response.data[0]['title']) {
                    this.PROPERTY.TITLE = $response.data[0]['title'];
                }

                if($response.data[0]['academy_title']) {
                    this.PROPERTY.TITLE = $response.data[0]['academy_title'];
                }
            });

            handler.catch(() => {
                console.log("@Exception: When we try to get user experiance");
            });
        };

        /**
         * @type
         * Request : FETCH : bundle : collection
         */
        this.request_FETCH_bundleCollection = () => {

            let handler = BundleModel.api.fetch.bundleSpecialCollection({
                "teacher_id" : UserModel.profile.my('user_id')
            });

            handler.then(($response) => {

                this.PROPERTY.COLLECTION = $response.data;
                PubSub.api.publish('event:ui:after_load');
            });

            handler.catch(() => {
                console.log("Exception: Collection of buncles can not be fetched");
            });
        };


        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {

            this.request_FETCH_user();
            this.request_FETCH_bundleCollection();
        };

    };

    Controller.$inject  = ['Translation', 'PubSub', 'UserModel', 'BundleModel', 'CampaignModel'];
    let Component       = {
        templateUrl     : './classroom-dashboard.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : { $router : '<'}
    };

    angular.module('application.components.page').component('classroomDashboard', Component);
})();
