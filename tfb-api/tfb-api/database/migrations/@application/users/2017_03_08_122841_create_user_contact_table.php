<?php

use \App\Http\Models\User\User;
use \App\Http\Models\User\UserContact;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserContactTable extends Migration
{

    private $table = UserContact::TABLE;

    public function up()
    {
        Schema::create($this->table, function(Blueprint $builder) {

            //# Primary Key
            $builder->char(UserContact::ID, 13)->primary();

            //# Foreign Key
//            $builder->char(UserContact::USER_ID, 13);
//            $builder->index(UserContact::USER_ID);
//            $builder->foreign(UserContact::USER_ID)->references(User::ID)->on(User::TABLE);

            //# Table Entity Collection
            $builder->string(UserContact::PHONE, 15)->unique();
            $builder->date(UserContact::BIRTH_DATE)->nullable();
            $builder->string(UserContact::BIRTH_PLACE)->nullable();
            $builder->string(UserContact::ADDRESS);

            //# Meta
            $builder->timestamps();
            $builder->softDeletes();
        });
    }

    public function down()
    {
      Schema::drop($this->table);
    }
}
