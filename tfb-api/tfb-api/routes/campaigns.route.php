<?php

// GET
// =====================================================================================================================
$app->get('/campaigns'                         , 'Tracker\CampaignController@showCampaignCollection'           );
$app->get('/campaigns/{id}'                    , 'Tracker\CampaignController@showCampaignSingle'               );

$app->get('/campaigns/{id}/terms'              , 'Tracker\CampaignController@showCampaignTermCollection'       );
$app->get('/campaigns/{id}/terms/{term_id}'    , 'Tracker\CampaignController@showCampaignTermSingle'           );

// POST routes
// =====================================================================================================================
$app->post('/campaigns'                        , 'Tracker\CampaignController@createCampaign'                   );
$app->post('/campaigns/{id}/terms'             , 'Tracker\CampaignController@createCampaignTerm'               );
$app->post('/campaigns/{id}/teacher'           , 'Tracker\CampaignController@createCampaignTeacher'            );

// PUT routes
// =====================================================================================================================
$app->put('/campaigns/{id}'                    , 'Tracker\CampaignController@updateCampaign'                   );
$app->put('/campaigns/{id}/terms/{term_id}'    , 'Tracker\CampaignController@updateCampaignTerm'               );

// DELETE routes
// =====================================================================================================================
$app->delete('/campaigns/{id}'                 , 'Tracker\CampaignController@deleteCampaign'                   );
$app->delete('/campaigns/{id}/terms/{term_id}' , 'Tracker\CampaignController@deleteCampaignTerm'               );