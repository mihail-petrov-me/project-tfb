<?php

namespace App\Services\Authentication;

use App\Services\Authentication\providers\ApplicationAuthenticationProvider;
use App\Services\Authentication\providers\GoogleAuthenticationProvider;

class AuthenticationService
{
    /**
     * @var array
     */
    private $EXCEPTION_COLLECTION = [
        'FACTORY'    => 'Invalid provider supplied for the authentication'
    ];

    /**
     * @var array
     */
    private $AUTH_PROVIDER = [
        'APP'       => 'app',
        'GOOGLE'    => 'google'
    ];

    /**
     *
     * @param string $provider
     * @param array               $collection
     *
     * @return providers\type|array
     * @throws \Exception
     */
    public function authenticate($provider, $collection)
    {
        return $this->factory($provider)->process($collection);
    }


    /**
     *
     * @param string $provider
     *
     * @return ApplicationAuthenticationProvider|GoogleAuthenticationProvider
     * @throws \RuntimeException
     */
    private function factory($provider)
    {
        switch ($provider) {
            case $this->AUTH_PROVIDER['GOOGLE']:
                return new GoogleAuthenticationProvider();

            case $this->AUTH_PROVIDER['APP'] :
                return new ApplicationAuthenticationProvider();

            default:
                throw new \RuntimeException($this->EXCEPTION_COLLECTION['FACTORY']);
        }
    }
}