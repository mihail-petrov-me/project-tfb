<?php

namespace App\Http\Models\Group\Fetch\Collection;
use DB;

class FetchGroupSubscriberCollection 
{
    public function execute($id)
    {
        return DB::table('user_collection')
            ->join('users', 'user_collection.user_id', '=', 'users.id')
            ->select('users.id', 'users.name', 'users.picture', 'users.signature')
            ->where('user_collection.collection_id', $id)->get();
    }
}
