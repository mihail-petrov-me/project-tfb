// System modules
// # =============================================
var fs  = require('fs.extra');
var EOL = require('os').EOL;

// Core components
// # =============================================
var settings    = require('../../settings');
var util        = require('../helpers/util');

/**
 *
 */
var buildNode = function ($reference, file, path, policy) {

    var index = file.split('.')[1];

    if (policy[index]) {

        if(!$reference[index]) $reference[index] = [];
        $reference[index].push(path);
    }
};


/**
 *
 */
var buildTree = function(from, pipe, callback) {

    var tree = {};

    util.traverse(from, function (file, directory, filePath) {
        buildNode(tree, file, filePath.replace(from, ""), pipe);
    }, function() { callback(tree); });
}


/**
 *
 */
var buildCollection = function(from, pipe, callback) {

    var scriptCollection    = [];
    buildTree(from, pipe, function(AST) {

        var collection = [];
        for(var index in pipe) {
            // console.log(index);

            if(AST[index]) {
                collection = collection.concat(AST[index]);
            }
        }

        callback(collection);
    });
}

/**
 *
 */
exports.build = buildCollection;
