(function () {


    var Controller = function (UserModel, Translation, State, PubSub) {

    };

    Controller.$inject = ['UserModel', 'Translation', 'State', 'PubSub'];

    var Component = {
        templateUrl     : './config-hour.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('configHour', Component);

})();
