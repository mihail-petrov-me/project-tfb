(function () {

    var Controller = function(Translation, PubSub, UserModel) {

        this.name = "FORM_USER_CONTACT";

        /**
         * @component
         * FIELD collection
         */
        this.FIELD = {

            "user_contact_phone": {
                name            : "user_contact_phone",
                type            : 'text',
                value           : null,
                show_errors     : false,
                isRequired      : true
            },

            "user_contact_birth_date": {
                name            : "user_contact_birth_date",
                type            : 'date',
                value           : null,
                show_errors     : false,
                isRequired      : false
            },

            "user_contact_birth_place": {
                name            : "user_contact_birth_place",
                type            : 'text',
                value           : null,
                show_errors     : false,
                isRequired      : false
            },

            "user_contact_address": {
                name            : "user_contact_address",
                type            : 'text',
                value           : null,
                show_errors     : false,
                isRequired      : true
            }
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                LABEL_PERSONAL_INFO :$provider({
                    "bg"    : "Лична информация",
                    "en"    : "Personal information"
                }),

                LABEL_CONTACT_INFO :$provider({
                    "bg"    : "Контактна информация",
                    "en"    : "Contact information"
                }),

                INPUT_USER_TYPE :  $provider({
                    "bg"    : "Изберете си позиция",
                    "en"    : "Chouse your type"
                }),

                INPUT_FNAME : $provider({
                    "bg"    : "Име",
                    "en"    : "First name"
                }),

                INPUT_LNAME : $provider({
                    "bg"    : "Фамилия",
                    "en"    : "Last name"
                }),

                INPUT_PHONE : $provider({
                    "bg"    : "Телефонен номер",
                    "en"    : "Mobile number"
                }),

                INPUT_EMAIL : $provider({
                    "bg"    : "Електронна поща",
                    "en"    : "E-mail"
                }),

                INPUT_BIRTH_DATE : $provider({
                    "bg"    : "Дата на раждане",
                    "en"    : "Birth date"
                }),

                INPUT_BIRTH_PLACE : $provider({
                    "bg"    : "Местораждане",
                    "en"    : "Birth place"
                }),

                INPUT_ADDRESS   : $provider({
                    "bg"    : "Постоянен адрес",
                    "en"    : "Current adress"
                })
            };
        });

        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {

            this.parent.FIELD  = this.FIELD;
            this.formContact   = this.parent.formContact;
        }

        /**
         * @type
         * Lifecycle event
         */
        this.$onChanges = () => {

            if (this.uiCollection) {

                for (var index in this.uiCollection) {

                    if (this.FIELD[index]) {
                        this.FIELD[index].value = this.uiCollection[index];
                    }
                }
            }
        }
    };


    /**
     * @type
     * Component injector
     */
    Controller.$inject = ['Translation', 'PubSub', 'UserModel'];

    var Component = {

        templateUrl     : './form-contact.template.html',
        controller      : Controller,
        controllerAs    : 'model',

        require         : {
            parent      : '^formWrapper'
        },

        bindings        : {
            uiCollection    : '<',
            uiIsDisabled    : '<'
        }
    };

    angular.module('application.components.page').component('formContact', Component);

})();
