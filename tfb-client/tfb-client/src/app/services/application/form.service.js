((() => {

    /**
     *
     */
    var Service = () => {

        /**
         *
         * @param {*} fieldCollection
         */
        function process(fieldCollection) {

            var collection = {};
            for(var i = 0; i < fieldCollection.length; i++) {
                collection = angular.extend(collection, loop(fieldCollection[i]));
            }

            return collection;
        }

        /**
         *
         * @param {*} fieldCollection
         */
        function clear(fieldCollection) {

            for (var index in fieldCollection) {
                fieldCollection[index].value = null;
            }
        }

        /**
         *
         * @param {*} fieldCollection
         */
        function loop(fieldCollection) {

            var collection = {};
            for (var field in fieldCollection) {
                collection[field] = fieldCollection[field].value
            }

            return collection;
        }

        return { process, clear };
    };

    angular.module('application.service').factory('Form', Service);

})());
