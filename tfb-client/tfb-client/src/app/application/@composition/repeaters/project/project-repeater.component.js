(function () {

    var Controller = function(UserModel) {
        this.uiCollection = UserModel.profile.projects();
    };

    var Component = {
        templateUrl     : './project-repeater.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.visual').component('projectRepeater', Component);
})();
