(function () {

    /**
     *
     * @param {*} Translation
     * @param {*} PubSub
     * @param {*} UserModel
     */
    var Controller = function (Translation, PubSub, UserModel) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;


        model.on_close = function() {
            PubSub.api.publish('close_modal_window');
        };
    };

    Controller.$inject = ['Translation', 'PubSub', 'UserModel'];

    var Component = {
        templateUrl     : './form-dialog.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        transclude      : true
    };

    angular.module('application.components.visual').component('formDialog', Component);

})();
