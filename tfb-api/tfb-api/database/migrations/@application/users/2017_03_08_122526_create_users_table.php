<?php

use App\Http\Models\User\User;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    private $table = User::TABLE;

    public function up()
    {
        Schema::create($this->table, function(Blueprint $builder) {

            //# Primary Key
            $builder->char(User::ID, 13)->primary();

            //# Table Entity Collection
            $builder->string(User::EMAIL, 100);
            $builder->string(User::FNAME, 100);
            $builder->string(User::LNAME, 100);
            $builder->string(User::NAME, 100);
            $builder->char(User::SIGNATURE, 4);
            $builder->string(User::PASSWORD, 256)->nullable();
            $builder->text(User::PICTURE)->nullable();

            //# Meta
            $builder->timestamps();
            $builder->softDeletes();
        });
    }

    public function down()
    {
      Schema::drop($this->table);
    }
}
