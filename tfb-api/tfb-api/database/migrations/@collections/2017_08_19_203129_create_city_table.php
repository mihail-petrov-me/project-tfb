<?php

use App\Http\Models\Collections\CollectionCity;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCityTable extends Migration
{
    private $table = CollectionCity::TABLE;

    public function up()
    {
        Schema::create($this->table, function(Blueprint $builder) {

            //# Primary Key
            $builder->increments(CollectionCity::ID, 10);

            //# Table Entity Collection
            $builder->integer(CollectionCity::MUNICIPALITY_ID);
            $builder->string(CollectionCity::TITLE, 200);
        });
    }

    public function down()
    {
        Schema::drop($this->table);
    }
}
