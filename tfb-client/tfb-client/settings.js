var ROOT  = (__dirname   + '/');
var APP   = (ROOT + 'src/');
var DEV   = (ROOT + '_dev/');
var PROD  = (ROOT + 'dist/');

module.exports = {
  root  : ROOT,
  app   : APP,
  dev   : DEV,
  prod  : PROD
};
