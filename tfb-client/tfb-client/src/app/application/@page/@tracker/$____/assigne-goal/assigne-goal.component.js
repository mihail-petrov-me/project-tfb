(function () {

    /**
     *
     * @param {*} UserModel
     * @param {*} Translation
     */
    var Controller = function (Translation, PubSub, UserModel, ClassModel) {

    };

    var ComponentRouter = [
        { path: '/'     , component: 'myClassListComponent'     , as: 'MyClassListComponent'  , useAsDefault: true },
        { path: '/:id'  , component: 'goalListComponent'        , as: 'GoalListComponent' },
    ];

    Controller.$inject  = ['Translation', 'PubSub', 'UserModel', 'ClassModel'];
    var Component       = {
        templateUrl     : './assigne-goal.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        $routeConfig    : ComponentRouter,
        bindings        : { $router     : '<' }
    };

    angular.module('application.components.page').component('assigneGoal', Component);

})();
