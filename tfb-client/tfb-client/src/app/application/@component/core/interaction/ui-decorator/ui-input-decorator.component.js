(function () {

    /**
     *
     * @param {*} Component
     * @param {*} PubSub
     * @param {*} Translation
     */
    var Controller = function (Component, PubSub, Translation) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;

        model.STATE         = {
            IS_TITLE_VISIBLE : false
        };

        model.value         = null;
        model.collection    = [];


        /**
         * @type
         * Event : on-keyup
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.on_add = function($event) {

            if($event.keyCode == 13) {
                model.uiModel.value.push(model.value);
                model.value = null;
            }
        };


        /**
         * @type
         * Event : on-click
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.on_remove = function($index) {
            model.uiModel.value.splice($index, 1);
        }


        /**
         * @type
         * Event : on-change
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.on_change = function() {

            model.STATE.IS_TITLE_VISIBLE    = model.uiModel.value;
            model.uiModel.show_errors       = model.uiForm[model.uiModel.name].$invalid;
        };

        /**
         * @type
         * Event : on-focus
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.on_focus = function() {
            console.log("Foccuss on field");
        }


        model.$onInit = function() {
            model.STATE.IS_TITLE_VISIBLE = model.uiIsDisabled;
        }

    };


    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['Component', 'PubSub', 'Translation'];


    /**
     *
     */
    var Component = {

        templateUrl : './ui-input-decorator.template.html',
        controller  : Controller,
        controllerAs: 'model',
        bindings        : {
            uiLabel         : '<',
            uiForm          : '<',
            uiName          : '<',
            uiModel         : '<',
            uiHint          : '<',
            uiPlaceholder   : '<',
            uiIsDisabled    : '<'
        }
    };

    /**
     *
     */
    angular.module('application.components.visual').component('uiInputDecorator', Component);

})();
