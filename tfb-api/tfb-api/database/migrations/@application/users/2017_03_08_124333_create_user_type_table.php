<?php

use \App\Http\Models\User\UserType;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTypeTable extends Migration
{
    private $table = UserType::TABLE;

    public function up()
    {
        Schema::create($this->table, function(Blueprint $builder) {

            //# Primary Key
            $builder->increments(UserType::ID);

            //# Table Entity Collection
            $builder->string(UserType::TITLE, 200);
        });
    }

    public function down()
    {
        Schema::drop($this->table);
    }
}
