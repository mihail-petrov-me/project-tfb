<?php
namespace App\Http\Models\Bundle\Update;

use App\Http\Models\Repository;
use DB;

class UpdateGoal extends Repository
{
    public function execute($bundleId, $goalId, $collection)
    {
//        var_dump($collection);

        return DB::table('bundle__goal')->where([
            'bundle_id' => $bundleId,
            'id'        => $goalId
        ])->update($collection);
    }

}