(function () {

    /**
     *
     * @param {*}
     * @param {*} Translation
     * @param {*} PubSub
     * @param {*} UserModel
     * @param {*} ClassModel
     */
    var Controller = function ($timeout, Translation, PubSub, UserModel, ClassModel) {

        /**
         *
         */
        this.isExpanded = false;


        /**
         *
         */
        this.STATE = {
            TOGGLE_FORM : false
        };

        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en' : '#Account information',
                    'bg' : '@Профил'
                }),

                LABEL_ACTION : $provider({
                    'en' : '#Account information',
                    'bg' : '#Настройки'
                }),

                LABEL_CONTACT : $provider({
                    'en' : 'Contact',
                    'bg' : 'Лична информация'
                }),

                LABEL_EDUCATION : $provider({
                    'en' : 'Education',
                    'bg' : 'Образование'
                }),

                LABEL_EMPLOYMENT : $provider({
                    'en' : 'Employment',
                    'bg' : 'Работодатели'
                }),

                LABEL_LANGUAGE : $provider({
                    'en' : 'Language Preferences',
                    'bg' : 'Езикови предпочитания'
                }),

                LABEL_LOGOUT : $provider({
                    'en' : 'Log out',
                    'bg' : 'Излез от системата'
                })
            };
        });



        /**
         *
         */
        this.request_CREATE_student = ($classId, $response) => {

            ClassModel.api.create
            .student($classId, $response)
                .then(this.handle_CREATE_student_success)
                .catch(this.handle_CREATE_student_error);
        };

        /**
         *
         */
        this.handle_CREATE_student_success = ($request) => {

            this.request_FETCH_students(UserModel.state('__class_id'));
            this.isExpanded = false;
        };

        /**
         *
         */
        this.handle_CREATE_student_error = () => {
            console.log("@Exception : Student could not be created");
        };

        /**
         *
         */
        this.request_FETCH_students = ($id) => {

            ClassModel.api.COLLECTION().fetch
                .students($id)
                    .then(this.handler_FETCH_students_success)
                    .catch(this.handler_FETCH_students_error)
        };

        /**
         *
         */
        this.handler_FETCH_students_success = ($request) => {
            this.collection = $request.data;
        };

        /**
         *
         */
        this.handler_FETCH_students_error = ()  => {
            console.log("@Exception : Student collection could not be fetched properly");
        };


        /**
         *
         */
        this.$onInit = () => {

            PubSub.form.end('FORM_CREATE_STUDENT', ($response) => {
                this.request_CREATE_student(UserModel.state('__class_id'), $response);
            });
        };


        /**
         *
         */
        this.$onDestroy = () => {
            PubSub.form.destroy();
        };


        /**
         * @type
         * Router event
         *
         * @description
         * This method is executed on component route init
         */
        this.$routerOnActivate = (next, previous) => {

            UserModel.state("__class_id", next.urlPath);
            this.request_FETCH_students(next.urlPath);
        };


    };

    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject  = ['$timeout','Translation', 'PubSub', 'UserModel', 'ClassModel'];
    var Component       = {
        templateUrl     : './student-list-component.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('studentListComponent', Component);

})();
