<?php

namespace App\Http\Models\User\Fetch\Collection;
use App\Http\Models\Repository;
use DB;

/**
 * Class FetchUserGroupCollection
 * @package App\Http\Models\User\Fetch\Collection
 */
class FetchUserGroupCollection extends Repository
{
    public function execute($id)
    {
        return DB::table('groups')
            ->join(
                    'user_collection', 
                    'user_collection.collection_id', '=', 'groups.id'
            )
            ->select(
                    'groups.id', 
                    'groups.title', 
                    'groups.description', 
                    'user_collection.is_admin',
                    'user_collection.is_owner'
            )
            ->where(
                    'user_collection.user_id', $id
            )
            ->get();
    }
}