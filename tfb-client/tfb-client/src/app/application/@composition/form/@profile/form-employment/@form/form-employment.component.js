(function () {

    /**
     *
     */
    var Controller = FormController(function(Translation) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;

        /**
         *
         */
        model.name = "employment";

        /**
         * @component
         * Fields collection
         *
         * @behaivior
         * This property contains all the fields of
         * the form controller. Every single field contain value and mutator properties
         * in order to check if this field is required or not you must provide
         *
         * @properties
         *  isRequired : true | false
         *  value : String
         */
        model.FIELD = {

            "employment_position": {
                name            : "employment_position",
                type            : 'text',
                isRequired      : true,
                value           : null,
                show_errors     : false,
            },

            "organisation": {
                name            : "organisation",
                type            : 'text',
                show_errors     : false,
                isRequired      : true,
                value           : null
            },

            "website": {
                name            : "website",
                type            : 'text',
                show_errors     : false,
                isRequired      : true,
                value           : null
            },

            "employment_date_start": {
                name            : "employment_date_start",
                type            : 'date',
                show_errors     : false,
                isRequired      : true,
                value           : null
            },

            "employment_date_end": {
                name            : "employment_date_end",
                type            : 'date',
                show_errors     : false,
                isRequired      : true,
                value           : null
            },

            "description": {
                name            : "description",
                type            : 'textarea',
                show_errors     : false,
                isRequired      : true,
                value           : null
            }
        };


        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(function($provider) {

            model.LABEL = {

                INPUT_EMPLOYMENT_POSITION     : $provider({
                    "bg"    : "Заемана позиция",
                    "en"    : "Employment position"
                }),

                LABEL_EMPLOYMENT     : $provider({
                    "bg"    : "Информация за последният ви работодател",
                    "en"    : "Last employer information"
                }),

                INPUT_ORGANISATION     : $provider({
                    "bg"    : "Работодател",
                    "en"    : "Employer"
                }),

                INPUT_WEBSITE     : $provider({
                    "bg"    : "Web адрес на работодателя",
                    "en"    : "Website of the employer"
                }),

                INPUT_EMPLOYMENT_DATE_START : $provider({
                    "bg"    : "Дата на започване на работа",
                    "en"    : "Employment start date"
                }),

                INPUT_EMPLOYMENT_DATE_END : $provider({
                    "bg"    : "Дата на приключване на работа",
                    "en"    : "Employment end date"
                }),

                INPUT_DESCRIPTION   : $provider({
                    "bg"    : "Офишете вашите задължения",
                    "en"    : "Describe your responsibilities"
                })
            };
        });
    });


    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['Translation', 'PubSub', 'Form'];

    /**
     *
     */
    var Component = {
        templateUrl     : './form-employment.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        transclude      : true,
        bindings        : {
            uiCollection    : '<',
            uiIsDisabled    : '<',
            uiPresentation  : '<',
            uiId            : '<'
        }
    };

    angular.module('application.components.page').component('formEmployment', Component);

})();
