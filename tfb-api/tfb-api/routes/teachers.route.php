<?php

// GET
// =====================================================================================================================
$app->get('/tracker/teacher/{id}'                                      , 'Tracker\BundleController@fetchTeacher'                       )->middleware('jwt');
$app->get('/tracker/teacher/{id}/subjects'                             , 'Tracker\BundleController@fetchTeacherSubjectCollection'      )->middleware('jwt');
$app->get('/tracker/teacher/{id}/tracker'                              , 'Tracker\BundleController@fetchTeacherBundleCollection'       )->middleware('jwt');
$app->get('/tracker/teacher/{id}/events'                               , 'Tracker\BundleController@fetchTeacherEventCollection'        )->middleware('jwt');
$app->get('/tracker/teacher/{id}/tracker/{bundle_id}'                  , 'Tracker\BundleController@fetchTeacherBundleSingle'           )->middleware('jwt');
$app->get('/tracker/goals'                                             , 'Tracker\BundleController@showPrimaryGoals'                   )->middleware('jwt');

// POST routes
// =====================================================================================================================
$app->post('/tracker/teacher/{id}/subject'                             , 'Tracker\BundleController@createTeacherSubject'               )->middleware('jwt');
$app->post('/tracker/teacher/{id}/bundle'                              , 'Tracker\BundleController@createTeacherBundle'                )->middleware('jwt');
$app->post('/tracker/teacher/{id}/event'                               , 'Tracker\BundleController@createTeacherEvent'                 )->middleware('jwt');
$app->post('/tracker/teacher/{id}/school'                              , 'Tracker\BundleController@createTeacherSchool'                )->middleware('jwt');

$app->post('/tracker/tracker/{bundle_id}/student'                      , 'Tracker\BundleController@createStudent'                      )->middleware('jwt');
$app->post('/tracker/goals'                                            , 'Tracker\BundleController@createGoals'                        )->middleware('jwt');
$app->post('/tracker/teachers/{teacher_id}/student/{student_id}/note'  , 'Tracker\BundleController@createNote'                         )->middleware('jwt');

// PUT routes
// =====================================================================================================================
$app->put('/tracker/teacher/{teacher_id}/subjects/{id}'                , 'Tracker\BundleController@updateTeacherSubjects'              )->middleware('jwt');
$app->put('/tracker/teacher/{teacher_id}/tracker/{id}'                 , 'Tracker\BundleController@updateTeacherBundles'               )->middleware('jwt');
$app->put('/tracker/teacher/{teacher_id}/events/{id}'                  , 'Tracker\BundleController@updateTeacherEvents'                )->middleware('jwt');

// DELETE routes
// =====================================================================================================================
$app->delete('/tracker/teacher/{teacher_id}/subjects/{id}'             , 'Tracker\BundleController@deleteTeacherSubjects'              )->middleware('jwt');
$app->delete('/tracker/teacher/{teacher_id}/tracker/{id}'              , 'Tracker\BundleController@deleteTeacherBundles'               )->middleware('jwt');
$app->delete('/tracker/teacher/{teacher_id}/events/{id}'               , 'Tracker\BundleController@deleteTeacherEvents'                )->middleware('jwt');