<?php
namespace App\Http\Models\User;

use App\Http\Models\Model;
use DB;

// # Fetch Collection
// # ===========================================================================
use App\Http\Models\User\Fetch\Collection\FetchUserCollection;
use App\Http\Models\User\Fetch\Collection\FetchUserGroupCollection;
use App\Http\Models\User\Fetch\Collection\FetchUserProjectCollection;
use App\Http\Models\User\Fetch\Collection\FetchUserInitiativeCollection;
use App\Http\Models\User\Fetch\Collection\FetchUserCommentCollection;

// # Fetch Single
// # ===========================================================================
use App\Http\Models\User\Fetch\Single\FetchUserSignature;
use App\Http\Models\User\Fetch\Single\FetchUserContact;
use App\Http\Models\User\Fetch\Single\FetchUserEducation;
use App\Http\Models\User\Fetch\Single\FetchUserEmployment;
use App\Http\Models\User\Fetch\Single\FetchUserLanguage;
use App\Http\Models\User\Fetch\Single\FetchUserExperiance;

// # Update
// # ===========================================================================
use App\Http\Models\User\Update\UpdateUserContact;
use App\Http\Models\User\Update\UpdateUserEducation;
use App\Http\Models\User\Update\UpdateUserEmployment;
use App\Http\Models\User\Update\UpdateUserExperince;
use App\Http\Models\User\Update\UpdateUserLanguage;

// # Create
// # ===========================================================================
use App\Http\Models\User\Create\CreateUser;

class UserEducation extends Model
{
    // Public fields
    const ID                = "user_education_id";
    const INSTITUTION       = "user_education_institution";
    const DATE_START        = "user_education_date_start";
    const DATE_END          = "user_education_date_end";
    const DEGREE            = "user_education_degree";
    const FIELD_OF_STUDY    = "user_education_field_of_study";

    // FK
    const USER_ID           = "user_id";

    // Meta fields
    const CREATED_AT        = "created_at";
    const UPDATED_AT        = "updated_at";
    const DELETED_AT        = "deleted_at";

    // Tables
    const TABLE             = "user__education";
}