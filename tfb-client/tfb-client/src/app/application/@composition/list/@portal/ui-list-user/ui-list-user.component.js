(function () {

    var Controller = function (PubSub, $compile) {

        var model = this;

        model.on_load = function() {
            PubSub.api.publish("ui-user-list:request:load");
        };
    }

    var Component = {
        templateUrl     : './ui-list-user.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            collection    : '<',
            uiLoadMore    : '<'
        }
    };

    angular.module('application.components.visual').component('uiListUser', Component);
})();
