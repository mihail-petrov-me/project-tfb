(function () {

    /**
     *
     * @param {*} State
     */
    var Controller = function(State) {


        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;

        /**
         *
         */
        model.state = function() {
            return State.state(model.stateDomain, model.stateId);
        }


        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        model.$onInit = function() {
            State.register(model.stateDomain, model.stateId, model.stateInit);
        };


    };

    var Component = {
        templateUrl     : './ui-state.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        transclude      : true,
        bindings : {
            stateDomain     : '@',
            stateId         : '@',
            stateInit       : '@'
        }
    };

    angular.module('application.components.visual').component('uiState', Component);
})();
