var fs       = require('fs.extra');
var sass     = require('npm-sass');
var mdirp    = require('mkdirp');
var fd       = require('filendir');
var chalk    = require('chalk');

exports.exec = function(options) {

  sass(options.from, function(err, result) {

    if(err) {
      console.log(chalk.blue("Error Report").red(err));
      process.exit();
    }

    var vendorCollection = [];
    for(var i = 0; i < options.vendor.length; i++) {
        vendorCollection.push(fs.readFileSync(options.vendor[i]));
    }

    var vendorStyle  = vendorCollection.join('');
    fd.writeFileSync(options.to, [vendorStyle, result.css].join(''));
  });
};
