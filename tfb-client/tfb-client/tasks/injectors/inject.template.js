// Core components
// # =============================================
var fs        = require('fs-extra');
var util      = require('../helpers/util');

// Application
// # =============================================
String.prototype.replaceAll = function(search, replacement) {
    return this.split(search).join(replacement);
};

// API
// # =============================================
exports.exec = function(source) {

  util.traverse(source, function(file, directory, filePath) {

    var path = directory.replace(source, '');
    var file = fs.readFileSync(filePath).toString().replaceAll('./', path);
    fs.writeFileSync(filePath, file);
  });
};

exports.single = function(source) {


    var collection  = source.split('/');
    var last        = collection.slice(-1)[0];
    var path        = source.replace(last, "");

    var file        = fs.readFileSync(source).toString().replaceAll('./', path.replace("/vagrant/alumni-portal-client/_dev/", ""));
    fs.writeFileSync(source, file);
};
