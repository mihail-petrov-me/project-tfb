(function () {

    /**
     *
     * @param {*} GroupModel
     * @param {*} PubSub
     * @param {*} Translation
     * @param {*} UserModel
     */
    var Controller = function (GroupModel, PubSub, Translation, UserModel) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;

        model.on_close = function() {
            PubSub.api.publish('close_model_window');
        };

    };

    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['GroupModel', 'PubSub', 'Translation', 'UserModel'];

    /**
     *
     */
    var Component = {
        templateUrl     : './group-search.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('groupSearch', Component);

})();
