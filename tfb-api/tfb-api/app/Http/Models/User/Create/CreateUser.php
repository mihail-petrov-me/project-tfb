<?php

namespace App\Http\Models\User\Create;

use App\Http\Models\Repository;
use App\Http\Models\User\User;
use DB;

class CreateUser extends Repository
{
    
    public function execute($collection)
    {
        $id = uniqid();
        $this->assign_user_meta($id, $collection);
        $this->assign_user_contact($id, $collection);
        $this->assign_user_education($id, $collection);
        $this->assign_user_employment($id, $collection);
        
        return true;
    }

    /**
     * @param $id
     * @param $collection
     * @return mixed
     */
    private function assign_user_meta($id, $collection)
    {
        $fullName = ($this->process($collection, User::FNAME) . ' ' . $this->process($collection, User::LNAME));

        return DB::table(User::TABLE)->insert(array(
            User::ID        => $id,
            User::FNAME     => $this->process($collection, User::FNAME),
            User::LNAME     => $this->process($collection, User::LNAME),
            User::NAME      => $fullName,
            User::EMAIL     => $this->process($collection, User::EMAIL),
            User::PASSWORD  => $this->process($collection, User::PASSWORD),
            User::SIGNATURE => $this->process($collection, User::SIGNATURE),
            User::PICTURE   => $this->process($collection, User::PICTURE)
        ));
    }

    /**
     * @param $id
     * @param $collection
     * @return mixed
     */
    private function assign_user_contact($id, $collection)
    {
        return DB::table('user_contact')->insert(array(
            'user_id'           => $id,
            'phone'             => $this->process($collection, 'phone'),
            'birth_date'        => $this->process($collection, 'birth_date'),
            'birth_place'       => $this->process($collection, 'birth_place'),
            'address'           => $this->process($collection, 'address')
        ));
    }
    
    /**
     * 
     * @param type $id
     * @param type $collection
     * @return type
     */
    private function assign_user_education($id, $collection)
    {
        return DB::table('user_education')->insert(array(

            'user_id'                   => $id,
            'institution'               => $this->process($collection, 'institution'),
            'date_start'                => $this->process($collection, 'education_date_start'),
            'date_end'                  => $this->process($collection, 'education_date_end'),
            'degree'                    => $this->process($collection, 'degree'),
            'field_of_study'            => $this->process($collection, 'field_of_study')
        ));        
    }

    /**
     * 
     * @param type $id
     * @param type $collection
     * @return type
     */
    private function assign_user_employment($id, $collection)
    {
        return DB::table('user_employment')->insert(array(
            
           'user_id'                    => $id,
           'organisation'               => $this->process($collection, 'organisation'),
           'employment_position'        => $this->process($collection, 'employment_position'),
           'website'                    => $this->process($collection, 'website'),
           'employment_date_start'      => $this->process($collection, 'employment_date_start'),
           'employment_date_end'        => $this->process($collection, 'employment_date_end'),
           'description'                => $this->process($collection, 'description'),
        ));
    }
}