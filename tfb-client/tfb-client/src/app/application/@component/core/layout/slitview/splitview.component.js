(function () {

    var Controller = function (PubSub) {

        this.STATE = {
            IS_SIDEBAR_VISIBLE      : false,
        };

        this.$onInit = () => {
            this.STATE.IS_SIDEBAR_VISIBLE = this.isSidebarVisible;
        }

        this.$onChanges = () => {
            this.STATE.IS_SIDEBAR_VISIBLE = this.isSidebarVisible;
        }
    };

    Controller.$inject = ['PubSub'];


    var Component = {
        templateUrl     : './splitview.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        transclude: {
            'splitview-dashboard' : 'splitviewDashboard',
            'splitview-sidebar'   : '?splitviewSidebar'
        },

        bindings    : {
            isSidebarVisible : '<'
        }
    };

    angular.module('application.components.visual').component('splitview', Component);
})();
