(function () {

    var ComponentRouter = [

        { path: '/:id/edit',  component: 'editBundle'    , as: 'EditBundle'     , useAsDefault: true},
        { path: '/assign'  ,  component: 'assignBundle'  , as: 'AssignBundle'       },
        { path: '/create'  ,  component: 'createBundle'  , as: 'CreateBundle'       }
    ];

    var Component       = {
        templateUrl     : './bundle.template.html',
        $routeConfig    : ComponentRouter,
        bindings        : { $router     : '<' }
    };

    angular.module('application.components.page').component('bundle', Component);
})();
