// Core components
// # =============================================
var fs = require('fs-extra');


// Task interface
// # =============================================
exports.exec = function(options) {

  for(var i = 0; i < options.from.length; i++) {
    fs.copySync(options.from[i], options.to[i]);
  }
};
