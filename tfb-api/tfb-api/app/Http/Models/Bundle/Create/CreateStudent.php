<?php
namespace App\Http\Models\Bundle\Create;

use App\Http\Models\Repository;
use App\Http\Models\Student\Student;
use DB;

class CreateStudent extends Repository
{
    /**
     * @param $bundleId
     * @param $collection
     * @return array
     */
    public function execute($bundleId, $collection)
    {
        $entity = $this->build($bundleId, $collection);
        DB::table(Student::TABLE)->insert($entity);

        return $entity;
    }

    /**
     * @param $id
     * @param $collection
     * @return array
     */
    public function build($id, $collection) : array
    {
        return array(
            Student::ID            => uniqid(),
            Student::BUNDLE_ID     => $id,
            Student::NAME          => $this->process($collection, Student::NAME)
        );
    }
}