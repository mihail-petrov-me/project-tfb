<?php
namespace App\Http\Models\Group;

use App\Http\Models\Group\Create\CreateGroupEntity;
use App\Http\Models\Model;

use \Carbon\Carbon;

use App\Http\Models\Group\Fetch\Collection\FetchGroupSubscriberCollection;

use App\Http\Models\Entity\Post\FetchPost;
use App\Http\Models\Entity\Article\FetchArticle;

use App\Http\Models\Entity\Feed\FetchFeed;
use App\Http\Models\Entity\Feed\FetchFeedCollection;

use DB;

class GroupCause extends Model
{
    // Public fields
    const ID            = "cause_id";
    const TITLE         = "cause_title";

    // Meta fields
    const CREATED_AT    = "created_at";
    const UPDATED_AT    = "updated_at";
    const DELETED_AT    = "deleted_at";

    // Tables
    const TABLE         = "group_causes";
}