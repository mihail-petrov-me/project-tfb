<?php

namespace App\Http\Models\Campaign\Create;
use App\Http\Models\Campaign\Campaign;
use App\Http\Models\Campaign\CampaignTerm;
use App\Http\Models\Repository;
use DB;

/**
 * Class CreateCampaign
 * @package App\Http\Models\Campaign\Create
 */
class CreateCampaign extends Repository
{
    /**
     * @param $collection
     * @param $query
     * @return array
     */
    public function execute($collection, $query): array
    {
        $entity = $this->buildCampaign($collection);
        DB::table(Campaign::TABLE)->insert($entity);

        DB::table(CampaignTerm::TABLE)->insert(
            $this->buildTerm($entity[Campaign::ID], $collection['terms'])
        );

        return $entity;
    }

    /**
     * @param $collection
     * @return array
     */
    public function buildCampaign($collection): array
    {
        return [
            Campaign::ID    => uniqid(),
            Campaign::TYPE  => $this->process($collection, Campaign::TYPE),
            Campaign::TITLE => $this->process($collection, Campaign::TITLE)
        ];
    }

    /**
     * @param $id
     * @param $collection
     *
     * @return mixed
     */
    public function buildTerm($id, $collection)
    {
        $terms = [];
        for($i = 0, $iMax = count($collection); $i < $iMax; $i++) {
            
            $terms[] = [
                CampaignTerm::ID            => uniqid(),
                Campaign::ID                => $id,
                CampaignTerm::TITLE         => $collection[$i][CampaignTerm::TITLE],
                CampaignTerm::DATE_START    => $collection[$i][CampaignTerm::DATE_START],
                CampaignTerm::DATE_END      => $collection[$i][CampaignTerm::DATE_END]
            ];
        }

        return $terms;
    }
}