(function () {

    /**
     *
     */
    var Controller = FormController(function(Translation) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;


        /**
         *
         */
        model.name = "education";

        /**
         * @component
         * Fields collection
         *
         * @behaivior
         * This property contains all the fields of
         * the form controller. Every single field contain value and mutator properties
         * in order to check if this field is required or not you must provide
         *
         * @properties
         *  isRequired : true | false
         *  value : String
         */

        model.FIELD = {

            "institution": {
                name            : "institution",
                type            : 'text',
                value           : null,
                show_errors     : false,
                isRequired      : true
            },

            "education_date_start": {
                name            : "education_date_start",
                type            : 'date',
                value           : null,
                show_errors     : false,
                isRequired      : true
            },

            "education_date_end": {
                name            : "education_date_end",
                type            : 'date',
                value           : null,
                show_errors     : false,
                isRequired      : true
            },

            "degree": {

                name            : "degree",
                type            : "dropdown",
                title           : 'Ниво на образование',
                value           : null,
                show_errors     : false,
                isRequired      : true
            },


            "field_of_study": {
                name            : "field_of_study",
                type            : 'text',
                value           : null,
                show_errors     : false,
                isRequired      : true
            }
        };


        model.on_edit = function() {
            model.uiIsDisabled = false;
        }

        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(function($provider) {

            model.LABEL = {

                TEST : $provider({
                    "bg"    : [
                        { 'id': 1, 'title': 'Бакалавър' },
                        { 'id': 2, 'title': 'Магистър'  },
                        { 'id': 3, 'title': 'Доктор'    }
                    ],
                    "en"    : [
                        { 'id': 1, 'title': 'Bachelor'},
                        { 'id': 2, 'title': 'Master'  },
                        { 'id': 3, 'title': 'Ph.D'   }
                    ]
                }),

                LABEL_EDUCATION :    $provider({
                    "bg"    : "Вашето образование",
                    "en"    : "Education information"
                }),

                INPUT_INSTITUTION     : $provider({
                    "bg"    : "Инситуция / Университет",
                    "en"    : "Instuitution / Univercity / Colage"
                }),

                INPUT_EDUCATION_DATE_START     : $provider({
                    "bg"    : "Дата на стартиране на образованието",
                    "en"    : "Education start date"
                }),

                INPUT_EDUCATION_DATE_END     : $provider({
                    "bg"    : "Дата на приключване на образованието",
                    "en"    : "Education date end"
                }),

                INPUT_DEGREE     : $provider({
                    "bg"    : "Степен на образованието",
                    "en"    : "Degree"
                }),

                INPUT_FIELD_OF_STUDY : $provider({
                    "bg"    : "Направление в което сте се обучавали",
                    "en"    : "Field of stady"
                })
            };
        });
    });


    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['Translation', 'PubSub', 'Form'];

    /**
     *
     */
    var Component = {
        templateUrl     : './form-education.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        transclude      : true,
        bindings        : {
            uiCollection    : '<',
            uiIsDisabled    : '<',
            uiPresentation  : '<',
            uiId            : '<'
        }
    };

    /**
     *
     */
    angular.module('application.components.page').component('formEducation', Component);
})();
