<?php

namespace App\Http\Middleware;

use App\Services\Token\TokenProvider;
use Closure;

use \Firebase\JWT\ExpiredException;
use DB;

class JwtMiddleware
{
    /**
     *
     * @var type 
     */
    private $tokenProvider = null;
    
    /**
     * 
     * @param TokenProvider $tokenProvider
     */
//    public function __construct(TokenProvider $tokenProvider)
//    {
//        $this->tokenProvider   = $tokenProvider;
//    }

    /**
     * @param         $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
                
//        if(($request->headers->get('x-authorize'))) {
//            $tokken = $request->headers->get('x-authorize');
//
//            try {
//
//                $object = $this->tokenProvider->decompose($tokken);
//                $isPresent = DB::table('users')->where('email', $object['profile']['email'])->first();
//                if(!$isPresent) {
//                    return response()->json("Forbiden object", 401);
//                }
//            } catch (\Exception $ex) {
//                return response()->json("Forbiden object", 401);
//            }
//        }
        
        return $next($request);      
    }
}
