<?php
namespace App\Http\Models\Campaign\Create;
use App\Http\Models\Repository;
use DB;

class CreateCampaignTeacher extends Repository
{
    /**
     * @param $id
     * @param $collection
     * @return array
     */
    public function execute($id, $collection)
    {
        $entity = $this->build($id, $collection);
        DB::table('campaign_teacher')->insert($entity);

        return $entity;
    }

    /**
     * @param $id
     * @param $collection
     * @return array
     */
    public function build($id, $collection)
    {
        return array(

            'campaign_id'       => $id,
            'teacher_id'        => $collection['teacher_id'],
            'coordinator_id'    => $collection['coordinator'],

            'school_id'         => $collection['school'],
        );
    }
}