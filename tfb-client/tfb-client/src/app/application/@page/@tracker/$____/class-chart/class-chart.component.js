(function () {

    /**
     *
     * @param {*} UserModel
     * @param {*} Translation
     */
    var Controller = function ($timeout, Translation, PubSub, UserModel, ClassModel) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;


        model.labels = [
            "Входно ниво",
            "Оценка първи срок",
            "Финална оценка"
        ];

        model.colors = [
            "#ee627d",
            "#54ca95",
            "#ffbf46",
            "#71649a",
            "#aeaeae",
            "#F16623"

        ];

        model.data = [
            [16, 35, 100],
            [1, 25, 66],
            [5, 15, 25],
            [5, 25, 66],
            [2, 13, 19],
            [64, 78, 97]
        ];
    };

    Controller.$inject = ['$timeout','Translation', 'PubSub', 'UserModel', 'ClassModel'];
    var Component = {
        templateUrl     : './class-chart.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('classChart', Component);

})();
