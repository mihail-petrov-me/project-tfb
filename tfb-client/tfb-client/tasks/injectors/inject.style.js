// Core components
// # =============================================
var fs = require('fs.extra');


// API
// # =============================================
exports.exec = function(source, link) {

  var SOURCE_FILE       = (source + 'index.html');
  var DESTINATION_FILE  = (source + 'index.html');


  var fileReference     = fs.readFileSync(SOURCE_FILE).toString();
  var styleCollection   = '<link rel="stylesheet" href="' + link + '">';
  var $compile          = fileReference.replace(/@task:LINK_STYLE/, styleCollection);

  fs.writeFileSync(DESTINATION_FILE, $compile);
};
