<?php
namespace App\Http\Models\Bundle\Fetch;

use App\Http\Models\Repository;
use DB;

class FetchTeacher extends Repository
{
    public function execute($id)
    {
        return DB::table('campaign_teacher')
            ->select(array(
                'campaign_teacher.campaign_id',
                'campaign_teacher.teacher_id',
                'campaign_teacher.coordinator_id',
                'campaign_teacher.school_id',
                'collection_school.title'
            ))
            ->leftJoin('collection_school',
                    'collection_school.id', '=', 'campaign_teacher.school_id')
            ->where(array(
            'campaign_teacher.teacher_id'  => $id
        ))->get();
    }

}