(function () {

    /**
     *
     * @param {*} UserModel
     * @param {*} Translation
     */
    var Controller = function ($timeout, Translation, PubSub, UserModel, ClassModel) {

    };


    Controller.$inject = ['$timeout','Translation', 'PubSub', 'UserModel', 'ClassModel'];
    var Component = {
        templateUrl     : './class-calendar-bar.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('classCalendarBar', Component);

})();
