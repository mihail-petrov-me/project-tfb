<?php

namespace App\Services\Authentication\providers;

use App\Http\Models\User\User;
use GuzzleHttp;
use DB;

/**
 * Class GoogleAuthenticationProvider
 * @package App\Services\Authentication\providers
 */
class GoogleAuthenticationProvider {

    /**
     *
     */
    const AUTHENTICATION_DOMAIN = 'zaednovchas.bg';

    /**
     * @var array
     */
    private $EXCEPTION_COLLECTION = [
        'HANDLE'    => 'NO valid credentials available for the current user'
    ];

    /**
     *
     * @param type $request
     *
     * @return type
     * @throws \RuntimeException
     */
    public function process($request)
    {
        $authEmail = $this->handle($request)['email'];

        if(!$this->isDomainValid($authEmail))
        {
            throw new \RuntimeException($this->EXCEPTION_COLLECTION['HANDLE']);
        }

        return $this->getProfile($authEmail);
    }

    /**
     * @param $request
     *
     * @return mixed
     */
    private function handle($request) 
    {
        
        $client = new GuzzleHttp\Client(['verify' => false]);

        $params = [
            'code'          => $request['code'],
            'client_id'     => $request['client_id'],
            'client_secret' => 'iLegCwy15hzeS2kWs40GR8zF',
            'redirect_uri'  => $request['redirect_uri'],
            'grant_type'    => 'authorization_code',
        ];

        $accessTokenResponse = $client->request('POST', 'https://accounts.google.com/o/oauth2/token', ['form_params' => $params]);
        $accessToken = json_decode($accessTokenResponse->getBody(), true);

        $profileResponse = $client->request('GET', 'https://www.googleapis.com/plus/v1/people/me/openIdConnect', [
            'headers' => ['Authorization' => 'Bearer ' . $accessToken['access_token']]
        ]);

        return json_decode($profileResponse->getBody(), true);
    }


    /**
     * @param $request
     *
     * @return bool
     */
    private function isDomainValid($request): bool
    {
        return (explode('@', $request)[1] === self::AUTHENTICATION_DOMAIN);
    }

    /**
     * @param $email
     *
     * @return mixed
     */
    private function getProfile($email)
    {
        $profileCollection  = (new User())->fetch_user([User::EMAIL => $email]);
        $profileCollection['permissions'] = $this->getPermissions($profileCollection[User::ID]);

        return $profileCollection;
    }

    /**
     * @param string $id
     * @return array
     */
    private function getPermissions(string $id): array
    {
        return (new User())->fetch_userPermission($id);
    }
}