(function () {

    var Controller = function (FormState, PubSub) {

        this.STATE = {
            IS_TITLE_VISIBLE : false,
            IS_EDIT          : false
        };


        /**
         * @type
         * Event : @ON : CHANGE
         */
        this.on_change = () => {

            this.STATE.IS_TITLE_VISIBLE = this.uiModel.value;
            this.uiModel.show_errors = this.uiForm[this.uiModel.name].$invalid;
        };

        /**
         * @Event
         */
        this.on_edit = () => {

            if(FormState.IS_EDIT) {
                console.log("CAN NOT EDIT THIS ");
            }
            else {
                this.STATE.IS_EDIT  = true;
                FormState.IS_EDIT   = true;
            }
        };

        /**
         * @Event
         */
        this.on_edit_cancel = () => {

            this.STATE.IS_EDIT = false;
            FormState.IS_EDIT  = false;
        };


        this.is_editable = () => {
            return !FormState.IS_EDIT;
        };


        /**
         * @type
         * Event : init
         */
        this.$onInit = () => {
            this.STATE.IS_TITLE_VISIBLE = this.uiIsDisabled;

            this.uiIsEditable = false;
            console.log(this.uiIsEditable);


            PubSub.api.subscribe('KEYBOARD_CANCEL_EVENT', () => {
                this.on_edit_cancel();
            });
        };
    }

    Controller.$injector = ['FormState', 'PubSub'];

    var Component = {
        templateUrl     : './ui-input-text.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            uiLabel         : '<',
            uiForm          : '<',
            uiName          : '<',
            uiModel         : '<',
            uiHint          : '<',
            uiPlaceholder   : '<',
            uiIsDisabled    : '<',


            // @ State Properties
            uiPresentation  : '@'
        }
    };

    angular.module('application.components.visual').component('uiInputText', Component);
})();
