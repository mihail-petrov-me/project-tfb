(function () {

    var Controller = function(Translation, PubSub) {

        this.name = "FORM_ASSIGN_TEACHER_SUBJECT";

        /**
         * @component
         * Fields collection
         */
        this.FIELD = {

            "subject": {

                name            : "subject",
                type            : "checkbox",
                value           : null,
                show_errors     : false,
                isRequired      : true,
                collection      : null
            }
        };

        this.next = 11;

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                OPTIONS_DROPDOWN_SUBJECTS : $provider({
                    "bg"    : [
                        { 'id': '1' , 'title': 'Възпитател' },
                        { 'id': '2' , 'title': 'Математика' },
                        { 'id': '3' , 'title': 'Химия' },
                        { 'id': '4' , 'title': 'Физика' },
                        { 'id': '5' , 'title': 'Биология' },
                        { 'id': '6' , 'title': 'Информатика' },
                        { 'id': '7' , 'title': 'География' },
                        { 'id': '8' , 'title': 'История' },
                        { 'id': '9' , 'title': 'Български език и Литература' },
                        { 'id': '10' , 'title': 'Англииски език' },
                        { 'id': '11' , 'title': 'Немски език' },
                    ],

                    "en"    : [
                        { 'id': '1' , 'title': '1' },
                        { 'id': '2' , 'title': '2' },
                        { 'id': '3' , 'title': '3' }
                    ]
                })
            };
        });

        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {

            this.parent.FIELD  = this.FIELD;
            this.formContact   = this.parent.formContact;

            PubSub.form.end("FORM_ADD_SUBJECT", ($response) => {
                this.LABEL.OPTIONS_DROPDOWN_SUBJECTS.push({
                    'id'            : (++this.next),
                    'title' : $response.new_subject
                });
            });
        }
    };

    /**
     * @type
     * Component injector
     */
    Controller.$inject = ['Translation', 'PubSub'];
    let Component = {
        templateUrl     : './form-assigne-teacher-subject.template.html',
        controller      : Controller,
        controllerAs    : 'model',

        require             : {
            parent          : '^formWrapper'
        },

        bindings        : {
            uiCollection    : '<',
            uiIsDisabled    : '<',
            uiPresentation  : '<',
            uiId            : '<'
        }
    };

    angular.module('application.components.page').component('formAssigneTeacherSubject', Component);
})();
