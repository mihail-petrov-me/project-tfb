(function () {

    /**
     *
     * @param {*} PubSub
     * @param {*} Translation
     */
    var Controller = function (UserModel, EntityModel) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;

        /**
         * @type
         * Property Collection
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        // model.commentCollection = [];


        /**
         * @type
         * Event : click
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        model.on_commentSubmit = function($element) {

            EntityModel.api.create.comment({
                'entity_id'         : model.uiId,
                'comment_content'   : model.commentContent,
                'user_id'           : UserModel.profile.my("user_id")
            });

            model.commentContent = null;
            //request_REFRESH_comment();
        };

        /**
         * @type
         * Event : click
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        // model.on_load = function() {
        //     request_FETCH_comment();
        // };


        /**
         * @type
         * Request : CREATE
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        function request_CREATE_comment() {

            EntityModel.api.create.comment({
                'entity_id' : model.uiId,
                'content'   : model.commentContent,
                'user_id'   : UserModel.profile.my("user_id")
            });
        };


        /**
         * @type
         * Request : FETCH
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        function request_FETCH_comment() {

            EntityModel.api.PAGINATE().fetch
                .comments(model.uiId)
                .then(handler_FETCH_comment_success)
                .catch(handler_FETCH_comment_error);
        };

        /**
         * @type
         * Request : REFRESH - FETCH
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        // function request_REFRESH_comment() {

        //     EntityModel.api.REFRESH().fetch
        //         .comments(model.uiId)
        //         .then(handler_REFRESH_comment_success)
        //         .catch(handler_FETCH_comment_error)
        // };

        /**
         * @type
         * Handler : FECH : SUCCESS
         */
        // function handler_FETCH_comment_success($response) {

        //     model.commentCollection = model.commentCollection.concat($response.data);
        //     model.loadMore          = $response.pagination.has_next;
        // };

        /**
         * @type
         * Handler : FECH : ERROR
         */
        // function handler_FETCH_comment_error($response) {
        //     console.log("@Exception : Comment Collection fetch");
        // };

        /**
         * @type
         * Handler : REFRESH - FECH : SUCCESS
         */
        // function handler_REFRESH_comment_success($response) {
        //     model.commentCollection = model.commentCollection.concat($response.data);
        // };

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component change
         */
        model.$onChanges = function() {

            // if(model.uiShowComment) {
            //     request_FETCH_comment();
            // }
        }
    };

    Controller.$inject = ['UserModel', 'EntityModel'];

    var Component = {
        templateUrl     : './ui-block-comment.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            uiId                : '<',
            uiShowComment       : '<',
            uiCollection        : '<'
        }
    };

    angular.module('application.components.visual').component('uiBlockComment', Component);

})();
