<?php

namespace App\Http\Models\User\Fetch\Collection;

use App\Http\Models\Repository;
use App\Http\Models\User\Fetch\Single\FetchUserContact;
use App\Http\Models\User\Fetch\Single\FetchUserEducation;
use App\Http\Models\User\Fetch\Single\FetchUserEmployment;
use App\Http\Models\User\Fetch\Single\FetchUserSinglePermission;
use App\Http\Models\User\User;
use App\Http\Models\User\UserContact;
use App\Http\Models\User\UserEducation;
use App\Http\Models\User\UserEmployment;
use DB;

/**
 * Class FetchUserCollection
 * @package App\Http\Model\User\Fetch\Collection
 */
class FetchUserCollection extends Repository
{

    /**
     * @param $query
     *
     * @return mixed
     */
    public function execute($query)
    {
        return $this->marse();
    }

    public function parse()
    {

        return DB::table(User::TABLE)
            ->select([
                // *
                $this->table(User::TABLE, User::ID),
                $this->table(User::TABLE, User::FNAME),
                $this->table(User::TABLE, User::LNAME),
                $this->table(User::TABLE, User::NAME),
                $this->table(User::TABLE, User::SIGNATURE),
                $this->table(User::TABLE, User::PICTURE),
                $this->table(User::TABLE, User::PERMISSIONS),

                // *
                $this->table(UserContact::TABLE, UserContact::PHONE),
                $this->table(UserContact::TABLE, UserContact::ADDRESS),
                $this->table(UserContact::TABLE, UserContact::BIRTH_PLACE),
                $this->table(UserContact::TABLE, UserContact::BIRTH_DATE),

                // *
                $this->table(UserEducation::TABLE, UserEducation::INSTITUTION),
                $this->table(UserEducation::TABLE, UserEducation::DATE_START),
                $this->table(UserEducation::TABLE, UserEducation::DATE_END),
                $this->table(UserEducation::TABLE, UserEducation::DEGREE),
                $this->table(UserEducation::TABLE, UserEducation::FIELD_OF_STUDY),

                // *
                $this->table(UserEmployment::TABLE, UserEmployment::ORGANISATION),
                $this->table(UserEmployment::TABLE, UserEmployment::WEBSITE),
                $this->table(UserEmployment::TABLE, UserEmployment::POSITION),
                $this->table(UserEmployment::TABLE, UserEmployment::DATE_START),
                $this->table(UserEmployment::TABLE, UserEmployment::DATE_END),
                $this->table(UserEmployment::TABLE, UserEmployment::DESCRIPTION),
            ])
            ->leftJoin(
                UserContact::TABLE,
                $this->table(UserContact::TABLE, User::ID),
                '=',
                $this->table(User::TABLE, User::ID)
            )
            ->leftJoin(
                UserEducation::TABLE,
                $this->table(UserEducation::TABLE, User::ID),
                '=',
                $this->table(User::TABLE, User::ID)
            )
            ->leftJoin(
                UserEmployment::TABLE,
                $this->table(UserEmployment::TABLE, User::ID),
                '=',
                $this->table(User::TABLE, User::ID)
            )
            ->get();
    }

    public function parseEducationCollection()
    {

    }

    public function parseEmpoyCollection()
    {

    }

    public function marse()
    {
        var_dump($this->parse());
        die();


//        $collection = json_decode($this->parse(), true);
//        var_dump($collection);

//        $dd = [];

        // parse contact collection

        // parse employ collection

        // parse education collection

//        foreach ($collection as $element)
//        {
//            if(!array_key_exists($element[User::ID], $dd))
//            {
//                $dd[$element[User::ID]] = [
//                ];
//            }
//        }



        //return $collection;
//        $response   = [];
//
//        $userEducationModel     = new FetchUserEducation();
//        $userEmploymentModel    = new FetchUserEmployment();
//
//        foreach ($collection as $element) {
//
//            //$element['user_contact']     = $userContactModel->execute($element[User::ID]);
//            $element['user_education']   = $userEducationModel->execute($element[User::ID]);
//            $element['user_employment']  = $userEmploymentModel->execute($element[User::ID]);
//            $response[]                  = $element;
//        }
//
//        return $response;
    }
}