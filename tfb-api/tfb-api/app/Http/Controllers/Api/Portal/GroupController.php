<?php
namespace App\Http\Controllers\Api\Portal;

use App\Http\Models\Group\Group;

use \Illuminate\Http\Request;
use \App\Http\Controllers\Controller;

use \App\Services\TokenProvider;
use \Firebase\JWT\JWT;

use App\Http\Models\Entity\Article\CreateArticle;
use \App\Http\Models\Entity\Post\CreatePost;
use \App\Http\Models\Entity\Poll\CreatePoll;
use DB;

/**
 * Class GroupController
 * @package App\Http\Controllers\Api\Portal
 */
class GroupController extends Controller {

    
    /**
     *
     * @var type 
     */
    private $API_MESSAGES = [
        "GROUP_CREATED"     => "Group created successfuly",
        "GROUP_UPDATED"     => "Group udated successfully",
        "GROUP_DELETE"      => "Group deleted successfully"
    ];

    /**
     *
     * @var type 
     */
    private $API_ERROR_MESSAGES = [
        "GROUP_CREATED"     => "User created successfuly",
        "GROUP_UPDATED"     => "The requested resourse could not be updated"
    ];

    /**
     * Contains the service provider with witch the user can authenticate 
     * it self infront of the system
     * 
     * @var AuthenticationService 
     */
    private $tokenProvider = null;
    
    /**
     *
     * @var type 
     */
    private $model = null;

    /**
     *
     * @param User|Group $user
     * @param TokenProvider $tokenProvider
     * @internal param TokenProvider $tokkenProvider
     * @internal param AuthenticationService $provider
     */
    public function __construct(Group $user, TokenProvider $tokenProvider)
    {
        $this->model            = $user;
        $this->tokenProvider   = $tokenProvider;
    }

    // GET : groups
    // #
    public function index(Request $request) 
    {
        $collection = $this->model->all($request->query());
        return $this->responseSuccess(null, $collection, $this->model->getMeta());
    }

    // GET : groups/:id
    // #
    public function show($id) 
    {
        $model = $this->model->get($id);
        return $this->responseSuccess(null, $model);
    }


    // GET : groups/:id/members
    // #
    public function showParticipents($id) 
    {
        $model = $this->model->getParticipents($id);
        return $this->responseSuccess(null, $model);
    }

    // POST : groups/:id/join
    // #
    public function subscribe(Request $request) 
    {
        try {
            $collection = $this->model->subscribe_group($request->json()->all());
            if($collection) {
                
                return $this->responseSuccess("User successfully subscribed", 
                    $this->model->get($request['group_id'])
                );
            }

            return $this->responseNotFound("Unable to subscribe user");
        } 
        catch (\Exception $ex) {
            return $this->responseNotFound("User is already a subscriber");
        }
    }

    // POST : groups
    // #
    public function store(Request $request) {
        
        $groupEntity = $this->model->create_group($request->json()->all());
        return $this->responseSuccess($this->API_MESSAGES['GROUP_CREATED'], $groupEntity);
    }

    // PUT : groups/{id}/edit
    // #
    public function edit() {
        
    }

    // UPDATE : groups
    // #
    public function update() {
        // Get data from server;
        $this->model->update($request->json()->all());
        return $this->responseSuccess($this->API_MESSAGES['GROUP_CREATED']);
    }

    // DELETE : groups
    // #
    public function delete() {
        
    }


    /**
     * 
     * @param type $id
     * @param Request $request
     * @return type
     */
    public function createGroupPost($id, Request $request) 
    {
        (new CreatePost())->execute($id, $request);
        return $this->responseSuccess("Post is created", array()); 
    }
    
    
    /**
     * 
     * @param type $id
     * @param Request $request
     * @return type
     */
    public function createGroupArticle($id, Request $request) 
    {     
        (new CreateArticle())->execute($id, $request);
        return $this->responseSuccess("Article is created", array());
    }

    /**
     * 
     * @param type $id
     * @param Request $request
     * @return type
     */
    public function createGroupVideo($id, Request $request) {
        
        $modelId = uniqid();

        DB::table('block_video')->insertGetId(array(
            'id'            => $modelId,
            'title'         => $request['title'],
            'content'       => $request['content'],
            'user_id'       => $request['user_id']
        ));

        DB::table('group_entity')->insert(array(
            'entity_id'     => $modelId,
            'group_id'      => $id,
            'entity_type'   => 4
        ));


        return $this->responseSuccess("Video is created", array());
    }


    /**
     * 
     * @param type $id
     * @param Request $request
     * @return type
     */
    public function createGroupPoll($id, Request $request) 
    {    
        (new CreatePoll())->execute($id, $request);
        return $this->responseSuccess("Poll is createdd", array());  
    }


    
    
    public function getPoll($id, Request $request)
    {
        
        if($request->headers->get('x-authorize')) {
            
            $tokken = $request->headers->get('x-authorize');
            $object = $this->tokkenProvider->decompose($tokken);
            $userId = $object['profile']['id'];
        
            $entry = $this->model->get_pollSingle($id, $userId);
            return $this->responseSuccess(null, $entry);
        }
        
        return $this->responceUnauthorized("You are not authorise to do this request");
    }

    
    public function answerGroupPoll(Request $request)
    {
        
        // check if answer is posible 
        // #
        $isVoteAlreadyCast = DB::table("block_poll_answer_stat")->where(array(
            'user_id'   => $request['user_id'],
            'entity_id' => $request['entity_id'],
        ))->first();
        
        
        if($isVoteAlreadyCast) {
            return $this->responseNotFound("User already voted for this poll");
        }
        
        // Create new relation answer and entity
        // #
        DB::table('block_poll_answer_stat')->insert(array(
            'user_id'   => $request['user_id'],
            'entity_id' => $request['entity_id'],
            'answer_id' => $request['answer_id']
        ));
        
        // update answer count
        DB::table('block_poll_answer')
            ->where('id', $request['answer_id'])
            ->increment('count');
        
        return $this->responseSuccess("Poll is answers", array());
    }
    
    
    /**
     * 
     * @param type $id
     * @param Request $request
     * @param type $limit
     * @return type
     */
    public function getGroupData($id, Request $request) 
    {    
        $collection = $this->model->fetch_feed($id, '5920784198bca', $request->query());       
        return $this->responseSuccess(null, $collection,  $this->model->getMeta());
    }
}
