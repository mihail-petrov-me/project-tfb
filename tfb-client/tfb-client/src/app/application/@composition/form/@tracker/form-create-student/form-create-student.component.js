(function () {

    var Controller = function(Translation, PubSub) {

        this.name = "FORM_CREATE_STUDENT";

        this.FIELD = {

            "name": {
                name            : "name",
                type            : 'text',
                value           : null,
                show_errors     : false,
                isRequired      : true
            }
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                LABEL_EDUCATION:    $provider({
                    "bg"    : "Въведи нов ученик",
                    "en"    : "Add new student"
                }),

                INPUT_STUDENT_NAME: $provider({
                    "bg"    : "Име и фамилия на ученика",
                    "en"    : "Student first name"
                })
            };
        });


        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {

            this.parent.FIELD  = this.FIELD;
            this.formContact   = this.parent.formContact;
        }


        this.add_student = () => {
            PubSub.form.init('FORM_CREATE_STUDENT');
        };

        this.update_student = () => {
            PubSub.form.update('FORM_CREATE_STUDENT');
        };

        this.$onChanges = () => {

            if(this.uiUpdate) {
                this.FIELD['name'].value  = this.uiCollection.name;
                this.parent.uiId = this.uiCollection.id;
            }
        };
    };


    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['Translation', 'PubSub', 'Form'];

    var Component = {
        templateUrl     : './form-create-student.template.html',
        controller      : Controller,
        controllerAs    : 'model',

        require             : {
            parent          : '^formWrapper'
        },

        bindings        : {

            uiIsDisabled    : '<',
            uiId            : '<',

            uiUpdate        : '@',
            uiCollection    : '<'
        }
    };
    angular.module('application.components.page').component('formCreateStudent', Component);

})();
