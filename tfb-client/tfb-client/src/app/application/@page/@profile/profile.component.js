(function () {

    /**
     *
     * @param {*} UserModel
     * @param {*} Translation
     * @param {*} State
     * @param {*} PubSub
     */
    var Controller = function (UserModel, Translation, State, PubSub) {

        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en' : '#Account information',
                    'bg' : '@Профил'
                }),

                LABEL_ACTION : $provider({
                    'en' : '#Account information',
                    'bg' : '#Настройки'
                }),

                LABEL_CONTACT : $provider({
                    'en' : 'Contact',
                    'bg' : 'Лична информация'
                }),

                LABEL_EDUCATION : $provider({
                    'en' : 'Education',
                    'bg' : 'Образование'
                }),

                LABEL_EMPLOYMENT : $provider({
                    'en' : 'Employment',
                    'bg' : 'Работодатели'
                }),

                LABEL_LANGUAGE : $provider({
                    'en' : 'Language Preferences',
                    'bg' : 'Езикови предпочитания'
                }),

                LABEL_LOGOUT : $provider({
                    'en' : 'Log out',
                    'bg' : 'Излез от системата'
                })
            };
        });

        /**
         * @type
         * Еvent
         *
         * @description
         * Call user model in order to destroy the authetication state and
         * then redirects the user to the authentication page
         */
        this.on_expand = ($id) => {
            State.push('profile', $id);
        }


        /**
         * @type
         * Еvent
         *
         * @description
         * Call user model in order to destroy the authetication state and
         * then redirects the user to the authentication page
         */
        this.on_open = ($id) => {
            State.push('profile', $id);
        };


        /**
         * @type
         * Еvent
         *
         * @description
         * Call user model in order to destroy the authetication state and
         * then redirects the user to the authentication page
         */
        this.on_logout = () => {

            UserModel.profile.destroy();
            this.$router.navigate(['/Login']);
        };

        /**
         * @type
         * PubSub
         *
         * @description
         * Call user model in order to destroy the authetication state and
         * then redirects the user to the authentication page
         */

        PubSub.api.subscribe('close_modal_window', function() {
            State.pop('profile');
        });
    };

    var ComponentRouter = [
        { path: 'personal-info',         component: 'personalInfo',          as: 'PersonalInfo', useAsDefault: true},
        { path: 'education-info',        component: 'educationInfo',         as: 'EducationInfo'},
        { path: 'employment-info',       component: 'employmentInfo',        as: 'EmploymentInfo'},
        { path: 'language-preferences',  component: 'languagePreferences',   as: 'LanguagePreferences'},
        { path: 'alumni-list',           component: 'alumniList',            as: 'AlumniList'}
    ];

    Controller.$inject  = ['UserModel', 'Translation', 'State', 'PubSub'];
    var Component       = {
        templateUrl     : './profile.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        $routeConfig    : ComponentRouter,
        bindings        : { $router: '<'}
    };

    angular.module('application.components.page').component('profile', Component);

})();
