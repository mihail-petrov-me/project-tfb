(function () {

    var Component = {
        templateUrl     : './welcome-into.template.html',
        controllerAs    : 'model'
    };

    angular.module('application.components.visual').component('welcomeIntro', Component);

})();
