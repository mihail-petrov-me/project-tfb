(function () {

    var Controller = FormController(function(Translation, PubSub, Form, UserModel) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;

        /**
         *
         */
        model.name = "alumni-filter";


        /**
         * @component
         * Fields collection
         *
         * @behaivior
         * This property contains all the fields of
         * the form controller. Every single field contain value and mutator properties
         * in order to check if this field is required or not you must provide
         *
         * @properties
         *  isRequired : true | false
         *  value : String
         */
        model.FIELD = {

            "cohort": {
                name            : "cohort",
                type            : 'dropdown',
                title           : 'Випуск',
                value           : null,
                isRequired      : true,
                show_errors     : false
            },

            "area"              : {
                name            : "area",
                type            : 'dropdown',
                title           : 'Област',

                provider        : function() {
                    return UserModel.api.COLLECTION().fetch.mytest();
                },
                value           : null,
                isRequired      : true,
                show_errors     : false
            },

            "municipality"      : {
                name            : "municipality",
                type            : 'dropdown',
                title           : 'Община',

                provider        : function($id) {
                    return UserModel.api.fetch.mymest($id);
                },
                value           : null,
                isRequired      : true,
                show_errors     : false
            },

            "city"              : {
                name            : "city",
                type            : 'dropdown',
                title           : 'Община',

                provider        : function($id) {
                    return UserModel.api.fetch.mybest($id);
                },
                value           : null,
                isRequired      : true,
                show_errors     : false
            },

            "school": {
                name            : "school",
                type            : 'dropdown',
                title           : 'Училище',

                provider        : function($id) {
                    return UserModel.api.fetch.mygest($id);
                },
                value           : null,
                isRequired      : true,
                show_errors     : false
            }
        };

        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(function($provider) {

            model.LABEL = {

                OPTIONS_COHORT : $provider({
                    "bg"    : [
                        { 'id': 2011, 'title': '2011'    },
                        { 'id': 2012, 'title': '2012'    },
                        { 'id': 2013, 'title': '2013'    },
                        { 'id': 2014, 'title': '2014'    },
                        { 'id': 2015, 'title': '2015'    },
                        { 'id': 2016, 'title': '2016'    },
                        { 'id': 2017, 'title': '2017'    },
                        { 'id': 2018, 'title': '2018'    }
                    ],
                    "en"    : [
                        { 'id': 2011, 'title': '2011'    },
                        { 'id': 2012, 'title': '2012'    },
                        { 'id': 2013, 'title': '2013'    },
                        { 'id': 2014, 'title': '2014'    },
                        { 'id': 2015, 'title': '2015'    },
                        { 'id': 2016, 'title': '2016'    },
                        { 'id': 2017, 'title': '2017'    },
                        { 'id': 2018, 'title': '2018'    }
                    ]
                }),

                OPTIONS_COHORT_PLACEHOLDER : $provider({
                    "bg"    : "Випуск",
                    "en"    : "Cohort"
                }),

                OPTIONS_AREA_PLACEHOLDER : $provider({
                    "bg"    : "Област",
                    "en"    : "Area"
                }),

                OPTIONS_MUNICIPALITY_PLACEHOLDER : $provider({
                    "bg"    : "Община",
                    "en"    : "Municipality"
                }),

                OPTIONS_CITY_PLACEHOLDER : $provider({
                    "bg"    : "Град",
                    "en"    : "E-mail"
                }),

                OPTIONS_SCHOOL_PLACEHOLDER : $provider({
                    "bg"    : "Училище",
                    "en"    : "Mobile number"
                }),

                INPUT_SUBJECT : $provider({
                    "bg"    : "Предмет",
                    "en"    : "Birth date"
                })
            };
        });


        /**
         *
         */
        model.on_filter = function() {
            PubSub.api.publish(PubSub.events.form.PROCESS.INIT, model.name);
        };
    });




    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    // Controller.$inject = ['Translation', 'PubSub', 'Form'];

    /**
     *
     */
    var Component = {
        templateUrl     : './form-alumni-filter.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            uiCollection    : '<',
            uiIsDisabled    : '<'
        }
    };

    /**
     *
     */
    angular.module('application.components.page').component('formAlumniFilter', Component);

})();
