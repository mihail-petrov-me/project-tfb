<?php
namespace App\Http\Models\Project\Delete;

use DB;
use App\Http\Models\Project\Project;
use App\Http\Models\Project\ProjectTask;
use \App\Http\Models\Repository;

/**
 * Class DeleteProject
 */
class DeleteProjectTask extends Repository
{
    /**
     * @param $id
     *
     * @return mixed
     */
    public function execute($id)
    {
        return DB::table(Project::TABLE_TASK)->where([
            ProjectTask::ID => $id
        ])->delete();
    }
}