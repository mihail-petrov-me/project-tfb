<?php
namespace App\Http\Models\Project\Fetch;

use DB;
use App\Http\Models\Project\Project;
use \App\Http\Models\Repository;

/**
 * Class FetchProjectCollection
 */
class FetchProjectCollection extends Repository
{
    /**
     * @return mixed
     */
    public function execute()
    {
        return DB::table(Project::TABLE)->get();
    }
}