<?php

namespace App\Http\Models\Campaign\Fetch;
use App\Http\Models\Campaign\Campaign;
use App\Http\Models\Repository;
use DB;

/**
 * Class FetchCampaignCollection
 * @package App\Http\Models\Campaign\Fetch
 */
class FetchCampaignCollection extends Repository
{
    /**
     * @param $query
     * @return mixed
     */
    public function execute($query)
    {
        return DB::table(Campaign::TABLE)->select(
            Campaign::ID,
            Campaign::TYPE,
            Campaign::TITLE
        )->get();
    }
}
