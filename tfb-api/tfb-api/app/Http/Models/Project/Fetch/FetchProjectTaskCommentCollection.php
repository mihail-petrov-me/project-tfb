<?php
namespace App\Http\Models\Project\Fetch;

use App\Http\Models\Collections\CollectionComment;
use App\Http\Models\Project\ProjectTask;
use App\Http\Models\User\User;
use DB;
use App\Http\Models\Project\Project;
use \App\Http\Models\Repository;

/**
 * Class FetchProjectCollection
 */
class FetchProjectTaskCommentCollection extends Repository
{
    /**
     * @param $id
     *
     * @return mixed
     */
    public function execute($id)
    {
        $data = DB::table(CollectionComment::TABLE)
            ->select(
                $this->table(User::TABLE, User::SIGNATURE),
                $this->table(User::TABLE, User::ID),
                $this->table(User::TABLE, User::NAME),
                $this->table(CollectionComment::TABLE, CollectionComment::CONTENT)
            )
            ->join(
                User::TABLE,
                $this->table(User::TABLE, User::ID),
                '=',
                $this->table(CollectionComment::TABLE, User::ID)
            )
            ->where([
                $this->table(CollectionComment::TABLE, CollectionComment::ENTITY_ID) => $id
            ])
            ->get();

        return json_decode($data, true);
    }
}