(function () {

    /**
     *
     * @param {*} PubSub
     * @param {*} Translation
     */
    var Controller = function (PubSub, Translation) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;

        /**
         * @type
         * State collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.STATE = {
            IS_PREVIEW_VISIBLE  : true
        };

        /**
         * @type
         * Subscribe collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.SUBSCRIBE = {
            'finish'  : PubSub.events.component.FINISH
        }

        /**
         * @type
         * Publish collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.PUBLISH = {
            'article' : PubSub.events.component.ARTICLE.PROCESS,
            'post'    : PubSub.events.component.POST.PROCESS,
            'poll'    : PubSub.events.component.POLL.PROCESS,
            'video'   : PubSub.events.component.VIDEO.PROCESS,
            'image'   : PubSub.events.component.IMAGE.PROCESS
        };

        /**
         * @type
         * Component collection
         *
         * @description
         * The component collection contain every single component entity
         */
        model.COMPONENT = {
            'article' : {   IS_ACTIVE : false   },
            'post'    : {   IS_ACTIVE : false   },
            'poll'    : {   IS_ACTIVE : false   },
            'video'   : {   IS_ACTIVE : false   },
            'image'   : {   IS_ACTIVE : false   }
        };

        /**
         * @type
         * Property collection
         *
         * @description
         * The property collection contain set of component specific properties
         */
        model.PROPERTY = {
            ACTIVE_ELEMENT_REFERENCE : null
        };


        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(function($provider) {

            model.LABEL = {

                BLOCK_PLACEHOLDER     : $provider({
                    'en': 'Share valuable info with the other members',
                    'bg': 'Сподели ценна информация с твоите колеги'
                }),

                BUTTON_ACTION     : $provider({
                    'en': 'Post',
                    'bg': 'Публикувай'
                })
            };
        });


        /**
         * @type
         * Event : on
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.on_activate = function () {

            model.STATE.IS_PREVIEW_VISIBLE          = false;
            model.COMPONENT.post.IS_ACTIVE          = true;
            model.PROPERTY.ACTIVE_ELEMENT_REFERENCE = 'post';
        };

        /**
         * @type
         * Event : on
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.on_submit = function () {
            PubSub.api.publish(model.PUBLISH[model.PROPERTY.ACTIVE_ELEMENT_REFERENCE]);
        };

        /**
         * @type
         * Event : on
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.on_hide = function () {

            for(type in model.COMPONENT) {
                model.COMPONENT[type].IS_ACTIVE = false;
            }
        };


        /**
         * @type
         * Event : on
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.on_change = function (type) {

            model.on_hide();

            model.STATE.IS_PREVIEW_VISIBLE          = false;
            model.COMPONENT[type].IS_ACTIVE         = true;
            model.PROPERTY.ACTIVE_ELEMENT_REFERENCE = type;
        };


        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        model.$onInit = function () {

            PubSub.api.subscribe(model.SUBSCRIBE.finish, function() {

                model.STATE.IS_PREVIEW_VISIBLE = true;
                model.COMPONENT[model.PROPERTY.ACTIVE_ELEMENT_REFERENCE].IS_ACTIVE = false;
            });
        };

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component destroy
         */
        model.$onDestroy = function () {
            PubSub.api.unsubscribe(model.SUBSCRIBE.finish);
        };
    };


    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['PubSub', 'Translation'];

    /**
     *
     */
    var Component = {
        templateUrl     : './ui-block.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.visual').component('uiBlock', Component);

})();
