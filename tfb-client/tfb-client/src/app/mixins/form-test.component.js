(function () {

    /**
     *
     * @param {*} Translation
     * @param {*} PubSub
     * @param {*} Form
     */
    var Controller = function(Translation, PubSub, Form) {

            /**
             * @type
             * State provider
             *
             * @description
             * Preserve the component state insithe the component and component scope
             * it is eqvivalent of @scope in non component environment
             */
            var model = this;

            /**
             *
             */
            model.FIELD = null;


            /**
             * @type
             * Event Publish collection
             *
             * @description
             * The state collection contain every single state thro out the current component
             */
            var PUBLISH = {
                FORM_PROCESS_END            : PubSub.events.form.PROCESS.END,
                FORM_PROCESS_UPDATE_END     : PubSub.events.form.PROCESS.UPDATE_END,
                FORM_CHANGE                 : PubSub.events.form.CHANGE
            };

            /**
             * @type
             * Event Subscribe collection
             *
             * @description
             * The state collection contain every single state thro out the current component
             */
            var SUBSCRIBE = {
                FORM_PROCESS_INIT           : PubSub.events.form.PROCESS.INIT,
                FORM_PROCESS_UPDATE_INIT    : PubSub.events.form.PROCESS.UPDATE,
            };

            /**
             *
             */
            var ERROR_MESSAGES = {
                REQUIRED: 'Полето е задължително'
            };

            /**
             *
             */
            var customInputErrorsCollection = 0;


            /**
             *
             */
            model.on_change = function() {
                PubSub.api.publish(PUBLISH.FORM_CHANGE, { 'form_change_valid' : model.formContact.$valid });
            };

            /**
             * @type
             * Event Callback
             *
             * @description
             * Collect an object containing relevant form information and send it back to the
             * request isuer / user object
             */
            function on_process (name) {

                if (model.formId == name && !model.uiId) {

                    for (var index in model.FIELD) {
                        (model.FIELD[index].type == 'dropdown') ? _process_dropdown(index) : _process_input(index);
                    }

                    if (model.formContact.$valid && (customInputErrorsCollection == 0)) {
                        PubSub.api.publish(PUBLISH.FORM_PROCESS_END, Form.process([model.FIELD]));
                        Form.clear(model.FIELD);
                    }

                    customInputErrorsCollection = 0;
                }
            };

            /**
             * @type
             * Event Callback
             *
             * @description
             * Collect an object containing relevant form information and send it back to the
             * request isuer / user object
             */
            function on_update ($object) {

                if (model.name == $object.name && model.uiId == $object.id) {

                    for (var index in model.FIELD) {
                        (model.FIELD[index].type == 'dropdown') ? _process_dropdown(index) : _process_input(index);
                    }

                    if (model.formContact.$valid && (customInputErrorsCollection == 0)) {

                        var $c  = Form.process([model.FIELD]);
                        $c.id   = model.uiId;
                        PubSub.api.publish(PUBLISH.FORM_PROCESS_UPDATE_END, $c);
                    }

                    customInputErrorsCollection = 0;
                }
            };

            /**
             *
             * @param {*} index
             */
            function _process_dropdown(index) {

                model.FIELD[index].show_errors  = (model.FIELD[index].value == null);
                customInputErrorsCollection     += (model.FIELD[index].value == null) ? 1 : 0;
                model.FIELD[index].error        = ERROR_MESSAGES.REQUIRED;
            }

            /**
             *
             * @param {*} index
             */
            function _process_input(index) {

                model.FIELD[index].show_errors = model.formContact[index].$invalid;
                model.FIELD[index].error = ERROR_MESSAGES.REQUIRED;
            }


            /**
             * @type
             * Lifecycle event
             *
             * @description
             * This method is executed on component init
             */
            model.$onInit = function () {

                PubSub.api.subscribe(SUBSCRIBE.FORM_PROCESS_INIT, on_process);
                PubSub.api.subscribe(SUBSCRIBE.FORM_PROCESS_UPDATE_INIT, on_update);
            };

            /**
             * @type
             * Lifecycle event
             *
             * @description
             * This method is executed on component changes
             */
            model.$onChanges = function () {

                if (model.uiCollection) {

                    for (var index in model.uiCollection) {

                        if (model.FIELD[index]) {
                            model.FIELD[index].value = model.uiCollection[index];
                        }
                    }
                }
            }

            /**
             * @type
             * Lifecycle event
             *
             * @description
             * This method is executed on component destroy
             */
            model.$onDestroy = function () {
                PubSub.api.unsubscribe(SUBSCRIBE.FORM_PROCESS_INIT);
                PubSub.api.unsubscribe(SUBSCRIBE.FORM_PROCESS_UPDATE_INIT);
            };
    };


    /**
     *
     */
    var Component = {
        templateUrl     : './form-test.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        transclude      : true,

        bindings        : {
            formId      :"@"
        }
    };

    angular.module('application.components.visual').component('formTest', Component);

})();
