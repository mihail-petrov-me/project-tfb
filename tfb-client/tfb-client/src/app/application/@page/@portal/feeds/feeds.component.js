(function () {

    var Controller = function (Translation, PubSub, UserModel, EntityModel) {

        /**
         * @type
         * Event Publish collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        this.SUBSCRIBE = {
            GROUP_LIST_INIT     : PubSub.events.page.GROUP.LIST.INIT,
            GROUP_LIST_LOAD     : PubSub.events.page.GROUP.LIST.LOAD,
            GROUP_INFO_INIT     : PubSub.events.page.GROUP.INFO.INIT,
            GROUP_INFO_LOAD     : PubSub.events.page.GROUP.INFO.LOAD
        };

        this.STATE = {
            CAN_COLLECTION_LOAD_MORE : false
        };


        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en' : '@Feed',
                    'bg' : '@Актуално'
                })
            };
        });

        /**
         * @type
         * Request : FETCH : userfeed
         */
        this.request_FETCH_userfeed = () => {

            EntityModel.api.PAGINATE().fetch
            .userfeed(UserModel.profile.my('id'))
                .then(this.handler_FETCH_userfeed_success)
                .catch(this.handler_FETCH_userfeed_error);
        };

        /**
         * @type
         * Handler : FECH : SUCCESS
         */
        this.handler_FETCH_userfeed_success = ($response) => {

            this.collection                     = $response.data;
            this.STATE.CAN_COLLECTION_LOAD_MORE = $response.pagination.has_next;

            PubSub.api.publish('event:ui:after_load');
        };

        /**
         * @type
         * Handler : FECH : ERROR
         */
        this.handler_FETCH_userfeed_error = ($response) => {
            console.log("@Exception : User feed");
        };

        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {
            this.request_FETCH_userfeed();
        };

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        this.$onDestroy = function() {

            PubSub.api.unsubscribe('close_model_window');
            PubSub.api.unsubscribe(this.SUBSCRIBE.GROUP_LIST_INIT);
            PubSub.api.unsubscribe(this.SUBSCRIBE.GROUP_LIST_LOAD);
            PubSub.api.unsubscribe(this.SUBSCRIBE.GROUP_INFO_INIT);
            PubSub.api.unsubscribe(this.SUBSCRIBE.GROUP_INFO_LOAD);
        };
    };

    Controller.$inject  = ['Translation', 'PubSub', 'UserModel', 'EntityModel'];
    var Component       = {
        templateUrl     : './feeds.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('feeds', Component);
})();
