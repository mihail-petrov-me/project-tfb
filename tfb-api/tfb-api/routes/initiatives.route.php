<?php

// GET routes
$app->get('/initiatives'           , 'InitiativeController@index');
$app->get('/initiatives/{id}'      , 'InitiativeController@show');
$app->get('/initiatives/{id}/edit' , 'InitiativeController@edit');

// POST routes
$app->post('/initiatives'          , 'InitiativeController@store');

// PUT routes
$app->put('/initiatives/{id}'      , 'InitiativeController@update');

// DELETE routes
$app->delete('/initiatives/{id}'      , 'InitiativeController@delete');
