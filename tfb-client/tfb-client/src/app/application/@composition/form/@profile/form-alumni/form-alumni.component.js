(function () {

    var Controller = FormController(function(Translation, PubSub, Form, UserModel) {

        /**
         * @type
         * State provider
         */
        var model = this;

        /**
         *
         */
        model.name = "alumni";


        /**
         * @component
         * Fields collection
         */
        model.FIELD = {

            "cohort": {
                name            : "cohort",
                type            : 'dropdown',
                title           : 'Випуск',
                value           : null,
                isRequired      : true,
                show_errors     : false
            },

            "area"              : {
                name            : "area",
                type            : 'dropdown',
                title           : 'Област',

                provider        : function() {
                    return UserModel.api.COLLECTION().fetch.test();
                },
                value           : null,
                isRequired      : true,
                show_errors     : false
            },

            "municipality"      : {
                name            : "municipality",
                type            : 'dropdown',
                title           : 'Община',

                provider        : function($id) {
                    return UserModel.api.fetch.mest($id);
                },
                value           : null,
                isRequired      : true,
                show_errors     : false
            },

            "city"              : {
                name            : "city",
                type            : 'dropdown',
                title           : 'Община',

                provider        : function($id) {
                    return UserModel.api.fetch.best($id);
                },
                value           : null,
                isRequired      : true,
                show_errors     : false
            },


            "school": {
                name            : "school",
                type            : 'dropdown',
                title           : 'Училище',

                provider        : function($id) {
                    return UserModel.api.fetch.gest($id);
                },
                value           : null,
                isRequired      : true,
                show_errors     : false
            },

            "subject": {
                name            : "subject",
                type            : 'text',
                value           : null,
                isRequired      : true,
                show_errors     : false
            }
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(function($provider) {

            model.LABEL = {

                OPTIONS_COHORT : $provider({
                    "bg"    : [
                        { 'id': 2011, 'title': '2011'    },
                        { 'id': 2012, 'title': '2012'    },
                        { 'id': 2013, 'title': '2013'    },
                        { 'id': 2014, 'title': '2014'    },
                        { 'id': 2015, 'title': '2015'    },
                        { 'id': 2016, 'title': '2016'    },
                        { 'id': 2017, 'title': '2017'    },
                        { 'id': 2018, 'title': '2018'    }
                    ],
                    "en"    : [
                        { 'id': 2011, 'title': '2011'    },
                        { 'id': 2012, 'title': '2012'    },
                        { 'id': 2013, 'title': '2013'    },
                        { 'id': 2014, 'title': '2014'    },
                        { 'id': 2015, 'title': '2015'    },
                        { 'id': 2016, 'title': '2016'    },
                        { 'id': 2017, 'title': '2017'    },
                        { 'id': 2018, 'title': '2018'    }
                    ]
                }),

                OPTIONS_COHORT_PLACEHOLDER : $provider({
                    "bg"    : "Випуск",
                    "en"    : "Cohort"
                }),

                OPTIONS_AREA_PLACEHOLDER : $provider({
                    "bg"    : "Област",
                    "en"    : "Area"
                }),

                OPTIONS_MUNICIPALITY_PLACEHOLDER : $provider({
                    "bg"    : "Община",
                    "en"    : "Municipality"
                }),

                OPTIONS_CITY_PLACEHOLDER : $provider({
                    "bg"    : "Град",
                    "en"    : "E-mail"
                }),

                OPTIONS_SCHOOL_PLACEHOLDER : $provider({
                    "bg"    : "Училище",
                    "en"    : "Mobile number"
                }),

                INPUT_SUBJECT : $provider({
                    "bg"    : "Предмет",
                    "en"    : "Birth date"
                })
            };
        });


    });




    /**
     * @type
     * Component injector
     */
    // Controller.$inject = ['Translation', 'PubSub', 'Form'];

    /**
     *
     */
    var Component = {
        templateUrl     : './form-alumni.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            uiCollection    : '<',
            uiIsDisabled    : '<'
        }
    };

    /**
     *
     */
    angular.module('application.components.page').component('formAlumni', Component);

})();
