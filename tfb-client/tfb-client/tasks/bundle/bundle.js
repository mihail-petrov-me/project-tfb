// System modules
// # =============================================
var fs  = require('fs.extra');
var EOL = require('os').EOL;

// Core components
// # =============================================
var buildCollection = require('../helpers/build');

// get application referenece pointer
function replace($fileReference, $origin) {

    var reference = fs.readFileSync($fileReference).toString();
    var referenceCollection = reference.match(/(templateUrl) *: *'([a-z\.\/\-]+)'/);

    if(referenceCollection) {

        var $templateReferencePath = ($origin + referenceCollection[2]);
        var $template = fs.readFileSync($templateReferencePath).toString()
            .replace(/(\r\n|\n|\r)/gm, "").replace(/\s\s+/g, ' ').replace(/"/g, '\\"').replace(/'/g, "\\'");

        return reference.replace(referenceCollection[1], 'template').replace(referenceCollection[1], 'template').replace(referenceCollection[2], $template);
    }

    return reference;
}



// API : Interface
// # =============================================
exports.exec = function(options) {

    buildCollection.build(options.from, options.pipe, function(collection) {

        for(var i = 0; i < collection.length; i++) {

            var $reference = replace((options.from + collection[i]), options.from);
            fs.appendFileSync(options.to + 'application.js', $reference);
        }
    });
};
