(function () {

    /**
     *
     * @param {*} UserModel
     * @param {*} Translation
     */
    var Controller = function ($timeout, Translation, PubSub, UserModel, ClassModel) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;


        model.labels = [
            "1-4",
            "5-8",
            "9-12"
        ];

        model.colors = [
            "#ee627d",
            "#54ca95",
            "#ffbf46"

        ];

        model.data = [
            [16, 35, 100],
            [1, 25, 66],
            [5, 15, 25]
        ];
    };

    Controller.$inject = ['$timeout','Translation', 'PubSub', 'UserModel', 'ClassModel'];
    var Component = {
        templateUrl     : './class-chart-avg.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('classChartAvg', Component);

})();
