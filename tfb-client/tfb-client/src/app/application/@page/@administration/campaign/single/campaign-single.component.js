(function () {

    var Controller = function (Translation, UserModel, PubSub, CampaignModel) {

        this.collection = [];

        this.itemSelected = null;

        this.campaignId = null;


        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en' : '@Education campaign',
                    'bg' : '@Образователна кампания'
                })
            };
        });

        /**
         * @type
         * Event
         */
        this.on_close = () => {
            this.itemSelected = null;
        }

        /**
         * @type
         * Expression
         */
        this.when_state_selected = (element) => {
            return (this.itemSelected == null) || (this.itemSelected== element.id);
        };

        /**
         * @type
         * Expression
         */
        this.when_state_init = () => {
            return this.itemSelected != null;
        };

        /**
         * @type
         * Request : FETCH : userfeed
         */
        this.request_FETCH_teachers = () => {

            UserModel.api.PAGINATE().fetch
            .teachers()
                .then(this.handler_FETCH_userfeed_success)
                .catch(this.handler_FETCH_userfeed_error);
        };

        /**
         * @type
         * Handler : FECH : SUCCESS
         */
        this.handler_FETCH_userfeed_success = ($response) => {
            this.collection = $response.data;
        };

        /**
         * @type
         * Handler : FECH : ERROR
         */
        this.handler_FETCH_userfeed_error = ($response) => {
            console.log("@Exception: Cannot fetch current active teachers");
        };


        /**
         * @type
         * Request : FETCH : userfeed
         */
        this.request_ASSIGN_teacherSchool = ($id, $collection) => {

            CampaignModel.api.create.teacher($id, $collection)
                .then(this.handle_ASSIGN_teacherSchool_success)
                .catch(this.handle_ASSIGN_teacherSchool_error);
        };

        /**
         * @type
         * Handler : FECH : SUCCESS
         */
        this.handle_ASSIGN_teacherSchool_success = () => {

        };

        /**
         * @type
         * Handler : FECH : ERROR
         */
        this.handle_ASSIGN_teacherSchool_error = () => {
            console.log("@Exception: Cannot assigne teacher to school");
        };


        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {

            this.request_FETCH_teachers();


            PubSub.api.subscribe('LIST_ITEM_SELECTED', ($response) => {
                if($response.item_slug == 'card-assign-teacher') {

                    if($response.item_command == 'open') {
                        this.itemSelected = $response.item_id;
                    }
                }
            });


            PubSub.api.subscribe('LIST_ITEM_SELECTED', ($response) => {
                if($response.item_slug == 'card-assign-teacher') {

                    if($response.item_command == 'close') {
                        this.itemSelected = null;
                    }
                }
            });



            PubSub.form.end('FORM_ASSIGNE_TEACHER_SCHOOL', ($response) => {

                console.log($response);

                // $response['teacher_id'] = this.itemSelected;
                // this.request_ASSIGN_teacherSchool(this.campaignId, $response);
            });
        };

        /**
         * @type
         * Router event
         */
        this.$routerOnActivate = function (next, previous) {
            this.campaignId = next.params.id;
        };
    };

    Controller.$inject  = ['Translation', 'UserModel', 'PubSub', 'CampaignModel'];
    var Component       = {
        templateUrl     : './campaign-single.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('campaignSingle', Component);
})();
