<?php

use \App\Http\Models\Subject\Subject;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectTable extends Migration
{
    private $table = Subject::TABLE;

    public function up()
    {
        Schema::create($this->table, function(Blueprint $builder) {

            //# Primary Key
            $builder->char(Subject::ID, 13)->primary();

            //# Table Entity Collection
            $builder->string(Subject::TEACHER_ID, 100);
            $builder->string(Subject::BUNDLE_ID, 100);
            $builder->string(Subject::TITLE, 100);

            //# Meta
            $builder->timestamps();
            $builder->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop($this->table);
    }
}
