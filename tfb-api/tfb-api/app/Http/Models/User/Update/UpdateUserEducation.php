<?php
namespace App\Http\Models\User\Update;
use App\Http\Models\Repository;
use App\Http\Models\User\UserEducation;
use DB;

class UpdateUserEducation extends Repository
{
    public function execute($id, $collection)
    {
        return DB::table(UserEducation::TABLE)
            ->where(array(
                    'user_id'   => $id,
                    'id'        => $collection['id']
                )
            )
            ->update(array(
                UserEducation::INSTITUTION    => $this->process($collection, UserEducation::INSTITUTION),
                UserEducation::DATE_START     => $this->process($collection, UserEducation::DATE_START),
                UserEducation::DATE_END       => $this->process($collection, UserEducation::DATE_END),
                UserEducation::DEGREE         => $this->process($collection, UserEducation::DEGREE),
                UserEducation::FIELD_OF_STUDY => $this->process($collection, UserEducation::FIELD_OF_STUDY)
        ));          
    }
}