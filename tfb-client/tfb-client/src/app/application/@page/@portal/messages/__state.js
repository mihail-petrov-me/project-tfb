/**
 * @type
 * State provider
 *
 * @description
 * Preserve the component state insithe the component and component scope
 * it is eqvivalent of @scope in non component environment
 */
var model = this;


/**
 * @type
 * Event Publish collection
 *
 * @description
 * The state collection contain every single state thro out the current component
 */
model.PUBLISH = {
    PAGE_LOAD           : PubSub.events.page.LOAD
};

/**
 * @type
 * Event Subscribe collection
 *
 * @description
 * The state collection contain every single state thro out the current component
 */
model.SUBSCRIBE = {

};
