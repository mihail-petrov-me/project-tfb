(function () {

    var Component = {
        templateUrl     : './welcome-outro.template.html',
        controllerAs    : 'model'
    };

    angular.module('application.components.visual').component('welcomeOutro', Component);

})();
