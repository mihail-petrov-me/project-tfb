(function () {

    var Controller = function(Translation, PubSub) {

        this.name = "FORM_ADD_SUBJECT";

        /**
         * @component
         * Fields collection
         */
        this.FIELD = {

            "new_subject" : {
                name            : "new_subject",
                type            : "text",
                value           : null,
                show_errors     : false,
                isRequired      : true,
            }
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                INPUT_NEW_SUBJECT : $provider({
                    "bg" : "Добави предмет",
                    "en" : "Add subject",
                })
            };
        });

        /**
         *
         */
        this.alabala = () => {
            PubSub.form.init("FORM_ADD_SUBJECT");
        };

        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {

            this.parent.FIELD  = this.FIELD;
            this.formContact   = this.parent.formContact;
        }
    };

    /**
     * @type
     * Component injector
     */
    Controller.$inject = ['Translation', 'PubSub'];
    let Component = {
        templateUrl     : './form-add-subject.template.html',
        controller      : Controller,
        controllerAs    : 'model',

        require             : {
            parent          : '^formWrapper'
        },

        bindings        : {
            uiCollection    : '<',
            uiIsDisabled    : '<',
            uiPresentation  : '<',
            uiId            : '<'
        }
    };

    angular.module('application.components.page').component('formAddSubject', Component);
})();
