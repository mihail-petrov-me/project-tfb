(function () {

    var Controller = function (Translation, GroupModel, UserModel, PubSub) {

        /**
         * @type
         * State collection
         */
        this.STATE = {
            IS_ACTION_LOADING : false
        };


        /**
         * @type
         * Event Publish collection
         */
        this.PUBLISH = {

            // #LOCAL CUSTOM EVENTS
            //component specific events
            GROUP_CREATE_INIT       : PubSub.events.page.GROUP.CREATE.INIT,
            GROUP_CREATE_DESTROY    : PubSub.events.page.GROUP.CREATE.DESTROY,

            // #PAGE CUSTOM EVENTS
            // global component events
            PAGE_LOAD               : PubSub.events.page.LOAD,
            FORM_PROCESS_INIT       : PubSub.events.form.PROCESS.INIT,

            // #COMPONENT CUSTOM EVENTS
            // global component events
            UI_SIDEBAR_DISABLE      : PubSub.events.component.UISIDEBAR.STATE.DISABLE,
        };

        /**
         * @type
         * Event Subscribe collection
         */
        this.SUBSCRIBE = {
            FORM_PROCESS_END: PubSub.events.form.PROCESS.END
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                PAGE_TITLE: $provider({
                    'en': 'Create New Group',
                    'bg': 'Създай нова група'
                }),

                BUTTON_CREATE: $provider({
                    'en': 'Create New Group',
                    'bg': 'Нова група'
                })
            };
        });

        /**
         * @type
         * Event : on
         */
        this.on_process = () => {
            this.STATE.IS_ACTION_LOADING  = true;
            PubSub.form.init('FORM_CREATE_GROUP');
        };


        /**
         * @type
         * Handler : Success
         */
        this.end_process = ($collection) => {

            var $requestBody = angular.extend($collection, {
                'user_id' : UserModel.profile.my('id')
            });

            GroupModel.api.create
                .group($requestBody)
                    .then(this.handler_process_success)
                    .catch(this.handler_process_error);
        };


        /**
         * @type
         * Handler : Success
         */
        this.handler_process_success = ($response) => {

            // Save new group subscribtion
            UserModel.profile.groups($response.data);

            // Redirect to the newgroup page
            this.STATE.IS_ACTION_LOADING = false;
            window.location.href = "#!/dashboard/groups/" + $response.data.id;

            // Close the window
            this.on_close();
        };

        /**
         * @type
         * Handler : Error
         */
        this.handler_process_error = ($response) => {
            this.STATE.IS_ACTION_LOADING = false;
        };

        /**
         * @type
         * Event
         */
        this.on_close = () => {
            PubSub.api.publish('close_model_window');
        };


        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {

            /**
             * @type
             * Publisher
             */
            PubSub.api.publish(this.PUBLISH.UI_SIDEBAR_DISABLE);

            /**
             * @type
             * Publisher
             */
            PubSub.api.publish(this.PUBLISH.GROUP_CREATE_INIT);

            /**
             * @type
             * Subscriber
             */
            PubSub.form.end('FORM_CREATE_GROUP', ($response) => {
                this.end_process($response);
            });
        };

        /**
         * @type
         * Lifecycle event
         */
        this.$onDestroy = () => {

            PubSub.api.publish(this.PUBLISH.GROUP_CREATE_DESTROY);
            PubSub.api.unsubscribe(this.SUBSCRIBE.FORM_PROCESS_END);
            PubSub.form.destroy();
        };

        /**
         * @type
         * Lifecycle event
         */
        this.$postLink = () => {
            PubSub.api.publish(this.PUBLISH.PAGE_LOAD);
        };
    };

    /**
     * @type
     * Component injector
     */
    Controller.$inject = ['Translation', 'GroupModel', 'UserModel', 'PubSub'];
    var Component = {
        templateUrl     : './group-create.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('groupModify', Component);

})();
