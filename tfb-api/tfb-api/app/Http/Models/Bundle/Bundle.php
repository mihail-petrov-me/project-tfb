<?php
namespace App\Http\Models\Bundle;

use App\Http\Models\Bundle\Create\CreateBundle;
use App\Http\Models\Bundle\Create\CreateEvent;
use App\Http\Models\Bundle\Create\CreateGoal;
use App\Http\Models\Bundle\Create\CreateStudent;
use App\Http\Models\Bundle\Create\CreateStudentNote;
use App\Http\Models\Bundle\Create\CreateSubject;

use App\Http\Models\Bundle\Create\CreateTeacherSchool;
use App\Http\Models\Bundle\Delete\DeleteGoal;
use App\Http\Models\Bundle\Delete\DeleteStudent;
use App\Http\Models\Bundle\Fetch\FetchBundleSingle;
use App\Http\Models\Bundle\Fetch\FetchBundleSpecialCollection;
use App\Http\Models\Bundle\Fetch\FetchGoalCollection;
use App\Http\Models\Bundle\Fetch\FetchPrimaryGoals;
use App\Http\Models\Bundle\Fetch\FetchStudentCollection;
use App\Http\Models\Bundle\Fetch\FetchTeacher;
use App\Http\Models\Bundle\Fetch\FetchBundleCollection;
use App\Http\Models\Bundle\Fetch\FetchTeacherEventCollection;
use App\Http\Models\Bundle\Fetch\FetchTeacherSubjectCollection;

use App\Http\Models\Bundle\Delete\DeleteBundle;
use App\Http\Models\Bundle\Delete\DeleteTeacherEventCollection;
use App\Http\Models\Bundle\Delete\DeleteTeacherSubjectCollection;

use App\Http\Models\Bundle\Update\UpdateBundle;
use App\Http\Models\Bundle\Update\AssignTeacherBundle;
use App\Http\Models\Bundle\Update\UpdateGoal;
use App\Http\Models\Bundle\Update\UpdateStudent;
use App\Http\Models\Bundle\Update\UpdateTeacherEventCollection;
use App\Http\Models\Bundle\Update\UpdateTeacherSubjectCollection;
use Illuminate\Database\QueryException;

/**
 * Class Bundle
 * @package App\Http\Models\Bundle
 */
class Bundle
{
    // Public keys
    const ID                = 'bundle_id';
    const TFB_ID            = 'tfb_bundle_id';
    const INIT_YEAR         = 'init_year';
    const LABEL             = 'label';
    const POSITION          = 'position';
    const SIGNATURE         = 'signature';

    // Foreign keys
    const SCHOOL_ID         = 'school_id';

    // Public tables
    const TABLE             = 'bundles';
    const TABLE_TEACHERS    = 'bundle__teacher';
    const TABLE_GOALS       = 'bundle__goal';

    /**
     * @param $query
     * @return mixed
     */
    public function collection($query)
    {
        return (new FetchBundleCollection())->execute($query);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function collectionSpecial($query)
    {
        return (new FetchBundleSpecialCollection())->execute($query);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function single($id)
    {
        return (new FetchBundleSingle())->execute($id);
    }

    /**
     * @param $collection
     * @return array
     */
    public function create($collection)
    {
        return (new CreateBundle())->execute($collection);
    }


    public function update($id, $collection)
    {
        return (new UpdateBundle())->execute($id, $collection);
    }

    public function add($id, $collection)
    {
        return (new AssignTeacherBundle())->execute($id, $collection);
    }

    public function delete($id)
    {
        return (new DeleteBundle())->execute($id);
    }


    public function create_goal($id, $collection)
    {
            return (new CreateGoal())->execute($id, $collection);
    }


    /**
     * @param $id
     * @return mixed
     */
    public function fetch_teacher_bundles($id)
    {
        return (new FetchTeacherBundleCollection())->execute($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function fetch_teacher_subjects($id)
    {
        return (new FetchTeacherSubjectCollection())->execute($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function fetch_teacher_events($id)
    {
        return (new FetchTeacherEventCollection())->execute($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function fetch_teacher($id)
    {
        return (new FetchTeacher())->execute($id);
    }


    public function fetch_bundle_single($id, $teacherId)
    {
        return (new FetchBundleSingle())->execute($id, $teacherId);
    }

    public function fetch_primary_goals()
    {
        return (new FetchPrimaryGoals())->execute();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function collectionStudent($id)
    {
        return (new FetchStudentCollection())->execute($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function collectionGoal($id)
    {
        return (new FetchGoalCollection())->execute($id);
    }

    /**
     * @param $id
     * @param $collection
     * @return array
     */
    public function create_bundle($id, $collection)
    {
        return (new CreateBundle())->execute($id, $collection);
    }

    /**
     * @param $id
     * @param $collection
     */
    public function create_subject($id, $collection)
    {
        return (new CreateSubject())->execute($id, $collection);
    }

    /**
     * @param $id
     * @param $collection
     */
    public function create_event($id, $collection)
    {
        return (new CreateEvent())->execute($id, $collection);
    }

    /**
     * @param $id
     * @param $collection
     * @return array
     */
    public function create_teacher_school($id, $collection)
    {
        return (new CreateTeacherSchool())->execute($id, $collection);
    }

    /**
     * @param $id
     * @param $collection
     * @return array
     */
    public function create_student($id, $collection)
    {
        return (new CreateStudent())->execute($id, $collection);
    }


    /**
     * @param $bundleId
     * @param $goalId
     * @param $collection
     * @return mixed
     * @internal param $id
     */
    public function update_goal($bundleId, $goalId, $collection)
    {
        return (new UpdateGoal())->execute($bundleId, $goalId, $collection);
    }

    /**
     * @param $bundleId
     * @param $studentId
     * @param $collection
     * @return mixed
     * @internal param $goalId
     * @internal param $id
     */
    public function update_student($bundleId, $studentId, $collection)
    {
        return (new UpdateStudent())->execute($bundleId, $studentId, $collection);
    }

    public function create_goals($collection)
    {
        return (new CreateSubject())->execute($collection);
    }

    public function create_note($teacherId, $studentId, $collection)
    {
        return (new CreateStudentNote())->execute($teacherId, $studentId, $collection);
    }

    /**
     * @param $id
     * @param $collection
     * @return mixed
     */
    public function update_bundle($id, $collection)
    {
        return (new UpdateTeacherBundleCollection())->execute($id, $collection);
    }

    public function update_subject($id, $collection)
    {
        return (new UpdateTeacherSubjectCollection())->execute($id, $collection);
    }

    /**
     * @param $id
     * @param $collection
     * @return mixed
     */
    public function update_event($id, $collection)
    {
        return (new UpdateTeacherEventCollection())->execute($id, $collection);
    }



    /**
     * @param $id
     * @return mixed
     */
    public function delete_bundle($id)
    {
        return (new DeleteTeacherBundleCollection())->execute($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete_subject($id)
    {
        return (new DeleteTeacherSubjectCollection())->execute($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete_event($id)
    {
        return (new DeleteTeacherEventCollection())->execute($id);
    }


    /**
     * @param $bundleId
     * @param $studentId
     * @return mixed
     */
    public function deleteStudent($bundleId, $studentId)
    {
        return (new DeleteStudent())->execute($bundleId, $studentId);
    }

    /**
     * @param $bundleId
     * @param $goalId
     * @return mixed
     * @internal param $studentId
     */
    public function deleteGoal($bundleId, $goalId)
    {
        return (new DeleteGoal())->execute($bundleId, $goalId);
    }
}