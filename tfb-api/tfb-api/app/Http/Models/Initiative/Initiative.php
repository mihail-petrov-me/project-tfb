<?php
namespace App\Http\Models\Initiative;

use App\Http\Models\Model;
use DB;
use \Carbon\Carbon;

class Initiative extends Model
{
    // Public fields
    const ID            = "initiative_id";
    const TITLE         = "initiative_title";
    const DESCRIPTION   = "initiative_description";
    const PICTURE       = "initiative_picture";

    // Meta fields
    const CREATED_AT    = "created_at";
    const UPDATED_AT    = "updated_at";
    const DELETED_AT    = "deleted_at";

    // Tables
    const TABLE         = "initiatives";

    /**
     * 
     * @param type $collection
     * @return type
     */
    public function create($collection) 
    {
       return DB::table($this->table)->insert(array(
            'title'         => $collection['initiative_title'],
            'description'   => $collection['initiative_description'],
       ));
    }
    
    /**
     * 
     * @param type $limit
     * @return type
     */
    public function get($id, $limit = null) 
    {
        return DB::table($this->table)->where('id', $id)->limit($limit)->get();
    }
    
    /**
     * 
     * @param type $limit
     * @return type
     */
    public function all($limit = null) 
    {
        return DB::table($this->table)->limit($limit)->get();
    }
    
    /**
     * 
     * @param type $id
     * @param type $collection
     */
    public function update($id, $collection)
    {
        return DB::table($this->table)->where('id', $id)->update(array(
            'fname'     => $collection['personal_information']['fname'],
            'lname'     => $collection['personal_information']['lname'],
            'email'     => $collection['personal_information']['email']
        ));
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function delete($id)
    {
        return DB::table($this->table)->where('id', $id)->update(array(
            'deleted_at'  => Carbon::now()
        ));
    }
}
