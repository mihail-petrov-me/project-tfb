(function () {


    /**
     *
     * @param {*} PubSub
     * @param {*} UserModel
     */
    var Controller = function($timeout, Translation, PubSub, UserModel) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;



        model.formCollection = [];



        model.$reference        = null;
        model.$referenceIndex   = null;


        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(function($provider) {

            model.LABEL = {

                BUTTON_ACTION : $provider({
                    'en' : 'Update',
                    'bg' : 'Актуализирай'
                })
            };
        });

        model.on_edit_state = function($id) {
            return (model.$reference == $id);
        }

        model.on_edit = function($id) {
            model.$reference = $id;
        };


        model.on_update = function($element, $index) {

            model.$referenceIndex = $index;

            PubSub.api.publish(PubSub.events.form.PROCESS.UPDATE, {
                name    : "employment",
                id      : $element.id
            });
        };

        model.on_cancel = function() {
            model.$reference = null;
        }


        model.$onInit = function() {

            PubSub.api.subscribe(PubSub.events.form.PROCESS.UPDATE_END, function($response) {

                UserModel.api.update
                    .employment(UserModel.profile.my("user_id"), $response)
                        .then(function(){
                            model.$reference = null;
                            model.uiCollection[model.$referenceIndex] = $response;
                        })
                        .catch(function() {
                            console.log("Error update : employment");
                        });
            });
        }


        /**
         *
         */
        model.$onDestroy = function() {
            PubSub.api.unsubscribe(PubSub.events.form.PROCESS.END);
        }
    };

    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['$timeout','Translation','PubSub', 'UserModel'];

    /**
     *
     */
    var Component = {
        templateUrl     : './form-employment-list.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            uiCollection : '<'
        }
    };

    /**
     *
     */
    angular.module('application.components.page').component('formEmploymentList', Component);

})();
