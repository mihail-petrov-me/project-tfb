<?php

namespace App\Http\Models\Entity\Feed;
use DB;

class FetchFeedCollection
{
    public function execute($limit, $offset)
    {    
        return DB::raw("(SELECT * FROM collection_entity ORDER BY created_at DESC LIMIT {$limit} OFFSET {$offset})  collection_entity");
    }
}