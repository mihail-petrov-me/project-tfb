var fs        = require('fs.extra');

exports.exec = function(from, to, moduleCollection) {


  fs.mkdirSync(to + 'fonts');

  var dest = to + 'fonts/';

  for(var i = 0; i < moduleCollection.length; i++) {

    var moduleReference =  moduleCollection[i];

    if(moduleReference.path) {

      var source = (moduleReference.path);
      var destination = (dest + moduleReference.script);

      fs.copy(source, destination, function(error) {
        if(error) console.log('Error ocure durring the copy proecess');
      });
    }
  }
};
