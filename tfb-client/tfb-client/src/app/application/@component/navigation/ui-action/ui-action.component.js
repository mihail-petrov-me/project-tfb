(function () {

    var Component = {
        templateUrl     : './ui-action.template.html',
        controllerAs    : 'model',
        bindings : {
            uiClick : '&',
            title   : '<',
            uiIcon  : '@',
        }
    };

    angular.module('application.components.visual').component('uiAction', Component);
})();
