<?php

namespace App\Utils;

class Util
{
    public static function signature($name)
    {
        $nameCollection  = explode(' ', $name);
        return mb_substr($nameCollection[0], 0, 1) . mb_substr($nameCollection[1], 0, 1);
    }
}