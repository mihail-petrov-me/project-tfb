(function () {

    var Controller = function (PubSub, $element) {

        this.STATE = {
            IS_EXPANDED         : false,
            IS_TITLE_VISIBLE    : false
        }

        /**
         * @type
         * Event Publish collection
         */
        this.PUBLISH = {
            SELECT_AFTER : 'event:ui:dropdown:after_select'
        };

        /**
         * @type
         * State
         */
        this.uiIsActive = null;


        /**
         *
         */
        this.uiActiveElement = null;

        /**
         * @type
         * Еvent
         */
        this.on_expand =  () => {
            this.STATE.IS_EXPANDED = !this.STATE.IS_EXPANDED;
        };

        /**
         * @type
         * Event
         */
        this.on_select = (element, $event) => {

            this.STATE.IS_TITLE_VISIBLE    = true;

            // Set component element
            this.STATE.IS_EXPANDED = false;
            this.uiTitle           = element.title;
            this.uiIsActive        = element.id;
            this.uiModel.value     = element.id;

            var collection = {};
            if(this.uiId) {
                collection.id = this.uiId;
            }

            collection.element = element;


            // Emit event to the world
            // #
            PubSub.api.publish(this.PUBLISH.SELECT_AFTER, collection);


            // Remove errors
            this.uiModel.show_errors = false;
        };


        /**
         * @type
         * Application inner function
         *
         */
        this.setActiveCursor = () => {

            for(var i = 0; i < this.uiOptions.length; i++) {

                if (this.uiOptions[i].id == this.uiActive) {
                    this.on_select(this.uiOptions[i]);
                    break;
                }
            }
        };

        /**
         * @type
         * Event
         */
        this.on_activate = function() {

            if(this.uiOptions) {

                angular.element(".ui-dropdown__title").addClass('ui-dropdown--active');

                for(var i = 0; i < this.uiOptions.length; i++) {

                    if (this.uiOptions[i].id == this.uiModel.value) {
                        this.on_select(this.uiOptions[i]);
                        break;
                    }
                }
            }
        };

        /**
         * @type
         * Request : FETCH
         */
        this.request_FETCH_providerData = ($dependancy) => {

            this.uiProvider($dependancy)
                .then(this.handle_FETCH_providerData_success)
                .catch(this.handle_FETCH_providerData_error);
        };


        /**
         *
         */
        this.transform = ($transformer, object) => {

            var o = {};

            for(var index in $transformer) {
                o[index] = object[$transformer[index]];
            }
            return o;
        }

        /**
         * @type
         * Handler : Success
         */
        this.handle_FETCH_providerData_success = ($response) => {

            if(this.uiReducer) {

                var $collection = [];
                for(var element in $response.data) {
                    $collection.push(this.transform(this.uiReducer, $response.data[element]));
                }

                this.uiOptions     = $collection;
                this.uiIsVisible   = true;
            }
            else {
                this.uiOptions     = $response.data;
                this.uiIsVisible   = true;
            }
        };

        /**
         * @type
         * Handler : Error
         */
        this.handle_FETCH_providerData_error = ($response) => {
            console.log("@Exception : service provider with dependancy");
        };


        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {

            if(!this.uiTitle) {
                this.uiTitle = this.uiModel.title;
                this.uiTitle = this.uiPlaceholder;
            }

            this.on_activate();
        };

        /**
         * @type
         * Lifecycle event
         */
        this.$onChanges = () => {


            if(this.uiProvider) {

                if(this.uiHasDependancy && this.uiDependOn) {
                    this.request_FETCH_providerData(this.uiDependOn);
                }

                if(!this.uiHasDependancy) {
                    this.request_FETCH_providerData();
                }
            }
        };

    };

    /**
     *
     */
    var Component = {
        templateUrl     : './ui-input-dropdown.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings: {
            uiTitle         : '<',
            uiOptions       : '<',
            uiIsActive      : '<',
            uiModel         : '<',
            uiActive        : '<',
            uiPlaceholder   : '<',
            uiId            : '<',
            uiIsDisabled    : '<',
            uiIsVisible     : '<',


            uiProvider      : '<',
            uiDependOn      : '<',

            uiHasDependancy : '<',
            uiReducer       : '<'
        }
    };

    /**
     *
     */
    angular.module('application.components.visual').component('uiInputDropdown', Component);

})();
