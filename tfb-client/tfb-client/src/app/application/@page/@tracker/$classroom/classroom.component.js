(function () {

    /**
     *
     * @param {*} UserModel
     * @param {*} Translation
     * @param {*} State
     * @param {*} PubSub
     */
    var Controller = function (UserModel, Translation, State, PubSub, CampaignModel) {

        /**
         * @type
         * EVENT : request
         */
        this.request_FETCH_user = () => {

            let $requestQuery = {
                'teacher_id': UserModel.profile.my("user_id")
            };

            CampaignModel.api.COLLECTION().fetch
                .campaignCollection($requestQuery)
                .then(this.handler_experiance_success)
                .catch(this.handler_experiance_error);
        };

        /**
         * @type
         * HANDLER :  success
         */
        this.handler_experiance_success = ($response) => {
            UserModel.state("tracker", JSON.stringify($response.data[0]));
        };

        /**
         * @type
         * HANDLER :  error
         */
        this.handler_experiance_error = () => {
            console.log("@Exception: When we try to get user experiance");
        };

        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => { this.request_FETCH_user() };
    };

    var ComponentRouter = [
        { path: '/dashboard',  component: 'classroomDashboard',   as: 'ClassroomDashboard', useAsDefault: true  },
        { path: '/...'      ,  component: 'classroomSingle',      as: 'ClassroomSingle'                    },
    ];

    var Component       = {
        templateUrl     : './classroom.template.html',
        controllerAs    : 'model',
        controller      : Controller,
        $routeConfig    : ComponentRouter,
        bindings        : { $router     : '<' }
    };

    angular.module('application.components.page').component('classroom', Component);
})();
