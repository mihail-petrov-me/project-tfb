<?php

namespace App\Http\Models\Project\Create;

use DB;
use App\Http\Models\Project\ProjectGroup;
use App\Http\Models\Project\ProjectTask;
use App\Http\Models\Project\Project;
use App\Http\Models\Repository;

/**
 * Class AddMemberToProject
 * @package App\Http\Models\Project\Create
 */
class AddTaskToProject extends Repository
{
    /**
     * @param $id
     * @param $collection
     *
     * @return array
     */
    public function execute(string $id, array $collection): array
    {
        $entity = $this->build($id, $collection);
        DB::table(Project::TABLE_TASK)->insert($entity);

        return $entity;
    }

    /**
     * @param string $id
     * @param array $collection
     *
     * @return array
     */
    public function build(string $id, array $collection): array
    {
        return [
            ProjectGroup::ID            => $id,
            ProjectTask::ID             => uniqid(),

            ProjectTask::TITLE          => $this->process($collection, ProjectTask::TITLE),
            ProjectTask::DESCRIPTION    => $this->process($collection, ProjectTask::DESCRIPTION),
            ProjectTask::PARENT         => $this->process($collection, ProjectTask::PARENT),
            ProjectTask::IS_COMPLETED   => $this->process($collection, ProjectTask::IS_COMPLETED, 0),
            ProjectTask::USERS          => $this->process($collection, ProjectTask::USERS),
        ];
    }
}