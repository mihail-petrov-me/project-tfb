<?php
namespace App\Http\Models\Goal;

use App\Http\Models\Goal\Create\CreateSubject;

use App\Http\Models\Goal\Fetch\FetchGoalSingle;
use App\Http\Models\Goal\Fetch\FetchSubjectCollection;

use App\Http\Models\Goal\Delete\DeleteSubject;
use App\Http\Models\Goal\Update\UpdateSubject;

class Goal
{
    // Public fields
    const ID            = "goal_id";

    const BUNDLE_ID     = "bundle_id";
    const TEACHER_ID    = "teacher_id";
    const SUBJECT_ID    = "subject_id";

    const PARENT_ID     = "parent_id";
    const TITLE         = "goal_title";
    const RIGAR         = "goal_rigar";
    const TYPE          = "goal_type";

    // Meta fields
    const CREATED_AT    = "created_at";
    const UPDATED_AT    = "updated_at";
    const DELETED_AT    = "deleted_at";

    // Tables
    const TABLE         = "goals";

    /**
     * @param $query
     * @return mixed
     */
    public function collection($query)
    {
        return (new FetchSubjectCollection())->execute($query);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function single($id)
    {
        return (new FetchGoalSingle())->execute($id);
    }

    /**
     * @param $collection
     * @return array
     */
    public function create($collection)
    {
        return (new CreateSubject())->execute($collection);
    }

    /**
     * @param $id
     * @param $collection
     * @return mixed
     */
    public function update($id, $collection)
    {
        return (new UpdateSubject())->execute($id, $collection);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return (new DeleteSubject())->execute($id);
    }
}