<?php
namespace App\Http\Models\User\Fetch\Single;

use App\Http\Models\Repository;
use App\Http\Models\User\User;
use App\Http\Models\User\UserEmployment;
use DB;

/**
 * Class FetchUserEmployment
 * @package App\Http\Models\User\Fetch\Single
 */
class FetchUserEmployment extends Repository
{

    /**
     * @param $id
     *
     * @return mixed
     */
    public function execute($id)
    {
        $data = DB::table(UserEmployment::TABLE)
            ->select([
                UserEmployment::ID,
                UserEmployment::ORGANISATION,
                UserEmployment::WEBSITE,
                UserEmployment::POSITION,
                UserEmployment::DATE_START,
                UserEmployment::DATE_END,
                UserEmployment::DESCRIPTION
            ])
            ->where([
                UserEmployment::ID => $id
            ]);

        return $this->response($data, true);
    }
}