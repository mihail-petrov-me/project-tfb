(function () {

    /**
     *
     * @param {*} ProjectModel
     * @param {*} PubSub
     * @param {*} Translation
     * @param {*} UserModel
     */
    var Controller = function (ProjectModel, PubSub, Translation, UserModel, SessionService) {

        /**
         * @type
         * State collection
         */
        this.STATE = {
            "IS_EMPTY": false
        };

        /**
         * @type
         * Data collection
         */
        this.COLLECTION = {
            project : []
        };

        /**
         * @type
         * Event Publish collection
         */
        this.PUBLISH = {
            GROUP_LIST_INIT     : PubSub.events.page.GROUP.LIST.INIT,
            GROUP_LIST_LOAD     : PubSub.events.page.GROUP.LIST.LOAD,
            GROUP_LIST_DESTROY  : PubSub.events.page.GROUP.LIST.DESTROY,

            PAGE_LOAD           : PubSub.events.page.LOAD
        };


        /**
         *
         */
        this.copyCollection = null;

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                HEADER : $provider({
                    'en': 'Active Projects',
                    'bg': 'Активни проекти'
                }),

                EMPTY_LIST_MESSAGE: $provider({
                    'en': 'No new projects',
                    'bg': 'Няма създадени проекти'
                }),

                BUTTON_CREATE_PROJECT: $provider({
                    'en': 'Create new project',
                    'bg': 'Създай нов проект'
                })
            };
        });



        /**
         * @type
         * Event : init
         */
        this.init_modelCollection = () => {

            let handler = ProjectModel.api.fetch.projects();

            handler.then(($request) => {
                this.COLLECTION.project = $request.data;
            });

            handler.catch(() => {
                console.log("#Error");
            });
        };


        /**
         * @type
         * Event: Click
         */
        this.on_select = ($element) => {

            // set current active project
            SessionService.set("tfb:project_collection", $element, true);

            // go to next page element
            this.$router.navigate(['/Dashboard/Projects/ProjectProfile', {'id' : $element.project_id}]);
        };



        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {

            /**
             * @type
             * Publisher
             */
            PubSub.api.publish(this.PUBLISH.GROUP_LIST_INIT);

            /**
             * Create and procduce a list of group elements. This request is
             * going to provide only a limited number of entities, in order to
             * initialise the component with some data
             */
            this.init_modelCollection();
        };


        /**
         * @type
         * Lifecycle event
         */
        this.$onDestroy = function () {
            PubSub.api.publish(this.PUBLISH.GROUP_LIST_DESTROY);
        };

    };

    /**
     * @type
     * Component injector
     */
    Controller.$inject = ['ProjectModel', 'PubSub', 'Translation', 'UserModel', 'SessionService'];

    /**
     *
     */
    var Component = {
        templateUrl: './project-list.template.html',
        controller: Controller,
        controllerAs: 'model',
        bindings        : {
            $router     : '<'
        }
    };

    angular.module('application.components.page').component('projectList', Component);

})();
