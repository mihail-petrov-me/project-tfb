(function () {

    /**
     *
     * @param {*} window
     * @param {*} http
     * @param {*} PubSub
     * @param {*} UserModel
     */
    var Controller = function (PubSub, UserModel, Translation, Upload) {

        var model = this;

        /**
         * @type
         * State collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.STATE = {

            "IS_NEXT_VISIBLE"   : true,
            "IS_PREV_VISIBLE"   : false,
            "IS_SUBMIT_VISIBLE" : false,
            "IS_SUBMIT_STATE"   : false,

            handle: function () {

                // handle in state presense
                model.STATE.IS_PREV_VISIBLE     = model.FORM.has_stack();
                model.STATE.IS_SUBMIT_VISIBLE   = model.FORM.has_tail();
                model.STATE.IS_NEXT_VISIBLE     = !model.FORM.has_tail();

                if (model.FORM.has_overflow()) {

                    // Handle in state presense
                    model.STATE.IS_PREV_VISIBLE = false;
                    model.STATE.IS_SUBMIT_VISIBLE = false;
                    model.STATE.IS_NEXT_VISIBLE = false;

                    // Handle elsewhere
                    model.handle_userAuthorization();
                }


                if (model.FORM.has_tail()) {

                    // handle in state presens
                    model.STATE.IS_PREV_VISIBLE = true;
                    model.STATE.IS_NEXT_VISIBLE = false;
                    model.STATE.IS_SUBMIT_VISIBLE = true;
                }
            },

            /**
             *
             */
            show_submit: function () {
                return model.STATE.IS_SUBMIT_VISIBLE && model.STATE.IS_SUBMIT_STATE;
            }
        };


        /**
         * @type
         * Event Publish collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.PUBLISH = {
            FORM_PROCESS_INIT: PubSub.events.form.PROCESS.INIT,
            PAGE_LOAD: PubSub.events.page.LOAD
        };

        /**
         * @type
         * Event Subscribe collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.SUBSCRIBE = {
            FORM_PROCESS_END: PubSub.events.form.PROCESS.END
        };

        /**
         *
         */
        model.FORM = {

            COLLECTION  : {},
            STORE       : {},

            GROUPS: [
                [ 'intro', 'contact', 'education', 'employment', 'alumni-form' ]
            ],

            REFERENCE       : 0,
            ACTIVE_GROUP    : 0,

            set_active      : function ($group) {
                model.FORM.ACTIVE_GROUP = $group;
                return this;
            },

            /**
             *
             */
            get_active: function () {
                return model.FORM.GROUPS[model.FORM.ACTIVE_GROUP][model.FORM.REFERENCE];
            },

            /**
             *
             */
            store: function (record) {

                model.FORM.COLLECTION[model.FORM.get_active()] = record;
                return this;
            },

            push: function ($key, $value) {
                model.FORM.STORE[$key] = $value;
            },

            /**
             *
             */
            process: function () {

                model.FORM.REFERENCE++;
                return this;
            },

            /**
             *
             */
            backtrack: function () {

                model.FORM.REFERENCE--;
                return this;
            },

            /**
             *
             */
            has_stack: function () {
                return model.FORM.REFERENCE > 0
            },


            /**
             *
             */
            has_tail: function () {
                return (model.FORM.REFERENCE == model.FORM.GROUPS[model.FORM.ACTIVE_GROUP].length - 1);
            },

            /**
             *
             */
            has_overflow: function () {
                return (model.FORM.REFERENCE == model.FORM.GROUPS[model.FORM.ACTIVE_GROUP].length);
            },

            /**
             *
             */
            transform: function () {

                var data = {};
                for (var index = 0; index < model.FORM.GROUPS[model.FORM.ACTIVE_GROUP].length; index++) {
                    data = angular.extend(data, model.FORM.COLLECTION[model.FORM.GROUPS[model.FORM.ACTIVE_GROUP][index]]);
                }
                return angular.extend(data, model.FORM.STORE);
            }
        };

        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(function ($provider) {

            model.LABEL = {

                IMAGE: $provider({
                    'en': 'assets/logo_en.png',
                    'bg': 'assets/logo_bg.png'
                }),

                BUTTON_NEXT: $provider({
                    'en': 'Next',
                    'bg': 'Следваща стъпка'
                }),

                BUTTON_PREV: $provider({
                    'en': 'Previous',
                    'bg': 'Предишна стъпка'
                }),

                BUTTON_SUBMIT: $provider({
                    'en': 'Submit',
                    'bg': 'Изпрати'
                }),

                BUTTON_SUCCESS: $provider({
                    'en': 'Have a nice day',
                    'bg': 'Приятна работа'
                }),

                LABEL_SUCCESS_TITLE: $provider({
                    'en': 'Have a nice day',
                    'bg': 'Добре дошли'
                }),

                LABEL_SUCCESS_L1: $provider({
                    'en': 'You successfuly signed up at Teach for Bulgaria portal',
                    'bg': 'Вие успешно се регистрирахте в инфо портала на Заедно в час'
                }),

                LABEL_SUCCESS_L2: $provider({
                    'en': 'have a nacy and productive day',
                    'bg': 'нека работата ви бъде продуктива'
                }),
            };
        });


        /**
         * @type
         * Event : on
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.on_next = function () {

            if(model.FORM.get_active() != 'intro') {
                PubSub.api.publish(model.PUBLISH.FORM_PROCESS_INIT, model.FORM.get_active());
            }
            else {
                model.FORM.process();
            }
        };


        /**
         * @type
         * Event : on
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.on_prev = function () {

            model.FORM.backtrack();
            model.STATE.handle();
        };


        /**
         * @type
         * Event : on
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.on_process = function () {
            model.$router.navigate(['Dashboard']);
        }

        /**
         * @type
         * Event : on
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.on_change_image = function () {

            if (model.picture) {

                var reader = new FileReader();
                reader.readAsDataURL(model.picture);

                reader.onloadend = function () {
                    model.uiPicture = reader.result;
                };
            }
        };

        model.handle_formProcess = function (formCollection) {

            model.FORM.store(formCollection).process();
            model.STATE.handle();
        };


        model.handle_formTranslation = function (collection) {

            if (collection.id == 'input:dropdown:language') {

                model.FORM.push("language", collection.element.id);
                Translation.toggle(collection.element.id);
            }
        };

        model.handle_formChange = function (collection) {

            if (collection.id == 'input:dropdown:user_type') {

                model.STATE.IS_SUBMIT_STATE = true;
                model.FORM.push("type", collection.element.id);
            }
        };


        model.handle_userAuthorization = function () {

            UserModel.api.create
            .user(model.FORM.transform())
                .then(model.handle_authorization_success)
                .catch(model.handle_authorization_error);
        };



        model.handle_authorization_success = function ($response) {

            UserModel.profile.setSession($response.tokken);
            UserModel.profile.setCollection($response.collection);

            UserModel.api.update
            .picture(UserModel.profile.my('id'), { file: model.picture })
                .then(model.handle_upload_success)
                .catch(model.handle_upload_error);

            // State task
            model.STATE.IS_SUCCESS_VISIBLE = true;
        };


        model.handle_upload_success = function($response) {

            UserModel.profile.setSession($response.tokken);
            UserModel.profile.setCollection($response.collection);
        };


        model.handle_upload_error = function() {
            console.error("@Exception: Unable to upload the image");
        };


        model.handle_authorization_error = function (response) {
            console.error("@Exception: Authentication");
        };

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        model.$onInit = function () {

            // **
            model.uiPicture = UserModel.profile.my('picture');
            model.uiWho     = UserModel.profile.my('name');

            // **
            model.FORM.push("picture", model.uiPicture);
            model.FORM.push("fname", UserModel.profile.my('fname'));
            model.FORM.push("lname", UserModel.profile.my('lname'));
            model.FORM.push("email", UserModel.profile.my('email'));
            model.FORM.push("signature", UserModel.profile.my('signature'));


            PubSub.api.subscribe('event:ui:dropdown:after_select', model.handle_formTranslation);
            PubSub.api.subscribe('event:ui:dropdown:after_select', model.handle_formChange);
            PubSub.api.subscribe(model.SUBSCRIBE.FORM_PROCESS_END, model.handle_formProcess);
        };


        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        model.$postLink = function () {
            PubSub.api.publish(model.PUBLISH.PAGE_LOAD);
        };


        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        model.$onDestroy = function () {

            PubSub.api.unsubscribe(model.SUBSCRIBE.FORM_PROCESS_END);
            PubSub.api.unsubscribe('event:ui:dropdown:after_select');
            PubSub.api.unsubscribe('event:ui:dropdown:after_select');
        };
    };

    Controller.$inject  = ['PubSub', 'UserModel', 'Translation', 'Upload'];
    var Component       = {
        templateUrl     : './signup.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : { $router     : '<' }
    };

    angular.module('application.components.page').component('signup', Component);

})();
