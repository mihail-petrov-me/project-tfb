(function () {

    var Controller = function (UserModel) {

    }

    /**
     *
     */
    var Component = {
        templateUrl     : './ui-list-class-goal.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            collection    : '<'
        }
    };

    /**
     *
     */
    angular.module('application.components.visual').component('uiListClassGoal', Component);

})();
