(function () {

    var Controller = function(Translation) {

        this.name = "mest";

        /**
         * @component
         * FIELD collection
         */
        this.FIELD = {
            "phone": {
                name            : "phone",
                type            : 'text',
                value           : null,
                show_errors     : false,
                isRequired      : true
            },

            "birth_date": {
                name            : "birth_date",
                type            : 'date',
                value           : null,
                show_errors     : false,
                isRequired      : false
            },

            "birth_place": {
                name            : "birth_place",
                type            : 'text',
                value           : null,
                show_errors     : false,
                isRequired      : false
            },

            "address": {
                name            : "address",
                type            : 'text',
                value           : null,
                show_errors     : false,
                isRequired      : true
            }
        };

        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                LABEL_PERSONAL_INFO :$provider({
                    "bg"    : "Лична информация",
                    "en"    : "Personal information"
                }),

                LABEL_CONTACT_INFO :$provider({
                    "bg"    : "Контактна информация",
                    "en"    : "Contact information"
                }),

                INPUT_USER_TYPE :  $provider({
                    "bg"    : "Изберете си позиция",
                    "en"    : "Chouse your type"
                }),

                INPUT_FNAME : $provider({
                    "bg"    : "Име",
                    "en"    : "First name"
                }),

                INPUT_LNAME : $provider({
                    "bg"    : "Фамилия",
                    "en"    : "Last name"
                }),

                INPUT_PHONE : $provider({
                    "bg"    : "Телефонен номер",
                    "en"    : "Mobile number"
                }),

                INPUT_EMAIL : $provider({
                    "bg"    : "Електронна поща",
                    "en"    : "E-mail"
                }),

                INPUT_BIRTH_DATE : $provider({
                    "bg"    : "Дата на раждане",
                    "en"    : "Birth date"
                }),

                INPUT_BIRTH_PLACE : $provider({
                    "bg"    : "Местораждане",
                    "en"    : "Birth place"
                }),

                INPUT_ADDRESS   : $provider({
                    "bg"    : "Постоянен адрес",
                    "en"    : "Current adress"
                })
            };
        });


        this.$onInit = () => {

            this.parent.FIELD  = this.FIELD;
            this.formContact   = this.parent.formContact;
        }
    };


    Controller.$inject  = ['Translation'];
    var Component       = {
        templateUrl     : './form-mest.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        require         : {
            parent      : '^formWrapper'
        },
        bindings        : {
            uiCollection    : '<',
            uiIsDisabled    : '<'
        }
    };

    angular.module('application.components.page').component('formMest', Component);

})();
