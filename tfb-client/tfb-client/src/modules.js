exports.vendors = [
    {
      'path'    : 'node_modules/angular/angular.min.js',
      'script'  : 'vendor/angular.js'
    },
    {
      'path'    : 'node_modules/ngcomponentrouter/angular_1_router.min.js',
      'script'  : 'vendor/angular_1_router.js'
    },
    {
      'path'    : 'node_modules/angular-animate/angular-animate.js',
      'script'  : 'vendor/angular-animate.js'
    },
    {
      'path'    : 'node_modules/angularjs-datepicker/dist/angular-datepicker.min.js',
      'script'  : 'vendor/angular-datepicker.js'
    },
    {
      'path'    : 'node_modules/ng-file-upload/dist/ng-file-upload-shim.js',
      'script'  : 'vendor/ng-file-upload-shim.js'
    },
    {
      'path'    : 'node_modules/ng-file-upload/dist/ng-file-upload.js',
      'script'  : 'vendor/ng-file-upload.js'
    },
    {
      'path'    : 'node_modules/angular-meditor/dist/meditor.js',
      'script'  : 'vendor/angular-meditor.js'
    },
    {
      'path'    : 'node_modules/angular-upload/angular-upload.js',
      'script'  : 'vendor/angular-upload.js'
    },
    {
      'path'    : 'node_modules/angular-jwt/dist/angular-jwt.min.js',
      'script'  : 'vendor/angular-jwt.js'
    },

    {
      'path'    : 'node_modules/angular-chart.js/dist/angular-chart.js',
      'script'  : 'vendor/angular-chart.js'
    },
    {
        "path": "node_modules/moment/moment.js",
        "script": "moment.js"
    }
];


exports.cssVendors = [
  'node_modules/angularjs-datepicker/dist/angular-datepicker.css',
  'node_modules/angular-meditor/dist/meditor.css'
];

exports.fontVendors = [
    {
        'path'  : 'src/vendor/angular-editor/fonts/fontawesome-webfont.woff',
        'script' :'fontawesome-webfont.woff'
    }
];
