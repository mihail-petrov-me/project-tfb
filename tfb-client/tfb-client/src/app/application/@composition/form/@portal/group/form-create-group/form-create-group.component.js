(function () {

    var Controller = function(Translation) {

        this.name = "FORM_CREATE_GROUP";

        /**
         * @component
         */
        this.FIELD = {

            "group_title": {
                name            : "group_title",
                type            : 'text',
                value           : null,
                show_errors     : false,
                error           : 'Field is required'
            },

            "group_description": {
                name            : "group_description",
                type            : 'textarea',
                value           : null,
                show_errors     : false,
                error           : 'Field is required'
            },

            "group_couses": {
                name            : "group_couses",
                type            : 'text',
                value           : [],
                show_errors     : false,
                error           : 'Field is required'
            }
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                PAGE_TITLE          :  $provider({
                    'en' : 'Groups are where your team communicates. They’re best when organized around a serten specific topic.',
                    'bg' : 'Групите са мястото където екипа комуникира на различни теми, и въпроси споделя ресурси и коментира'
                }),

                BUTTON_CREATE       :  $provider({
                    'en' : 'Create New Group',
                    'bg' : 'Нова група'
                }),

                INPUT_TITLE         :  $provider({
                    'en' : 'Group name',
                    'bg' : 'Наименование'
                }),

                INPUT_DESCRIPTION   :  $provider({
                    'en' : 'Group description',
                    'bg' : 'Описание'
                }),

                INPUT_COUSES        :  $provider({
                    'en' : 'Group couses',
                    'bg' : 'Каузи'
                }),

                INPUT_COUSES_HINT   :  $provider({
                    'en' : 'Enter the title of the prefered couse you whant to create and press Enter. If you wish to remove specific cause just click on it',
                    'bg' : 'Въведете названието на вашата кауза и натиснете Enter. За да примахнете кауза просто кликнете върху елемента от списъка.'
                }),

            };
        });


        this.$onInit = () => {
            this.parent.FIELD  = this.FIELD;
            this.formContact   = this.parent.formContact;
        };
    };

    Controller.$inject  = ['Translation', 'PubSub', 'Form'];
    var Component       = {
        templateUrl     : './form-create-group.template.html',
        controller      : Controller,
        controllerAs    : 'model',


        require             : {
            parent          : '^formWrapper'
        },

    };

    angular.module('application.components.page').component('formCreateGroup', Component);
})();
