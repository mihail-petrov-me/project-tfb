<?php

use App\Http\Models\Bundle\Bundle;
use \App\Http\Models\Student\Student;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentTable extends Migration
{
    private $table = Student::TABLE;

    public function up()
    {
        Schema::create($this->table, function(Blueprint $builder) {

            //# Primary Key
            $builder->char(Student::ID, 13)->primary();

            //# Foreign Key
            $builder->char(Student::BUNDLE_ID, 13);

            //# Table Entity Collection
            $builder->string(Student::NAME, 100);

            //# Meta
            $builder->timestamps();
            $builder->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop($this->table);
    }
}
