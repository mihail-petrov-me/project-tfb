<?php
namespace App\Http\Models\Group\Create;

use App\Http\Models\Group\Group;
use App\Http\Models\Model;
use DB;

class CreateGroupEntity extends Model
{    
    /**
     * 
     * @param type $collection
     * @return array
     */
    public function execute($collection) 
    {
       $entity = $this->build($collection);
       extract($entity);
        
       $this->assign_group_entity($id, $title, $description, $causes);
//       $this->assign_group_couse($id, $couses);
//       $this->assign_group_user($id, $userId);

       return $entity;
    }

    /**
     * @param $collection
     * @return array
     */
    private function build($collection)
    {
        return array(
            Group::ID             => uniqid(),
            'userId'              => $this->process($collection, 'user_id'),
            Group::TITLE          => $this->process($collection, 'group_title'),
            Group::DESCRIPTION    => $this->process($collection, 'group_description'),
            Group::CAUSES         => $this->process($collection, 'group_causes')
        );
    }


    /**
     * @param $id
     * @param $title
     * @param $description
     * @param $causes
     */
    private function assign_group_entity($id, $title, $description, $causes)
    {
        DB::table(Group::TABLE)->insert(array(
            Group::ID            => $id,
            Group::TITLE         => $title,
            Group::DESCRIPTION   => $description,
            Group::CAUSES        => implode($causes, ',')
        ));
    }

    /**
     * @param $groupId
     * @param $collection
     */
    private function assign_group_couse($groupId, $collection)
    {
        foreach ($collection as $element) {
            
            // check if element is in database
            $isInDb = DB::table('group_couses')
                    ->select('couse_id')
                    ->where('couse_title', $element)
                    ->first();
            
            if(!is_null($isInDb)) {
                $this->assign_group_group_couse($groupId, $isInDb->couse_id);
            }
            else {
                
                $couseId = uniqid();
                
                DB::table('group_couses')->insert(array(
                    'couse_id'      => $couseId,
                    'couse_title'   => $element
                ));
                
                $this->assign_group_group_couse($groupId, $couseId);
            }
        }
    }
    
    /**
     * 
     * @param type $groupId
     * @param type $couseId
     */
    private function assign_group_group_couse($groupId, $couseId)
    {
        DB::table('group_group_couses')->insert(array(
            'group_id'      => $groupId,
            'couse_id'      => $couseId
        ));        
    }

    /**
     * 
     * @param type $userId
     * @param type $modelId
     */
    private function assign_group_user($modelId, $userId)
    {
        DB::table('user_collection')->insert(array(
            'collection_id'     => $modelId,
            'user_id'           => $userId,
            'is_owner'          => 1,
            'is_admin'          => 1,
            'is_editor'         => 1
       ));
    }
}