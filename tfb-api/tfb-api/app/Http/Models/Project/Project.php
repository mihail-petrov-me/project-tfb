<?php
namespace App\Http\Models\Project;

use App\Http\Models\Model;
use App\Http\Models\Project\Create\AddCommentToProjectTask;
use App\Http\Models\Project\Create\AddGroupToProject;
use App\Http\Models\Project\Create\AddMemberToProject;
use App\Http\Models\Project\Create\AddTaskToProject;
use App\Http\Models\Project\Create\CreateProject;

use App\Http\Models\Project\Fetch\FetchProjectCollection;
use App\Http\Models\Project\Fetch\FetchProject;
use App\Http\Models\Project\Fetch\FetchProjectMemberCollection;
use App\Http\Models\Project\Fetch\FetchProjectTaskCollection;

use App\Http\Models\Project\Fetch\FetchProjectTaskCommentCollection;
use App\Http\Models\Project\Update\UpdateProject;
use App\Http\Models\Project\Update\UpdateProjectTask;

use App\Http\Models\Project\Delete\DeleteProject;
use App\Http\Models\Project\Delete\DeleteProjectTask;

/**
 * Class Project
 * @package App\Http\Models\Project
 */
class Project extends Model
{
    // Public keys
    const ID                = 'project_id';
    const TITLE             = 'project_title';
    const DESCRIPTION       = 'project_description';
    const START_DATE        = 'project_date_start';
    const END_DATE          = 'project_date_end';

    // Public tables
    const TABLE             = 'projects';
    const TABLE_USER        = 'project__user';
    const TABLE_TASK        = 'project__task';
    const TABLE_COMMENT     = 'project__task__comment';
    const TABLE_GROUP       = 'project__group';


    /**
     * @param $collection
     *
     * @return array
     */
    public function create_project($collection)
    {
        return (new CreateProject())->execute($collection);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function fetch_project_members($id)
    {
        return (new FetchProjectMemberCollection())->execute($id);
    }

    /**
     * @param      $id
     * @param null $limit
     *
     * @return mixed
     */
    public function get($id, $limit = null) 
    {
        return (new FetchProject())->execute($id);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function all($query) 
    {
        return (new FetchProjectCollection())->execute();
    }

    /**
     * @param $id
     * @param $collection
     *
     * @return mixed
     */
    public function update($id, $collection)
    {
        return (new UpdateProject())->execute($id, $collection);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function delete($id)
    {
        return (new DeleteProject())->execute($id);
    }


    /**
     * @param $id
     * @param $collection
     */
    public function add_member_to_project($id, $collection)
    {
        return (new AddMemberToProject())->execute($id, $collection);
    }

    /**
     * @param $id
     * @param $collection
     *
     * @return mixed
     */
    public function add_task_to_project ($id, $collection)
    {
        return (new AddTaskToProject())->execute($id, $collection);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function fetch_task_collection ($id)
    {
        return (new FetchProjectTaskCollection())->execute($id);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function delete_task ($id)
    {
        return (new DeleteProjectTask())->execute($id);
    }

    /**
     * @param $projectId
     * @param $taskId
     *
     * @param $collection
     *
     * @return mixed
     */
    public function update_task($projectId, $taskId, $collection)
    {
        return (new UpdateProjectTask())->execute($projectId, $taskId, $collection);
    }

    /**
     * @param $id
     * @param $collection
     *
     * @return mixed
     */
    public function add_comment_to_task ($id, $collection)
    {
        return (new AddCommentToProjectTask())->execute($id, $collection);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function fetch_task_comment_collection($id)
    {
        return (new FetchProjectTaskCommentCollection())->execute($id);
    }

    /**
     * @param $id
     *
     * @param $collection
     *
     * @return mixed
     */
    public function add_group_to_project($id, $collection)
    {
        return (new AddGroupToProject())->execute($id, $collection);
    }
}