(function () {

    /**
     *
     */
    var VideoDomain = {

        /**
         *
         * @param {*} url
         */
        youtube : function($url) {
            return ["https://www.youtube.com/embed", $url.split('=')[1]].join('/');
        },

        /**
         *
         * @param {*} url
         */
        vimeo : function($url) {

            var collection = $url.split('/');
            var id = collection[collection.length - 1];
            return ["https://player.vimeo.com/video/", id, "?title=0&byline=0&portrait=0"].join();
        },

        /**
         *
         * @param {*} url
         */
        voiddomain : function($url) {
            return "";
        }
    };


    /**
     *
     * @param {*} sce
     */
    var ServiceHandler = function ($sce) {

        /**
         *
         * @param {*} url
         */
        function getDomain($url) {

            if (~$url.indexOf('youtube')) return 'youtube';
            if (~$url.indexOf('vimeo')) return 'vimeo';
            return 'voiddomain';
        };

        /**
         *
         * @param {*} url
         */
        function getDomainURL($url) {

            var url =  VideoDomain[getDomain($url)]($url);
            return $sce.trustAsResourceUrl(url);
        }

        return {
            getDomainURL : getDomainURL
        }
    };


    ServiceHandler.$inject = ['$sce'];
    angular.module('application.service').factory('Video', ServiceHandler);

}());
