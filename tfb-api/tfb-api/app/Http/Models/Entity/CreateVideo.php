<?php

namespace App\Http\Models\Entity;
use DB;

class CreateVideo
{

    /**
     * 
     * @param type $collection
     * @return type
     */
    public function execute($groupId, $collection) 
    {
        
       $entityCollection = $this->transform_query($collection);
       extract($entityCollection);
        
       $this->assigne_article($id, $userId, $title, $content);
       $this->assigne_article_group($id, $groupId); 
    }    
    
    /**
     * 
     * @param type $collection
     * @return type
     */
    private function transform_query($collection)
    {
        return array(
            'id'        => uniqid(),
            'title'     => $collection['title'],
            'content'   => $collection['content'],
            'userId'    => $collection['user_id']
        );
    }

    /**
     * 
     * @param type $id
     * @param type $userId
     * @param type $title
     * @param type $content
     */
    private function assigne_article($id, $userId, $title, $content)
    {
        DB::table('block_article')->insertGetId(array(
            'id'        => $id,
            'title'     => $title,
            'content'   => $content,
            'user_id'   => $userId
        ));
    }
    
    
    /**
     * 
     * @param type $modelId
     * @param type $groupId
     */
    private function assigne_article_group($modelId, $groupId)
    {
        DB::table('group_entity')->insert(array(
            'entity_id'     => $modelId,
            'group_id'      => $groupId,
            'entity_type'   => 2
        ));
    }
}
