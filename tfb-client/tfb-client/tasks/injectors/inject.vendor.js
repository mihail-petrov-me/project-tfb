// System modules
// # =============================================
var fs  = require('fs.extra');
var EOL = require('os').EOL;

// Application components
// # =============================================
var buildScriptCollection = function(collection) {

    var scriptCollection = [];

    for (var i = 0; i < collection.length; i++) {
        scriptCollection.push(['<script src="', collection[i], '"></script>'].join(''));
    }

    return scriptCollection;
};


// API : Interface
// # =============================================
exports.exec = function(options) {

    var scriptCollection = buildScriptCollection(options.pipe);
    var index   = fs.readFileSync(options.from + 'index.html').toString();
    var result  = index.replace(/@task:BUILD/, scriptCollection.join(EOL));
    fs.writeFileSync(options.to + 'index.html', result);
};
