(function () {

    /**
     *
     * @param {*} UserModel
     * @param {*} Translation
     */
    var Controller = function ($scope, Translation, PubSub, UserModel, ClassModel) {


        this.month = null;

        this.eventCollection = {};

        this.STATE = {
            NEW_TASK    : true,
            TASK_LIST   : false
        };

        /**
         * @type
         * Event Publish collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        this.PUBLISH = {
            FORM_PROCESS_INIT       : PubSub.events.form.PROCESS.INIT,
        };

        /**
         * @type
         * Event Subscribe collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        this.SUBSCRIBE = {
            FORM_PROCESS_END: PubSub.events.form.PROCESS.END
        };

        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en' : '#Account information',
                    'bg' : '@Профил'
                }),

                LABEL_ACTION : $provider({
                    'en' : '#Account information',
                    'bg' : '#Настройки'
                }),

                LABEL_CONTACT : $provider({
                    'en' : 'Contact',
                    'bg' : 'Лична информация'
                }),

                LABEL_EDUCATION : $provider({
                    'en' : 'Education',
                    'bg' : 'Образование'
                }),

                LABEL_EMPLOYMENT : $provider({
                    'en' : 'Employment',
                    'bg' : 'Работодатели'
                }),

                LABEL_LANGUAGE : $provider({
                    'en' : 'Language Preferences',
                    'bg' : 'Езикови предпочитания'
                }),

                LABEL_LOGOUT : $provider({
                    'en' : 'Log out',
                    'bg' : 'Излез от системата'
                })
            };
        });


        /**
         *
         */
        this.submit_program = () => {
            PubSub.api.publish(this.PUBLISH.FORM_PROCESS_INIT, 'form-schedule');
        };

        /**
         *
         */
        this.on_state = ($state) => {

            if($state == 'new_task') {

                this.STATE.TASK_LIST    = false;
                this.STATE.NEW_TASK     = true;
            }

            if($state == "task_list") {

                this.STATE.TASK_LIST    = true;
                this.STATE.NEW_TASK     = false;
            }
        }

        /**
         *
         */
        this.listEventForToday  = () => this.eventCollection[(+this.month)];

        /**
         *
         */
        this.hasEventForToday   = () => (this.eventCollection[(+this.month)]);



        // /**
        //  * @type
        //  * Event : @Request
        //  *
        //  * @description
        //  * Emit event stating that this component is initialised and
        //  * it is ready notifaing the sidebar component to rerender its state
        //  */
        // this.request_fetchEventCollection = (provideTeacherID) => {
        //     return ClassModel.api.COLLECTION().fetch.events(provideTeacherID)
        //         .then(this.handle_fetchEventCollection_success)
        //         .catch(this.handle_fetchEventCollection_error);
        // };

        // /**
        //  * @type
        //  * Handler : Success
        //  *
        //  * @description
        //  * Emit event stating that this component is initialised and
        //  * it is ready notifaing the sidebar component to rerender its state
        //  */
        // this.handle_fetchEventCollection_success = () => {
        //     this.eventCollection = $response.data;
        //     PubSub.api.publish('ui-calender:event:reload');
        // };

        // /**
        //  * @type
        //  * Handler : Error
        //  *
        //  * @description
        //  * Emit event stating that this component is initialised and
        //  * it is ready notifaing the sidebar component to rerender its state
        //  */
        // this.handle_fetchEventCollection_error = () => {
        //     console.log("@Exception : It is not posible for the events to be fetched");
        // };


        // /**
        //  * @type
        //  * Event : @Request
        //  *
        //  * @description
        //  * Emit event stating that this component is initialised and
        //  * it is ready notifaing the sidebar component to rerender its state
        //  */
        // this.request_createEvent = ($teacherID) => {

        //     return ClassModel.api.create.event($response)
        //         .then(($teacherID) => this.request_fetchEventCollection($teacherID))
        //         .catch(this.handle_createEvent_error);
        // };


        // /**
        //  * @type
        //  * Handler : Error
        //  *
        //  * @description
        //  * Emit event stating that this component is initialised and
        //  * it is ready notifaing the sidebar component to rerender its state
        //  */
        // this.handle_createEvent_error = () => {
        //     console.log("@Exception : Event is not able to be saved into the database");
        // };


        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        this.$onInit = () => {

            this.provideTeacherID = UserModel.profile.my("user_id");

            PubSub.api.subscribe('ui-calendar:event:select', ($response) => {
                this.month = $response;
            });

            PubSub.api.subscribe(this.SUBSCRIBE.FORM_PROCESS_END, ($response) => {

                if(!this.eventCollection[(+this.month)]) {
                    this.eventCollection[(+this.month)] = [];
                }


                var $res = $response.response;
                $res.timestamp = (+this.month);
                $res.teacher_id = this.provideTeacherID;
                this.eventCollection[(+this.month)].push($res);

                ClassModel.api.create.event($res).then(($response) => {

                    ClassModel.api.COLLECTION().fetch.events(this.provideTeacherID).then(($response) => {

                        this.eventCollection = $response.data;
                        PubSub.api.publish('ui-calender:event:reload');
                    })
                    .catch(() => {
                        console.log("@Exception : It is not posible for the events to be fetched");
                    });
                })
                .catch(function() {
                    console.log("@Exception : Event is not able to be saved into the database");
                });
            });

            ClassModel.api.COLLECTION().fetch.events(this.provideTeacherID).then(($response) => {

                this.eventCollection = $response.data;
                PubSub.api.publish('ui-calender:event:reload');
            })
            .catch(() => {
                console.log("@Exception : It is not posible for the events to be fetched");
            });
        };

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component destroy
         */
        this.$onDestroy = () => {

        };
    };

    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject  = ['$scope','Translation', 'PubSub', 'UserModel', 'ClassModel'];
    var Component       = {
        templateUrl     : './assigne-program.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('assigneProgram', Component);

})();
