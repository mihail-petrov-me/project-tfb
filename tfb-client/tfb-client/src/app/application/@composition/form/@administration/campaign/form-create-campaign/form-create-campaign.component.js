(function () {

    /**
     *
     * @param {*} Translation
     * @param {*} GroupModel
     * @param {*} PubSub
     */
    var Controller = function(Translation) {

        this.name          = "FORM_CREATE_CAMPAIGN";
        this.collection    = [];


        /**
         * @component
         */
        this.FIELD = {

            "campaign_title": {
                name            : "campaign_title",
                type            : 'text',
                value           : null,
                show_errors     : false,
                isRequired      : true
            },

            "campaign_type": {
                name            : "campaign_type",
                type            : 'dropdown',
                title           : 'Вид кампания',
                value           : null,
                isRequired      : true,
                show_errors     : false
            },

        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                PAGE_TITLE:  $provider({
                    'en' : 'Title',
                    'bg' : 'Заглавие на кампанията'
                }),

                INPUT_TITLE:  $provider({
                    'en' : 'Title',
                    'bg' : 'Заглавие на кампанията'
                }),


                LABEL_CAMPAIGN_TYPE : $provider({
                    'en' : 'Campaign type',
                    'bg' : 'Тип на провеждана кампания'
                }),

                OPTIONS_CAMPAIGN_TYPE : $provider({
                    "bg"    : [
                        { 'id': 1, 'title': 'Редовна кампания'    },
                        { 'id': 2, 'title': 'Учебна кампания'    },
                    ],
                    "en"    : [
                        { 'id': 1, 'title': 'Regular campaign'    },
                        { 'id': 2, 'title': 'Treany campaign'    },
                    ]
                }),
            };
        });

        this.$onInit = () => {

            this.parent.FIELD  = this.FIELD;
            this.formContact   = this.parent.formContact;
        }
    };

    /**
     * @type
     * Component injector
     */
    Controller.$inject  = ['Translation', 'PubSub', 'Form'];
    var Component       = {
        templateUrl     : './form-create-campaign.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        require         : {
            parent      : '^formWrapper'
        },
    };

    angular.module('application.components.page').component('formCreateCampaign', Component);

})();
