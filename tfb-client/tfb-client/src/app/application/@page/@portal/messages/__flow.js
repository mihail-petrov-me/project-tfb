/**
 * @type
 * System event
 *
 * @description
 * This method is executed on component init
 */
PubSub.api.subscribe('scroll:top:event', model.on_reFetch);

/**
 * @type
 * Lifecycle event
 *
 * @description
 * This method is executed on component init
 */
model.$onInit = function() {
    model.init_threads();
};

/**
 * @type
 * Lifecycle event
 *
 * @description
 * This method is executed on component init
 */
model.$postLink = function () {
    PubSub.api.publish(model.PUBLISH.PAGE_LOAD);
};


/**
 * @type
 * Lifecycle event
 *
 * @description
 * This method is executed on component init
 */
model.$onDestroy = function () {
    PubSub.api.unsubscribe('scroll:top:event');
};
