<?php
namespace App\Http\Models\User\Update;
use App\Http\Models\Repository;
use App\Http\Models\User\UserContact;
use DB;

class UpdateUserContact extends Repository
{
    public function execute($id, $collection)
    {
        return DB::table(UserContact::TABLE)
            ->where(array(
                UserContact::ID => $id
            ))
            ->update(array(
                UserContact::PHONE             => $this->process($collection, UserContact::PHONE),
                UserContact::BIRTH_DATE        => $this->process($collection, UserContact::BIRTH_DATE),
                UserContact::BIRTH_PLACE       => $this->process($collection, UserContact::BIRTH_PLACE),
                UserContact::ADDRESS           => $this->process($collection, UserContact::ADDRESS)
        ));
    }
}