<?php
namespace App\Http\Models\Bundle\Delete;

use App\Http\Models\Bundle\Bundle;
use App\Http\Models\Repository;

class DeleteBundle extends Repository
{
    public function execute($id)
    {
        $this->delete(Bundle::TABLE, [
            Bundle::ID  => $id
        ]);
    }
}