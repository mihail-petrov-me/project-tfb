<?php
namespace App\Http\Models\User\Fetch\Single;

use App\Http\Models\Repository;
use App\Http\Models\User\User;
use App\Http\Models\User\UserContact;
use DB;

/**
 * Class FetchUserContact
 * @package App\Http\Models\User\Fetch\Single
 */
class FetchUserContact extends Repository
{
    /**
     * @param $id
     * @return mixed
     */
    public function execute($id)
    {
            $data =  DB::table(UserContact::TABLE)
            ->leftJoin(
                User::TABLE,
                $this->table(User::TABLE, User::ID),
                '=',
                $this->table(UserContact::TABLE, User::ID)
            )
            ->select([
                $this->table(User::TABLE, User::ID),
                $this->table(User::TABLE, User::EMAIL),
                $this->table(User::TABLE, User::NAME),
                $this->table(User::TABLE, User::FNAME),
                $this->table(User::TABLE, User::LNAME),
                $this->table(User::TABLE, User::PERMISSIONS),
                $this->table(User::TABLE, User::PICTURE),

                $this->table(UserContact::TABLE, UserContact::BIRTH_DATE),
                $this->table(UserContact::TABLE, UserContact::BIRTH_PLACE),
                $this->table(UserContact::TABLE, UserContact::PHONE),
                $this->table(UserContact::TABLE, UserContact::ADDRESS),
            ])
            ->where([
                $this->table(User::TABLE, User::ID) => $id
            ]);

        return $this->response($data, true);
    }
}