(function () {

    /**
     *
     */
    var Controller = FormController(function(Translation) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;


        /**
         *
         */
        model.name = "type";


        /**
         * @component
         * Fields collection
         *
         * @behaivior
         * This property contains all the fields of
         * the form controller. Every single field contain value and mutator properties
         * in order to check if this field is required or not you must provide
         *
         * @properties
         *  isRequired : true | false
         *  value : String
         */
        model.FIELD = {

            "type": {

                name            : "type",
                type            : "dropdown",
                title           : 'Chose your type',
                value           : null,
                show_errors     : false,
                isRequired      : true
            },
        };


        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(function($provider) {

            model.LABEL = {

                INPUT_USER_TYPE : $provider({
                    "bg"    : "Какъв си ти в организацията",
                    "en"    : "What is your profile"
                }),

                TYPE_OPTIONS : $provider({
                    "bg"    : [
                            {   'id': 1, 'title': 'Алумни'  },
                            {   'id': 2, 'title': 'Учител' },
                            {   'id': 3, 'title': 'Служител'  }
                        ],
                    "en"    : [
                            {   'id': 1, 'title': 'Alumni'  },
                            {   'id': 2, 'title': 'Teacher' },
                            {   'id': 3, 'title': 'Employ'  }
                        ]
                })
            };
        });
    });


    /**
     *
     */
    Controller.$inject = ['Translation', 'PubSub', 'Form'];

    /**
     *
     */
    var Component = {
        templateUrl     : './form-type.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            uiCollection    : '<',
            uiIsDisabled    : '<'
        }
    };

    angular.module('application.components.page').component('formType', Component);

})();
