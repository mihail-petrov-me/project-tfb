(function () {

    'use strict';

    // Modules
    // ##
    angular.module('application.service'                , []);
    angular.module('application.models'                 , []);
    angular.module('application.components.page'        , []);
    angular.module('application.components.visual'      , []);
    angular.module('application.components.directive'   , []);


    /**
     * Angular component modules contain
     * only components thats are shiped directly by the Framework vendor
     * and add extra utility in order to copmleat serten task
     */
    let AngularModules = [
        'ngComponentRouter',
        'ngAnimate',
        'ngSanitize'
    ];

    /**
     * Vendor modules are third party modules that are provided by
     * developers using the Angular frameworkr as a base entry point
     */
    let VendorModules = [
        '720kb.datepicker',
        'angular-meditor',
        'lr.upload',
        'angular-jwt',
        'ngFileUpload',
        'chart.js'
    ];

    /**
     * Application based components serving the purpose of the current application
     * Providing services and components.
     */
    let CustomModules = [
        'application.service',
        'application.models',
        'application.components.page',
        'application.components.visual',
        'application.components.directive',
    ];

    let ModuleDependancy = [].concat(AngularModules, VendorModules, CustomModules);

    let ComponentRouter = [
        { path: 'login'          ,component: 'login'     ,as: 'Login',},
        { path: 'signup'         ,component: 'signup'    ,as: 'Signup'},
        { path: 'expire'         ,component: 'expire'    ,as: 'Expire'},
        { path: 'dashboard/...'  ,component: 'dashboard' ,as: 'Dashboard',useAsDefault: true}
    ];

    /**
     * @templateUrl : Application template
     * This is the default template for this component
     *
     * @routeConfig : ApplicationRoute
     * The route component is contained in global route provider mechanisum
     */
    let Component = {
        templateUrl     : './application.template.html',
        $routeConfig    : ComponentRouter
    };


    //
    let Application = angular.module('application', ModuleDependancy)
        .component('bootstrap', Component)
        .value('$routerRootComponent', 'bootstrap')
        .config(function($httpProvider, $sceProvider) {
            $httpProvider.interceptors.push('SessionService');
        })
        .run( function($document, $rootScope, $location, UserModel, Request, SessionService, PubSub) {

            $rootScope.$on("$locationChangeStart", function(event, next, current) {

                $(".page-loader").show();

                // Check if session is expired
                if(SessionService.getTokken() === null) {
                    SessionService.destroy();
                    window.location.href = "#!/login";
                }

                Request.flush();

                let $collection = next.split('/');
                let $reference = $collection[$collection.length - 1];



                // If user is oin logi page
                if($reference == 'login') {

                    if(UserModel.profile.isAuthorized()) {
                        window.location.href = "#!/dashboard";
                    }


                    if(UserModel.profile.isAuthenticated()) {
                        window.location.href = "#!/signup";
                    }
                }

                if($reference == 'signup') {

                    if(UserModel.profile.isAuthorized()) {
                        window.location.href = "#!/dashboard";
                    }

                    if(!UserModel.profile.isAuthenticated()) {
                        window.location.href = "#!/login";
                    }
                }
            });

            PubSub.api.subscribe('event:ui:after_load', function() {
                $(".page-loader").hide();
            });

            //#
            $document.on('keydown', function(e) {

                if(e.keyCode == 27) {
                    PubSub.api.publish("KEYBOARD_CANCEL_EVENT");
                }
            });


        });

    Application.$inject = ['$document', '$rootScope', '$location', 'UserModel', 'Request', 'SessionService', 'PubSub'];
})();
