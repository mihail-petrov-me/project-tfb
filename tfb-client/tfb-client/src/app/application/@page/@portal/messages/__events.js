/**
 * @type
 * Event : INIT
 *
 * @description
 * This method is resposible for handlimng the initialisation of
 * the form component collection
 */
model.init_threads = function() {

    var handler = UserModel.api.fetch.threads(UserModel.profile.my("user_id"));
    handler.then(model.handle_init_success);
    handler.catch(model.handle_init_error);
}

/**
 * @type
 * Events : ON
 *
 * @description
 * This method contains collection of every single label that neads to be translated for
 * the current component, in orderfor the localisation to work properly
 */
model.on_reFetch = function($id) {

    var handler = UserModel.api.fetch.thread(UserModel.state("thread_id"), {
        'stack_message' : UserModel.state('stack_message')
    });

    handler.then(model.handle_reFetchMessages_success);
    handler.catch(model.handle_reFetchMessages_error);
};

/**
 * @type
 * Events : ON
 *
 * @description
 * This method contains collection of every single label that neads to be translated for
 * the current component, in orderfor the localisation to work properly
 */
model.on_threadSelect = function($id) {

    UserModel.state("thread_id", $id);
    var handler = UserModel.api.fetch.thread($id);
    handler.then(model.handle_fetchMessages_success);
    handler.catch(model.handle_fetchMessages_error);
};

/**
 * @type
 * Events : ON
 *
 * @description
 * This method contains collection of every single label that neads to be translated for
 * the current component, in orderfor the localisation to work properly
 */
model.on_messageSend = function() {

    var handler = UserModel.api.create.message($id);
    handler.then(model.handle_createMessages_success);
    handler.catch(model.handle_createMessages_error)
};
