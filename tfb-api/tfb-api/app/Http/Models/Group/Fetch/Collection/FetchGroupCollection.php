<?php

namespace App\Http\Models\Group\Fetch\Collection;
use DB;

class CreateGroupCollection
{
    public function execute($id)
    {
        $collection = $this->select(
                DB::table($this->table)->orderBy('created_at', 'desc'), 
        $query);
        
        return $this->transform_Groups($collection);
    }
    
    /**
     * 
     * @param type $collection
     */
    private function transform_Groups($collection)
    {
        foreach($collection as $element) {
            $element->couses = explode(',', $element->couses);
        }
        
        return $collection;
    }    
}
