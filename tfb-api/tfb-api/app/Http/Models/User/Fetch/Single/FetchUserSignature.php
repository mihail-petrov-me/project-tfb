<?php
namespace App\Http\Models\User\Fetch\Single;

use App\Http\Models\Repository;
use App\Http\Models\User\User;
use DB;

class FetchUserSignature extends Repository
{
    public function execute($id)
    {
        return DB::table(User::TABLE)
            ->select(
                User::FNAME,
                User::LNAME,
                User::NAME,
                User::SIGNATURE,
                User::PICTURE
            )
            ->where(
                User::ID, $id
            )
            ->first();  
    }
}