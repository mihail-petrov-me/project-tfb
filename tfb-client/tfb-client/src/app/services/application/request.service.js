((() => {

    let PAGINATION              = { limit: 10, start: 0, has_next: false };
    let REFRESH                 = { request: 'refresh' , timestamp: null };

    let $requestMiddleware      = { type: null };

    let PaginationCollection    = {};
    let RefreshCollection       = {};


    const isRefresh     = ($type) => ($type == "refresh");
    const isCollection  = ($type) => ($type == "collection");
    const isSingle      = ($type) => ($type == "single");
    const isPagination  = ($type) => ($type == "pagination");

    /**
     *
     * @param {*}
     * @param {*}
     * @param {*} AppManager
     * @param {*} Upload
     * @param {*} SessionService
     */
    var Service = ($http, $q,  AppManager, Upload, SessionService) => {

        /**
         *
         * @param {*} response
         */
        function _handler_query_success($response) {

            var [$id] = $response.config.url.split('?');

            if($response.data.success.data.tokken) {
                SessionService.setSession($response.data.success.data.tokken);
            }

            if(PaginationCollection[$id]) {
                PaginationCollection[$id].start     = $response.data.success.pagination.next;
                PaginationCollection[$id].has_next  = $response.data.success.pagination.has_next;
            }

            if(RefreshCollection[$id]) {
                RefreshCollection[$id].timestamp    = $response.data.success.timestamp;
            }
            else {
                // create new object
                RefreshCollection[$id]              = angular.copy(REFRESH);
                RefreshCollection[$id].timestamp    = $response.data.success.timestamp;
            }

            // Data responce
            return $response.data.success;
        }

        /**
         *
         * @param {*} response
         */
        function _handler_refresh_success($response) {

            var [$id] = $response.config.url.split('?');

            if(RefreshCollection[$id] && $response.data.success.timestamp) {
                RefreshCollection[$id].timestamp = $response.data.success.timestamp;
            }

            return $response.data;
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handler_error(response) {
            return $q.reject(response.status);
        }

        /**
         *
         * @param {*} promise
         */
        function _resolver(promise) {
            return promise.then(_handler_refresh_success).catch(_handler_error);
        }

        /**
         *
         * @param {*} promise
         */
        function _queryResolver(promise) {
            return promise.then(_handler_query_success).catch(_handler_error);
        }

        /**
         *
         * @param {*} urlCollection
         */
        function __buildQuery($urlCollection, $queryCollection) {

            var $id         = AppManager.url($urlCollection);
            var $reference  = null;

            if(PaginationCollection[$id]) {
                $reference = PaginationCollection[$id];
            }
            else {
                $reference = angular.copy(PAGINATION);
                PaginationCollection[$id] = $reference;
            }

            $reference = angular.extend($reference, $queryCollection);
            return $reference;
        }

        /**
         *
         * @param {*} urlCollection
         */
        function __bq($urlCollection, $queryCollection) {

            var $id         = AppManager.url($urlCollection);
            return angular.extend({}, $queryCollection);
        }

        /**
         *
         * @param {*} urlCollection
         */
        function __buildRefresh($urlCollection) {

            var $id = AppManager.url($urlCollection);
            return (RefreshCollection[$id]) ? RefreshCollection[$id] : null;
        }

        return {

            beffore($configuration) {
                $requestMiddleware = $configuration;
                return this;
            },

            flush() {
                PaginationCollection = {};
                RefreshCollection    = {};
            },

             get ($urlCollection, queryColelction) {

                if(isRefresh($requestMiddleware.type)) {
                    return _resolver($http.get(AppManager.url($urlCollection, __buildRefresh($urlCollection))));
                }

                if(isPagination($requestMiddleware.type)) {
                    return _queryResolver($http.get(AppManager.url($urlCollection, __buildQuery($urlCollection, queryColelction))));
                }

                if(isSingle($requestMiddleware.type)) {
                    return _resolver($http.get(AppManager.url($urlCollection)));
                }

                if(isCollection($requestMiddleware.type)) {
                    return _resolver($http.get(AppManager.url($urlCollection, __bq($urlCollection, queryColelction))));
                }

                return _resolver($http.get(AppManager.url($urlCollection)));
            },

            post    : ($urlCollection, body)    => _resolver($http.post(AppManager.url($urlCollection), body)),
            put     : ($urlCollection, body)    => _resolver($http.put(AppManager.url($urlCollection), body)),
            delete  : ($urlCollection)          => _resolver($http.delete(AppManager.url($urlCollection))),
            upload  : ($urlCollection, $data)   =>  Upload.upload({ url : AppManager.url($urlCollection), data: $data }).then(_handler_success).catch(_handler_error)
        };
    };

    angular.module('application.service').factory('Request', Service);

})());
