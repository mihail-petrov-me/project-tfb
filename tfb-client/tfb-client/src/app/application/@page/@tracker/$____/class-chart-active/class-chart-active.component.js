(function () {

    /**
     *
     * @param {*} UserModel
     * @param {*} Translation
     */
    var Controller = function ($timeout, Translation, PubSub, UserModel, ClassModel) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;


        model.labels = [
            "Входно ниво",
            "Оценка първи срок",
            "Финална оценка"
        ];

        model.colors = [
            "#ee627d"

        ];

        model.data = [
            [16, 35, 100]
        ];
    };

    Controller.$inject = ['$timeout','Translation', 'PubSub', 'UserModel', 'ClassModel'];
    var Component = {
        templateUrl     : './class-chart-active.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('classChartActive', Component);

})();
