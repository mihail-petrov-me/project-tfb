var Calendar = function (model, options, date) {

    // Default Values
    this.Options = {
        NavShow             : true,
        NavVertical         : false,
        NavLocation         : '',
        DateTimeFormat      : 'mmm, yyyy',
        DatetimeLocation    : '',
        EventClick          : '',
        EventTargetWholeDay : false,
        DisabledDays        : [],
        ModelChange         : model
    };

    // Overwriting default values
    // ================================================================================================
    for (var key in options) {
        this.Options[key] = typeof options[key] == 'string' ? options[key].toLowerCase() : options[key];
    }

    model ? this.Model = model : this.Model = {};
    this.Today = new Date();

    this.Selected       = this.Today
    this.Today.Month    = this.Today.getMonth();
    this.Today.Year     = this.Today.getFullYear();

    if (date) { this.Selected = date }

    this.Selected.Month = this.Selected.getMonth();
    this.Selected.Year  = this.Selected.getFullYear();

    this.Selected.Days      = new Date(this.Selected.Year, (this.Selected.Month + 1), 0).getDate();
    this.Selected.FirstDay  = new Date(this.Selected.Year, (this.Selected.Month), 1).getDay();
    this.Selected.LastDay   = new Date(this.Selected.Year, (this.Selected.Month + 1), 0).getDay();

    this.Prev = new Date(this.Selected.Year, (this.Selected.Month - 1), 1);

    if (this.Selected.Month == 0) {
        this.Prev = new Date(this.Selected.Year - 1, 11, 1);
    }

    this.Prev.Days = new Date(this.Prev.getFullYear(), (this.Prev.getMonth() + 1), 0).getDate();
};



/**
 *
 * @param {*} calendar
 * @param {*} element
 * @param {*} adjuster
 */
function createCalendar(calendar, element, adjuster) {

    if (typeof adjuster !== 'undefined') {

        var newDate = new Date(calendar.Selected.Year, calendar.Selected.Month + adjuster, 1);
        calendar    = new Calendar(calendar.Model, calendar.Options, newDate);
        element.innerHTML = '';

    }
    else {
        for (var key in calendar.Options) {
            typeof calendar.Options[key] != 'function' && typeof calendar.Options[key] != 'object' && calendar.Options[key] ? element.className += " " + key + "-" + calendar.Options[key] : 0;
        }
    }

    var months = ["Януари", "Февруари", "Март", "Април", "Май", "Юни", "Юли", "Август", "Септември", "Октомври", "Ноември", "Декември"];

    var mainSection = document.createElement('div');
    mainSection.className += "cld-main";

    function AddDateTime() {

        var datetime = document.createElement('div');
        datetime.className += "cld-datetime";

        if (calendar.Options.NavShow && !calendar.Options.NavVertical) {

            var rwd = document.createElement('div');
            rwd.className += " cld-rwd cld-nav";
            rwd.addEventListener('click', function () { createCalendar(calendar, element, -1); });
            rwd.innerHTML = '<i class="material-icons">&#xE314;</i>';
            datetime.appendChild(rwd);
        }

        var today = document.createElement('div');
        today.className += ' today';
        today.innerHTML = months[calendar.Selected.Month] + ", " + calendar.Selected.Year;
        datetime.appendChild(today);

        if (calendar.Options.NavShow && !calendar.Options.NavVertical) {

            var fwd = document.createElement('div');
            fwd.className += " cld-fwd cld-nav";
            fwd.addEventListener('click', function () { createCalendar(calendar, element, 1); });
            fwd.innerHTML = '<i class="material-icons">&#xE315;</i>';
            datetime.appendChild(fwd);
        }


        mainSection.appendChild(datetime);
    }

    /**
     *
     */
    function RenderLabel() {

        var labels          = document.createElement('ul');
        labels.className    = 'cld-labels';
        var labelsList      = ["Нед", "Пон", "Вт", "Ср", "Чет", "Пет", "Съб"];

        for (var i = 0; i < labelsList.length; i++) {

            var label = document.createElement('li');
            label.className += "cld-label";
            label.innerHTML = labelsList[i];
            labels.appendChild(label);
        }
        mainSection.appendChild(labels);
    }


    function AddDays() {

        // Create Number Element
        function DayNumber(n) {

            var number = document.createElement('p');
            number.className += "cld-number";
            number.innerHTML += n;
            return number;
        }

        var days        = document.createElement('ul');
        days.className  += "cld-days";

        // Previous Month's Days
        for (var i = 0; i < (calendar.Selected.FirstDay); i++) {

            var day = document.createElement('li');
            day.className += "cld-day prevMonth";

            //Disabled Days
            var d = i % 7;
            for (var q = 0; q < calendar.Options.DisabledDays.length; q++) {
                if (d == calendar.Options.DisabledDays[q]) {
                    day.className += " disableDay";
                }
            }

            var number = DayNumber((calendar.Prev.Days - calendar.Selected.FirstDay) + (i + 1));
            day.appendChild(number);

            days.appendChild(day);
        }

        // Current Month's Days
        for (var i = 0; i < calendar.Selected.Days; i++) {

            var day = document.createElement('li');
            day.className += "cld-day currMonth";
            //Disabled Days
            var d = (i + calendar.Selected.FirstDay) % 7;

            for (var q = 0; q < calendar.Options.DisabledDays.length; q++) {
                if (d == calendar.Options.DisabledDays[q]) {
                    day.className += " disableDay";
                }
            }
            var number = DayNumber(i + 1);

            // Check Date against Event Dates
            day.appendChild(number);
            // If Today..
            if ((i + 1) == calendar.Today.getDate() && calendar.Selected.Month == calendar.Today.Month && calendar.Selected.Year == calendar.Today.Year) {
                day.className += " today";
            }
            days.appendChild(day);
        }


        // Next Month's Days
        // Always same amount of days in calander
        var extraDays = 13;

        if (days.children.length > 35) {
            extraDays = 6;
        }
        else if (days.children.length < 29) {
            extraDays = 20;
        }

        for (var i = 0; i < (extraDays - calendar.Selected.LastDay); i++) {
            var day = document.createElement('li');
            day.className += "cld-day nextMonth";

            //Disabled Days
            var d = (i + calendar.Selected.LastDay + 1) % 7;
            for (var q = 0; q < calendar.Options.DisabledDays.length; q++) {
                if (d == calendar.Options.DisabledDays[q]) {
                    day.className += " disableDay";
                }
            }

            var number = DayNumber(i + 1);
            day.appendChild(number);

            days.appendChild(day);
        }
        mainSection.appendChild(days);
    }


    element.appendChild(mainSection);

    AddDateTime();
    RenderLabel();
    AddDays();
}


(function () {

    var Provider = function (UserModel) {

        this.$onInit = () => {

            var events = [
                {'Date': new Date(2017, 5, 7), 'Title': 'Doctor appointment at 3:25pm.'},
                {'Date': new Date(2017, 5, 18), 'Title': 'New Garfield movie comes out!', 'Link': 'https://garfield.com'},
                {'Date': new Date(2017, 5, 27), 'Title': '25 year anniversary', 'Link': 'https://www.google.com.au/#q=anniversary+gifts'},
            ];

            var settings = {
                EventClick : function() {
                    console.log("This is super mega click event");
                }
            };
            var element = document.getElementById('caleandar-dom');
            var obj = new Calendar(events, settings);
            createCalendar(obj, element);

            var element2 = document.getElementById("caleandar-dom");

            element2.addEventListener('click', (e) => {
                console.log(obj);
            });

        };

    };

    Provider.$inject = ['UserModel']
    var Component    = {
        templateUrl: './ui-calendar.template.html',
        controllerAs: 'model',
        controller: Provider,
    };

    /**
     *
     */
    angular.module('application.components.visual').component('uiCalenda', Component);

})();
