(function () {

    var Controller = function (PubSub, CampaignModel) {

        this.COLLECTION = {
            term : []
        };

        this.STATE = {
            IS_TERM_VISIBLE             : false,
            IS_CREATE_BUTTON_VISIBLE    : false
        }

        /**
         *
         */
        this.on_create = () => {
            PubSub.form.init("FORM_CREATE_CAMPAIGN");
        };

        /**
         *
         */
        this.$onInit = () => {

            PubSub.form.change('FORM_CREATE_CAMPAIGN', ($response) => {

                this.STATE.IS_TERM_VISIBLE          = $response.form_change_valid;
                this.STATE.IS_CREATE_BUTTON_VISIBLE = (this.COLLECTION.term.length > 0 && this.STATE.IS_TERM_VISIBLE);
            });


            PubSub.form.end('FORM_CREATE_CAMPAIGN', ($response) => {

                PubSub.api.publish("PANEL_CREATE_CAMPAIGN:END", {
                    campaign_title           : $response.campaign_title,
                    campaign_type            : $response.campaign_type,
                    terms                    : this.COLLECTION.term
                });
            });


            PubSub.form.end('FORM_CREATE_TERM', ($response) => {

                this.COLLECTION.term.push({
                    campaign_term_title    : $response.campaign_term_title,
                    campaign_date_start    : $response.campaign_date_start,
                    campaign_date_end      : $response.campaign_date_end,
                });

                this.STATE.IS_CREATE_BUTTON_VISIBLE = (this.COLLECTION.term.length > 0 && this.STATE.IS_TERM_VISIBLE);
            })
        }
    }

    var Component = {
        templateUrl     : './panel-create-campaign.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        transclude      : true,
    };

    angular.module('application.components.visual').component('panelCreateCampaign', Component);

})();
