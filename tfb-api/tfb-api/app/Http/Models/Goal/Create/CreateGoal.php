<?php
namespace App\Http\Models\Goal\Create;
use App\Http\Models\Repository;
use DB;

class CreateGoal extends Repository
{
    public function execute($collection)
    {
        $entity = $this->build($collection);
        DB::table('goals')->insert($entity);

        return $entity;
    }

    /**
     * @param $collection
     * @return array
     */
    public function build($collection)
    {
        return array(
            'id'            => uniqid(),
            'parent_id'     => $this->process($collection, 'parent_id', null),
            'teacher_id'    => $this->process($collection, 'teacher_id'),
            'bundle_id'     => $this->process($collection, 'bundle_id'),
            'subject_id'    => $this->process($collection, 'subject_id'),
            'title'         => $this->process($collection, 'title'),
        );
    }
}