(function () {


    var Controller = function() {

    };

    var Component       = {
        templateUrl     : './assesment.template.html',
        controllerAs    : 'model',
        controller      : Controller
    };

    angular.module('application.components.page').component('assesment', Component);
})();
