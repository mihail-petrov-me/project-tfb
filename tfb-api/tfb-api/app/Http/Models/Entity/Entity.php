<?php

namespace App\Http\Models\Entity;


use App\Http\Models\Entity\Comment\Create\CreateComment;
use App\Http\Models\Entity\Comment\Fetch\FetchComment;
use App\Http\Models\Entity\Feed\FetchUserFeed;

use App\Http\Models\Entity\Article\FetchArticle;
use App\Http\Models\Entity\Article\FetchArticleCollection;

use App\Http\Models\Entity\Post\FetchPost;
use App\Http\Models\Entity\Post\FetchPostCollection;

use App\Http\Models\Entity\Poll\FetchPoll;
use App\Http\Models\Entity\Poll\FetchPollCollection;
use App\Http\Models\Model;


class Entity extends Model
{
    
    /**
     * 
     * @param type $collection
     * @return type
     */
    public function create_comment($collection)
    {
        return (new CreateComment())->execute($collection);
    }
    
    /**
     * 
     * @param type $id
     * @param type $query
     * @return type
     */
    public function show_comment($id, $query)
    {
        $this->setRefresh('entity_comment.created_at');
        return $this->select((new FetchComment())->execute($id), $query);
    }
    
    /**
     * 
     * @param type $id
     * @param type $query
     * @return type
     */
    public function fetch_post_entity($id, $query)
    {
        return (new FetchPost())->execute($id, $query);
    }
    
    /**
     * 
     * @param type $id
     * @param type $query
     * @return type
     */
    public function fetch_post_collection($id, $query)
    {
        $this->setRefresh('block_post.created_at');
        return $this->select((new FetchPostCollection())->execute($id, $query), $query);
    }

    /**
     * 
     * @param type $id
     * @param type $query
     * @return type
     */
    public function fetch_poll_entity($id, $query)
    {
        return (new FetchPoll())->execute($id, $query);
    }
    
    /**
     * 
     * @param type $id
     * @param type $query
     * @return type
     */
    public function fetch_poll_collection($id, $query)
    {
        // $this->setRefresh('entity_comment.created_at');
        return $this->select((new FetchPollCollection())->execute($id, $query), $query);
    }    

    /**
     * 
     * @param type $id
     * @param type $query
     * @return type
     */
    public function fetch_article_entity($id, $query)
    {
        return (new FetchArticle())->execute($id, $query);
    }
    
    /**
     * 
     * @param type $id
     * @param type $query
     * @return type
     */
    public function fetch_article_collection($id, $query)
    {
         $this->setRefresh('block_article.created_at');
        return $this->select((new FetchArticleCollection())->execute($id, $query), $query);
    }


    /**
     * @param $id
     * @param $query
     * @return type
     */
    public function show_user_feed($id, $query)
    {
        return $this->select((new FetchUserFeed())->execute($id), $query);
    }


    /**
     * @param $userId
     * @param $query
     * @return array
     */
    public function fetch_user_feed($userId, $query)
    {
        $build = (new FetchUserFeed())->execute($userId);
        $this->setRefresh('collection_entity.created_at');
        $b  = $this->select($build, $query);
        return $this->parsePoll($b, $userId);
    }

    /**
     * @param $collection
     * @param $userId
     * @return array
     */
    public function parsePoll($collection, $userId)
    {   
        $questionCollection = array();
        $answerCollection   = array();
        $idCollection       = array();
        $totalCollection    = array();
                
        foreach($collection as $element) {
            
            
            if($element->entity_type == 3) {
                
                if(!array_key_exists($element->entity_id, $questionCollection)) {
                    $questionCollection[$element->entity_id] = $this->assigne_poll_question($element, $userId);
                }                
                
                if(!array_key_exists($element->entity_id, $answerCollection)) {

                    $answerCollection[$element->entity_id]      = array();
                    $answerCollection[$element->entity_id][]    = $this->assigne_poll_answer($element);
                    $idCollection[]                             = $element->poll_answer_id;
                    $totalCollection[$element->entity_id]       = ((int)$element->poll_answer_count);
                }

                if(array_key_exists($element->entity_id, $answerCollection) && (!in_array($element->poll_answer_id, $idCollection))) {

                    $answerCollection[$element->entity_id][] = $this->assigne_poll_answer($element);
                    $idCollection[]                          = $element->poll_answer_id;
                    $totalCollection[$element->entity_id]    = ($totalCollection[$element->entity_id] + (int)$element->poll_answer_count);
                }                
            }
            
            if($element->entity_type == 1) {
                $questionCollection[$element->entity_id] = $this->assigne_post_question($element, $userId);
            }
            
            if($element->entity_type == 2) {
                $questionCollection[$element->entity_id] = $this->assigne_article_question($element, $userId);
            }            
        }
        
        return $this->assigne_poll_collection($questionCollection, $answerCollection, $totalCollection);
    }

    /**
     * @param $element
     * @param $userId
     * @return array
     */
    private function assigne_post_question($element, $userId)
    {
                
        return array(
            'post_content'=> $element->post_content,

            // Get user meta data                   
            // ##
            'name'        => $element->name,
            'position'    => $element->position,
            'type'        => $element->type,
            'signature'   => $element->signature,
            'picture'     => $element->picture,

            // Get collection meta data                   
            // ##
            'entity_id'   => $element->entity_id,
            'entity_type' => $element->entity_type,
            'created_at'  => $element->created_at,
        );
    }

    /**
     * @param $element
     * @param $userId
     * @return array
     */
    private function assigne_article_question($element, $userId)
    {
        return array(
            'article_content' => $element->article_content,
            'article_title'   =>  $element->article_title,

            // Get user meta data                   
            // ##
            'name'        => $element->name,
            'position'    => $element->position,
            'type'        => $element->type,
            'signature'   => $element->signature,            
            'picture'     => $element->picture,

            // Get collection meta data                   
            // ##
            'entity_id'   => $element->entity_id,
            'entity_type' => $element->entity_type,
            'created_at'  => $element->created_at,
        );
    }


    /**
     * @param $element
     * @param $userId
     * @return array
     */
    private function assigne_poll_question($element, $userId)
    {
        return array(
            'poll_question' => $element->poll_question,
            'id'            => $element->poll_answer_id,
            'has_answer'    => ($element->has_answer == $userId),
            
            
            // Get user meta data                   
            // ##
            'name'        => $element->name,
            'position'    => $element->position,
            'type'        => $element->type,
            'signature'   => $element->signature,             
            'picture'     => $element->picture,

            // Get collection meta data                   
            // ##
            'entity_id'   => $element->entity_id,
            'entity_type' => $element->entity_type,
            'created_at'  => $element->created_at,         
        );
    }

    /**
     * @param $element
     * @return array
     */
    private function assigne_poll_answer($element)
    {
        return array(
            'content'   => $element->poll_answer_content,
            'id'        => $element->poll_answer_id,
            'count'     => $element->poll_answer_count
        );
    }

    /**
     * @param $element
     * @param $answers
     * @param $collectionOfIds
     */
    private function assigne_poll_answer_collection($element, $answers, $collectionOfIds)
    {
        if(array_key_exists($element->id, $answers)) {

            if(!in_array($element->poll_answer_id, $collectionOfIds)) {

                $total[$element->id] = $total[$element->id] + (int)$element->poll_answer_count;

                array_push($answers[$element->id], $this->assigne_poll_answer($element));
                array_push($collectionOfIds, $element->poll_answer_id);
            }    
        }
        else {
            $answers[$element->id] = array();
            $total[$element->id] = (int)$element->poll_answer_count;
        }
        
           array_push($answers[$element->id], $this->assigne_poll_answer($element));                                
            array_push($collectionOfIds, $element->poll_answer_id);
    }


    /**
     * @param $question
     * @param $answers
     * @param $total
     * @return array
     */
    private function assigne_poll_collection($question, $answers, $total)
    {   
        $collection = array();
        foreach ($question as $key => $value) {            
            
            if($question[$key]['entity_type'] == "1") {
                
                
                $p              = $question[$key];
                array_push($collection, $p);                               
            }
            
            if($question[$key]['entity_type'] == "2") {
                
                $p              = $question[$key];
                array_push($collection, $p);               
            }

            if($question[$key]['entity_type'] == "3") {
                
                $p              = $question[$key];
                $p['answers']   = $answers[$key];
                $p['total']     = $total[$key];            
                array_push($collection, $p);               
                
            }                
        }
        
        return $collection;
    }
}