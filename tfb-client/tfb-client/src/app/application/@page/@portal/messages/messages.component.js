(function () {

    var Controller = function ($timeout, $window, PubSub, UserModel) {

        "@INJECT:__state.js"
        "@INJECT:__events.js"
        "@INJECT:__handlers.js"
        "@INJECT:__flow.js"
    };

    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['$timeout','$window', 'PubSub', 'UserModel'];

    /**
     *
     */
    var Component = {
        templateUrl     : './messages.template.html',
        controllerAs    : 'model',
        controller      : Controller
    };

    angular.module('application.components.page').component('messages', Component);

})();
