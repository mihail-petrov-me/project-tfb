<?php

use \App\Http\Models\Bundle\Bundle;
use \App\Http\Models\User\User;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBundleTeacherTable extends Migration
{
    private $table = Bundle::TABLE_TEACHERS;

    public function up()
    {
        Schema::create($this->table, function(Blueprint $builder)
        {
            //# Primary Key
            $builder->char(Bundle::ID, 13);
            $builder->char(User::TEACHER_ID, 13);

            $builder->primary([Bundle::ID, User::TEACHER_ID]);
        });
    }

    public function down()
    {
        Schema::drop($this->table);
    }
}
