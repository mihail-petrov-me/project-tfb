

/**
 * @type
 * Handler : INIT : SUCCESS
 *
 * @description
 * This method is resposible for handlimng the initialisation of
 * the form component collection
 */
model.handle_init_success = function(response) {
    model.threadCollection = response.data;
};

/**
 * @type
 * Handler : INIT : ERROR
 *
 * @description
 * This method is resposible for handlimng the initialisation of
 * the form component collection
 */
model.handle_init_error = function() {

};


/**
 * @type
 * Handler : fetchMessages : SUCCESS
 *
 * @description
 * This method is resposible for handlimng the initialisation of
 * the form component collection
 */
model.handle_fetchMessages_success = function(response) {

    model.collection = response.data;

    // When the collction of message is populated the system must handle two independat state
    // # Resolve the new system messageon --> send
    // # Resolve the old messages on scroll top action
    UserModel.state("last_message", response.data[response.data.length -1 ].created_at);
    UserModel.state("stack_message", response.data[0].created_at);

    $timeout(function() {
        var data = document.documentElement.scrollTop || document.body.scrollTop
        document.body.scrollTop = $("#placeholder--message").height();
    });
};

/**
 * @type
 * Handler : fetchMessages : ERROR
 *
 * @description
 * This method is resposible for handlimng the initialisation of
 * the form component collection
 */
model.handle_fetchMessages_error = function() {

};


/**
 * @type
 * Handler : fetchMessages : SUCCESS
 *
 * @description
 * This method is resposible for handlimng the initialisation of
 * the form component collection
 */
model.handle_reFetchMessages_success = function(response) {

    // When the collction of message is populated the system must handle two independat state
    var blockHeight = $("#placeholder--message").height();

    UserModel.state('stack_message', response.data[0].created_at);
    model.collection = response.data.concat(model.collection);

    $timeout(function() {
        document.body.scrollTop = ($("#placeholder--message").height() - blockHeight);
    });
};

/**
 * @type
 * Handler : fetchMessages : ERROR
 *
 * @description
 * This method is resposible for handlimng the initialisation of
 * the form component collection
 */
model.handle_reFetchMessages_error = function() {

};


/**
 * @type
 * Handler : fetchMessages : SUCCESS
 *
 * @description
 * This method is resposible for handlimng the initialisation of
 * the form component collection
 */
model.handle_createMessages_success = function(response) {
    model.collection = model.collection.concat(response.data);
};

/**
 * @type
 * Handler : createMessages : ERROR
 *
 * @description
 * This method is resposible for handlimng the initialisation of
 * the form component collection
 */
model.handle_createMessages_error = function() {

};
