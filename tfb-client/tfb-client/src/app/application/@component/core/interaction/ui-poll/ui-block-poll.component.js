(function () {

    /**
     *
     * @param {*} Component
     * @param {*} PubSub
     * @param {*} Translation
     * @param {*} GroupModel
     */
    var Controller = function (Component, PubSub, Translation, GroupModel) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;

        /**
         * @type
         * Publish collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.PUBLISH = {
            'poll'    : PubSub.events.component.POLL.FINISH,
            'finish'  : PubSub.events.component.FINISH
        };


        /**
         * @type
         * Publish collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.SUBSCRIBE = {
            'poll' : PubSub.events.component.POLL.PROCESS
        };


        /**
         * @type
         * State collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.STATE = {
            IS_OPTION_INPUT_VISIBLE         : false,
            IS_ADD_OPTION_BUTTON_VISIBLE    : false
        };

        /**
         * @type
         * Field collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.FIELD = {
            QUESTION    : null,
            OPTION      : null
        };


        /**
         * @type
         * Property collection
         *
         * @description
         * The property collection contain every single state thro out the current component
         */
        model.PROPERTY = {
            COLLECTION  : [],
            REFERENCE   : 0
        };

        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(function($provider) {

            model.LABEL = {

                BUTTON_ADD_OPTION   : $provider({
                    'en'    : 'Ask my anythiong',
                    'bg'    : 'Задай въпроса на анкетата'
                }),

                INPUT_QUESTION     : $provider({
                    'en'    : 'Add option',
                    'bg'    : 'Добави възможен отговор'
                }),

                INPUT_OPTION        : $provider({
                    'en'    : 'Add option',
                    'bg'    : 'Добави'
                })
            };
        });


        /**
         * @type
         * Event : on
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.on_questionInput = function(data) {
            model.STATE.IS_OPTION_INPUT_VISIBLE = (model.FIELD.QUESTION.length > 0);
        };


        model.on_optionInput = function() {
            model.STATE.IS_OPTION_BUTTON_VISIBLE = (model.FIELD.OPTION.length > 0);
        };


        /**
         * @type
         * Event : on
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.on_remove = function(element) {

            for(var i = 0; i < model.PROPERTY.COLLECTION.length; i++) {
                if(model.PROPERTY.COLLECTION[i].id == element.id) {
                    model.PROPERTY.COLLECTION.splice(i, 1);
                }
            }
        };

        /**
         * @type
         * Event : on
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.on_edit = function() {

        };


        /**
         * @type
         * Event : on
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.on_add = function() {

            model.PROPERTY.COLLECTION.push({
                'id'        : model.PROPERTY.REFERENCE++,
                'content'   : model.FIELD.OPTION
            });

            model.FIELD.OPTION = null
            model.STATE.IS_ADD_OPTION_BUTTON_VISIBLE = false;
        };

        /**
         * @type
         * Event : init
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.init_process = function() {

            PubSub.api.publish(model.PUBLISH.poll, {
                'question'  : model.FIELD.QUESTION,
                'options'   : model.PROPERTY.COLLECTION
            });

            PubSub.api.publish(model.PUBLISH.finish);
        };


        /**
         * @type
         * Event : init
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.vote_up = function($element) {

            PubSub.api.publish('upvote_poll', {
                'answer_id' : $element.id,
                'entity_id' : model.uiId
            });
        };


        /**
         * @type
         * Event : init
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.$init_interactive = function () {

            Component.$interactive(model.uiMode, function () {

                model.STATE.IS_MODE_INTERACTIVE     = true;
                model.STATE.IS_MODE_PRESENTATION    = false;

                PubSub.api.subscribe(model.SUBSCRIBE.poll, model.init_process);
            });
        };

        /**
         * @type
         * Event : init
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.$init_presentation = function () {

            Component.$presentation(model.uiMode, function () {

                model.STATE.IS_MODE_PRESENTATION    = true;
                model.STATE.IS_MODE_INTERACTIVE     = false;


                model.FIELD.QUESTION        = model.uiQuestion;
                model.PROPERTY.COLLECTION   = model.uiAnswers;


                PubSub.api.subscribe('update_poll_after_vote', function($response) {

                    if($response.entity_id == model.uiId) {

                        model.uiHasAnswer       = $response.has_answer
                        model.FIELD.COLLECTION  = $response.answers;
                    }
                })
            });
        };

        /**
         * @type
         * Event : init
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.parseGraph = function($count) {

            if(model.uiTotal) {
                return 'width : ' + ((100 * $count) / model.uiTotal) + '%';
            }

            return 'width: 0';
        }


        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        model.$onInit = function () {

            model.$init_interactive();
            model.$init_presentation();
        };

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component destroy
         */
        model.$onDestroy = function () {
            PubSub.api.unsubscribe(model.SUBSCRIBE.poll);
        };
    }

    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['Component', 'PubSub', 'Translation', 'GroupModel'];

    var Component = {
        templateUrl : './ui-block-poll.template.html',
        controller  : Controller,
        controllerAs: 'model',
        bindings        : {
            uiQuestion  : '<',
            uiAnswers   : '<',
            uiMode      : '<',
            uiId        : '<',
            uiHasAnswer : '<',
            uiTotal     : '<'
        }
    };

    angular.module('application.components.visual').component('uiBlockPoll', Component);

})();
