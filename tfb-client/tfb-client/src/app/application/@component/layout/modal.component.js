(function () {

    /**
     *
     */
    var Controller = function (PubSub) {

        this.on_close = () => {
            PubSub.api.publish('close_modal_window');
        };
    };

    Controller.$inject = ['PubSub'];

    var Component = {
        templateUrl     : './modal.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        transclude      : true,
        bindings        : {
            uiTitle     : '<'
        }
    };

    angular.module('application.components.visual').component('modal', Component);
})();
