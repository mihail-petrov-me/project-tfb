(function () {

    var Controller = function(Translation) {

        this.name = "FORM_CREATE_CLASS";

        /**
         * @component
         * Fields collection
         */

        this.FIELD = {

            "bundle_type": {

                name            : "bundle_type",
                type            : "dropdown",
                title           : 'Ниво на образование',
                value           : null,
                show_errors     : false,
                isRequired      : true
            },

            "bundle_position": {

                name            : "bundle_position",
                type            : "dropdown",
                title           : 'Ниво на образование',
                value           : null,
                show_errors     : false,
                isRequired      : true
            },

            "bundle_label": {

                name            : "bundle_label",
                type            : "dropdown",
                title           : 'Ниво на образование',
                value           : null,
                show_errors     : false,
                isRequired      : true
            }
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                OPTIONS_DROPDOWN_TYPE : $provider({
                    "bg"    : [
                        { 'id': 1 , 'title': 'Стандартен клас' },
                        { 'id': 2 , 'title': 'Смесена група' }
                    ],
                    "en"    : [
                        { 'id': 1 , 'title': 'Classic bundle' },
                        { 'id': 2 , 'title': 'General group' }
                    ]
                }),


                OPTIONS_DROPDOWN_POSITION : $provider({
                    "bg"    : [
                        { 'id': 1 , 'title': '1' },
                        { 'id': 2 , 'title': '2' },
                        { 'id': 3 , 'title': '3' },
                        { 'id': 4 , 'title': '4' },
                        { 'id': 5 , 'title': '5' },
                        { 'id': 6 , 'title': '6' },
                        { 'id': 7 , 'title': '7' },
                        { 'id': 8 , 'title': '8' },
                        { 'id': 9 , 'title': '9' },
                        { 'id': 10, 'title': '10' },
                        { 'id': 11, 'title': '11' },
                        { 'id': 12, 'title': '12' }
                    ],
                    "en"    : [
                        { 'id': 1 , 'title': '1' },
                        { 'id': 2 , 'title': '2' },
                        { 'id': 3 , 'title': '3' },
                        { 'id': 4 , 'title': '4' },
                        { 'id': 5 , 'title': '5' },
                        { 'id': 6 , 'title': '6' },
                        { 'id': 7 , 'title': '7' },
                        { 'id': 8 , 'title': '8' },
                        { 'id': 9 , 'title': '9' },
                        { 'id': 10, 'title': '10' },
                        { 'id': 11, 'title': '11' },
                        { 'id': 12, 'title': '12' }
                    ]
                }),

                OPTIONS_DROPDOWN_LABEL : $provider({
                    "bg"    : [
                        { 'id': 'А' , 'title': 'А' },
                        { 'id': 'Б' , 'title': 'Б' },
                        { 'id': 'В' , 'title': 'В' },
                        { 'id': 'Г' , 'title': 'Г' },
                        { 'id': 'Д' , 'title': 'Д' },
                        { 'id': 'Е' , 'title': 'Е' },
                        { 'id': 'Ж' , 'title': 'Ж' },
                        { 'id': 'З' , 'title': 'З' },
                        { 'id': 'И' , 'title': 'И' },
                        { 'id': 'Й' , 'title': 'Й' },
                        { 'id': 'К' , 'title': 'К' },
                        { 'id': 'Л' , 'title': 'Л' }
                    ],
                    "en"    : [
                        { 'id': 'А' , 'title': 'А' },
                        { 'id': 'Б' , 'title': 'Б' },
                        { 'id': 'В' , 'title': 'В' },
                        { 'id': 'Г' , 'title': 'Г' },
                        { 'id': 'Д' , 'title': 'Д' },
                        { 'id': 'Е' , 'title': 'Е' },
                        { 'id': 'Ж' , 'title': 'Ж' },
                        { 'id': 'З' , 'title': 'З' },
                        { 'id': 'И' , 'title': 'И' },
                        { 'id': 'Й' , 'title': 'Й' },
                        { 'id': 'К' , 'title': 'К' },
                        { 'id': 'Л' , 'title': 'Л' }
                    ]
                }),

                LABEL_DROPDOWN_TYPE :    $provider({
                    "bg"    : "Тип на класа",
                    "en"    : "Type of bundle"
                }),

                LABEL_DROPDOWN_POSITION :    $provider({
                    "bg"    : "Номер на класа",
                    "en"    : "Class number"
                }),

                LABEL_DROPDOWN_LABEL :    $provider({
                    "bg"    : "Буква на класа",
                    "en"    : "Class label"
                }),

                LABEL_EDUCATION :    $provider({
                    "bg"    : "Добави нов клас",
                    "en"    : "Add new class"
                })
            };
        });

        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {
            this.parent.FIELD  = this.FIELD;
            this.formContact   = this.parent.formContact;
        }
    };


    /**
     * @type
     * Component injector
     */
    Controller.$inject = ['Translation', 'PubSub', 'Form'];
    var Component = {
        templateUrl     : './form-create-class.template.html',
        controller      : Controller,
        controllerAs    : 'model',

        require             : {
            parent          : '^formWrapper'
        },

        bindings        : {
            uiCollection    : '<',
            uiIsDisabled    : '<',
            uiPresentation  : '<',
            uiId            : '<'
        }
    };

    angular.module('application.components.page').component('formCreateClass', Component);
})();
