<?php
namespace App\Http\Models\Collections;

use App\Http\Models\Model;
use DB;

// # Fetch Collection
// # ===========================================================================
use App\Http\Models\User\Fetch\Collection\FetchUserCollection;
use App\Http\Models\User\Fetch\Collection\FetchUserGroupCollection;
use App\Http\Models\User\Fetch\Collection\FetchUserProjectCollection;
use App\Http\Models\User\Fetch\Collection\FetchUserInitiativeCollection;
use App\Http\Models\User\Fetch\Collection\FetchUserCommentCollection;

// # Fetch Single
// # ===========================================================================
use App\Http\Models\User\Fetch\Single\FetchUserSignature;
use App\Http\Models\User\Fetch\Single\FetchUserContact;
use App\Http\Models\User\Fetch\Single\FetchUserEducation;
use App\Http\Models\User\Fetch\Single\FetchUserEmployment;
use App\Http\Models\User\Fetch\Single\FetchUserLanguage;
use App\Http\Models\User\Fetch\Single\FetchUserExperiance;

// # Update
// # ===========================================================================
use App\Http\Models\User\Update\UpdateUserContact;
use App\Http\Models\User\Update\UpdateUserEducation;
use App\Http\Models\User\Update\UpdateUserEmployment;
use App\Http\Models\User\Update\UpdateUserExperince;
use App\Http\Models\User\Update\UpdateUserLanguage;

// # Create
// # ===========================================================================
use App\Http\Models\User\Create\CreateUser;

/**
 * Class CollectionComment
 * @package App\Http\Models\Collections
 */
class CollectionComment extends Model
{
    // Public fields
    const ID                = "comment_id";
    const ENTITY_ID         = "entity_id";
    const CONTENT           = "comment_content";

    // Table
    const TABLE             = "collection_comment";
}