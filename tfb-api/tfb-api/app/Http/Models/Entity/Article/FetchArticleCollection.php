<?php

namespace App\Http\Models\Entity\Article;
use DB;

class FetchArticleCollection
{

    public function execute($id, $query) 
    {
        
        return DB::table("block_article", $query)
            ->leftJoin(
                    'collection_entity', 
                    'block_article.id', '=', 'collection_entity.entity_id')
                
            ->leftJoin(
                    'users', 
                    'block_article.user_id', '=', 'users.id')
                
            ->leftJoin(
                    'user_experience', 
                    'user_experience.user_id', '=', 'users.id')
                
            ->select(
                    // Get content data
                    // #
                    'block_article.content AS article_content',
                    'block_article.title AS article_title',
                    'block_article.created_at AS created_at',                    
                    
                    // Get user meta data
                    // #
                    'users.name',
                    'users.picture',
                    'users.signature',
                    
                    // Get collection meta data                   
                    // #
                    'collection_entity.entity_id',
                    'collection_entity.entity_type'                    
            )                            
            ->where(
                    'collection_entity.collection_id', $id)
            ->orderBy(
                    'block_article.created_at', 'desc');
    }
}
