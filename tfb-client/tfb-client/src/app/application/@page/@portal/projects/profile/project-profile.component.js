(function () {


    var Controller = function (PubSub, Translation, ProjectModel, UserModel, SessionService) {

        /**
         * @type
         * State collection
         */
        this.STATE = {
            SHOW_GROUP              : false,
            SUB_TASK_CONTENT        : null,
            IS_SIDEBAR_VISIBLE      : false,
            IS_ADD_NEW_TASK         : false,
            IS_ADD_NEW_SUB_TASK     : false,
            IS_DESCRIPTION_ACTIVE   : false,

            show_datepicker         : false,
            show_member             : false,


            // Not a state
            element_index           : null
        };

        this.COLLECTION = {
            task            : [],
            comment         : [],
            sub_task        : [],
            group           : [],
            user            : []
        };

        this.ENTITY = {
            user_id             : UserModel.profile.my("user_id"),
            project_id          : null,
            project_title       : SessionService.get('tfb:project_collection', true)['project_title'],

            group_id            : null,
            task_parent_id      : null,
            task_id             : null,
            task_title          : null,
            task_description    : null,
            task_comment_content: null,
            task_is_completed   : null,
            task_user           : null
        };

        this.INPUT = {
            task_title          : null,
            group_title         : null
        };


        this.INDEX = {
            selected_task       : null,
        }


        this.dateValue = null;


        /**
         * @type
         * Event: ON
         */
        this.on_remove_task = ($id, $index) => {

            let handler = ProjectModel.api.archive.task($id);

            handler.then(($response) => {
                this.COLLECTION.task.splice($index, 1);
                this.STATE.IS_SIDEBAR_VISIBLE = false;
            });

            handler.catch(() => {
                console.log("@Exception: Could not remove task");
            });
        };


        /**
         * @event
         * => CHANGE
         *
         * @action
         * => STATE CHANGE
         */
        this.on_select_task = ($element, $index) => {

            // Show task detail view
            this.STATE.IS_SIDEBAR_VISIBLE  = true;

            // get task meta information
            this.ENTITY.task_title          = $element.task_title;
            this.ENTITY.task_description    = $element.task_description;
            this.ENTITY.task_id             = $element.task_id;
            this.ENTITY.group_id            = $element.group_id;


            console.log(this.STATE.IS_SIDEBAR_VISIBLE);
        };


        /**
         * @event
         * => CHANGE

         * @action
         * => STATE CHANGE
         */
        this.on_change_task_description = () => {
            this.STATE.IS_DESCRIPTION_ACTIVE = true;
        };

        /**
         * @event
         * => ON

         * @action
         * => HTTP REQUEST
         */
        this.on_update_taskDescription = () => {

            let handler = ProjectModel.api.update.task(this.ENTITY.project_id,  this.ENTITY.task_id, this.ENTITY);

            handler.then(($response) => {
                this.STATE.IS_DESCRIPTION_ACTIVE = false;
            });

            handler.catch(() => {
                console.log("@Exception: Could not update task description");
            });
        };

        /**
         * @event
         * => ON

         * @action
         * => HTTP REQUEST
         */
        this.on_create_taskComment = () => {

            let handler = ProjectModel.api.create.comment(this.ENTITY.project_id,  this.ENTITY.task_id, this.ENTITY);

            handler.then(($response) => {
                console.log($response);
            });

            handler.catch(() => {
                console.log("@Exception: Could not update task");
            });
        };

        /**
         * @event
         * => ON

         * @action
         * => STATE CHANGE
         * => HTTP REQUEST
         */
        this.on_complete_task = ($event, $element) => {

            this.on_select($element);
            angular.element($event.currentTarget).toggleClass('ui-checkbox--checked');

            this.ENTITY.task_is_completed = angular.element($event.currentTarget).hasClass('ui-checkbox--checked') ? 1 : 0;

            let handler = ProjectModel.api.update.task(this.ENTITY.project_id,  this.ENTITY.task_id, this.ENTITY);
            handler.then(($response) => {

            });

            handler.catch(() => {

            });
        };


        /**
         * @event
         * => ON

         * @action
         * => STATE CHANGE
         */
        this.on_init_datePicker = ($id, $index) => {
            this.STATE.show_datepicker = !this.STATE.show_datepicker;
        };

        /**
         * @event
         * => ON

         * @action
         * => STATE CHANGE
         * => HTTP REQUEST
         */
        this.on_init_memberPicker = ($id, $index) => {

            // @
            this.STATE.show_memberpicker    = !this.STATE.show_memberpicker;
            this.STATE.element_index        = $index;
            // @

            let handler = UserModel.api.fetch.users();

            handler.then(($response) => {
                this.COLLECTION.user = $response.data;
            });

            handler.catch(($response) => {
                console.log("@Exception: Could not fetch user collection");
            })
        };

        /**
         * @event
         * => ON

         * @action
         * => STATE CHANGE
         */
        this.on_init_group = () => {
            this.STATE.SHOW_GROUP = true;
        };

        /**
         * @event
         * => ON
         *
         * @action
         * => HTTP REQUEST
         */
        this.on_create_group = () => {

            let handler = ProjectModel.api.create.group(this.ENTITY.project_id, { 'group_title' : this.INPUT.group_title });

            handler.then(($response) => {
                this.COLLECTION.group.push($response.data);
            });

            handler.catch(() => {
                console.log("@Exception: Could not create new group");
            });
        };


        /**
         * @event
         * => ON
         *
         * @action
         * => HTTP REQUEST
         */
        this.on_create_taskSubTask = () => {

            let handler = ProjectModel.api.create.task(this.ENTITY.project_id, this.ENTITY)

            handler.then(($response) => {

                this.COLLECTION.task.push($response.data);
                this.ENTITY.task_title     = null;
            });

            handler.catch(() => {
                console.log("@Exception: Could not create new task");
            });
        };


        /**
         *
         */
        this.$onInit = () => {

            PubSub.form.end('FORM_CREATE_TASK', ($response) => {

                this.ENTITY.task_title = $response.task_title;

                let handler = ProjectModel.api.create.task($response.group_id, this.ENTITY);

                    handler.then(($response) => {

                        // find the group item in collection
                        for(var i = 0; i < this.COLLECTION.group.length; i++) {
                            if(this.COLLECTION.group[i].group_id == $response.data.group_id) {
                                this.COLLECTION.group[i].tasks.push($response.data);
                            }
                        }

                        this.INPUT.task_title = null;
                    });

                    handler.catch(() => {
                        console.log("@Exception: Could not create new task");
                    });
            });


            PubSub.api.subscribe('UI_MEMBER_PICKER::ITEM_SELECTED', ($element) => {

                this.ENTITY.task_user = JSON.stringify([$element.user_id]);

                let handler = ProjectModel.api.update.task(this.ENTITY.project_id, this.ENTITY.task_id, this.ENTITY);

                handler.then(() => {

                    // find the group item in collection
                    for(var i = 0; i < this.COLLECTION.group.length; i++) {
                        if(this.COLLECTION.group[i].group_id == $response.data.group_id) {
                            this.COLLECTION.group[i].tasks.push($response.data);
                        }
                    }

                    this.INPUT.task_title = null;
                });

                handler.catch(() => {
                    console.log("@Exception: Could not create new task");
                });
            });
        };



        /**
         * @type
         * Router event
         */
        this.$routerOnActivate = function (next, previous) {

            this.ENTITY.project_id  = next.urlPath;

            let handler = ProjectModel.api.fetch.tasks(this.ENTITY.project_id);

            handler.then(($response) => {
                this.COLLECTION.group = $response.data;
            });

            handler.catch(() => {
                console.log("@Exception: Could not fetch task collection");
            });
        };
    };

    /**
     * @type
     * Component injector
     */
    Controller.$inject = [
        'PubSub',
        'Translation',
        'ProjectModel',
        'UserModel',
        'SessionService'
    ];

    var Component = {
        templateUrl     : './project-profile.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        $routerCanActivate: function () {
            return true;
        }
    };

    angular.module('application.components.page').component('projectProfile', Component);
})();
