// Core components
// # =============================================
var fs        = require('fs-extra');
var util      = require('../helpers/util');

var componentTemplate = function(template) {
    return `(function() {
        ${template}
})();`;
}


// API
// # =============================================


/**
 *
 */
exports.exec = function(source) {

  util.traverse(source, function(file, directory, filePath) {

    var path = directory.replace(source, '');
    var ffile = componentTemplate(fs.readFileSync(filePath).toString());
    fs.writeFileSync(filePath, ffile);
  });
};

/**
 *
 */
exports.single = function(source) {

    var collection  = source.split('/');
    var last        = collection.slice(-1)[0];
    var path        = source.replace(last, "");

    var ffile        = componentTemplate(fs.readFileSync(source).toString());
    fs.writeFileSync(source, ffile);
};
