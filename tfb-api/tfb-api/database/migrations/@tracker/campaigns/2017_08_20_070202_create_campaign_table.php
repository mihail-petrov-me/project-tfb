<?php

use \App\Http\Models\Campaign\Campaign;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignTable extends Migration
{
    private $table = Campaign::TABLE;

    public function up()
    {
        Schema::create($this->table, function(Blueprint $builder)
        {
            //# Primary Key
            $builder->char(Campaign::ID, 13)->primary();

            //# Table Entity Collection
            $builder->tinyInteger(Campaign::TYPE);
            $builder->string(Campaign::TITLE, 200);

            //# Meta
            $builder->timestamps();
            $builder->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop($this->table);
    }
}
