(function () {

    /**
     *
     * @param {*} Translation
     * @param {*} PubSub
     */
    var Controller = function (PubSub, UserModel) {


        /**
         * @type
         * Event
         */
        this.$onInit = () => {

        }
    };

    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject  = ['Translation', 'PubSub', 'UserModel'];

    var Component       = {
        templateUrl     : './ui-top.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.visual').component('uiTop', Component);

})();
