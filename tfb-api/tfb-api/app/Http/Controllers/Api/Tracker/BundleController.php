<?php
namespace App\Http\Controllers\Api\Tracker;

use App\Http\Models\Bundle\Bundle;
use Illuminate\Database\QueryException;
use \Illuminate\Http\Request;
use \App\Http\Controllers\Controller;
use \Illuminate\Http\JsonResponse;

/**
 * Class BundleController
 * @package App\Http\Controllers\Api\Tracker
 */
class BundleController extends Controller
{

    private $API_ERROR_MESSAGES = [
        'FETCH' => 'Requested entity was not found'
    ];


    private $model;

    /**
     *
     * @param Bundle $model
     * @internal param AuthenticationService $provider
     * @internal param User $user
     */
    public function __construct(Bundle $model)
    {
        $this->model = $model;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param Request $reques
     */
    public function fetchBundleCollection(Request $request): JsonResponse
    {
        $collection = $this->model->collection($request->query());
        return $this->responseSuccess($collection);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function fetchBundleSpecialCollection(Request $request): JsonResponse
    {
        $collection = $this->model->collectionSpecial($request->query());
        return $this->responseSuccess($collection);
    }

    /**
     * @param $id
     *
     * @return JsonResponse
     */
    public function fetchBundleSingle($id): JsonResponse
    {
        $collection = $this->model->single($id);
        return $this->responseSuccess($collection);
    }

    /**
     * @param         $id
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function fetchStudentCollection($id, Request $request): JsonResponse
    {
        $collection = $this->model->collectionStudent($id);
        return $this->responseSuccess($collection);
    }

    /**
     * @param         $id
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function fetchGoalCollection($id, Request $request): JsonResponse
    {
        $collection = $this->model->collectionGoal($id);
        return $this->responseSuccess($collection);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createBundle(Request $request)
    {
        try {
            $collection = $this->model->create($request->json()->all());
            return $this->responseSuccess($collection);
        }
        catch (QueryException $exception)
        {
            return $this->responseNotFound('Entity already exists');
        }
    }

    /**
     * @param         $id
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createStudent($id, Request $request): JsonResponse
    {
        $collection = $this->model->create_student($id, $request->json()->all());
        return $this->responseSuccess($collection);
    }


    /**
     * @param         $id
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function updateBundle($id, Request $request)
    {
        $collection = $this->model->update($id, $request->json()->all());
        return $this->responseSuccess( $collection);
    }

    /**
     * @param         $id
     * @param         $goalId
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function updateGoal($id, $goalId, Request $request): JsonResponse
    {
        $collection = $this->model->update_goal($id, $goalId, $request->json()->all());
        return $this->responseSuccess( $collection);
    }

    /**
     * @param         $id
     * @param         $studentId
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function updateStudent($id, $studentId, Request $request): JsonResponse
    {
        $collection = $this->model->update_student($id, $studentId, $request->json()->all());
        return $this->responseSuccess( $collection);
    }


    /**
     * @param         $id
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addBundleToTeacher($id, Request $request): JsonResponse
    {
        $collection = $this->model->add($id, $request->json()->all());
        return $this->responseSuccess( $collection);
    }


    /**
     * @param $id
     *
     * @return JsonResponse
     */
    public function deleteBundle($id): JsonResponse
    {
        $collection = $this->model->delete($id);
        return $this->responseSuccess($collection);
    }

    /**
     * @param         $id
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createGoal($id, Request $request): JsonResponse
    {
        $collection = $this->model->create_goal($id, $request->json()->all());
        return $this->responseSuccess( $collection);
    }


    /**
     * @param $bundleId
     * @param $studentId
     *
     * @return JsonResponse
     */
    public function deleteStudent($bundleId, $studentId): JsonResponse
    {
        $this->model->deleteStudent($bundleId, $studentId);
        return $this->responseSuccess();
    }

    /**
     * @param $bundleId
     * @param $goalId
     *
     * @return JsonResponse
     */
    public function archiveGoal($bundleId, $goalId): JsonResponse
    {
        $this->model->deleteGoal($bundleId, $goalId);
        return $this->responseSuccess();
    }
}