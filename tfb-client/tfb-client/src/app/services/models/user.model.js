(function () {

    /**
     *
     */
    var AuthenticationCollection = {

        "app" : {

        },

        "google" : {
            "client_id"     : "152370577815-1575o6o4shgl2rvmhic0e98cpdd24l9i.apps.googleusercontent.com",
            "redirect_uri"  : (window.location.origin),
            "grant_type"    : "authorization_code"
        }
    };


    /**
     * @type :
     * Base url
     */
    var BASE_URL = "users";

    /**
     * @type
     * User profile
     */
    var ProfileProvider = function(SessionService) {

        let PROVIDER_KEY = 'data';


        /**
         * @type
         * Data Provider
         *
         * Return data set from the user profile
         *
         */
        function my($key) {
            return __provide(PROVIDER_KEY, $key);
        }

        /**
         *
         */
        function position() {

            if(__provide(PROVIDER_KEY, 'position')) {
                return __provide(PROVIDER_KEY, 'position');
            }
            else {
                var $id  = __provide(PROVIDER_KEY, 'type')
                return ($id == 1) ? 'Алумни' : 'Учител';
            }
        }

        /**
         * @type
         * Data Provider
         *
         * Return data set from the user profile
         *
         */
        function groups($collection) {

            if($collection) {
                var $c = SessionService.getCollection();
                $c['groups'].push($collection);
                SessionService.setCollection($c);
            }
            return SessionService.getCollection()['groups'];
        }

        /**
         * @type
         * Data Provider
         *
         * Return data set from the user profile
         *
         */
        function projects($collection) {
            return SessionService.getCollection()['projects'];
        }

        /**
         * @type
         * Data Provider
         *
         * Return data set from the user profile
         *
         */
        function initiatives($collection) {
            return SessionService.getCollection()['collection'];
        }


        /**
         * @type
         * Session Provider
         *
         * Return data set from application session
         *
         */
        function __provide($what, $key) {

            let $resource = SessionService.getTokken();

            if($resource) {
                return ($key) ? $resource[$what][$key] : $resource[$what];
            }

            return null;
        }


        /**
         *
         * @param {*} data
         */
        function setSession(data) {
            SessionService.setSession(data);
        }

        /**
         *
         * @param {*} data
         */
        function setCollection(data) {
            SessionService.setCollection(data);
        }

        /**
         *
         * @param {*} data
         */
        function setPermission(data) {
            SessionService.setPermission(data);
        }

        /**
         *
         * @param {*}
         */
        function isPermited($collection) {

            var permissionsCollection = SessionService.getPermission();

            var intersect = function(a, b) {
                return a.filter(Set.prototype.has, new Set(b));
            }

            return (intersect($collection, permissionsCollection).length > 0);
        }


        /**
         *
         */
        function isAuthenticated() {
            if(SessionService.getSession()) return true;
        }

        /**
         *
         */
        function isAuthorized() {
            return (isAuthenticated()) ? my("is_authorized") : false;
        }

        /**
         *
         */
        function destroy() {
            return SessionService.destroy();
        }

        /**
         *
         * @param {*} ofWhat
         * @param {*} modelId
         */
        function isMember(ofWhat, modelId) {

            var collection = (groups());
            for(var i = 0; i < collection.length; i++) {
                if(collection[i].id == modelId) return true;
            }
            return false;
        }

        /**
         *
         * @param {*} ofWhat
         * @param {*} modelId
         */
        function isAdmin(ofWhat, modelId) {

        }

        /**
         *
         * @param {*} ofWhat
         * @param {*} modelId
         */
        function isOwner(ofWhat, modelId) {

        }


        /**
         * @type
         * Api
         */
        return {
            my              : my,
            position        : position,
            groups          : groups,
            projects        : projects,
            initiatives     : initiatives,

            setSession      : setSession,
            setCollection   : setCollection,
            setPermission   : setPermission,

            destroy         : destroy,
            isAuthenticated : isAuthenticated,
            isAuthorized    : isAuthorized,

            isMember        : isMember,
            isOwner         : isOwner,
            isAdmin         : isAdmin,

            isPermited      : isPermited
        };
    };

    /**
     * @type
     * User creation services
     *
     * @description
     * Contains collection of methods for creating an user related items
     */
    var create = function(request) {

        return {
            messages    : ($collection)      => request.post([BASE_URL, "messages"], $collection),
            message     : ($collection)      => request.post([BASE_URL], $collection),
            user        : ($collection)      => request.post([BASE_URL], $collection),
            subscribe   : ($collection)      => request.post([BASE_URL, 'subscribe'], $collection),
            comment     : ($collection)      => request.post([BASE_URL, 'comments'], $collection),
            education   : ($id, $collection) => request.post([BASE_URL, $id, 'education'], $collection),
            employment  : ($id, $collection) => request.post([BASE_URL, $id, 'employment'], $collection)
        };
    };


    /**
     * @type
     * User update services
     *
     * @description
     * Contains collection of methods for updating an user related items
     */
    var update = function(request) {

        return {
            contact     : ($id, $body) => request.put([BASE_URL, $id, "contact"],  $body),
            education   : ($id, $body) => request.put([BASE_URL, $id, "education"],  $body),
            employment  : ($id, $body) => request.put([BASE_URL, $id, "employment"],  $body),
            language    : ($id, $body) => request.put([BASE_URL, $id, "language"],  $body),
            picture     : ($id, $body) => request.upload([BASE_URL, $id, "picture"],  $body)
        };
    };


    /**
     * @type
     * User fetch services
     *
     * @description
     * Contains collection of methods for fetching an user related items
     */
    var fetch = function(request, requestType) {

        return {
            users           : ($query)   => request.beffore(requestType).get([BASE_URL], $query),
            signature       : ($id)      => request.beffore(requestType).get([BASE_URL, $id, "signature"]),
            contact         : ($id)      => request.beffore(requestType).get([BASE_URL, $id, "contact"]),
            education       : ($id)      => request.beffore(requestType).get([BASE_URL, $id, "education"]),
            employment      : ($id)      => request.beffore(requestType).get([BASE_URL, $id, "employment"]),
            language        : ($id)      => request.beffore(requestType).get([BASE_URL, $id, "language"]),
            comments        : ($id)      => request.beffore(requestType).get([BASE_URL, $id, "comments"]),
            experiance      : ($id)      => request.beffore(requestType).get([BASE_URL, $id, "experiance"]),

            // ===================================
            teachers        : () => request.beffore(requestType).get([BASE_URL], { type : '1'}),
            alumnies        : () => request.beffore(requestType).get([BASE_URL], { type : '2'}),
            employees       : () => request.beffore(requestType).get([BASE_URL], { type : '3'}),
            coordinators    : () => request.beffore(requestType).get([BASE_URL], { type : '4'}),
        };
    }

    /**
     * @type
     * User fetch services
     *
     * @description
     * Contains collection of methods for archiving an user related items
     */
    var archive = function(request, base, provider) {

        return {};
    };

    /**
     *
     * @param {*} request
     * @param {*} base
     * @param {*} provider
     */
    var ApiProvider = function(request) {

        var requestType = { type : 'collection' };

        return {

            PAGINATE : function() {
                requestType.type = 'pagination';
                return this;
            },

            REFRESH : function() {
                requestType.type = 'refresh';
                return this;
            },

            COLLECTION : function() {
                requestType.type = 'collection';
                return this;
            },

            SINGLE : function() {
                requestType.type = 'single';
                return this;
            },

            create  : create(request),
            fetch   : fetch(request, requestType),
            update  : update(request),
            archive : archive(request)
        }
    };

    /**
     *
     * @param {*} http
     * @param {*} q
     * @param {*} AppManager
     */
    var ServiceHandler = function (Request, SessionService) {

        return {

            /**
             *
             */
            profile : ProfileProvider(SessionService),

            /**
             *
             */
            api     : ApiProvider(Request),

            /**
             *
             */
            auth    : function($provider, collection) {

                var requestObject = angular.extend({}, AuthenticationCollection[$provider], collection);
                return Request.post([BASE_URL, "auth", $provider], requestObject);
            },

            /**
             *
             */
            state : function($key, $value) {

                if($value) {
                    SessionService.set(("__id__" + $key), $value);
                }

                return SessionService.get(("__id__" + $key));
            }
        };
    };

    ServiceHandler.$inject = ['Request', 'SessionService'];
    angular.module('application.models').factory('UserModel', ServiceHandler);
})();
