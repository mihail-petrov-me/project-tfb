<?php
namespace App\Http\Models\Subject\Create;

use App\Http\Models\Repository;
use DB;

class CreateSubject extends Repository
{
    public function execute($collection)
    {
        $entity = $this->build($collection);
        DB::table('teacher__subject')->insert($entity);

        return $entity;
    }

    /**
     * @param $collection
     * @return array
     */
    public function build($collection)
    {
        $c = array();
        foreach ($collection as $element) {
            array_push($c, array(
            'subject_id'    => uniqid(),
            'teacher_id'    => $this->process($element, 'teacher_id'),
            'bundle_id'     => $this->process($element, 'bundle_id'),
            'subject_title' => $this->process($element, 'subject_title'),
            ));
        }

        return $c;
    }
}