<?php 

namespace App\Http\Controllers;

use Exception;
use Laravel\Lumen\Routing\Controller as BaseController;
use \Illuminate\Support\Facades\Input;
use Illuminate\Http\JsonResponse;

use RuntimeException;

/**
 * Class Controller
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{
    /**
     * @var array
     */
    public static $STATUS_NOT_FOUND = [
        'code'      => 404,
        'message'   => 'Entity not found',
        'key'       => 'error'
    ];

    /**
     * @var array
     */
    public static $STATUS_SUCCESS = [
        'code'      => 200,
        'message'   => 'Success',
        'key'       => 'success'
    ];

    /**
     * @var array
     */
    public static $STATUS_UNAUTHORIZED = [
        'code'      => 401,
        'message'   => 'You don\'t have permission to access this resource',
        'key'       => 'error'
    ];

    /**
     * @var string
     */
    private $resourceTypeExceptionMessage = 'Response type must be provided in order to create the response';

    /**
     * @var number
     */
    private $responseStatus;

    /**
     * @var string
     */
    private $responseMessage;

    /**
     * @var array
     */
    private $responseData;

    /**
     * @var array
     */
    private $responseType;

    /**
     * @var array
     */
    private $responseMeta;

    /**
     * @param null $data
     * @param null $meta
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseSuccess($data = null, $meta = null): JsonResponse
    {
        if(null === $data) {
            return $this->success()->status(200)->response();
        }
        return $this->success()->status(200)->data($data)->meta($meta)->response();
    }

    /**
     * @param string|null $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseNotFound($message = null): JsonResponse
    {
        return $this->error()->status(404)->message($message)->response();
    }

    /**
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function responseUnauthorized($message = null): JsonResponse
    {
        return $this->unauthorized()->status(401)->message($message)->response();
    }

    /**
     * @return int
     */
    public function requestLimit(): int
    {
        return Input::get('limit') ?: 10;
    }

    /**
     * 
     * @param int $status
     * @return $this
     */
    private function status($status)
    {
        $this->responseStatus = $status;
        return $this;
    }

    /**
     * 
     * @param string $message
     * @return $this
     */
    private function message($message)
    {
        $this->responseMessage = $message;
        return $this;
    }

    /**
     * @param $collection
     *
     * @return $this
     */
    private function meta($collection)
    {        
        $this->responseMeta = $collection;
        return $this;
    }

    /**
     * 
     * @param array $data
     * @return $this
     */
    private function data($data)
    {
        $this->responseData = $data;
        return $this;
    }    

    /**
     *
     * @return $this
     */
    private function success()
    {
        $this->responseType = self::$STATUS_SUCCESS;
        return $this;
    }

    /**
     *
     * @return $this
     */
    private function error()
    {
        $this->responseType = self::$STATUS_NOT_FOUND;
        return $this;
    }

    /**
     *
     * @return $this
     */
    private function unauthorized()
    {
        $this->responseType = self::$STATUS_UNAUTHORIZED;
        return $this;
    }

    /**
     * 
     */
    private function response()
    {        
        $resultCollection = [];

        if (null !== $this->responseData) {

            $resultCollection['data'] = $this->responseData;
            $this->responseData = null;
        }

        if (null !== $this->responseMeta) {
            
            foreach($this->responseMeta as $key => $value) {
                $resultCollection[$key] = $value;
            }
        }        
        
        $resultCollection['status'] = $this->getResponseStatus();
        $resultCollection['message'] = $this->getResponseMessage();


//        return response()->json([
//            $this->responseType['key'] => $resultCollection
//        ], $resultCollection['status']);

        return response()->json($resultCollection, $resultCollection['status']);
    }

    /**
     * 
     * @return int
     * @throws Exception
     */
    private function getResponseStatus(): int
    {

        if (null === $this->responseType) {
            throw new RuntimeException($this->resourceTypeExceptionMessage);
        }

        $response = $this->responseStatus ?? $this->responseType['code'];
        $this->responseStatus = null;

        return $response;
    }

    /**
     * 
     * @return string
     * @throws Exception
     */
    private function getResponseMessage() : string
    {
        if (null === $this->responseType) {
            throw new RuntimeException($this->resourceTypeExceptionMessage);
        }

        $response = $this->responseMessage ?? $this->responseType['message'];
        $this->responseMessage = null;

        return $response;
    }
}
