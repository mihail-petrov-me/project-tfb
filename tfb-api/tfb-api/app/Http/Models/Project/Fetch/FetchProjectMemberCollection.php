<?php
namespace App\Http\Models\Project\Fetch;

use App\Http\Models\User\User;
use DB;
use App\Http\Models\Project\Project;
use \App\Http\Models\Repository;

/**
 * Class FetchProjectCollection
 */
class FetchProjectMemberCollection extends Repository
{
    /**
     * @param $id
     *
     * @return mixed
     */
    public function execute($id)
    {
        $data =  DB::table(Project::TABLE_USER)
            ->select(
                User::ID,
                User::IS_OWNER,
                User::IS_ADMIN
            )->where([
                Project::ID => $id
            ])->get();

        return json_decode($data, true);
    }
}