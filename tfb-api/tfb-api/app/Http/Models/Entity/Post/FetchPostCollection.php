<?php

namespace App\Http\Models\Entity\Post;
use DB;

class FetchPostCollection
{

    public function execute($id, $query) 
    {
        
        return DB::table("block_post", $query)
            ->leftJoin(
                    'collection_entity', 
                    'block_post.id', '=', 'collection_entity.entity_id')
            ->leftJoin(
                    'users', 
                    'block_post.user_id', '=', 'users.id')
            ->leftJoin(
                    'user_experience', 
                    'user_experience.user_id', '=', 'users.id')
            ->select(
                    // Get content data
                    // #
                    'block_post.content AS post_content',                    
                    'block_post.created_at AS created_at',                    
                    
                    // Get user meta data
                    // #
                    'users.name',
                    'users.picture',
                    'users.signature',
                    
                    // Get collection meta data                   
                    // #
                    'collection_entity.entity_id',
                    'collection_entity.entity_type'                    
            )                   
            ->where(
                    'collection_entity.collection_id', $id)
            ->orderBy(
                    'block_post.created_at', 'desc');
    }
}
