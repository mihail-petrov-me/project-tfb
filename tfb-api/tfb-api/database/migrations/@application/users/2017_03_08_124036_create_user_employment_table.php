<?php

use \App\Http\Models\User\User;
use \App\Http\Models\User\UserEmployment;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserEmploymentTable extends Migration
{
    private $table = UserEmployment::TABLE;

    public function up()
    {
        Schema::create($this->table, function(Blueprint $builder) {

            //# Primary Key
            $builder->char(UserEmployment::ID, 13)->primary();

            //# Foreign Key
//            $builder->char(UserEmployment::USER_ID, 13);
//            $builder->index(UserEmployment::USER_ID);
//            $builder->foreign(UserEmployment::USER_ID)->references(User::ID)->on(User::TABLE);

            //# Table Entity Collection
            $builder->string(UserEmployment::ORGANISATION, 200)->nullable();
            $builder->string(UserEmployment::POSITION, 200)->nullable();
            $builder->string(UserEmployment::WEBSITE, 200)->nullable();
            $builder->date(UserEmployment::DATE_START)->nullable();
            $builder->date(UserEmployment::DATE_END)->nullable();
            $builder->text(UserEmployment::DESCRIPTION)->nullable();

            //# Meta
            $builder->timestamps();
            $builder->softDeletes();
        });
    }

    public function down()
    {
      Schema::drop($this->table);
    }
}
