(function () {

    var Controller = function () {

        this.STATE = {
            IS_TITLE_VISIBLE : false
        };


        /**
         * @type
         * Event : @ON : CHANGE
         */
        this.on_change = () => {

            this.STATE.IS_TITLE_VISIBLE = this.uiModel.value;
            this.uiModel.show_errors = this.uiForm[this.uiModel.name].$invalid;
        };

        /**
         * @type
         * Event : init
         *
         */
        this.$onInit = () => {
            this.STATE.IS_TITLE_VISIBLE = this.uiIsDisabled;
        }
    }

    var Component = {
        templateUrl     : './ui-input-textarea.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            uiLabel         : '<',
            uiForm          : '<',
            uiName          : '<',
            uiModel         : '<',
            uiName          : '<',
            uiPlaceholder   : '<',
            uiIsDisabled    : '<'
        }
    };

    angular.module('application.components.visual').component('uiInputTextarea', Component);

})();
