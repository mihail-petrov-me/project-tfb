# Update the liost of available packages
# ======================================
sudo apt-get -y update

# Install the latest version of mysql server
# ======================================
sudo apt-get -y install apache2

# Create new host file for Development
# ======================================
sudo echo "<VirtualHost *:80>

        ServerName portal.zaednovchas.bg
        ServerAdmin webmaster@localhost

        DocumentRoot /var/www/html/tfb-client
        <Directory /var/www/html/tfb-client>
                AllowOverride All
                Options Indexes FollowSymLinks
                Order allow,deny
                Allow from all
                Require all granted
        </Directory>

</VirtualHost>" > /etc/apache2/sites-available/tfb-client.conf

sudo a2ensite tfb-client.conf
sudo service apache2 restart

# Create simbolic link in order to server the application
# ======================================

cd /var/www/html
sudo rm index.html
sudo ln -s /vagrant/tfb-client/_dev/ tfb-client

# Create new host file for Development
# ======================================
sudo echo "<VirtualHost *:80>

        ServerName build.zaednovchas.bg
        ServerAdmin webmaster@localhost

        DocumentRoot /var/www/html/tfb-build
        <Directory /var/www/html/tfb-build>
                AllowOverride All
                Options Indexes FollowSymLinks
                Order allow,deny
                Allow from all
                Require all granted
        </Directory>

</VirtualHost>" > /etc/apache2/sites-available/tfb-build.conf

sudo a2ensite tfb-build.conf
sudo service apache2 restart

# Create simbolic link in order to server the application
# ======================================

cd /var/www/html
sudo rm index.html
sudo ln -s /vagrant/tfb-client/dist/ tfb-build


# Install nodejs 
# ======================================
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs

# Populate the current environment
# ======================================
cd /vagrant/tfb-client
npm install --no-bin-links