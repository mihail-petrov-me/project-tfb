(function () {

    window.FormController = function (callback) {

        return function (Translation, PubSub, Form, UserModel) {

            /**
             * @type
             * State provider
             *
             * @description
             * Preserve the component state insithe the component and component scope
             * it is eqvivalent of @scope in non component environment
             */
            var model = this;


            /**
             * @type
             * Event Publish collection
             *
             * @description
             * The state collection contain every single state thro out the current component
             */
            model.PUBLISH = {
                FORM_PROCESS_END            : PubSub.events.form.PROCESS.END,
                FORM_PROCESS_UPDATE_END     : PubSub.events.form.PROCESS.UPDATE_END,
                FORM_CHANGE                 : PubSub.events.form.CHANGE
            };

            /**
             * @type
             * Event Subscribe collection
             *
             * @description
             * The state collection contain every single state thro out the current component
             */
            model.SUBSCRIBE = {
                FORM_PROCESS_INIT           : PubSub.events.form.PROCESS.INIT,
                FORM_PROCESS_UPDATE_INIT    : PubSub.events.form.PROCESS.UPDATE,
            };

            var ERROR_MESSAGES = {
                REQUIRED: 'Полето е задължително'
            };

            var customInputErrorsCollection = 0;


            /**
             *
             */
            model.on_change = function() {

                PubSub.api.publish(model.PUBLISH.FORM_CHANGE, {
                    'form_change_valid' : (model.formContact.$valid)
                });
            };

            /**
             * @type
             * Event Callback
             *
             * @description
             * Collect an object containing relevant form information and send it back to the
             * request isuer / user object
             */
            model.on_process = function (name) {

                if (model.name == name && !model.uiId) {

                    for (var index in model.FIELD) {

                        if (model.FIELD[index].type == 'dropdown') {

                            model.FIELD[index].show_errors  = (model.FIELD[index].value == null);
                            customInputErrorsCollection     += (model.FIELD[index].value == null) ? 1 : 0;
                            model.FIELD[index].error        = ERROR_MESSAGES.REQUIRED;
                        }
                        else {
                            model.FIELD[index].show_errors = model.formContact[index].$invalid;
                            model.FIELD[index].error = ERROR_MESSAGES.REQUIRED;
                        }
                    }

                    if (model.formContact.$valid && (customInputErrorsCollection == 0)) {
                        PubSub.api.publish(model.PUBLISH.FORM_PROCESS_END, Form.process([model.FIELD]));
                        Form.clear(model.FIELD);
                    }

                    customInputErrorsCollection = 0;
                }
            };


            /**
             * @type
             * Event Callback
             *
             * @description
             * Collect an object containing relevant form information and send it back to the
             * request isuer / user object
             */
            model.on_update = function ($object) {

                if (model.name == $object.name && model.uiId == $object.id) {

                    for (var index in model.FIELD) {

                        if (model.FIELD[index].type == 'dropdown') {

                            model.FIELD[index].show_errors  = (model.FIELD[index].value == null);
                            customInputErrorsCollection     += (model.FIELD[index].value == null) ? 1 : 0;
                            model.FIELD[index].error        = ERROR_MESSAGES.REQUIRED;
                        }
                        else {
                            model.FIELD[index].show_errors = model.formContact[index].$invalid;
                            model.FIELD[index].error = ERROR_MESSAGES.REQUIRED;
                        }
                    }

                    if (model.formContact.$valid && (customInputErrorsCollection == 0)) {

                        var $c= Form.process([model.FIELD]);
                        $c.id = model.uiId;
                        PubSub.api.publish(model.PUBLISH.FORM_PROCESS_UPDATE_END, $c);
                    }

                    customInputErrorsCollection = 0;
                }
            };


            /**
             * @type
             * Lifecycle event
             *
             * @description
             * This method is executed on component init
             */
            model.$onInit = function () {

                PubSub.api.subscribe(model.SUBSCRIBE.FORM_PROCESS_INIT, model.on_process);
                PubSub.api.subscribe(model.SUBSCRIBE.FORM_PROCESS_UPDATE_INIT, model.on_update);
            };

            /**
             * @type
             * Lifecycle event
             *
             * @description
             * This method is executed on component changes
             */
            model.$onChanges = function () {

                if (model.uiCollection) {

                    for (var index in model.uiCollection) {

                        if (model.FIELD[index]) {
                            model.FIELD[index].value = model.uiCollection[index];
                        }
                    }
                }
            }


            /**
             * @type
             * Lifecycle event
             *
             * @description
             * This method is executed on component destroy
             */
            model.$onDestroy = function () {
                PubSub.api.unsubscribe(model.SUBSCRIBE.FORM_PROCESS_INIT);
                PubSub.api.unsubscribe(model.SUBSCRIBE.FORM_PROCESS_UPDATE_INIT);
            };


            callback.apply(this, arguments);
        }
    }
})();
