<?php

namespace App\Http\Transformars;

class ProjectTransformer extends Transformer
{
    /**
     *
     * @var type 
     */
    protected $required = array(
        'title', 'description', 'budget'
    );
        
    /**
     * 
     * @param type $element
     * @return type
     */
    public function transform($element)
    {   
        return array(
            'title'         => $element['title'],
            'description'   => $element['description'],
            'budget'        => $element['budget'],
        );
    }
}
