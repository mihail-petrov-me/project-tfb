(function () {

    /**
     *
     * @param {*} Component
     * @param {*} PubSub
     * @param {*} Translation
     * @param {*} GroupModel
     */
    var Controller = function (Component, PubSub, Translation, UserModel) {

        this.STATE = {
            IS_OPTION_INPUT_VISIBLE         : false,
            IS_ADD_OPTION_BUTTON_VISIBLE    : false
        };

        this.FIELD = {
            QUESTION    : null,
            OPTION      : null
        };

        this.PROPERTY = {
            COLLECTION  : [],
            REFERENCE   : 0
        };

        this.COLLECTION = {
            user : []
        }

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                BUTTON_ADD_OPTION   : $provider({
                    'en'    : 'Ask my anythiong',
                    'bg'    : 'Задай въпроса на анкетата'
                }),

                INPUT_QUESTION     : $provider({
                    'en'    : 'Add option',
                    'bg'    : 'Добави възможен отговор'
                }),

                INPUT_OPTION        : $provider({
                    'en'    : 'Add option',
                    'bg'    : 'Добави'
                })
            };
        });

        /**
         * Event
         */
        this.on_select = ($element) => {
            PubSub.api.publish('UI_MEMBER_PICKER::ITEM_SELECTED', $element);
        };


        /**
         * @type
         * Lifecycle event
         */
        this.$onInit =  () => { // TODO Request on invokation not on init
            console.log("INIT COMPONENT");
        };

        /**
         * @type
         * Lifecycle event
         */
        this.$onDestroy =  () => {

        };
    }

    /**
     * @type
     * Component injector
     */
    Controller.$inject = ['Component', 'PubSub', 'Translation', 'UserModel'];

    var Component = {
        templateUrl : './ui-member-picker.template.html',
        controller  : Controller,
        controllerAs: 'model',
        bindings        : {
            uiCollection : '<'
        }
    };

    angular.module('application.components.visual').component('uiMemberPicker', Component);
})();
