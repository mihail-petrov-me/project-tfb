<?php

// GET routes
// =====================================================================================================================
$app->get('/projects'                                  , 'Portal\ProjectController@index');
$app->get('/projects/{id}'                             , 'Portal\ProjectController@show');
$app->get('/projects/{id}/users'                       , 'Portal\ProjectController@fetchProjectMembers');
$app->get('/projects/{id}/tasks'                       , 'Portal\ProjectController@fetchProjectTasks');
$app->get('/projects/{id}/tasks/{task_id}/comments'    , 'Portal\ProjectController@fetchProjectTaskComments');

// POST routes
// =====================================================================================================================
$app->post('/projects'                                 , 'Portal\ProjectController@store');
$app->post('/projects/{id}/users'                      , 'Portal\ProjectController@addMemberToProject');
$app->post('/projects/{id}/tasks'                      , 'Portal\ProjectController@addTaskToProject');
$app->post('/projects/{project_id}/groups'             , 'Portal\ProjectController@addGroupToProject');
$app->post('/projects/{id}/tasks/{task_id}/comments'   , 'Portal\ProjectController@addCommentToProjectTask');

// PUT routes
// =====================================================================================================================
$app->put('/projects/{project_id}'                             , 'Portal\ProjectController@update');
$app->put('/projects/{project_id}/tasks/{task_id}'             , 'Portal\ProjectController@updateProjectTask');

// DELETE routes
// =====================================================================================================================
$app->delete('/projects/{id}'                          , 'Portal\ProjectController@delete');
$app->delete('/projects/{id}/tasks'                    , 'Portal\ProjectController@deleteProjectTask');