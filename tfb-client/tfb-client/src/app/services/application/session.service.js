((() => {

    const TOKKEN_ID       = "tfb:token_identificator";
    const COLLECTION_ID   = "tfb:user_collection";
    const PERMISSION_DATA = "tfb:user_permission";

    const ACTIVE_PROJECT  = "tfb:project_collection";


    /**
     *
     * @param {*}
     * @param {*} jwtHelper
     */
    var ServiceHandler = ($window, jwtHelper) => {

        /**
         *
         * @param {*} key
         * @param {*} value
         * @param {*} stringify?
         */
        function set($key, $value, stringify = false) {

            var value = (stringify) ? JSON.stringify($value) : $value;
            $window.localStorage.setItem($key, value);
        }

        /**
         *
         * @param {*} key
         */
        function get($key, parse = false) {

            var collection = $window.localStorage.getItem($key);
            return (parse) ? JSON.parse(collection) : collection;
        }


        /**
         *
         * @param {*} key
         */
        function remove($key) {
            $window.localStorage.removeItem($key);
        }

        /**
         *
         * @param {*} tokken
         */
        function setSession(tokken) {

            if(!tokken) {
                throw new Error("Please provide a valide tokken string for the session object");
            }

            set(TOKKEN_ID, tokken);
        }

        /**
         *
         */
        function getPermission() {
            return get(PERMISSION_DATA, true);
        }


        /**
         *
         * @param {*} tokken
         */
        function setPermission($permission) {
            set(PERMISSION_DATA, $permission, true);
        }

        /**
         *
         */
        function getSession() {
            return get(TOKKEN_ID);
        }

        /**
         *
         */
        function setCollection($collection) {
            set(COLLECTION_ID, $collection, true);
        }

        /**
         *
         */
        function getCollection() {
            return get(COLLECTION_ID, true);
        }

        /**
         *
         */
        function getTokken() {
            return (getSession()) ? jwtHelper.decodeToken(getSession()) : null;
        }

        /**
         *
         */
        function isExpired() {
            return (getSession()) ? (!jwtHelper.isTokenExpired(getSession())) : false;
        }

        /**
         * Remove all local storage key - value pairs. This behaivior is useful when
         * user is connected for the first time after an token experation is in play
         */
        function destroy() {
            $window.localStorage.clear();
        }

        /**
         *
         * @param {*} config
         */
        function request(config) {

            config.headers['X-Authorize'] = getSession();
            return config;
        }

        /**
         *
         * @param {*} rejection
         */
        function responseError($rejection) {

            if($rejection.status == 401) {
                destroy();
                window.location.href = "#!/login";
            }
        }



        return {
            set,
            get,
            setSession,
            getSession,
            setCollection,
            getCollection,
            getPermission,
            setPermission,
            destroy,
            getTokken,
            request,
            responseError,
            isExpired
        }
    };

    angular.module('application.service').factory('SessionService', ServiceHandler);

})());
