(function () {

    var Controller = function (PubSub, Translation, GroupModel, UserModel, $window, $location, $timeout, $sce, $http, EntityModel, Request) {

            /**
             * @type
             * State provider
             *
             * @description
             * Preserve the component state insithe the component and component scope
             * it is eqvivalent of @scope in non component environment
             */
            var model = this;


            /**
             * @type
             * Event Publish collection
             *
             * @description
             * The state collection contain every single state thro out the current component
             */
            model.PUBLISH = {
                GROUP_INFO_INIT     : PubSub.events.page.GROUP.INFO.INIT,
                GROUP_INFO_LOAD     : PubSub.events.page.GROUP.INFO.LOAD,
                PAGE_LOAD           : PubSub.events.page.LOAD
            };

            /**
             * @type
             * Event Subscribe collection
             *
             * @description
             * The state collection contain every single state thro out the current component
             */
            model.SUBSCRIBE = {
                COMPONENT_ARTICLE_PROCESS   : PubSub.events.component.ARTICLE.FINISH,
                COMPONENT_POST_PROCESS      : PubSub.events.component.POST.FINISH,
                COMPONENT_VIDEO_PROCESS     : PubSub.events.component.VIDEO.FINISH,
                COMPONENT_IMAGE_PROCESS     : PubSub.events.component.IMAGE.FINISH,
                COMPONENT_POLL_PROCESS      : PubSub.events.component.POLL.FINISH
            };

            /**
             *
             */
            model.STATE = {
                HAS_PERMITION_TO_WRITE      : false,
                CAN_COLLECTION_LOAD_MORE    : false
            };


            /**
             *
             */
            model.TIMESTAMP_REFERENCE       = null;

            /**
             *
             */
            model.collection = [];


            /**
             * @type
             * Event : init
             *
             * @description
             * This method is executed when the initialisation of
             * the current component ocurre. It is responsible for fetching
             * a collection of related groups.
             *
             * @return
             * void
             */
            model.init_FETCH_data = function () {

                GroupModel.api.PAGINATE().fetch
                    .data(UserModel.state('__group_id'))
                        .then(model.handler_fetch_groupData_success)
                        .catch(model.handler_fetch_groupData_error);
            };

            /**
             * @type
             * Handler : @success
             *
             * @description
             * This method is executed when the initialisation of
             * the current component ocurre. It is responsible for fetching
             * a collection of related groups.
             *
             * @return
             * void
             */
            model.handler_fetch_groupData_success = function($response) {

                if($response.data.length > 0) {
                    model.collection = model.collection.concat($response.data);
                }

                model.STATE.CAN_COLLECTION_LOAD_MORE = $response.pagination.has_next;
            }

            /**
             * @type
             * Handler : @error
             *
             * @description
             * This method is executed when the initialisation of
             * the current component ocurre. It is responsible for fetching
             * a collection of related groups.
             *
             * @return
             * void
             */
            model.handler_fetch_groupData_error = function() {
                console.log("@Exception : FETCH : GROUP INFO");
            };

            /**
             * @type
             * Event : init
             *
             * @description
             * This method is executed when the initialisation of
             * the current component ocurre. It is responsible for fetching
             * a collection of related groups.
             *
             * @return
             * void
             */
            model.init_modelCollection = function () {

                GroupModel.api.SINGLE().fetch
                    .groups(UserModel.state('__group_id'))
                        .then(model.handler_FETCH_group_success)
                        .catch(model.handler_FETCH_group_error)
            };


            /**
             * @type
             * Handler : @success
             *
             * @description
             * This handler is colled after the authentication request is made
             * based on the result the user will be redirected to the application
             * or to regissstration form in order to compleat it's registration
             */
            model.handler_FETCH_group_success = function ($response) {

                model.group_title       = $response.data.title;
                model.group_description = $response.data.description;
                model.group_couses      = $response.data.couses;

                 model.init_members();
            };

            /**
             * @type
             * Handler : @error
             *
             * @description
             * This handler is colled after the authentication request is made
             * based on the result the user will be redirected to the application
             * or to regissstration form in order to compleat it's registration
             */
            model.handler_FETCH_group_error = function (res) {
                console.log("@Exception : GROUP : BASE INFORMATION");
            };


            /**
             * @type
             * Event : init
             *
             * @description
             * This method is executed when the initialisation of
             * the current component ocurre. It is responsible for fetching
             * a collection of related groups.
             *
             * @return
             * void
             */
            model.init_members = function () {

                GroupModel.api.COLLECTION().fetch
                    .members(UserModel.state('__group_id'))
                        .then(model.handle_FETCH_member_success)
                        .catch(model.handler_FETCH_member_error);
            };


            /**
             * @type
             * handler method : SUCCESS
             *
             * @description
             * This handler is colled after the authentication request is made
             * based on the result the user will be redirected to the application
             * or to regissstration form in order to compleat it's registration
             */
            model.handle_FETCH_member_success = function (response) {

                PubSub.api.publish(model.PUBLISH.GROUP_INFO_LOAD, {
                    member_collection   : response.data,
                });
            };

            /**
             * @type
             * handler method : ERROR
             *
             * @description
             * This handler is colled after the authentication request is made
             * based on the result the user will be redirected to the application
             * or to regissstration form in order to compleat it's registration
             */
            model.handler_FETCH_member_error = function (response) {
                console.log("@Exception : MEMBERS : LOAD");
            };


            /**
             * @type
             * Event : on
             *
             * @description
             * This handler is colled after the authentication request is made
             * based on the result the user will be redirected to the application
             * or to regissstration form in order to compleat it's registration
             */
            model.on_subscribe = function() {

                var $request = {
                    'user_id'   : UserModel.profile.my("user_id"),
                    'group_id'  : UserModel.state('__group_id')
                };

                GroupModel.api.create
                    .subscribe($request)
                        .then(model.handler_SUBSCRIBE_success)
                        .catch(model.handler_SUBSCRIBE_error);
            };


            /**
             * @type
             * Handler : @success
             *
             * @description
             * This handler is colled after the authentication request is made
             * based on the result the user will be redirected to the application
             * or to regissstration form in order to compleat it's registration
             */
            model.handler_SUBSCRIBE_success = function($response) {

                // add new group information into the user session
                UserModel.profile.groups($response.data);

                // Check permision for entity
                model.PERMISION = {
                    IS_MEMBER   : UserModel.profile.isMember('groups', UserModel.state('__group_id')),
                    IS_ADMIN    : UserModel.profile.isAdmin('groups', UserModel.state('__group_id')),
                    IS_OWNER    : UserModel.profile.isOwner('groups', UserModel.state('__group_id')),
                };

                //reinitialise member collection
                model.init_members();
            };

            /**
             * @type
             * Handler : @error
             *
             * @description
             * This handler is colled after the authentication request is made
             * based on the result the user will be redirected to the application
             * or to regissstration form in order to compleat it's registration
             */
            model.handler_SUBSCRIBE_error = function() {
                console.log("@EXPECTION : SUBSCRIBE : MEMBER : Failed attempt to subscribe");
            };


            /**
             * @type
             * Lifecycle event
             *
             * @description
             * This method is executed on component init
             */
            model.$onInit = function () {

                /**
                 * @type
                 * Publisher
                 *
                 * @description
                 * Emit event stating that this component is initialised and
                 * it is ready notifaing the sidebar component to rerender its state
                 */
                PubSub.api.publish(model.PUBLISH.GROUP_INFO_INIT);

                /**
                 * @type
                 * Subscriber
                 *
                 * @description
                 * Emit event stating that this component is initialised and
                 * it is ready notifaing the sidebar component to rerender its state
                 */
                PubSub.api.subscribe(PubSub.events.component.POST.FINISH, function ($response) {

                    var $requestBody =  {
                        user_id : UserModel.profile.my("user_id"),
                        content : $response.content
                    };

                    GroupModel.api.create
                        .post(UserModel.state('__group_id'), $requestBody)
                            .then(model.handler_postRequest_success)
                            .catch(model.handler_postRequest_error);
                });


                /**
                 * @type
                 * Subscriber
                 *
                 * @description
                 * Emit event stating that this component is initialised and
                 * it is ready notifaing the sidebar component to rerender its state
                 */
                PubSub.api.subscribe(model.SUBSCRIBE.COMPONENT_ARTICLE_PROCESS, function ($response) {

                    var $requestBody =  {
                        user_id : UserModel.profile.my("user_id"),
                        title   : $response.title,
                        content : $response.content
                    };


                    GroupModel.api.create
                        .article(UserModel.state('__group_id'), $requestBody)
                            .then(model.handler_postRequest_success)
                            .catch(model.handler_postRequest_error);
                });


                /**
                 * @type
                 * Subscriber
                 *
                 * @description
                 * Emit event stating that this component is initialised and
                 * it is ready notifaing the sidebar component to rerender its state
                 */
                PubSub.api.subscribe(model.SUBSCRIBE.COMPONENT_POLL_PROCESS, function ($response) {

                    var $requestBody = {
                        user_id     : UserModel.profile.my("user_id"),
                        question    : $response.question,
                        options     : $response.options
                    };

                    GroupModel.api.create
                        .poll(UserModel.state('__group_id'), $requestBody)
                            .then(model.handler_postRequest_success)
                            .catch(model.handler_postRequest_error);
                });

                /**
                 * @type
                 * Subscriber
                 *
                 * @description
                 * Emit event stating that this component is initialised and
                 * it is ready notifaing the sidebar component to rerender its state
                 */
                PubSub.api.subscribe(model.SUBSCRIBE.COMPONENT_VIDEO_PROCESS, function (response) {

                    GroupModel.api.create
                        .poll(UserModel.state('__group_id'), $requestBody)
                            .then(model.handler_postRequest_success)
                            .catch(model.handler_postRequest_error);
                });


                PubSub.api.subscribe('ui_block_list_load_more', function() {

                    GroupModel.api.PAGINATE().fetch
                        .data(UserModel.state('__group_id'))
                            .then(model.handler_fetch_groupData_success)
                            .catch(model.handler_fetch_groupData_error);
                });



                PubSub.api.subscribe('upvote_poll', function($response) {

                    var $requestBody = {
                        'user_id'   : UserModel.profile.my('id'),
                        'entity_id' : $response.entity_id,
                        'answer_id' : $response.answer_id
                    };


                    var $ENTITY_ID = $response.entity_id;

                    GroupModel.api.create
                        .vote(UserModel.state('__group_id'), $requestBody)
                            .then(function($response) {

                                console.log("You answer the question");
                                GroupModel.api.SINGLE().fetch
                                    .poll($ENTITY_ID)
                                        .then(function($response) {
                                            console.log("Response poll : ");
                                            console.log($response);

                                            PubSub.api.publish('update_poll_after_vote', $response.data)
                                        })
                                        .catch(function($response) {
                                            console.log("@EXCEPTION : POLL : FETCH : SINGLE");
                                        });

                            })
                            .catch(function($response) {

                            });
                });

            };


            /**
             * @type
             * handler method : SUCCESS
             *
             * @description
             * This handler is colled after the authentication request is made
             * based on the result the user will be redirected to the application
             * or to regissstration form in order to compleat it's registration
             */
            model.handler_postRequest_success = function (response) {

                GroupModel.api.REFRESH().fetch
                    .data(UserModel.state('__group_id'))
                        .then(function($response) {

                            for(var i = ($response.data.length - 1); i >= 0; i--) {
                                model.collection.unshift($response.data[i]);
                            }
                        })
                        .catch(model.handler_fetch_groupData_error);
            };

            /**
             * @type
             * handler method : ERROR
             *
             * @description
             * This handler is colled after the authentication request is made
             * based on the result the user will be redirected to the application
             * or to regissstration form in order to compleat it's registration
             */
            model.handler_postRequest_error = function (response) {
                console.log("@Exception : CREATE : ENTITY")
            };


            PubSub.api.subscribe('filter_discusion_da', function($subscribtion) {

                    Request.flush();

                    GroupModel.api.PAGINATE().fetch
                    .data(UserModel.state('__group_id'), { 'filter' : $subscribtion})
                    .then(function($response) {
                        model.collection = $response.data
                    })
                    .catch(model.handler_fetch_groupData_error);
            });


            /**
             * @type
             * Router event
             *
             * @description
             * This method is executed on component route init
             */
            model.$routerOnActivate = function (next, previous) {

                UserModel.state("__group_id", next.urlPath);
                $timeout(model.init_modelCollection);
                $timeout(model.init_FETCH_data);

                model.PERMISION = {
                    IS_MEMBER   : UserModel.profile.isMember('groups', UserModel.state('__group_id')),
                    IS_ADMIN    : UserModel.profile.isAdmin('groups', UserModel.state('__group_id')),
                    IS_OWNER    : UserModel.profile.isOwner('groups', UserModel.state('__group_id')),
                };

                model.init_members();
            };


            /**
             *
             */
            model.$onDestroy = function() {

                PubSub.api.unsubscribe(PubSub.events.component.POST.FINISH);
                PubSub.api.unsubscribe(model.SUBSCRIBE.COMPONENT_ARTICLE_PROCESS);
                PubSub.api.unsubscribe(model.SUBSCRIBE.COMPONENT_POLL_PROCESS);
                PubSub.api.unsubscribe(model.SUBSCRIBE.COMPONENT_VIDEO_PROCESS);
                PubSub.api.unsubscribe('ui_block_list_load_more');
                PubSub.api.unsubscribe('upvote_poll');
            };
    };


    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['PubSub', 'Translation', 'GroupModel', 'UserModel', '$window', '$location', '$timeout', '$sce', '$http', 'EntityModel', 'Request'];



    /**
     *
     */
    var Component = {
        templateUrl         : './group-profile.template.html',
        controller          : Controller,
        controllerAs        : 'model',
        $routerCanActivate  : function () {
            return true;
        }
    };

    angular.module('application.components.page').component('groupProfile', Component);
})();
