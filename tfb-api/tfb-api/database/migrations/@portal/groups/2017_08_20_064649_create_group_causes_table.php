<?php

use \App\Http\Models\Group\GroupCause;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupCausesTable extends Migration
{
    private $table = GroupCause::TABLE;

    public function up()
    {
        Schema::create($this->table, function (Blueprint $builder)
        {
            //# Primary Key
            $builder->char(GroupCause::ID, 13)->primary();

            //# Table Entity Collection
            $builder->string(GroupCause::TITLE, 100);

            //# Meta
            $builder->timestamps();
            $builder->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop($this->table);
    }
}
