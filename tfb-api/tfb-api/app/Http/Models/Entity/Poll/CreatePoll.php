<?php

namespace App\Http\Models\Entity\Poll;
use DB;

class CreatePoll 
{

    /**
     * 
     * @param type $collection
     * @return type
     */
    public function execute($collectionId, $collection) 
    {
        $entityCollection = $this->transform_query($collection);
        extract($entityCollection);
         
        $this->assigne_poll_entity($id, $question, $userId);
        $this->assigne_poll_answers($id, $options);
        $this->assigne_collection_entity($id, $collectionId, $userId);
    }    
    
   /**
     * 
     * @param type $collection
     * @return type
     */
    private function transform_query($collection)
    {
        return array(
            'id'            => uniqid(),
            'question'      => $collection['question'],
            'userId'        => $collection['user_id'],
            'options'       => $collection['options']
        );
    }
    
    
    /**
     * 
     * @param type $id
     * @param type $question
     * @param type $userId
     */
    private function assigne_poll_entity($id, $question, $userId)
    {        
        DB::table('block_poll')->insert(array(
            'id'        => $id,
            'question'  => $question,
            'user_id'   => $userId
        ));
    }
    
    /**
     * 
     * @param type $entityId
     * @param type $content
     */
    private function assigne_poll_answers($id, $options)
    {
        for ($i = 0; $i < count($options); $i++) {
            $options[$i]['id']             = uniqid();
            $options[$i]['entity_id']      = $id;
        }
        
        DB::table('block_poll_answer')->insert($options);
    }
    
    
    /**
     * 
     * @param type $modelId
     * @param type $groupId
     */
    private function assigne_collection_entity($id, $collectionId, $userId)
    {
        DB::table('collection_entity')->insert(array(
            'entity_id'     => $id,
            'collection_id' => $collectionId,
            'user_id'       => $userId,
            'entity_type'   => 3
        ));
    }
}
