(function () {

    var Controller = function(PubSub) {

        var model = this;

        model.on_load = function() {
            PubSub.api.publish('ui_list_load_more');
        };
    };

    Controller.$inject = ['PubSub'];

    var Component = {
        templateUrl     : './ui-list-project.template.html',
        controllerAs    : 'model',
        controller      : Controller,
        bindings        : {
            uiCollection    : '<',
            uiLoadMore      : '<'
        }
    };

    angular.module('application.components.visual').component('uiListProject', Component);
})();
