<?php
namespace App\Http\Controllers\Api\Portal;

use App\Http\Models\Initiative\Initiative;
use GuzzleHttp;
use DB;

use \Illuminate\Http\Request;
use \App\Http\Controllers\Controller;

class InitiativeController extends Controller
{
    
    /**
     *
     * @var type 
     */
    private $API_MESSAGES = array(
        "INITIATIVE_CREATED" => "Initiative created successfuly",
        "INITIATIVE_UPDATED" => "Initiative udated successfully",
        "INITIATIVE_DELETE"  => "Initiative deleted successfully"
    );
    
    /**
     *
     * @var type 
     */
    private $API_ERROR_MESSAGES = array(
        "INITIATIVE_CREATED" => "Initiative created successfuly",
        "INITIATIVE_UPDATED" => "The requested resourse could not be updated"
    );    
    
    
    /**
     *
     * @var type 
     */
    private $model = null;

    /**
     * 
     * @param AuthenticationService $provider
     * @param User $user
     */
    public function __construct(Initiative $model)
    {
        $this->model = $model;
    }        
    

    // GET : initiatives
    // #
    public function index(Request $request)
    {
        // get all initiatives
        // #
        $collection = DB::table('initiatives')->get();
        return $this->responseSuccess(null, $collection);
    }


    // POST : initiatives
    // #
    public function store(Request $request)
    {
        // Get data from server;
        $this->model->create($request->json()->all());
        return $this->responseSuccess($this->API_MESSAGES['INITIATIVE_CREATED']);        
    }


    // PUT : initiatives/{id}/edit
    // #
    public function edit()
    {

    }

    // UPDATE : initiatives
    // #
    public function update()
    {

    }

    // DELETE : initiatives
    // #
    public function delete()
    {

    }
}
