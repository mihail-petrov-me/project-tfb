(function () {

    /**
     *
     * @param {*} UserModel
     * @param {*} Translation
     * @param {*} State
     * @param {*} PubSub
     */
    var Controller = function (UserModel, Translation, State, PubSub) {

        this.STATE = {
            INIT            : false,

            X               : null,
            Y               : null,

            EVENT_DAY       : null,
            EVENT_PERIOD    : null
        };

        var ENUM = {
            DAY     : ["Понеделник", "Вторник", "Сряда", "Четвъртък", "Петък"],
            PERIOD  : ["1-ви", "2-ри", "3-ти", "4-ти", "5-ти", "6-ти", "7-ми"]
        };

        this.add_event = ($event, day, period) => {

            this.STATE.INIT         = true;

            this.STATE.X            = $event.pageX;
            this.STATE.Y            = $event.pageY;
            this.STATE.EVENT_DAY    = ENUM.DAY[day];
            this.STATE.EVENT_PERIOD = ENUM.PERIOD[period];
        };

        this.get_coordinates = () => {

            if(!this.STATE.INIT) {
                return;
            }

            return {
                display     : "block",
                position    : 'absolute',
                top         : this.STATE.Y + "px",
                left        : (this.STATE.X + 32) + "px"
            };
        };

        this.on_close = () => {
            this.STATE.EVENT_VIEW = false;
        }


        this.$onInit = () => {

            PubSub.form.end('FORM_ASSIGNE_EVENT', ($response) => {
                console.log($response);
            });

            PubSub.api.publish('event:ui:after_load');
        }
    };

    Controller.$inject = ['UserModel', 'Translation', 'State', 'PubSub'];
    var Component = {
        templateUrl     : './schedule-table.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('scheduleTable', Component);

})();
