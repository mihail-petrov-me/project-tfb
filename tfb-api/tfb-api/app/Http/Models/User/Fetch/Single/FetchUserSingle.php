<?php
namespace App\Http\Models\User\Fetch\Single;

use App\Http\Models\Repository;
use App\Http\Models\User\User;
use DB;

/**
 * Class FetchUserSingle
 * @package App\Http\Models\User\Fetch\Single
 */
class FetchUserSingle extends Repository
{
    /**
     * @var array
     */
    public $fieldCollection = [
        User::ID,
        User::FNAME,
        User::LNAME,
        User::NAME,
        User::SIGNATURE,
        User::PICTURE,
        User::PERMISSIONS
    ];

    /**
     * @param $bundle
     *
     * @return mixed
     */
    public function execute($bundle)
    {
        return $this->fetchSingle(User::TABLE, $bundle, $this->fieldCollection);
    }
}