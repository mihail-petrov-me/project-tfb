(function () {

    var ComponentRouter = [
        { path: 'campaign',         component: 'campaignList',      as: 'CampaignList', useAsDefault: true},
        { path: 'campaign/:id',     component: 'campaignSingle',    as: 'CampaignSingle', },
        { path: 'campaign/create',  component: 'campaignCreate',    as: 'CampaignCreate', },


        { path: 'users',         component: 'usersList',      as: 'UsersList'},
        { path: 'users/:id',     component: 'usersSingle',    as: 'UsersSingle', },
        { path: 'users/create',  component: 'usersCreate',    as: 'UsersCreate', },

    ];

    var Component       = {
        templateUrl     : './administration.template.html',
        controllerAs    : 'model',
        $routeConfig    : ComponentRouter,
        bindings        : { $router: '<' }
    };

    angular.module('application.components.page').component('administration', Component);
})();
