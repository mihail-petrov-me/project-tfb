<?php

use App\Http\Models\Collections\CollectionBlock;
use App\Http\Models\User\User;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCollectionTable extends Migration
{
    private $table = User::TABLE_USER_COLLECTION;

    public function up()
    {
        Schema::create($this->table, function(Blueprint $builder) {

            //# Primary Key
            $builder->char(User::ID, 13);
            $builder->char(CollectionBlock::ID, 13);

            //# Public field
            $builder->tinyInteger(User::IS_ADMIN);
            $builder->tinyInteger(User::IS_EDITOR);
            $builder->tinyInteger(User::IS_OWNER);
        });
    }

    public function down()
    {
        Schema::drop($this->table);
    }
}
