(function () {

    /**
     *
     * @param {*} Translation
     * @param {*} GroupModel
     * @param {*} PubSub
     */
    var Controller = function (Translation, GroupModel, PubSub, UserModel, State, EntityModel) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;


        /**
         * @type
         * Router event
         *
         * @description
         * This method is executed on component route init
         */
        model.$routerOnActivate = function (next, previous) {

            EntityModel.api.SINGLE().fetch
                .entity()
                    .then(function($response) {

                    })
                    .catch(function($response) {

                    });
        };


    };


    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['Translation', 'GroupModel', 'PubSub', 'UserModel', 'State', 'EntityModel'];

    var Component = {
        templateUrl         : './post.template.html',
        controller          : Controller,
        controllerAs        : 'model'
    };

    angular.module('application.components.page').component('post', Component);
})();
