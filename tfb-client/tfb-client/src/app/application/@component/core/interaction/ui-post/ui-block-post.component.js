(function () {

    /**
     *
     * @param {*} Component
     * @param {*} PubSub
     * @param {*} Translation
     * @param {*}
     */
    var Controller = function (Component, PubSub, Translation, $sce) {


        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;


        /**
         * @type
         * Publish collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.PUBLISH = {
            'post'    : PubSub.events.component.POST.FINISH,
            'finish'  : PubSub.events.component.FINISH
        };

        /**
         * @type
         * Subscribe collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.SUBSCRIBE = {

            // Component action
            // #
            'post'              : PubSub.events.component.POST.PROCESS,

            //  Component mode
            // #
            'mode_interactive'  : PubSub.events.component.POST.MODE_INTERACTIVE,
            'mode_presentation' : PubSub.events.component.POST.MODE_PRESENTATION,
        };


        /**
         *
         */
        model.STATE = {
            'IS_MODE_INTERACTIVE'   : false,
            'IS_MODE_PRESENTATION'  : true
        };


        /**
         * @type
         * Field collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.FIELD = {
            CONTENT : null
        };


        /**
         * @type
         * Event : init
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.init_process = function() {

            PubSub.api.publish(model.PUBLISH.post, {
                'content'   : model.FIELD.CONTENT
            });

            // PubSub.api.publish(model.PUBLISH.finish);
            model.FIELD.CONTENT = null;
        };


        /**
         * @type
         * Event : init
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.$init_interactive =  function() {

            Component.$interactive(model.uiMode, function() {

                model.STATE.IS_MODE_INTERACTIVE     = true;
                model.STATE.IS_MODE_PRESENTATION    = false;

                PubSub.api.subscribe(model.SUBSCRIBE.post, model.init_process);
            });
        };

        /**
         * @type
         * Event : init
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.$init_presentation = function() {

            Component.$presentation(model.uiMode, function() {

                model.STATE.IS_MODE_PRESENTATION    = true;
                model.STATE.IS_MODE_INTERACTIVE     = false;

                model.FIELD.CONTENT = model.uiContent;
            });
        };

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        model.$onInit = function() {

            model.$init_interactive();
            model.$init_presentation();

            PubSub.api.subscribe(model.SUBSCRIBE.mode_interactive, function($entityId) {

                if($entityId == model.uiId) {

                    model.STATE.IS_MODE_INTERACTIVE     = true;
                    model.STATE.IS_MODE_PRESENTATION    = false;
                }
            });

            PubSub.api.subscribe(model.SUBSCRIBE.mode_presentation, function($entityId) {

                if($entityId == model.uiId) {

                    model.STATE.IS_MODE_INTERACTIVE     = false;
                    model.STATE.IS_MODE_PRESENTATION    = true;
                }
            });
        };

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        model.$onDestroy = function() {

            PubSub.api.unsubscribe(model.SUBSCRIBE.mode_presentation);
            PubSub.api.unsubscribe(model.SUBSCRIBE.mode_interactive);
            PubSub.api.unsubscribe(model.SUBSCRIBE.post);
        };
    };

    Controller.$inject = ['Component', 'PubSub', 'Translation', '$sce'];

    var Component = {

        templateUrl : './ui-block-post.template.html',
        controller  : Controller,
        controllerAs: 'model',
        bindings    : {
            uiId        : '<',
            uiContent   : '<',
            uiMode      : '<',
            uiIsEditable: '<'
        }
    };

    angular.module('application.components.visual').component('uiBlockPost', Component);

})();
