(function () {

    /**
     * @type :
     * Base url
     */
    var BASE_URL = "app";


    /**
     * @type
     * User creation services
     *
     * @description
     * Contains collection of methods for creating an user related items
     */
    var create = function(request) {

        return {

        };
    };


    /**
     * @type
     * User update services
     *
     * @description
     * Contains collection of methods for updating an user related items
     */
    var update = function(request) {

        return {

        };
    };


    /**
     * @type
     * User fetch services
     *
     * @description
     * Contains collection of methods for fetching an user related items
     */
    var fetch = function(request, requestType) {

        return {

            areas               :  ()       => request.beffore(requestType).get([BASE_URL, "area"]),
            municipalities      :  ($id)    => request.beffore(requestType).get([BASE_URL,'area'        , $id,"municipality"]),
            cities              :  ($id)    => request.beffore(requestType).get([BASE_URL,'municipality', $id, "city"]),
            schools             :  ($id)    => request.beffore(requestType).get([BASE_URL,'city'        , $id, "school"]),
            academies           :  ()       => request.beffore(requestType).get([BASE_URL,"academy"]),
        };
    }

    /**
     * @type
     * User fetch services
     *
     * @description
     * Contains collection of methods for archiving an user related items
     */
    var archive = function(request, base, provider) {

        return {};
    };

    /**
     *
     * @param {*} request
     * @param {*} base
     * @param {*} provider
     */
    var ApiProvider = function(request) {

        var requestType = { type : 'collection' };

        return {

            PAGINATE : function() {
                requestType.type = 'pagination';
                return this;
            },

            REFRESH : function() {
                requestType.type = 'refresh';
                return this;
            },

            COLLECTION : function() {
                requestType.type = 'collection';
                return this;
            },

            SINGLE : function() {
                requestType.type = 'single';
                return this;
            },

            create  : create(request),
            fetch   : fetch(request, requestType),
            update  : update(request),
            archive : archive(request)
        }
    };

    /**
     *
     * @param {*} http
     * @param {*} q
     * @param {*} AppManager
     */
    var ServiceHandler = function (Request, SessionService) {

        return {
            api: ApiProvider(Request)
        };
    };

    ServiceHandler.$inject = ['Request', 'SessionService'];
    angular.module('application.models').factory('AppModel', ServiceHandler);
})();
