(function () {

    /**
     *
     * @param {*} PubSub
     * @param {*} UserModel
     */
    var Controller = function(Translation, PubSub, UserModel) {

        /**
         * @type
         * State collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        this.STATE = {
            IS_LOADING : false
        };


        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                BUTTON_ACTION : $provider({
                    'en' : 'Update',
                    'bg' : 'Актуализирай'
                })
            };
        });


        /**
         * @type
         * Event : on
         *
         * @description
         * This method is executed on component destroy
         */
        this.on_update = () => {

            // @STATE tchange
            this.STATE.IS_LOADING = true;

            // @Publisher
            PubSub.api.publish(PubSub.events.form.PROCESS.INIT, "language");
        };


        /**
         * @type
         * Event : init
         */
        this.init_formModel = () => {

            UserModel.api.fetch
            .language(UserModel.profile.my("user_id"))
                .then(this.handle_init_success_formModel)
                .catch(this.handle_init_error_formModel);
        };

        /**
         * @type
         * Handler : INIT : SUCCESS
         *
         * @description
         * This method is resposible for handlimng the initialisation of
         * the form component collection
         */
        this.handle_init_success_formModel = (response) => {

            this.fieldCollection = response;
            PubSub.api.publish(PubSub.events.PAGE.LOAD_FINISH);
        };


        /**
         * @type
         * Handler : INIT : ERROR
         *
         * @description
         * This method is resposible for handlimng the initialisation of
         * the form component collection
         */
        this.handle_init_error_formModel = () => {

        };


        /**
         * @type
         * Request
         *
         * @description
         * This method is responsioble for processing an update data to the BACKEND
         */
        this.request_update_formModel = (formCollection) => {

            UserModel.api.update
            .language(UserModel.profile.my("user_id"), formCollection)
                .then(this.handle_update_success_formModel)
                .catch(this.handle_update_error_formModel);
        };


        /**
         * @type
         * Request Handler
         *
         * @description
         * This method is responsioble for requesting and handling application data
         */
        this.handle_update_success_formModel = (response) => {

            // @Change state
            this.STATE.IS_LOADING = false;

            // @Store new tokken variable
            UserModel.profile.setSession(response.tokken);
        };

        /**
         * @type
         * Request Handler
         *
         * @description
         * This method is responsioble for requesting and handling application data
         */
        this.handle_update_error_formModel = () => {
            this.STATE.IS_LOADING = false;
        };


        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        this.$onInit = () => {

             // On component init send a request for retreaving information about the
             // specific form this. This information is going to populate the form data
            this.init_formModel();

             // After form update is process the component is expecting a responce containing
             // an object with form data
            PubSub.api.subscribe(PubSub.events.form.PROCESS.END, this.request_update_formModel);
        };

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component destroy
         */
        this.$onDestroy = () => {
            PubSub.api.unsubscribe(PubSub.events.form.PROCESS.END);
        };
    };

    Controller.$inject  = ['Translation','PubSub', 'UserModel'];
    var Component       = {
        templateUrl     : './language-preferences.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('languagePreferences', Component);

})();
