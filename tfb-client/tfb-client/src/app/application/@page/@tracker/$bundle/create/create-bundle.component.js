(function () {

    var Controller = function (Translation, PubSub, UserModel, BundleModel) {


        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en' : '@New bundle',
                    'bg' : '@Нов клас'
                })
            };
        });


        this.on_create_bundle = () => {
            PubSub.form.init('FORM_CREATE_CLASS');
        };


        /**
         * @type
         * EVENT : request
         */
        this.request_CREATE_bundle = ($request) => {

            BundleModel.api.create.bundle(UserModel.profile.my("user_id"), $request)
                .then(this.handle_CREATE_bundle_success)
                .catch(this.handle_CREATE_bundle_error);
        };

        /**
         * @type
         * HANDLER :  success
         */
        this.handle_CREATE_bundle_success = ($response) => {
            this.$router.navigate(['/Dashboard/Bundle/AddStudents', {'id' : $response.data.id}]);
        };

        /**
         * @type
         * HANDLER :  error
         */
        this.handle_CREATE_bundle_error = () => {
            console.log("@Exception: can not create new bundle");
        };

        /**
         * @type
         * Lifecycle event : INIT
         */
        this.$onInit = () => {

            PubSub.form.end('FORM_CREATE_CLASS', ($response) => {

                $response['school_id']  = JSON.parse(UserModel.state('tracker'))[0]['school_id'];
                console.log($response['school_id']);


                this.request_CREATE_bundle($response);
            });
        };

        /**
         * @type
         * Lifecycle event : Destroy
         */
        this.$onDestroy = () => {
            PubSub.form.destroy('FORM_CREATE_CLASS');
        };
    };

    Controller.$inject  = ['Translation', 'PubSub', 'UserModel', 'BundleModel'];
    var Component       = {
        templateUrl     : './create-bundle.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            $router     : '<'
        }
    };

    angular.module('application.components.page').component('createBundle', Component);
})();
