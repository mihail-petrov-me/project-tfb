<?php

namespace App\Http\Models\Group;
use DB;

class SubscribeToGroupEntity {
    
    /**
     *
     * @var type 
     */
    private $message = array(
        'group_exception_subscribtion' => array(
            'message' => 'User is already subscribed to this groups',
            'code'    => 'GE_1'
        )
    );
    
    /**
     * 
     * @param type $collection
     * @return type
     */
    public function execute($collection) 
    {
        $entityCollection = $this->transform_query($collection);
        extract($entityCollection);
        
        if($this->is_subsriber($userId, $groupId)) {
            throw new \Exception($this->message['group_exception_subscribtion']);
        }
        
        return $this->assigne_user_group($userId, $groupId);
    }
    
    
    /**
     * @Transformer
     * # =======================================================================
     * @param type $entity
     * @return int
     */
    private function transform_query($collection)
    {
        return array(
            'userId'  => $collection['user_id'],
            'groupId' => $collection['group_id']
        );
    }
    
    /**
     * 
     * @param type $collection
     * @return type
     */
    private function assigne_user_group($userId, $groupId)
    {
        return DB::table('user_group')->insert(array(
            'user_id'    => $userId,
            'group_id'   => $groupId
        ));
    }


    /**
     * 
     * @param type $collection
     * @return type
     */
    private function is_subsriber($userId, $groupId)
    {
        return DB::table('user_group')->where(array(
            'user_id'    => $userId,
            'group_id'   => $groupId
        ))->first();
    }
}