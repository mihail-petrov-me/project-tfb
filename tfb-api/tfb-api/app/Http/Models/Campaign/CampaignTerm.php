<?php

namespace App\Http\Models\Campaign;

use App\Http\Models\Model;
use App\Http\Models\Campaign\Create\CreateCampaignTeacher;
use App\Http\Models\Campaign\Create\CreateCampaign;
use App\Http\Models\Campaign\Fetch\FetchCampaignCollection;
use App\Http\Models\Campaign\Fetch\FetchCampaignSingle;
use App\Http\Models\Campaign\Fetch\FetchCampaignTeacher;

/**
 * Class CampaignTerm
 * @package App\Http\Models\Campaign
 */
class CampaignTerm extends Model
{
    // Public fields
    const ID            = 'campaign_term_id';
    const CAMPAIGN_ID   = 'campaign_id';
    const TITLE         = 'campaign_term_title';
    const DATE_START    = 'campaign_date_start';
    const DATE_END      = 'campaign_date_end';

    // Meta fields
    const CREATED_AT    = 'created_at';
    const UPDATED_AT    = 'updated_at';
    const DELETED_AT    = 'deleted_at';

    // Tables
    const TABLE         = 'campaign__term';
}