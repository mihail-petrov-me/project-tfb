<?php
namespace App\Services\Token;

use \Firebase\JWT\JWT;

/**
 * Class TokenProvider
 * @package App\Services\Token
 */
class TokenProvider
{
    /**
     *
     */
    const TOKEN_KEY = 'brvywdgV*wS=9P+`(%Egt~mj+>}xRf6=a`56t$Hq-6GLT)(7H*wXt)!=;2p*;@tZ_`?Z<CFvh<~\\)-=^"_n/S<EZMb!Fh2^s\\}n`MDy@CfADw%~;dCb~-HTaxW(8Ep:F)FtMXK_{4)';

    /**
     *
     */
    const EXPIRATION_TIME_LIMIT = 3600;

    /**
     *
     * @var array
     */
    private $collection = [];

    /**
     * @param $key
     * @param $value
     */
    public function push($key, $value)
    {
        if(array_key_exists($key, $this->collection)) {
            $this->collection[$key] = $value;
        }
        else {
            $this->collection[$key] = [];
            $this->collection[$key] = $value;
        }
    }

    /**
     *
     * @param $token
     *
     * @return mixed
     * @throws \UnexpectedValueException
     * @throws \Firebase\JWT\SignatureInvalidException
     * @throws \Firebase\JWT\ExpiredException
     * @throws \Firebase\JWT\BeforeValidException
     * @internal param type $token
     */
    public function decompose($token)
    {
        $object = JWT::decode($token, static::TOKEN_KEY, ['HS256']);
        return json_decode(json_encode($object), true);
    }

     /**
      * 
      * @return string
      */
    public function build(): string
    {
        $collection = array_merge($this->collection, $this->expiration());
        return JWT::encode($collection, static::TOKEN_KEY);
    }

    /**
     * 
     * @return array
     */
    private function expiration() : array
    {
        return [
            'exp' => (time() + 10) + static::EXPIRATION_TIME_LIMIT
        ];
    }
}