(function () {

    let Controller = function (Authentication, UserModel, PubSub, Translation) {

        this.STATE = {
            'IS_ERRORS_VISIBLE'         : false,
            'IS_FORM_ERRORS_VISIBLE'    : false,
            'IS_LOADING_IN_PROGRESS'    : false,
            'IS_APPLOGIN_VISIBLE'       : false
        };

        /**
         * @type
         * Event Publish collection
         */
        this.PUBLISH = {
            FORM_PROCESS_INIT   : PubSub.events.form.PROCESS.INIT,
            PAGE_LOAD           : PubSub.events.page.LOAD
        };

        /**
         * @type
         * Event Subscribe collection
         */
        this.SUBSCRIBE = {
            FORM_PROCESS_END : PubSub.events.form.PROCESS.END
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                IMAGE : $provider({
                    'en' : 'assets/logo_en.png',
                    'bg' : 'assets/logo_bg_white.png'
                }),

                LOGIN_GOOGLE : $provider({
                    'en' : 'Sign in Google Mail account',
                    'bg' : 'Влез с Google Mail акаунт'
                }),

                LOGIN_GOOGLE_INFO : $provider({
                    'en' : 'Sign in Google',
                    'bg' : 'Ако си служител, учител или алумни'
                }),

                LOGIN_REGULAR : $provider({
                    'en' : 'Log in',
                    'bg' : 'Добре дошли в портала'
                }),

                LOGIN_REGULAR_BUTTON : $provider({
                    'en' : 'Sign in',
                    'bg' : 'Влез в системата'
                })
            };
        });

        /**
         * @type
         * Event
         */
        this.on_authenticateGoogle = () => {
            this.request_googleAuthentication();
        };

        /**
         * @type
         * Event
         */
        this.on_authenticateApp = () => {
            PubSub.api.publish(this.PUBLISH.FORM_PROCESS_INIT, "authentication");
        };


        /**
         * @type
         * Handler
         */
        this.handle_authentication_success = ($response) => {

            UserModel.profile.setSession($response.token);
            UserModel.profile.setPermission($response.data.user_permissions);

            // let redirectEndpoint = (response.data.has_authentication) ? ['Dashboard'] : ['Signup'];
            this.$router.navigate(['Dashboard']);
        };

        /**
         * @type
         * Handler
         */
        this.handle_authentication_google_error = () => {

            console.error("@Exception : unable to authenticate with google provider");
            this.STATE.IS_ERRORS_VISIBLE       = true;
            this.STATE.IS_LOADING_IN_PROGRESS  = false;
        };

        /**
         * @type
         * Handler
         */
        this.handle_authentication_app_error = () => {

            console.error("@Exception : unable to authenticate with regular autehntication");
            this.STATE.IS_FORM_ERRORS_VISIBLE  = true;
            this.STATE.IS_LOADING_IN_PROGRESS  = false;
        };


        /**
         * @type
         * Request
         */
        this.request_googleAuthentication = () => {

            this.STATE.IS_LOADING_IN_PROGRESS = true;

            Authentication.authenticate('google')
                .then(this.handle_authentication_success)
                .catch(this.handle_authentication_google_error);
        };


        /**
         * @type
         * Request
         */
        this.request_appAuthentication =(collection) => {

            Authentication.authenticate('app', collection)
                .then(this.handle_authentication_success)
                .catch(this.handle_authentication_app_error);
        };


        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {

            PubSub.api.subscribe(this.SUBSCRIBE.FORM_PROCESS_END, this.request_appAuthentication);
            PubSub.api.publish(this.PUBLISH.PAGE_LOAD);
        };


        /**
         * @type
         * Lifecycle event
         */
        this.$onDestroy = () => {
            PubSub.api.unsubscribe(this.SUBSCRIBE.FORM_PROCESS_END);
        };
    };


    Controller.$inject  = ['Authentication', 'UserModel', 'PubSub', 'Translation'];

    let Component       = {
        templateUrl     : './login.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : { $router     : '<' }
    };

    angular.module('application.components.page').component('login', Component);
})();
