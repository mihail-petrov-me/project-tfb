(function () {

    var Controller = function (Translation, PubSub, UserModel, BundleModel) {


        this.PROPERTY = {
            BUNDLE_ID   : null
        };

        this.COLLECTION = {
            collection  : []
        };

        this.STATE = {
            GO_NEXT             : false,
            SHOW_MESSAGE        : false,

            MUTATED_OBJECT_ID   : null,
            DELETE              : false,
            EDIT                : false,
            CREATE              : false
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en' : '@New bundle',
                    'bg' : '@Нов клас'
                })
            };
        });


        this.on_delete = ($id) => {

            // remove delete state
            this.STATE.EDIT     = false;
            this.STATE.CREATE   = false;

            // set initial state
            this.STATE.DELETE = true;
            this.STATE.MUTATED_OBJECT_ID = $id;
        };

        this.on_edit = ($id) => {

            // remove delete state
            this.STATE.DELETE = false;
            this.STATE.CREATE = false;

            // set initial state
            this.STATE.EDIT = true;
            this.STATE.MUTATED_OBJECT_ID = $id;
        };

        this.on_cancel = () => {

            this.STATE.DELETE = false;
            this.STATE.MUTATED_OBJECT_ID = null;
        };

        this.show_form_interactive = () => {
            return ((this.STATE.DELETE == false) && (this.STATE.EDIT == false));
        };

        this.show_form_edit = ($id) => {
            return (this.STATE.EDIT == true && this.STATE.MUTATED_OBJECT_ID == $id);
        };

        this.show_form_delete = ($id) => {
            return (this.STATE.DELETE == true && this.STATE.MUTATED_OBJECT_ID == $id);
        };

        this.show_row = () => {
            return (this.STATE.EDIT == false);
        };

        this.on_delete_process = () => {
            this.request_ARCHIVE_student(this.PROPERTY.BUNDLE_ID, this.STATE.MUTATED_OBJECT_ID);
        };

        this.on_create = () => {
            this.STATE.CREATE = true;
        };

        this.show_form_create = () => {
            return this.STATE.CREATE;
        };


        this.show_message = () => {
            return this.STATE.SHOW_MESSAGE;
        }



        this.on_create_student = () => {
            PubSub.form.init('FORM_CREATE_STUDENT');
        };


        this.updateStudent = ($id, $collection) => {

            for(let i = 0; i < this.COLLECTION.collection.length; i++) {

                if(this.COLLECTION.collection[i].id == $id) {
                   this.COLLECTION.collection[i].name =  $collection.name
                }
            }
        };


        this.deleteStudent = ($id) => {

            for(let i = 0; i < this.COLLECTION.collection.length; i++) {
                if(this.COLLECTION.collection[i].id == $id) {
                   this.COLLECTION.collection.splice(i, 1);
                }
            }
        };


        /**
         * @type
         * EVENT : request : FETCH
         */
        this.request_FETCH_studentCollection = () => {

            BundleModel.api.COLLECTION().fetch
            .studentCollection(this.uiBundle)
                .then(this.handle_FETCH_student_successCollection)
                .catch(this.handle_FETCH_student_errorCollection);
        };

        /**
         * @type
         * HANDLER :  success
         */
        this.handle_FETCH_student_successCollection = ($response) => {

            this.COLLECTION.collection = $response.data;
            if($response.data.length == 0) {
                this.STATE.SHOW_MESSAGE = true;
            }
        };

        /**
         * @type
         * HANDLER :  error
         */
        this.handle_FETCH_student_errorCollection = () => {
            console.log("@Exception: can not fetch a collection of students");
        };

        /**
         * @type
         * EVENT : request : CREATE
         */
        this.request_CREATE_student = ($bundle, $request) => {

            BundleModel.api.create.student( $bundle, $request)
                .then(this.handle_CREATE_student_success)
                .catch(this.handle_CREATE_student_error);
        };

        /**
         * @type
         * HANDLER :  success
         */
        this.handle_CREATE_student_success = ($response) => {

            this.COLLECTION.collection.push($response.data);

            if(this.COLLECTION.collection.length > 0) {
                this.STATE.GO_NEXT = true;
            }
        };

        /**
         * @type
         * HANDLER :  error
         */
        this.handle_CREATE_student_error = () => {
            console.log("@Exception: can not create new student");
        };



        /**
         * @type
         * EVENT : request : UPDATE
         */
        this.request_UPDATE_student = ($bundleId, $studentId, $request) => {

            BundleModel.api.update.student($bundleId, $studentId, $request)
                .then(this.handle_UPDATE_student_success)
                .catch(this.handle_UPDATE_student_error);
        };

        /**
         * @type
         * HANDLER :  success
         */
        this.handle_UPDATE_student_success = ($response) => {

            this.updateStudent($response.data.id, $response.data);
            this.STATE.EDIT                 = false;
            this.STATE.MUTATED_OBJECT_ID    = null;
        };

        /**
         * @type
         * HANDLER :  error
         */
        this.handle_UPDATE_student_error = () => {
            console.log("@Exception: can not update new student");
        };


        /**
         * @type
         * EVENT : request : CREATE
         */
        this.request_ARCHIVE_student = ($bundle, $studentId) => {

            BundleModel.api.archive.student($bundle, $studentId)
                .then(this.handle_ARCHIVE_student_success)
                .catch(this.handle_ARCHIVE_student_error);
        };

        /**
         * @type
         * HANDLER :  success
         */
        this.handle_ARCHIVE_student_success = ($response) => {

            this.deleteStudent(this.STATE.MUTATED_OBJECT_ID);
            this.STATE.MUTATED_OBJECT_ID = null;
            this.STATE.DELETE = false;
        };

        /**
         * @type
         * HANDLER :  error
         */
        this.handle_ARCHIVE_student_error = () => {
            console.log("@Exception: can not archive the student");
        };


        this.on_next = () => {
            PubSub.api.publish("WIZARD::PROCESS");
        };


        /**
         * @type
         * Lifecycle event : INIT
         */
        this.$onInit = () => {

            this.PROPERTY.BUNDLE_ID = this.uiBundle || UserModel.state('__bundle_id__');
            this.request_FETCH_studentCollection();


            PubSub.form.end('FORM_CREATE_STUDENT', ($response) => {
                this.request_CREATE_student(this.PROPERTY.BUNDLE_ID, $response);
            });

            /**
             *
             */
            PubSub.form.endUpdate("FORM_CREATE_STUDENT", ($response) => {
                this.request_UPDATE_student(this.PROPERTY.BUNDLE_ID, $response.id, $response);
            });
        };

        /**
         * @type
         * Lifecycle event : Destroy
         */
        this.$onDestroy = () => {
            PubSub.form.destroy('FORM_CREATE_STUDENT');
        };
    };

    let Component       = {
        templateUrl     : './add-students.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            $router     : '<',
            uiBundle    : '<'
        }
    };

    angular.module('application.components.page').component('addStudents', Component);
})();
