<?php

use App\Http\Models\Collections\CollectionArea;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaTable extends Migration
{
    private $table = CollectionArea::TABLE;

    public function up()
    {
        Schema::create($this->table, function(Blueprint $builder) {

            //# Primary Key
            $builder->increments(CollectionArea::ID, 10);

            //# Table Entity Collection
            $builder->string(CollectionArea::TITLE, 200);
        });
    }

    public function down()
    {
        Schema::drop($this->table);
    }
}
