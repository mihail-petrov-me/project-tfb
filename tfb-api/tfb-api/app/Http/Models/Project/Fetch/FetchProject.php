<?php
namespace App\Http\Models\Project\Fetch;

use DB;
use App\Http\Models\Project\Project;
use \App\Http\Models\Repository;

/**
 * Class FetchProjectCollection
 */
class FetchProject extends Repository
{
    /**
     * @param $id
     *
     * @return mixed
     */
    public function execute($id)
    {
        return DB::table(Project::TABLE)->select(
            Project::ID,
            Project::TITLE,
            Project::DESCRIPTION,
            Project::START_DATE,
            Project::END_DATE
        )->where([
            Project::ID => $id
        ])->get();
    }
}