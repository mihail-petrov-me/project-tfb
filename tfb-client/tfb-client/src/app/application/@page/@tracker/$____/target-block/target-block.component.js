(function () {

    /**
     *
     * @param {*} UserModel
     * @param {*} Translation
     */
    var Controller = function (Translation, PubSub, UserModel, ClassModel) {


        this.SHOW_COLLECTION = true;

        this.collection = [];


        this.isCollectionVisible    = () => ((this.collection.length > 0) && this.SHOW_COLLECTION);
        this.isFormVisible          = () => ((this.collection.length == 0) || !this.SHOW_COLLECTION);

        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en' : '#Account information',
                    'bg' : '@Профил'
                }),

                LABEL_ACTION : $provider({
                    'en' : '#Account information',
                    'bg' : '#Настройки'
                }),

                LABEL_CONTACT : $provider({
                    'en' : 'Contact',
                    'bg' : 'Лична информация'
                }),

                LABEL_EDUCATION : $provider({
                    'en' : 'Education',
                    'bg' : 'Образование'
                }),

                LABEL_EMPLOYMENT : $provider({
                    'en' : 'Employment',
                    'bg' : 'Работодатели'
                }),

                LABEL_LANGUAGE : $provider({
                    'en' : 'Language Preferences',
                    'bg' : 'Езикови предпочитания'
                }),

                LABEL_LOGOUT : $provider({
                    'en' : 'Log out',
                    'bg' : 'Излез от системата'
                })
            };
        });


        /**
         * @type
         * Request : FETCH
         *
         * @description
         * This method is responsioble for processing an update data to the BACKEND
         */
        this.request_FETCH_classes = () => {

            ClassModel.api.COLLECTION().fetch
                .all('100070')
                    .then(this.handler_FETCH_classes_success)
                    .catch(this.handler_FETCH_classes_error);
        };

        /**
         * @type
         * Handler : Success
         *
         * @description
         * Emit event stating that this component is initialised and
         * it is ready notifaing the sidebar component to rerender its state
         */
        this.handler_FETCH_classes_success = ($response) => {
            this.collection = $response.data;
        };

        /**
         * @type
         * Handler : Error
         *
         * @description
         * Emit event stating that this component is initialised and
         * it is ready notifaing the sidebar component to rerender its state
         */
        this.handler_FETCH_classes_error = () => {
            console.error("@Exception : Class collection could not be obtained");
        };


        /**
         * @type
         * Handler : Success
         *
         * @description
         * Emit event stating that this component is initialised and
         * it is ready notifaing the sidebar component to rerender its state
         */
        this.handle_CREATE_class_success = () => {

            this.request_FETCH_classes();
            this.SHOW_COLLECTION = true;
        };

        /**
         * @type
         * Handler : Error
         *
         * @description
         * Emit event stating that this component is initialised and
         * it is ready notifaing the sidebar component to rerender its state
         */
        this.handle_CREATE_class_error = () => {
            console.error("@Exception : Classe created");
        };

        /**
         *
         */
        this.show_class_form = () => {
            this.SHOW_COLLECTION =  false;
        };


        /**
         *
         */
        this.submit_class = () => {
            PubSub.api.publish(PubSub.events.form.PROCESS.INIT, 'form-class');
        };

        /**
         *
         */
        this.show_list = () => {

            this.SHOW_COLLECTION = true;
            this.request_FETCH_classes();
        };


        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        this.$onInit = () => {

            // get class information
            this.request_FETCH_classes();


            // #
            PubSub.api.subscribe(PubSub.events.form.PROCESS.END, ($response) => {

                $response['school_id']  = "100070";
                $response['teacher_id'] = UserModel.profile.my('id');

                ClassModel.api.create
                    .classentity($response)
                        .then(this.handle_CREATE_class_success)
                        .catch(this.handle_CREATE_class_error);
            });
        };

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component destroy
         */
        this.$onDestroy = () => {
            PubSub.api.unsubscribe(PubSub.events.form.PROCESS.INIT);
        };
    };

    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject  = ['Translation', 'PubSub', 'UserModel', 'ClassModel'];
    var Component       = {
        templateUrl     : './target-block.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('targetBlock', Component);

})();
