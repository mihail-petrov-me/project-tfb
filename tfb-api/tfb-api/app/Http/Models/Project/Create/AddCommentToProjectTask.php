<?php

namespace App\Http\Models\Project\Create;

use App\Http\Models\Project\ProjectTaskComment;
use App\Http\Models\User\User;
use DB;
use App\Http\Models\Project\ProjectTask;
use App\Http\Models\Project\Project;
use App\Http\Models\Repository;

/**
 * Class AddCommentToProjectTask
 * @package App\Http\Models\Project\Create
 */
class AddCommentToProjectTask extends Repository
{
    /**
     * @param $id
     * @param $collection
     *
     * @return array
     */
    public function execute(string $id, array $collection): array
    {
        $entity = $this->build($id, $collection);
        DB::table(Project::TABLE_COMMENT)->insert($entity);

        return $entity;
    }

    /**
     * @param string $id
     * @param array $collection
     *
     * @return array
     */
    public function build(string $id, array $collection): array
    {
        return [
            ProjectTask::ID                 => $id,
            ProjectTaskComment::ID          => uniqid(),
            User::ID                        => $this->process($collection, User::ID),
            ProjectTaskComment::CONTENT     => $this->process($collection, ProjectTaskComment::CONTENT)
        ];
    }
}