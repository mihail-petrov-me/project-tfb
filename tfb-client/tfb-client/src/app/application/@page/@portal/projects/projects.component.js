(function () {

    /**
     *
     * @param {*} Translation
     * @param {*} ProjectModel
     * @param {*} PubSub
     */
    var Controller = function (Translation, ProjectModel, PubSub, UserModel, State) {

        /**
         * @type
         * State collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        this.STATE = {
            "IS_SIDEBAR_VISIBLE"        : false,
            "IS_OPTIONS_ENTITY_VISIBLE" : false,
            "IS_OPTIONS_SEARCH_VISIBLE" : false,
            "IS_OPTIONS_FILTER_VISIBLE" : false,
            "IS_OPTIONS_JOIN_VISIBLE"   : false,
            "IS_DESCRIPTION_VISIBLE"    : false,

            "IS_SIDEBAR_DISABLED"       : true
        };

        /**
         * @type
         * Event Subscribe collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        this.SUBSCRIBE = {

            GROUP_LIST_INIT: PubSub.events.page.GROUP.LIST.INIT,
            GROUP_LIST_LOAD: PubSub.events.page.GROUP.LIST.LOAD,

            GROUP_INFO_INIT: PubSub.events.page.GROUP.INFO.INIT,
            GROUP_INFO_LOAD: PubSub.events.page.GROUP.INFO.LOAD,

            GROUP_CREATE_INIT: PubSub.events.page.GROUP.CREATE.INIT,
        };


        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en': '@Projects',
                    'bg': '@Проекти'
                })
            };
        });


        this.on_create = function() {
            State.push("projects", "modal-create");
        };


        PubSub.api.subscribe('close_model_window', function() {
            State.pop("projects");
        });
    };


    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */


    var ComponentRoute = [
        { path: '/',        component: 'projectList',   as: 'ProjectList', useAsDefault: true},
        { path: '/:id',     component: 'projectProfile',as: 'ProjectProfile'},
        { path: 'create',   component: 'projectModify', as: 'ProjectModify'},
        { path: 'plan',     component: 'projectPlan',   as: 'ProjectPlan'}
    ];

    Controller.$inject  = ['Translation', 'ProjectModel', 'PubSub', 'UserModel', 'State'];
    var Component       = {
        templateUrl     : './projects.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        $routeConfig    : ComponentRoute,
        bindings        : { $router: '<' }
    };

    angular.module('application.components.page').component('projects', Component);
})();
