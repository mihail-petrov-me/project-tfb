<?php

use \App\Http\Models\User\User;
use \App\Http\Models\Collections\CollectionBlock;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockTable extends Migration
{
    private $table = CollectionBlock::TABLE;

    public function up()
    {
        Schema::create($this->table, function(Blueprint $builder) {

            //# Primary Key
            $builder->char(CollectionBlock::ID, 13)->primary();

            //# Foreign key
            $builder->char(CollectionBlock::USER_ID, 13);
            $builder->char(CollectionBlock::BLOCK_ID, 13);

            $builder->tinyInteger(CollectionBlock::TYPE);

            //# Meta
            $builder->timestamps();
            $builder->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop($this->table);
    }
}
