var fs      = require('fs.extra');
var util    = require('../helpers/util');
var uglify  = require('uglify-js');


/**
 *
 */
function replace(file, dir) {

  var reference = fs.readFileSync(file).toString();
  var referenceCollection = reference.match(/(templateUrl) *: *'([a-z\.\/]+)'/);
  if(referenceCollection) {

    var pathReference = referenceCollection[2].replace('./', dir);
    var template = fs.readFileSync(pathReference).toString().replace(/(\r\n|\n|\r)/gm,"");
    return reference.replace(referenceCollection[1], 'template').replace(referenceCollection[2], template);
  }
    return reference;
}

exports.exec = function(options) {

  //util.traverse(setting.app + 'app/', function(file, directory, filePath) {
  util.traverse(options.input, function(file, directory, filePath) {


      // console.log(replace(filePath, directory));
      // console.log("===");
      uglify.minify(replace(filePath, directory), { fromString: true });

      //var fileReference = uglify.minify(replace(filePath, directory), { fromString: true });
      // fs.appendFileSync(options.output, fileReference.code);
  });

};
