(function () {

    /**
     *
     */
    var Controller = FormController(function(Translation) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;


        /**
         *
         */
        model.name = "language";


        /**
         * @component
         * Fields collection
         *
         * @behaivior
         * This property contains all the fields of
         * the form controller. Every single field contain value and mutator properties
         * in order to check if this field is required or not you must provide
         *
         * @properties
         *  isRequired : true | false
         *  value : String
         */
        model.FIELD = {

            "language": {

                name            : "language",
                type            : "dropdown",
                title           : 'Chose the prefered language test',
                value           : null,
                show_errors     : false,
                error           : 'Field is required',
                // options         : [
                //     {
                //         'id': 1, 'title': 'English'
                //     },
                //     {
                //         'id': 2, 'title': 'Bulgarian'
                //     }
                // ]
            }
        };


        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(function($provider) {

            model.LABEL = {

                LABEL_SECTION_LANGUAGE     : $provider({
                    "bg"    : "Езикови предпочитания",
                    "en"    : "Language preferences"
                }),

                TEST : $provider({
                    "en"    : [
                        {
                            'id': 1, 'title': 'English'
                        },
                        {
                            'id': 2, 'title': 'Bulgarian'
                        }
                    ],
                    "bg"    : [
                        {
                            'id': 1, 'title': 'Англииски'
                        },
                        {
                            'id': 2, 'title': 'Български'
                        }
                    ]
                })
            };
        });


    });


    /**
     *
     */
    Controller.$inject = ['Translation', 'PubSub', 'Form'];

    /**
     *
     */
    var Component = {
        templateUrl     : './form-language.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            uiCollection    : '<',
            uiIsDisabled    : '<'
        }
    };

    angular.module('application.components.page').component('formLanguage', Component);

})();
