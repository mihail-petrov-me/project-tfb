(function () {

    var Component = {
        templateUrl     : './ui-list-bundle.template.html',
        controllerAs    : 'model',
        transclude      : true,
        bindings        : {
            collection    : '<',
            uiLoadMore    : '<'
        }
    };

    angular.module('application.components.visual').component('uiListBundle', Component);

})();
