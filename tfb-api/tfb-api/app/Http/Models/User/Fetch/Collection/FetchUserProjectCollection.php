<?php

namespace App\Http\Models\User\Fetch\Collection;
use App\Http\Models\Repository;
use DB;

/**
 * Class FetchUserProjectCollection
 * @package App\Http\Models\User\Fetch\Collection
 */
class FetchUserProjectCollection extends Repository
{
    /**
     * @return mixed
     */
    public function execute()
    {
        return DB::table('projects')
            ->join(
                    'user_collection', 
                    'user_collection.collection_id', '=', 'projects.id'
            )
            ->select(
                    'projects.id', 
                    'projects.title', 
                    'projects.description', 
                    'user_collection.is_admin', 
                    'user_collection.is_owner'
            )
            ->get();
    }
}