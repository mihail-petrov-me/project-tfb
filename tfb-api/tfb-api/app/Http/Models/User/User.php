<?php
namespace App\Http\Models\User;

use App\Http\Models\User\Fetch\Collection\FetchUserCollection;
use DB;

use App\Http\Models\Model;
use App\Http\Models\User\Fetch\Single\FetchUserSingle;
use App\Http\Models\User\Fetch\Single\FetchUserSinglePermission;


// # Fetch Collection
// # ===========================================================================
use App\Http\Models\User\Fetch\Collection\FetchUserGroupCollection;
use App\Http\Models\User\Fetch\Collection\FetchUserProjectCollection;
use App\Http\Models\User\Fetch\Collection\FetchUserInitiativeCollection;
use App\Http\Models\User\Fetch\Collection\FetchUserCommentCollection;

// # Fetch Single
// # ===========================================================================
use App\Http\Models\User\Fetch\Single\FetchUserSignature;
use App\Http\Models\User\Fetch\Single\FetchUserContact;
use App\Http\Models\User\Fetch\Single\FetchUserEducation;
use App\Http\Models\User\Fetch\Single\FetchUserEmployment;
use App\Http\Models\User\Fetch\Single\FetchUserLanguage;
use App\Http\Models\User\Fetch\Single\FetchUserExperiance;

// # Update
// # ===========================================================================
use App\Http\Models\User\Update\UpdateUserContact;
use App\Http\Models\User\Update\UpdateUserEducation;
use App\Http\Models\User\Update\UpdateUserEmployment;
use App\Http\Models\User\Update\UpdateUserExperince;
use App\Http\Models\User\Update\UpdateUserLanguage;

// # Create
// # ===========================================================================
use App\Http\Models\User\Create\CreateUser;

class User extends Model
{
    // Public fields
    const ID                = "user_id";
    const TEACHER_ID        = "teacher_id";
    const COORDINATOR_ID    = "coordinator_id";

    const EMAIL             = "user_email";
    const FNAME             = "user_fname";
    const LNAME             = "user_lname";
    const NAME              = "user_name";
    const SIGNATURE         = "user_signature";
    const COHORT            = "user_cohort";
    const PASSWORD          = "user_password";
    const PICTURE           = "user_picture";
    const PERMISSIONS       = "user_permissions";

    // Meta fields
    const CREATED_AT        = "created_at";
    const UPDATED_AT        = "updated_at";
    const DELETED_AT        = "deleted_at";

    // Meta fields
    const IS_OWNER          = "is_owner";
    const IS_ADMIN          = "is_admin";
    const IS_EDITOR         = "is_editor";

    // Tables
    const TABLE                  = "users";
    const TABLE_USER_TYPES       = "user__user_type";
    const TABLE_USER_COLLECTION  = "user__collection";


    /**
     * 
     * @param type $collection
     * @return type
     */
    public function store_user($collection) 
    {
        return (new CreateUser())->execute($collection);
    }


    /**
     * @param null $query
     *
     * @return mixed
     */
    public function all($query = null) 
    {
        return (new FetchUserCollection())->execute($query);
    }

    /**
     *
     * @param type $id
     * @param type $limit
     * @return type
     */
    public function get_userSignature($id)
    {
        return (new FetchUserSignature())->execute($id);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function get_userContact($id)
    {
        return (new FetchUserContact())->execute($id);
    }

    /**
     *
     * @param type $id
     * @param type $limit
     * @return type
     */
    public function get_userEducation($id)
    {
        return (new FetchUserEducation())->execute($id);
    }

    /**
     *
     * @param type $id
     * @param type $limit
     * @return type
     */
    public function get_userEmployment($id) 
    {    
        return (new FetchUserEmployment())->execute($id);   
    }

    /**
     * 
     * @param type $id
     * @param type $limit
     * @return type
     */
    public function get_userLanguage($id) 
    {    
      return (new FetchUserLanguage())->execute($id);           
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function get_userExperiance($id)
    {
        return (new FetchUserExperiance())->execute($id);
    }


    /**
     * Get user specific groups 
     * @param type $id
     * @return type
     */
    public function get_userGroups($id) 
    {    
        return (new FetchUserGroupCollection())->execute($id);
    }

    /**
     * Get user specific projects 
     * @param type $id
     * @return type
     */
    public function get_userProjects($id) 
    {    
        return (new FetchUserProjectCollection())->execute($id);
    }
    
    /**
     * Get user initiatives 
     * @param type $id
     * @return type
     */
    public function get_userInitiatives($id) 
    {    
        return (new FetchUserInitiativeCollection())->execute($id);
    }
    
    
    
    public function get_userComments($id, $query)
    {
        $this->setRefresh('created_at');
        return $this->select((new FetchUserCommentCollection())->execute($id), $query);
    }
    
    
    /**
     * 
     * @param type $id
     * @param type $collection
     */
    public function update_userContact($id, $collection)
    {
        return (new UpdateUserContact())->execute($id, $collection);
    }
    
    /**
     * 
     * @param type $id
     * @param type $collection
     * @return type
     */
    public function update_userEducation($id, $collection)
    {
        return (new UpdateUserEducation())->execute($id, $collection);       
    }

    /**
     * 
     * @param type $id
     * @param type $collection
     * @return type
     */
    public function update_userEmployment($id, $collection)
    {
        return (new UpdateUserEmployment())->execute($id, $collection);      
    }
    
    /**
     * 
     * @param type $id
     * @param type $collection
     * @return type
     */
    public function update_userExperiance($id, $collection)
    {
        return (new UpdateUserExperince())->execute($id, $collection);      
    }
    
    /**
     * 
     * @param type $id
     * @param type $collection
     * @return type
     */
    public function update_userLanguage($id, $collection)
    {
        return (new UpdateUserLanguage())->execute($id, $collection);      
    }

    /**
     * @param $bundle
     *
     * @return mixed
     */
    public function fetch_user($bundle)
    {
        return (new FetchUserSingle())->execute($bundle);
    }

    /**
     * @param $id
     *
     * @return mixed
     * @internal param $bundle
     *
     */
    public function fetch_userPermission($id)
    {
        return (new FetchUserSinglePermission())->execute($id);
    }
}