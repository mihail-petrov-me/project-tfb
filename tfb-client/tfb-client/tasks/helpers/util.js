var fs      = require('fs.extra');
var events  = require('events');
var eventEmitter = new events.EventEmitter();
/**
 *
 */
var isDirectory = function(file) {
  return fs.lstatSync(file).isDirectory();
};

/**
 *
 */
var isFile = function(file) {
  return fs.lstatSync(file).isFile();
};

/**
 *
 */
var isJs = function(file) {

  var collection = file.split('.');
  var extention = collection[collection.length - 1];

  return (extention == 'js');
}

/**
 *
 */
var traverse = function(directory, callback, successCallback) {

  var fileCollection = fs.readdirSync(directory);

  for(var i = 0; i < fileCollection.length; i++) {

    var filePointer = (directory + fileCollection[i]);

    if(isFile(filePointer) && isJs(filePointer)) {
      callback(fileCollection[i], directory, filePointer);
    }

    if(isDirectory(filePointer)) {
      traverse((filePointer + '/'), callback, null);
    }
  }


  if(successCallback != null) {
    successCallback();
  }
};


exports.traverse = traverse;
exports.isJs = isJs;
