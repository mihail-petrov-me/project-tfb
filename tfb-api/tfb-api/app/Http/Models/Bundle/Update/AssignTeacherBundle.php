<?php
namespace App\Http\Models\Bundle\Update;

use App\Http\Models\Repository;
use DB;

class AssignTeacherBundle extends Repository
{
    public function execute($id, $collection)
    {
        DB::table('teacher__bundle')->insert(array(
            'teacher_id'    => $this->process($collection, 'teacher_id'),
            'bundle_id'      => $id
        ));
    }
}