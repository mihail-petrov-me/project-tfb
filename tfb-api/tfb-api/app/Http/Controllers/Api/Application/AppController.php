<?php
namespace App\Http\Controllers\Api\Application;

use \App\Http\Controllers\Controller;
use App\Http\Models\User\Fetch\Single\FetchUserSinglePermission;
use App\Http\Models\User\User;
use \Illuminate\Http\JsonResponse;
use DB;

/**
 * Class AppController
 * @package App\Http\Controllers\Api\Application
 */
class AppController extends Controller
{
    /**
     *
     * @var type 
     */
    private $API_ERROR_MESSAGES = [
        "USER_AUTH"     => "Unnable to authenticate the user",
        "USER_CREATED"  => "Something whent wrong during user initialisation",
        "USER_UPDATED"  => "The requested resourse could not be updated"
    ];


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function showAreaCollection(): JsonResponse
    {
        return $this->responseSuccess(\Illuminate\Support\Facades\DB::table('collection_area')->get());
    }

    /**
     * @param $id
     *
     * @return JsonResponse
     */
    public function showMunicipalityCollection($id): JsonResponse
    {
        $build = \Illuminate\Support\Facades\DB::table('collection_municipality')->where('area_id', $id)->get();
        return $this->responseSuccess($build);
    }

    /**
     * @param $id
     *
     * @return JsonResponse
     */
    public function showCityCollection($id): JsonResponse
    {
        $build = \Illuminate\Support\Facades\DB::table('collection_city')->where('municipality_id', $id)->get();
        return $this->responseSuccess($build);
    }

    /**
     * @param $id
     *
     * @return JsonResponse
     */
    public function showSchoolCollection($id): JsonResponse
    {
        $build = \Illuminate\Support\Facades\DB::table('collection_school')->where('city_id', $id)->get();
        return $this->responseSuccess($build);
    }

    /**
     * @return JsonResponse
     */
    public function showAcademyCollection(): JsonResponse
    {
        $build = \Illuminate\Support\Facades\DB::table('collection_academies')->get();
        return $this->responseSuccess($build);
    }
}