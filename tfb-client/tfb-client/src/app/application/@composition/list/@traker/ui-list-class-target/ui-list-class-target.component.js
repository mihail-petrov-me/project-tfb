(function () {

    var Component = {
        templateUrl     : './ui-list-class-target.template.html',
        controllerAs    : 'model',
        bindings        : {
            collection    : '<',
            uiLoadMore    : '<'
        }
    };

    angular.module('application.components.visual').component('uiListClassTarget', Component);

})();
