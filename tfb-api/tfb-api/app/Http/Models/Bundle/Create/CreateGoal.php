<?php
namespace App\Http\Models\Bundle\Create;

use App\Http\Models\Bundle\Bundle;
use App\Http\Models\Goal\Goal;
use App\Http\Models\Repository;
use DB;

class CreateGoal extends Repository
{
    /**
     * @param $id
     * @param $collection
     *
     * @return array
     */
    public function execute($id, $collection)
    {
        $entity = $this->build($id, $collection);
        DB::table(Goal::TABLE)->insert($entity);

        return $entity;
    }

    /**
     * @param $id
     * @param $collection
     *
     * @return array
     */
    public function build($id, $collection) : array
    {
        return array(
            Goal::ID            =>  uniqid(),
            Goal::BUNDLE_ID     =>  $id,
            Goal::TEACHER_ID    =>  $this->process($collection, Goal::TEACHER_ID),
            Goal::PARENT_ID     =>  $this->process($collection, Goal::PARENT_ID, null),
            Goal::RIGAR         =>  $this->process($collection, Goal::RIGAR),
            Goal::TYPE          =>  $this->process($collection, Goal::TYPE),
            Goal::SUBJECT_ID    =>  $this->process($collection, Goal::SUBJECT_ID),
            Goal::TITLE         =>  $this->process($collection, Goal::TITLE)
        );
    }
}