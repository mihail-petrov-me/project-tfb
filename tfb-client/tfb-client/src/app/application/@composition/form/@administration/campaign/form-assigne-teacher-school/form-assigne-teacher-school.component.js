(function () {

    var Controller = function(Translation, PubSub, Form, AppModel, UserModel) {

        this.name = "FORM_ASSIGNE_TEACHER_SCHOOL";

        /**
         * @component
         * Fields collection
         */
        this.FIELD = {

            "area"              : {
                name            : "area",
                type            : 'dropdown',
                title           : 'Област',

                provider        : function() {
                    return AppModel.api.COLLECTION().fetch.areas();
                },
                value           : null,
                isRequired      : true,
                show_errors     : false
            },

            "municipality"      : {
                name            : "municipality",
                type            : 'dropdown',
                title           : 'Община',

                provider        : function($id) {
                    return AppModel.api.fetch.municipalities($id);
                },
                value           : null,
                isRequired      : true,
                show_errors     : false
            },

            "city"              : {
                name            : "city",
                type            : 'dropdown',
                title           : 'Община',

                provider        : function($id) {
                    return AppModel.api.fetch.cities($id);
                },
                value           : null,
                isRequired      : true,
                show_errors     : false
            },

            "school": {
                name            : "school",
                type            : 'dropdown',
                title           : 'Училище',

                provider        : function($id) {
                    return AppModel.api.fetch.schools($id);
                },
                value           : null,
                isRequired      : true,
                show_errors     : false
            },


            "academies": {
                name            : "academies",
                type            : 'dropdown',
                title           : 'Училище',

                provider        : function() {
                    return AppModel.api.COLLECTION().fetch.academies();
                },
                value           : null,
                isRequired      : true,
                show_errors     : false
            },


            "coordinator": {
                name            : "coordinator",
                type            : 'dropdown',
                title           : 'Координатор',

                provider        : function($id) {
                    return UserModel.api.fetch.coordinators($id);
                },

                reducer         : {
                    'id'    : 'id',
                    'title' : 'name'
                },

                value           : null,
                isRequired      : true,
                show_errors     : false
            }
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                OPTIONS_COHORT_PLACEHOLDER : $provider({
                    "bg"    : "Випуск",
                    "en"    : "Cohort"
                }),

                OPTIONS_AREA_PLACEHOLDER : $provider({
                    "bg"    : "Област",
                    "en"    : "Area"
                }),

                OPTIONS_MUNICIPALITY_PLACEHOLDER : $provider({
                    "bg"    : "Община",
                    "en"    : "Municipality"
                }),

                OPTIONS_CITY_PLACEHOLDER : $provider({
                    "bg"    : "Град",
                    "en"    : "E-mail"
                }),

                OPTIONS_SCHOOL_PLACEHOLDER : $provider({
                    "bg"    : "Училище",
                    "en"    : "School"
                }),

                OPTIONS_SCHOOL_ACADEMY : $provider({
                    "bg"    : "Академии",
                    "en"    : "Academy"
                }),

                INPUT_SUBJECT : $provider({
                    "bg"    : "Предмет",
                    "en"    : "Birth date"
                }),

                OPTIONS_COORDINATOR_PLACEHOLDER : $provider({
                    "bg"    : "Координатор",
                    "en"    : "Coordinator"
                })
            };
        });


        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        this.$onInit = () => {
            this.parent.FIELD  = this.FIELD;
            this.formContact   = this.parent.formContact;
        }
    };

    var Component = {
        templateUrl     : './form-assigne-teacher-school.template.html',
        controller      : Controller,
        controllerAs    : 'model',

        require             : {
            parent          : '^formWrapper'
        },

        bindings        : {
            uiCampaignType : '<'
        }
    };

    angular.module('application.components.page').component('formAssigneTeacherSchool', Component);

})();
