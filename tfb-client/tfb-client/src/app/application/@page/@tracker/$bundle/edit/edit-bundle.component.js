(function () {

    var Controller = function (Translation, UserModel, PubSub, BundleModel) {

        this.PROPERTY = {
            BUNDLE_ID : null
        };

        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {

        };

        /**
         * @type
         * Router event
         */
        this.$routerOnActivate = (next, previous) => {
            this.PROPERTY.BUNDLE_ID = next.params.id;
            PubSub.api.publish('event:ui:after_load');
        };
    };

    Controller.$inject  = ['Translation', 'UserModel', 'PubSub', 'BundleModel'];
    var Component       = {
        templateUrl     : './edit-bundle.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('editBundle', Component);
})();
