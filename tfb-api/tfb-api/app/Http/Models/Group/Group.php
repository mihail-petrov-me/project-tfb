<?php
namespace App\Http\Models\Group;

use App\Http\Models\Group\Create\CreateGroupEntity;
use App\Http\Models\Model;

use \Carbon\Carbon;

use App\Http\Models\Group\Fetch\Collection\FetchGroupSubscriberCollection;

use App\Http\Models\Entity\Post\FetchPost;
use App\Http\Models\Entity\Article\FetchArticle;

use App\Http\Models\Entity\Feed\FetchFeed;
use App\Http\Models\Entity\Feed\FetchFeedCollection;

use DB;

class Group extends Model
{
    // Public fields
    const ID            = "group_id";
    const TITLE         = "group_title";
    const DESCRIPTION   = "group_description";
    const CAUSES        = "group_causes";
    const PICTURE       = "group_picture";

    // Meta fields
    const CREATED_AT    = "created_at";
    const UPDATED_AT    = "updated_at";
    const DELETED_AT    = "deleted_at";

    // Tables
    const TABLE         = "groups";
    const TABLE_CAUSES  = "group__group_cause";
    const TABLE_BLOCKS  = "group__block";

    /**
     * 
     * @param type $collection
     * @return array
     */
    public function create_group($collection) 
    {        
        return (new CreateGroupEntity)->execute($collection);
    }
    
    /**
     * 
     * @param type $id
     * @return entity | single
     */
    public function get($id) 
    {
        return DB::table($this->table)
                ->where('id', $id)
                ->first();
    }
    
    /**
     * 
     * @param type $id
     */
    public function getParticipents($id) 
    {
        return (new FetchGroupSubscriberCollection())->execute($id);
    }
    
    /**
     * 
     * @param type $collection
     * @return type
     */
    public function subscribe_group($collection) 
    {
        return (new SubscribeToGroupEntity)->execute($collection);
    }

    /**
     * 
     * @param type $query
     * @return type
     */
    public function all($query) 
    {
        $collection = $this->select(
                DB::table($this->table)->orderBy('created_at', 'desc'), 
        $query);
        
        return $this->transform_Groups($collection);
    }
    
    /**
     * 
     * @param type $id
     * @param type $collection
     */
    public function update($id, $collection)
    {
        
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function delete($id)
    {
        return DB::table($this->table)->where('id', $id)->update(array(
            'deleted_at'  => Carbon::now()
        ));
    }

    
       
    /**
     * 
     * @param type $collection
     */
    private function transform_Groups($collection)
    {
        foreach($collection as $element) {
            $element->couses = explode(',', $element->couses);
        }
        
        return $collection;
    }
    
    /**
     * 
     * @param type $id
     * @param type $query
     * @return type
     */
    public function get_postCollection($id, $query) 
    {        
        $build = (new FetchPost())->execute($id, $query);
        $this->setRefresh('block_post.created_at');
        return $this->select($build, $query);
    }
    
    /**
     * 
     * @param type $id
     * @param type $query
     * @return type
     */
    public function get_articleCollection($id, $query) 
    {
        $build = (new FetchArticle())->execute($id, $query);        
        $this->setRefresh('block_article.created_at');
        return $this->select($build, $query);        
    }

    /**
     * 
     * @param type $id
     * @param type $query
     * @return type
     */
    public function get_pollCollection($id, $userId,  $query) 
    {
        $this->setPagination('start', $this->process($query, 'start', 0));
        $this->setPagination('limit', $this->process($query, 'limit', 0));
             
        $subQuery = "(SELECT * FROM block_poll LIMIT {$this->getPagination()["limit"]} OFFSET {$this->getPagination()["start"]}) block_poll";
        
        $collection = DB::table(DB::raw($subQuery))
            ->join(
                    'group_entity', 
                    'block_poll.id', '=', 'group_entity.entity_id')
            ->leftJoin(
                    'users', 
                    'block_poll.user_id', '=', 'users.id')                    
            ->leftJoin(
                    'user_experience', 
                    'user_experience.user_id', '=', 'users.id')                                        
            ->join(
                    'block_poll_answer', 
                    'block_poll.id', '=', 'block_poll_answer.poll_id')
            ->leftJoin(
                    'block_poll_answer_stat', 
                    'block_poll_answer_stat.entity_id', '=', 'block_poll.id')
            ->select(
                    'group_entity.entity_type',
                    'block_poll.id',
                    'block_poll.question', 
                    'block_poll.created_at',
                    'users.name',
                    'user_experience.position',
                    'block_poll_answer.content',
                    'block_poll_answer.count',
                    'block_poll_answer.id AS answer_id',
                    'block_poll_answer_stat.user_id AS has_answer'
            )
            ->where('group_entity.group_id', $id)
            ->orderBy('block_poll.created_at', 'desc')
            ->get();
        
        
        $hasNext = (count($collection) == $this->getPagination()["limit"]);
        $this->setPagination('has_next', $hasNext);
        $this->setPagination('next', (count($collection) + 1 + $this->getPagination()["start"]));
        
        
        if(isset($collection) && count($collection) > 0) {
            $this->timestamp = $collection[0]->created_at;
        }
        
        $this->setRefresh('block_poll.created_at');
        //$resultSet = $this->select($build, $query);

       
        $question   = array();
        $answers    = array();
        $total      = array();
      
        $collectionOfIds = array();
        
        // extract question and answer
        foreach($collection as $element) {
         
            if(!array_key_exists($element->id, $question)) {
                
                $question[$element->id] = array(
                    'entity_type'   => '3',
                    'entity_id'     => $element->id,
                    'question'      => $element->question,
                    'id'            => $element->answer_id,
                    'name'          => $element->name,
                    'position'      => $element->position,
                    'has_answer'    => ($element->has_answer == $userId)
                );
            }
            
            if(array_key_exists($element->id, $answers)) {
                                    
                if(!in_array($element->answer_id, $collectionOfIds)) {

                    array_push($answers[$element->id], array(
                        'content'   => $element->content,
                        'id'        => $element->answer_id,
                        'count'     => $element->count
                    ));

                    $total[$element->id] = $total[$element->id] + (int)$element->count;
                    array_push($collectionOfIds, $element->answer_id);
                }    
            }
            else {
                $answers[$element->id] = array();
                array_push($answers[$element->id], array(
                    'content'   => $element->content,
                    'id'        => $element->answer_id,
                    'count'     => $element->count
                ));
                
                //$total = $total + (int)$element->count;
                $total[$element->id] = (int)$element->count;
                array_push($collectionOfIds, $element->answer_id);
            }
        }
        
        
        $rr = array();
        

        foreach ($question as $key => $value) {
            
            $p = $question[$key];
            $p['answers']   = $answers[$key];
            $p['total']     = $total[$key];            
            array_push($rr, $p);
        }
        
        return $rr;
    }
    
    
    
    
    /**
     * 
     * @param type $id
     * @param type $query
     * @return type
     */
    public function get_pollSingle($id, $userId)
    {

        $collection = DB::table('block_poll')
            ->join(
                    'group_entity', 
                    'block_poll.id', '=', 'group_entity.entity_id')
            ->leftJoin(
                    'users', 
                    'block_poll.user_id', '=', 'users.id')                    
            ->leftJoin(
                    'user_experience', 
                    'user_experience.user_id', '=', 'users.id')                                        
            ->join(
                    'block_poll_answer', 
                    'block_poll.id', '=', 'block_poll_answer.poll_id')
            ->leftJoin(
                    'block_poll_answer_stat', 
                    'block_poll_answer_stat.entity_id', '=', 'block_poll.id')
            ->select(
                    'group_entity.entity_type',
                    'block_poll.id',
                    'block_poll.question', 
                    'block_poll.created_at',
                    'users.name',
                    'user_experience.position',
                    'block_poll_answer.content',
                    'block_poll_answer.count',
                    'block_poll_answer.id AS answer_id',
                    'block_poll_answer_stat.user_id AS has_answer'
            )
            ->where('block_poll.id', $id)
            ->get();
        
        $question   = array();
        $answers    = array();
        $total      = array();
      
        $collectionOfIds = array();
        
        // extract question and answer
        foreach($collection as $element) {
         
            if(!array_key_exists($element->id, $question)) {
                
                $question[$element->id] = array(
                    'entity_type'   => '3',
                    'entity_id'     => $element->id,
                    'question'      => $element->question,
                    'id'            => $element->answer_id,
                    'name'          => $element->name,
                    'position'      => $element->position,
                    'has_answer'    => ($element->has_answer == $userId)
                );
            }
            
            if(array_key_exists($element->id, $answers)) {
                                    
                if(!in_array($element->answer_id, $collectionOfIds)) {

                    array_push($answers[$element->id], array(
                        'content'   => $element->content,
                        'id'        => $element->answer_id,
                        'count'     => $element->count
                    ));

                    $total[$element->id] = $total[$element->id] + (int)$element->count;
                    array_push($collectionOfIds, $element->answer_id);
                }    
            }
            else {
                $answers[$element->id] = array();
                array_push($answers[$element->id], array(
                    'content'   => $element->content,
                    'id'        => $element->answer_id,
                    'count'     => $element->count
                ));
                
                $total[$element->id] = (int)$element->count;
                array_push($collectionOfIds, $element->answer_id);
            }
        }
        
        
        $rr = array();
        

        foreach ($question as $key => $value) {
            
            $p = $question[$key];
            $p['answers']   = $answers[$key];
            $p['total']     = $total[$key];            
            array_push($rr, $p);
        }
        
        return $rr[0];
    }
    
    
    /**
     * 
     * @param type $id
     * @param type $query
     * @return type
     */
    public function get_videoCollection($id, $query) 
    {
        $build = DB::table('block_video')
            ->leftJoin('group_entity', 'block_video.id', '=', 'group_entity.entity_id')
            ->leftJoin('users', 'block_video.user_id', '=', 'users.id')                    
            ->leftJoin('user_experience', 'user_experience.user_id', '=', 'users.id')                                        
            ->where('block_video.group_id', $id);
        
        $this->setRefresh('block_video.created_at');
        return $this->select($build, $query);        
    }
    
    /**
     * 
     * @param type $groupId
     * @param type $query
     * @return type
     */    
    public function fetch_feed($collectionId, $userId, $query)
    {
        if($this->isRefresh($query)) {
            
            
            if(isset($query['timestamp'])) {
           
                $collection = (new FetchFeed())->executeSecond($collectionId, $userId)
                ->where('collection_entity.created_at', '>', $query['timestamp'])
                ->get();

                $this->timestamp = $collection[0]->created_at;
                return $this->parsePoll($collection, $userId);                                
            }
            else {

                $collection = (new FetchFeed())->executeSecond($collectionId, $userId)
                ->get();

                $this->timestamp = $collection[0]->created_at;
                return $this->parsePoll($collection, $userId);                
            }
        }
        else {
  
            if(isset($query['filter'])) {
                
                $type____id = null;
                if($query['filter'] == 'posts') {
                    $type____id = 1;
                }
                
                if($query['filter'] == 'articles') {
                    $type____id = 2;
                }

                if($query['filter'] == 'polls') {
                    $type____id = 3;
                }                
                
                
                $this->setPagination('start', $this->process($query, 'start', 0));
                $this->setPagination('limit', $this->process($query, 'limit', 0));

                $q = (new FetchFeedCollection())->execute($this->getPagination()["limit"], $this->getPagination()["start"]);
                $collection = (new FetchFeed())->executeThird($q, $collectionId, $type____id);


                $hasNext = (count($collection) >= $this->getPagination()["limit"]);
                $this->setPagination('has_next', $hasNext);

                if((count($collection) >= $this->getPagination()["limit"])) {
                    $this->setPagination('next', ($this->process($query, 'limit')) + ($this->process($query, 'start')) );
                }
                else {
                    $this->setPagination('next', (count($collection) + 1 + $this->getPagination()["start"]));
                }


                if(isset($collection) && count($collection) > 0) {
                    $this->timestamp = $collection[0]->created_at;
                }

                $this->setRefresh('collection_entity.created_at');

                return $this->parsePoll($collection, $userId);
                
            }
            else {
            
                
                $this->setPagination('start', $this->process($query, 'start', 0));
                $this->setPagination('limit', $this->process($query, 'limit', 0));

                $q = (new FetchFeedCollection())->execute($this->getPagination()["limit"], $this->getPagination()["start"]);
                $collection = (new FetchFeed())->execute($q, $collectionId);


                $hasNext = (count($collection) >= $this->getPagination()["limit"]);
                $this->setPagination('has_next', $hasNext);

                if((count($collection) >= $this->getPagination()["limit"])) {
                    $this->setPagination('next', ($this->process($query, 'limit')) + ($this->process($query, 'start')) );
                }
                else {
                    $this->setPagination('next', (count($collection) + 1 + $this->getPagination()["start"]));
                }


                if(isset($collection) && count($collection) > 0) {
                    $this->timestamp = $collection[0]->created_at;
                }

                $this->setRefresh('collection_entity.created_at');

                return $this->parsePoll($collection, $userId);                
            }
            
                        
//            $this->setPagination('start', $this->process($query, 'start', 0));
//            $this->setPagination('limit', $this->process($query, 'limit', 0));
//
//            $q = (new FetchFeedCollection())->execute($this->getPagination()["limit"], $this->getPagination()["start"]);
//            $collection = (new FetchFeed())->execute($q, $collectionId, $userId);
//
//
//            $hasNext = (count($collection) >= $this->getPagination()["limit"]);
//            $this->setPagination('has_next', $hasNext);
//
//            if((count($collection) >= $this->getPagination()["limit"])) {
//                $this->setPagination('next', ($this->process($query, 'limit')) + ($this->process($query, 'start')) );
//            }
//            else {
//                $this->setPagination('next', (count($collection) + 1 + $this->getPagination()["start"]));
//            }
//
//
//
//            if(isset($collection) && count($collection) > 0) {
//                $this->timestamp = $collection[0]->created_at;
//            }
//
//            $this->setRefresh('collection_entity.created_at');
//
//            return $this->parsePoll($collection, $userId);
            
            
        }
    }
    
    public function parsePoll($collection, $userId)
    {   
        $questionCollection = array();
        $answerCollection   = array();
        $idCollection       = array();
        $totalCollection    = array();
                
        foreach($collection as $element) {
            
            
            if($element->entity_type == 3) {
                
                if(!array_key_exists($element->entity_id, $questionCollection)) {
                    $questionCollection[$element->entity_id] = $this->assigne_poll_question($element, $userId);
                }                
                
                if(!array_key_exists($element->entity_id, $answerCollection)) {

                    $answerCollection[$element->entity_id]      = array();
                    $answerCollection[$element->entity_id][]    = $this->assigne_poll_answer($element);
                    $idCollection[]                             = $element->poll_answer_id;
                    $totalCollection[$element->entity_id]       = ((int)$element->poll_answer_count);
                }

                if(array_key_exists($element->entity_id, $answerCollection) && (!in_array($element->poll_answer_id, $idCollection))) {

                    $answerCollection[$element->entity_id][] = $this->assigne_poll_answer($element);
                    $idCollection[]                          = $element->poll_answer_id;
                    $totalCollection[$element->entity_id]    = ($totalCollection[$element->entity_id] + (int)$element->poll_answer_count);
                }                
            }
            
            if($element->entity_type == 1) {
                $questionCollection[$element->entity_id] = $this->assigne_post_question($element, $userId);
            }
            
            if($element->entity_type == 2) {
                $questionCollection[$element->entity_id] = $this->assigne_article_question($element, $userId);
            }            
        }
        
        return $this->assigne_poll_collection($questionCollection, $answerCollection, $totalCollection);
    }

    /**
     * 
     * @param type $element
     * @param type $userId
     * @return type
     */
    private function assigne_post_question($element, $userId)
    {
                
        return array(
            'post_content'=> $element->post_content,

            // Get user meta data                   
            // ##
            'name'        => $element->name,
            'position'    => $element->position,
            'type'        => $element->type,
            'signature'   => $element->signature,
            'picture'     => $element->picture,

            // Get collection meta data                   
            // ##
            'entity_id'   => $element->entity_id,
            'entity_type' => $element->entity_type,
            'created_at'  => $element->created_at,
        );
    }
    
    /**
     * 
     * @param type $element
     * @param type $userId
     * @return type
     */
    private function assigne_article_question($element, $userId)
    {
        return array(
            'article_content' => $element->article_content,
            'article_title'   =>  $element->article_title,

            // Get user meta data                   
            // ##
            'name'        => $element->name,
            'position'    => $element->position,
            'type'        => $element->type,
            'signature'   => $element->signature,            
            'picture'     => $element->picture,

            // Get collection meta data                   
            // ##
            'entity_id'   => $element->entity_id,
            'entity_type' => $element->entity_type,
            'created_at'  => $element->created_at,
        );
    }    
    
    
    /**
     * 
     * @param type $element
     * @param type $userId
     * @return type
     */
    private function assigne_poll_question($element, $userId)
    {
        return array(
            'poll_question' => $element->poll_question,
            'id'            => $element->poll_answer_id,
            'has_answer'    => ($element->has_answer == $userId),
            
            
            // Get user meta data                   
            // ##
            'name'        => $element->name,
            'position'    => $element->position,
            'type'        => $element->type,
            'signature'   => $element->signature,             
            'picture'     => $element->picture,

            // Get collection meta data                   
            // ##
            'entity_id'   => $element->entity_id,
            'entity_type' => $element->entity_type,
            'created_at'  => $element->created_at,         
        );
    }
    
    /**
     * 
     * @param type $element
     * @return type
     */
    private function assigne_poll_answer($element)
    {
        return array(
            'content'   => $element->poll_answer_content,
            'id'        => $element->poll_answer_id,
            'count'     => $element->poll_answer_count
        );
    }
    
    /**
     * 
     * @param type $element
     * @param array $answers
     * @param type $collectionOfIds
     */
    private function assigne_poll_answer_collection($element, $answers, $collectionOfIds)
    {
        if(array_key_exists($element->id, $answers)) {

            if(!in_array($element->poll_answer_id, $collectionOfIds)) {

                $total[$element->id] = $total[$element->id] + (int)$element->poll_answer_count;

                array_push($answers[$element->id], $this->assigne_poll_answer($element));
                array_push($collectionOfIds, $element->poll_answer_id);
            }    
        }
        else {
            $answers[$element->id] = array();
            $total[$element->id] = (int)$element->poll_answer_count;
        }
        
           array_push($answers[$element->id], $this->assigne_poll_answer($element));                                
            array_push($collectionOfIds, $element->poll_answer_id);
    }


    /**
     * 
     * @param type $question
     * @param type $answers
     * @param type $total
     * @return array
     */
    private function assigne_poll_collection($question, $answers, $total)
    {   
        $collection = array();
        foreach ($question as $key => $value) {            
            
            if($question[$key]['entity_type'] == "1") {
                
                
                $p              = $question[$key];
                array_push($collection, $p);                               
            }
            
            if($question[$key]['entity_type'] == "2") {
                
                $p              = $question[$key];
                array_push($collection, $p);               
            }

            if($question[$key]['entity_type'] == "3") {
                
                $p              = $question[$key];
                $p['answers']   = $answers[$key];
                $p['total']     = $total[$key];            
                array_push($collection, $p);               
                
            }                
        }
        
        return $collection;
    }
}