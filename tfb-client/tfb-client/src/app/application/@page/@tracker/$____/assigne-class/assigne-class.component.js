(function () {

    var ComponentRouter = [
        { path: '/'     , component: 'classListComponent'   , as: 'ClassListComponent'  , useAsDefault: true },
        { path: '/:id'  , component: 'studentListComponent' , as: 'StudentListComponent' },
    ];

    var Component       = {
        templateUrl     : './assigne-class.template.html',
        controllerAs    : 'model',
        $routeConfig    : ComponentRouter,
        bindings        : {
            $router     : '<'
        }
    };

    angular.module('application.components.page').component('assigneClass', Component);

})();
