# Update the liost of available packages
# ======================================
sudo apt-get -y install python-software-properties
sudo apt-get -y update
sudo apt-get -y upgrade

# Install system extentions
# ======================================
sudo apt-get -y install zip

# Install the latest version of mysql server
# ======================================
sudo apt-get -y install apache2
sudo apt-get -y install php
sudo apt-get -y install libapache2-mod-php
sudo apt-get -y install php-mcrypt


# Database mysql configuration
debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again  password root'
sudo apt-get -y install mysql-server
sudo apt-get -y install php-mysql

# install phpmyadmin and give password(s) to installer
# for simplicity I'm using the same password for mysql and phpmyadmin
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password root"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password root"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password root"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2"
sudo apt-get -y install phpmyadmin

#
# ======================================
sudo a2enmod rewrite
sudo service apache2 restart

# Install application build extentions
# ======================================
sudo apt-get -y install composer

# Install PHP extentions
# ======================================
sudo apt-get -y install php-xml
sudo apt-get -y install php-mbstring

# Create new host file
# ======================================
sudo echo "<VirtualHost *:80>

        ServerName api.zaednovchas.bg
        ServerAdmin webmaster@localhost


        DocumentRoot /var/www/html/tfb-api
        <Directory /var/www/html/tfb-api>
                AllowOverride All
                Options Indexes FollowSymLinks
                Order allow,deny
                Allow from all
                Require all granted
        </Directory>

</VirtualHost>" > /etc/apache2/sites-available/tfb-api.conf

sudo a2ensite tfb-api.conf
sudo service apache2 restart

# Create simbolic link in order to server the application
# ======================================

cd /var/www/html
sudo rm index.html
sudo ln -s /vagrant/tfb-api/public/ tfb-api

# Init application
# ======================================
sudo composer install