(function () {

    var ServiceHandler = function () {

        return {
            IS_EDIT : false
        }
    };

    angular.module('application.models').factory('FormState', ServiceHandler);
})();
