<?php

namespace App\Http\Models\Entity\Feed;
use DB;

class FetchFeed 
{
    public function execute($coolection, $collectionId)
    {    
        return DB::table($coolection)
//        return DB::table('collection_entity')
            ->leftJoin(
                    'block_post',
                    'collection_entity.entity_id', '=', 'block_post.id'
            )
            ->leftJoin(
                    'block_article',
                    'collection_entity.entity_id', '=', 'block_article.id'
            )
            ->leftJoin(
                    'block_poll',
                    'collection_entity.entity_id', '=', 'block_poll.id'
            )
            ->leftJoin(
                    'block_poll_answer', 
                    'block_poll.id', '=', 'block_poll_answer.entity_id'
            )
            ->leftJoin(
                    'block_poll_answer_stat', 
                    'block_poll_answer_stat.entity_id', '=', 'block_poll.id'
            )                    
            ->leftJoin(
                    'users',
                    'collection_entity.user_id', '=', 'users.id'
            )         
            ->select(
                    // Get content data
                    // #
                    'block_post.content AS post_content',
                    
                    'block_article.content AS article_content',
                    'block_article.title AS article_title',
                    
                    
                    'block_poll.question AS poll_question',
                    'block_poll_answer.id AS poll_answer_id',
                    'block_poll_answer.content AS poll_answer_content',
                    'block_poll_answer.count AS poll_answer_count',
                    'block_poll_answer_stat.user_id AS has_answer',
                    
                    // Get user meta data
                    // #
                    'users.name',
                    'users.picture',
                    'users.signature',
                    
                    // Get collection meta data                   
                    // #
                    'collection_entity.entity_id',
                    'collection_entity.entity_type',
                    'collection_entity.created_at'
            )
            ->orderBy(
                    'collection_entity.created_at', 'desc'
            )
            ->where(
                    'collection_entity.collection_id', $collectionId
            )
            ->get();
    }
    
    
    
    public function executeSecond($collectionId, $userId)
    {
        return DB::table('collection_entity')
            ->leftJoin(
                    'block_post',
                    'collection_entity.entity_id', '=', 'block_post.id'
            )
            ->leftJoin(
                    'block_article',
                    'collection_entity.entity_id', '=', 'block_article.id'
            )
            ->leftJoin(
                    'block_poll',
                    'collection_entity.entity_id', '=', 'block_poll.id'
            )
            ->leftJoin(
                    'block_poll_answer', 
                    'block_poll.id', '=', 'block_poll_answer.entity_id'
            )
            ->leftJoin(
                    'block_poll_answer_stat', 
                    'block_poll_answer_stat.entity_id', '=', 'block_poll.id'
            )                    
            ->leftJoin(
                    'users',
                    'collection_entity.user_id', '=', 'users.id'
            )         
            ->select(
                    // Get content data
                    // #
                    'block_post.content AS post_content',
                    
                    'block_article.content AS article_content',
                    'block_article.title AS article_title',
                    
                    
                    'block_poll.question AS poll_question',
                    'block_poll_answer.id AS poll_answer_id',
                    'block_poll_answer.content AS poll_answer_content',
                    'block_poll_answer.count AS poll_answer_count',
                    'block_poll_answer_stat.user_id AS has_answer',
                    
                    // Get user meta data
                    // #
                    'users.name',
                    'users.picture',
                    'users.signature',
                    
                    // Get collection meta data                   
                    // #
                    'collection_entity.entity_id',
                    'collection_entity.entity_type',
                    'collection_entity.created_at'
            )
            ->orderBy(
                    'collection_entity.created_at', 'desc'
            )
            ->where(
                    'collection_entity.collection_id', $collectionId
            );
    }
    
    
    public function executeThird($coolection, $collectionId, $filter)
    {
                return DB::table($coolection)
//        return DB::table('collection_entity')
            ->leftJoin(
                    'block_post',
                    'collection_entity.entity_id', '=', 'block_post.id'
            )
            ->leftJoin(
                    'block_article',
                    'collection_entity.entity_id', '=', 'block_article.id'
            )
            ->leftJoin(
                    'block_poll',
                    'collection_entity.entity_id', '=', 'block_poll.id'
            )
            ->leftJoin(
                    'block_poll_answer', 
                    'block_poll.id', '=', 'block_poll_answer.entity_id'
            )
            ->leftJoin(
                    'block_poll_answer_stat', 
                    'block_poll_answer_stat.entity_id', '=', 'block_poll.id'
            )                    
            ->leftJoin(
                    'users',
                    'collection_entity.user_id', '=', 'users.id'
            )         
            ->select(
                    // Get content data
                    // #
                    'block_post.content AS post_content',
                    
                    'block_article.content AS article_content',
                    'block_article.title AS article_title',
                    
                    
                    'block_poll.question AS poll_question',
                    'block_poll_answer.id AS poll_answer_id',
                    'block_poll_answer.content AS poll_answer_content',
                    'block_poll_answer.count AS poll_answer_count',
                    'block_poll_answer_stat.user_id AS has_answer',
                    
                    // Get user meta data
                    // #
                    'users.name',
                    'users.picture',
                    'users.signature',
                    
                    // Get collection meta data                   
                    // #
                    'collection_entity.entity_id',
                    'collection_entity.entity_type',
                    'collection_entity.created_at'
            )
            ->orderBy(
                    'collection_entity.created_at', 'desc'
            )
            ->where(
                    'collection_entity.collection_id', $collectionId
            )
            ->where(
                    'collection_entity.entity_type', '=', $filter
            )
            ->get();
    }
}