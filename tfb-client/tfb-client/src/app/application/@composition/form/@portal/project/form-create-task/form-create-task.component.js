(function () {

    var Controller = function(Translation, PubSub, Form) {

        this.name = "FORM_CREATE_TASK";

        this.specialID = null;

        this.FIELD = {

            "task_title": {
                name            : "task_title",
                type            : 'text',
                value           : null,
                show_errors     : false,
                isRequired      : true
            }
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                PAGE_TITLE          :  $provider({
                    'en' : 'Create New Project',
                    'bg' : 'Нов проект'
                }),


                BUTTON_CREATE       :  $provider({
                    'en' : 'Create New Project',
                    'bg' : 'нов проект'
                }),

                INPUT_TITLE         :  $provider({
                    'en' : 'Project title',
                    'bg' : 'Наименование'
                }),

                INPUT_DESCRIPTION   :  $provider({
                    'en' : 'Project description',
                    'bg' : 'Описание'
                }),

                INPUT_DATE_START        :  $provider({
                    'en' : 'Project date start',
                    'bg' : 'Начало на проекта'
                }),

                INPUT_DATE_END        :  $provider({
                    'en' : 'Project date end',
                    'bg' : 'Край на проекта'
                }),
            };
        });

        /**
         *
         */
        this.$onInit = () => {

            this.parent.FIELD  = this.FIELD;
            this.formContact   = this.parent.formContact;
            this.specialID     = this.parent.SPECIAL_ID;
        }

        /**
         *
         */
        this.on_create = () => {
            PubSub.form.init({ name: this.name, uid: this.specialID });
        };
    };

    /**
     * @type
     * Component injector
     */
    Controller.$inject = ['Translation', 'PubSub', 'Form'];

    var Component = {
        templateUrl     : './form-create-task.template.html',
        controller      : Controller,
        controllerAs    : 'model',

        require         : {
            parent          : '^formWrapper'
        },

        bindings        : {
            groupId         : '<'
        }
    };

    angular.module('application.components.page').component('formCreateTask', Component);

})();
