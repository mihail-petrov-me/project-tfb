(function () {


    /**
     *
     * @param {*} PubSub
     * @param {*} UserModel
     */
    var Controller = function($timeout, Translation, PubSub, UserModel) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;

        model.formCollection = [];

        model.$onInit = function() {

            UserModel.api.COLLECTION().fetch
                .education(UserModel.profile.my("user_id"))
                .then(function($response) {
                    console.log($response);
                    model.formCollection = $response.data;
                })
                .catch(function($response) {
                    console.log("@Exception");
                });
        }
    };

    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['$timeout','Translation','PubSub', 'UserModel'];

    /**
     *
     */
    var Component = {
        templateUrl     : './education-info.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    /**
     *
     */
    angular.module('application.components.page').component('educationInfo', Component);

})();
