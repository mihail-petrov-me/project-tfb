<?php
namespace App\Http\Models\Subject;

use App\Http\Models\Subject\Create\CreateSubject;

use App\Http\Models\Subject\Fetch\FetchSubjectSingle;
use App\Http\Models\Subject\Fetch\FetchSubjectCollection;

use App\Http\Models\Subject\Delete\DeleteSubject;
use App\Http\Models\Subject\Update\UpdateSubject;
use DB;


class Subject
{
    // Public fields
    const ID            = "subject_id";
    const TEACHER_ID    = "teacher_id";
    const BUNDLE_ID     = "bundle_id";
    const TITLE         = "subject_title";

    // Meta fields
    const CREATED_AT = "created_at";
    const UPDATED_AT = "updated_at";
    const DELETED_AT = "deleted_at";

    // Tables
    const TABLE         = "subjects";

    /**
     * @param $query
     * @return mixed
     */
    public function collection($query)
    {
        return (new FetchSubjectCollection())->execute($query);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function single($id)
    {
        return (new FetchSubjectSingle())->execute($id);
    }

    /**
     * @param $collection
     * @return array
     */
    public function create($collection)
    {
        return (new CreateSubject())->execute($collection);
    }

    /**
     * @param $id
     * @param $collection
     * @return mixed
     */
    public function update($id, $collection)
    {
        return (new UpdateSubject())->execute($id, $collection);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return (new DeleteSubject())->execute($id);
    }


    /**
     * @param $teacherId
     * @param $bundleId
     * @return mixed
     */
    public function mySubjects($teacherId, $bundleId)
    {
        return DB::table('teacher__subject')->where(array(
            'teacher_id'    => $teacherId,
            'bundle_id'     => $bundleId
        ))->get();
    }
}