(function () {


    var Controller = function (Translation, PubSub, UserModel, State) {

        /**
         * @type
         * Event Publish collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        this.SUBSCRIBE = {
            GROUP_LIST_INIT     : PubSub.events.page.GROUP.LIST.INIT,
            GROUP_LIST_LOAD     : PubSub.events.page.GROUP.LIST.LOAD,
            GROUP_INFO_INIT     : PubSub.events.page.GROUP.INFO.INIT,
            GROUP_INFO_LOAD     : PubSub.events.page.GROUP.INFO.LOAD
        };

        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en' : '@Groups',
                    'bg' : '@Групи'
                }),

                LABEL_ACTION : $provider({
                    'en' : '#Account information',
                    'bg' : '#Настройки'
                }),

                LABEL_CONTACT : $provider({
                    'en' : 'Contact',
                    'bg' : 'Лична информация'
                }),

                LABEL_EDUCATION : $provider({
                    'en' : 'Education',
                    'bg' : 'Образование'
                }),

                LABEL_EMPLOYMENT : $provider({
                    'en' : 'Employment',
                    'bg' : 'Работодатели'
                }),

                LABEL_LANGUAGE : $provider({
                    'en' : 'Language Preferences',
                    'bg' : 'Езикови предпочитания'
                }),

                LABEL_LOGOUT : $provider({
                    'en' : 'Log out',
                    'bg' : 'Излез от системата'
                })
            };
        });

        /**
         *
         */
        this.on_filter = () => {
            State.push("groups", "filter");
        };

        /**
         *
         */
        this.on_create = () => {
            State.push("groups", "modal-create");
        };

        /**
         *
         */
        this.on_mygroupShow = () => {
            State.push("groups", "my-group");
        };


        /**
         *
         */
        this.on_discusion = ($state) => {
            PubSub.api.publish('filter_discusion_da', $state);
        };

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        this.$onInit = () => {

            PubSub.api.subscribe('close_model_window', function() {
                State.pop("groups");
            });

            PubSub.api.subscribe(this.SUBSCRIBE.GROUP_LIST_INIT, function($subscribe) {
                // State.put("option", true);
                // this.STATE_BLOCK.group_list_init();
            });

            PubSub.api.subscribe(this.SUBSCRIBE.GROUP_LIST_LOAD, function($subscribe) {
                // this.STATE.IS_SIDEBAR_VISIBLE = $subscribe.has_data;
            });

            PubSub.api.subscribe(this.SUBSCRIBE.GROUP_INFO_INIT, function() {

                this.groupCollection = UserModel.profile.groups();
                State.push('groups', 'discusion');
            });

            PubSub.api.subscribe(this.SUBSCRIBE.GROUP_INFO_LOAD, function($subscribe) {

                this.memberCollection  = $subscribe.member_collection;
                this.groupCollection   = UserModel.profile.groups();
                // State.put('discusion', true);
            });
        };

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component destroy
         */
        this.$onDestroy = () => {

            PubSub.api.unsubscribe('close_model_window');
            PubSub.api.unsubscribe(this.SUBSCRIBE.GROUP_LIST_INIT);
            PubSub.api.unsubscribe(this.SUBSCRIBE.GROUP_LIST_LOAD);
            PubSub.api.unsubscribe(this.SUBSCRIBE.GROUP_INFO_INIT);
            PubSub.api.unsubscribe(this.SUBSCRIBE.GROUP_INFO_LOAD);
        };
    };


    var ComponentRouter = [
        { path: '/',     component: 'groupList',     as: 'GroupList', useAsDefault: true},
        { path: '/:id',  component: 'groupProfile',  as: 'GroupProfile'},
        { path: '/create',component: 'groupModify',   as: 'GroupModify'}
    ];

    Controller.$inject  = ['Translation', 'PubSub', 'UserModel', 'State'];
    var Component       = {
        templateUrl         : './groups.template.html',
        controller          : Controller,
        controllerAs        : 'model',
        $routeConfig        : ComponentRouter,
        bindings            : { $router: '<' }
    };

    angular.module('application.components.page').component('groups', Component);
})();
