(function () {

    /**
     *
     * @param {*} Translation
     * @param {*} PubSub
     * @param {*} Form
     */
    var Controller = function(Translation, PubSub, Form) {

        /**
         *
         */
        this.FIELD = null;


        this.specialID = () => {
            return (Date.now() + Math.random());
        }

        /**
         *
         */
        this.uiCollection = [];

        /**
         * @type
         * Event Publish collection
         */
        var PUBLISH = {
            FORM_PROCESS_END            : PubSub.events.form.PROCESS.END,
            FORM_PROCESS_UPDATE_END     : PubSub.events.form.PROCESS.UPDATE_END,
            FORM_CHANGE                 : PubSub.events.form.CHANGE
        };

        /**
         * @type
         * Event Subscribe collection
         */
        var SUBSCRIBE = {
            FORM_PROCESS_INIT           : PubSub.events.form.PROCESS.INIT,
            FORM_PROCESS_UPDATE_INIT    : PubSub.events.form.PROCESS.UPDATE,
        };

        /**
         *
         */
        var ERROR_MESSAGES = {
            REQUIRED: 'Полето е задължително'
        };

        /**
         *
         */
        var customInputErrorsCollection = 0;

        /**
         *
         */
        this.process = ($type, index) => {

            switch($type) {
                case 'dropdown':
                    this.process_dropdown(index);
                break;

                case 'checkbox' :
                    this.process_checkbox(index);
                break;

                default :
                    this.process_input(index);
            }
        };


        /**
         *
         * @param {*} index
         */
        this.process_dropdown = (index) => {

            this.FIELD[index].show_errors  = (this.FIELD[index].value == null);
            customInputErrorsCollection   += (this.FIELD[index].value == null) ? 1 : 0;
            this.FIELD[index].error        = ERROR_MESSAGES.REQUIRED;
        };

        /**
         *
         * @param index
         */
        this.process_checkbox = (index) => {
            this.FIELD[index].value = this.FIELD[index].collection;
        };

        /**
         *
         * @param {*} index
         */
        this.process_input = (index) => {

            this.FIELD[index].show_errors  = this.formContact[index].$invalid;
            this.FIELD[index].error        = ERROR_MESSAGES.REQUIRED;
        };

        /**
         * @type
         * Event Callback
         */
        this.on_process = (name) => {

            if (this.formId == name.name && this.SPECIAL_ID == name.uid) {

                for (let index in this.FIELD) {
                    this.process(this.FIELD[index].type, index);
                }

                if (this.formContact.$valid || (customInputErrorsCollection == 0)) {

                    let inputCollection     = Form.process([this.FIELD]);
                    let responseCollection  = angular.extend({}, inputCollection, this.fields);

                    PubSub.api.publish(PUBLISH.FORM_PROCESS_END, {
                        "form_id"   : this.formId,
                        "response"  : responseCollection
                    });

                    Form.clear(this.FIELD);
                }

                customInputErrorsCollection = 0;
            }
        };

        /**
         * @type
         * Event Callback
         */
        this.on_update = (name) => {

            if (this.formId == name.name && this.SPECIAL_ID == name.uid) {

                for (var index in this.FIELD) {
                    (this.FIELD[index].type == 'dropdown') ? this.process_dropdown(index) : this.process_input(index);
                }

                if (this.formContact.$valid && (customInputErrorsCollection == 0)) {

                    var $c  = Form.process([this.FIELD]);
                    $c.id   = this.uiId;
                    PubSub.api.publish(PUBLISH.FORM_PROCESS_UPDATE_END, {
                        "form_id"   : this.formId,
                        "response"  : $c
                    });
                }

                customInputErrorsCollection = 0;
            }
        };


        /**
         * @type
         * Event: @ON CHANGE
         */
        this.on_change = () => {

            PubSub.api.publish(PUBLISH.FORM_CHANGE, {
                'form_id'           : this.formId,
                'form_change_valid' : this.formContact.$valid
            });
        };


        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {

            PubSub.api.subscribe(SUBSCRIBE.FORM_PROCESS_INIT, this.on_process);
            PubSub.api.subscribe(SUBSCRIBE.FORM_PROCESS_UPDATE_INIT, this.on_update);

            this.SPECIAL_ID = this.specialID();
        };

        /**
         * @type
         * Lifecycle event
         */
        this.$onChanges = () => {

            if (this.uiCollection) {

                for (var index in this.uiCollection) {

                    if (this.FIELD[index]) {
                        this.FIELD[index].value = this.uiCollection[index];
                    }
                }
            }
        };

        /**
         * @type
         * Lifecycle event
         */
        this.$onDestroy = () => {

            PubSub.api.unsubscribe(SUBSCRIBE.FORM_PROCESS_INIT);
            PubSub.api.unsubscribe(SUBSCRIBE.FORM_PROCESS_UPDATE_INIT);
        };
    };


    /**
     *
     */
    var Component = {
        templateUrl     : './form-wrapper.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        transclude      : true,

        bindings        : {
            formId      : "@",
            fields      : "<"
        }
    };

    angular.module('application.components.visual').component('formWrapper', Component);
})();
