<?php
namespace App\Http\Models\Bundle\Fetch;

use App\Http\Models\Bundle\Bundle;
use App\Http\Models\Repository;
use DB;

class FetchBundleSingle extends Repository
{
    public function execute($id)
    {
        return DB::fetch(Bundle::TABLE, [
            Bundle::ID  => $id
        ]);
    }
}