<?php

namespace App\Http\Models\Campaign\Fetch;
use DB;

class FetchCampaignTeacher
{
    public function execute()
    {
        return DB::table('users')
            ->select(array(
                'id',
                'name',
                'signature',
                'campaign_id'
            ))
            ->leftJoin('campaign_teacher', 'users.id', '=', 'campaign_teacher.campaign_id')
            ->leftJoin('user__user_type', 'users.id' , '=', 'users.user_id')
            ->where('user__user_type.user_type_id', '3')
            ->get();
    }
}
