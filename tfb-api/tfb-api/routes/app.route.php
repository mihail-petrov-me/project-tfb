<?php

// GET
// ###
$app->get('/app/area'                     , ['uses'  => 'Application\AppController@showAreaCollection'           ]);
$app->get('/app/area/{id}/municipality'   , ['uses'  => 'Application\AppController@showMunicipalityCollection'   ]);
$app->get('/app/municipality/{id}/city'   , ['uses'  => 'Application\AppController@showCityCollection'           ]);
$app->get('/app/city/{id}/school'         , ['uses'  => 'Application\AppController@showSchoolCollection'         ]);
$app->get('/app/academy'                  , ['uses'  => 'Application\AppController@showAcademyCollection'        ]);