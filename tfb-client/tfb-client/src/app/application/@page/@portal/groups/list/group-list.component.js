(function () {

    let Controller = function (GroupModel, PubSub, Translation) {

        /**
         * @type
         * State collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        this.STATE = {
            IS_INFOBLOCK_VISIBLE    : false,
            CAN_COLLECTION_LOAD_MORE: false
        };

        this.collection = [];

        /**
         * @type
         * Event Publish collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        this.PUBLISH = {

            // #LOCAL CUSTOM EVENTS
            GROUP_LIST_INIT     : PubSub.events.page.GROUP.LIST.INIT,
            GROUP_LIST_LOAD     : PubSub.events.page.GROUP.LIST.LOAD,
            GROUP_LIST_DESTROY  : PubSub.events.page.GROUP.LIST.DESTROY,

            // #PAGE CUSTOM EVENTS
            PAGE_LOAD           : PubSub.events.page.LOAD,

            // #COMPONENT CUSTOM EVENTS
            UI_SIDEBAR_DISABLE  : PubSub.events.component.UISIDEBAR.STATE.DISABLE,
        };

        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                EMPTY_LIST_MESSAGE: $provider({
                    'en': 'This list is empty pleace create a new one',
                    'bg': 'Няма създадени групи'
                }),

                BUTTON_CREATE_GROUP: $provider({
                    'en': 'Create new group',
                    'bg': 'Създай нова група'
                })
            };
        });


        /**
         * @type
         * EVENT : request
         */
        this.request_FETCH_modelCollection = () => {

            GroupModel.api.PAGINATE().fetch.groups()
                .then(this.handler_FETCH_modelCollection_success)
                .catch(this.handler_FETCH_modelCollection_error);
        };

        /**
         * @type
         * HANDLER :  success
         */
        this.handler_FETCH_modelCollection_success = ($response) => {

            this.STATE.CAN_COLLECTION_LOAD_MORE = $response.pagination.has_next;

            // #STATE : handle state of the application
            this.STATE.IS_INFOBLOCK_VISIBLE = ($response.data.length == 0);

            // #DATA: handle data
            if($response.data.length > 0) {
                this.collection = this.collection.concat($response.data);
            }

            // #EVENT : handle state of the application
            PubSub.api.publish(this.PUBLISH.GROUP_LIST_LOAD, {
                has_data : ($response.data.length > 0)
            });

            PubSub.api.publish(this.PUBLISH.PAGE_LOAD);
        };

        /**
         * @type
         * HANDLER :  error
         */
        this.handler_FETCH_modelCollection_error = () => {
            console.error("!!!Error");
        };


        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {

            /**
             * @type
             * Publisher
             */
            PubSub.api.publish(this.PUBLISH.GROUP_LIST_INIT);


            /**
             * @type
             * Subscriber
             */
            PubSub.api.subscribe('ui_list_load_more', () => {

                GroupModel.api.PAGINATE().fetch.groups()
                    .then(this.handler_FETCH_modelCollection_success)
                    .catch(this.handler_FETCH_modelCollection_error);
            });

            this.request_FETCH_modelCollection();
        };

        /**
         * @type
         * Lifecycle event
         */
        this.$onDestroy = () => {

            PubSub.api.publish(this.PUBLISH.GROUP_LIST_DESTROY);
            PubSub.api.unsubscribe('ui_list_load_more');
        };
    };

    /**
     * @type
     * Component injector
     */
    Controller.$inject = ['GroupModel', 'PubSub', 'Translation'];

    var Component = {
        templateUrl     : './group-list.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('groupList', Component);

})();
