(function () {

    var Service = function (PubSub) {

        /**
         *
         */
        var stateCollection = {};

        /**
         *
         */
        var stateReference = null;

        /**
         *
         * @param {*} domain
         * @param {*} id
         * @param {*} state
         */
        function register($domain, $id, $isInit) {

            // check if domain exists
            if(stateCollection[$domain]) {
                stateCollection[$domain]["repository"][$id] = { display : false };

                //
                // #
                stateCollection[$domain]["repository"][$id] = { display : false };
                if($isInit) {
                    push($domain, $id);
                }
            }
            else {

                stateCollection[$domain]                    = {};
                stateCollection[$domain]["repository"]      = {};
                stateCollection[$domain]["stack"]           = [];

                //
                // #
                stateCollection[$domain]["repository"][$id] = { display : false };
                if($isInit) {
                    push($domain, $id);
                }
            }
        }


        /**
         *
         */
        function push($domain, $id, $clearBackstack) {

            // get the current state repository
            // #
            var getDomainCollection = stateCollection[$domain]["repository"];


            // check if $id is valide
            // #
            if(!getDomainCollection[$id]) {
                return new Error("@Exception : Invalide state idenitticator");
            }

            var o   = {};
            o[$id]  = {display : true};
            stateCollection[$domain]["stack"].push(o);


            PubSub.api.publish('event:state:change', $domain);
        }

        /**
         *
         * @param {*} domain
         */
        function hasStack($domain) {

            if(stateCollection[$domain]) {
                return (stateCollection[$domain]["stack"].length > 1);
            }

            return false;
        }


        /**
         *
         * @param {*} callback
         */
        function listen(callback) {
            PubSub.api.subscribe('event:state:change', callback);
        }

        /**
         *
         * @param {*} id
         */
        function state($domain, $id) {

            // get the current state stack
            // #
            var getDomainCollection = stateCollection[$domain]["stack"];

            // get the current state stack length
            // #
            var stackLength = getDomainCollection.length - 1;

            // get the current state stack elemnt
            // #
            if(getDomainCollection[stackLength]) {
                if(getDomainCollection[stackLength][$id]) {
                    return getDomainCollection[stackLength][$id].display;
                }
            }
        }


        /**
         *
         * @param {*} domain
         */
        function pop($domain) {

            stateCollection[$domain]["stack"].pop();
            PubSub.api.publish('event:state:change', $domain);
        }


        /**
         *
         * @param {*} domain
         * @param {*} id
         * @param {*} state
         */
        function __add($domain, $id, $state) {

            // var isVisible                  = $state ? true : false;
            // stateCollection[$domain][$id]  = { display : isVisible };

            stateCollection[$domain][$id]  = { display : false };
        }

        return {
            register        : register,
            state           : state,
            hasStack        : hasStack,
            listen          : listen,

            //
            push            : push,
            pop             : pop,
        };
    };

    angular.module('application.service').factory('State', Service);
}());
