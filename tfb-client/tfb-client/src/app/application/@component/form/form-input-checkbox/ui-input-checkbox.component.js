(function () {

    var Controller = function () {


        this.collection = {};
        this.setCollection = (element) =>  {

            // check if element already in a collection
            if(this.collection[element.id]) {
                delete this.collection[element.id];
            }
            else {
                this.collection[element.id] = element.title;
            }
        }

        /**
         * @type
         * Event
         *
         * @description
         * Select element from drop down combobox
         */
        this.on_select = function ($event, element) {

            angular.element($event.currentTarget).toggleClass('checkbox-state--checked');
            this.setCollection(element);
            this.uiCollection = this.collection;
        };


        /**
         * @type
         * Request : FETCH : provider
         */
        this.request_PROVIDER = () => {

            this.uiProvider()
                .then(this.handle_PROVIDER_success)
                .catch(this.handle_PROVIDER_error)
        };

        /**
         * @type
         * Handler : FECH : SUCCESS
         */
        this.handle_PROVIDER_success = ($response) => {
            this.uiOptions = $response.data;
            this.uiCollection = $response.data;
        };

        /**
         * @type
         * Handler : FECH : ERROR
         */
        this.handle_PROVIDER_error = () => {
            console.log("@Exception : service provider");
        };

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        this.$onInit = () => {
            if(this.uiProvider) {
                this.request_PROVIDER();
            }
        };
    };

    let Component = {
        templateUrl: './ui-input-checkbox.template.html',
        controller: Controller,
        controllerAs: 'model',
        bindings: {
            uiTitle         : '<',
            uiOptions       : '<',
            uiIsActive      : '<',
            uiModel         : '<',
            uiActive        : '<',
            uiId            : '<',
            uiIsDisabled    : '<',

            uiProvider      : '<',

            uiCollection    : '='
        }
    };

    angular.module('application.components.visual').component('uiInputCheckbox', Component);

})();
