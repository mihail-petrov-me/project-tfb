<?php 

namespace App\Http\Transformars;

abstract class Transformer
{

    /**
     *
     * @var type 
     */
    protected $required = array();
    
    /**
     *
     * @var type 
     */
    protected $optional = array(
        
    );
    
    /**
     *
     * @var type 
     */
    protected $error = null;
    
    /**
     *
     * @var type 
     */
    protected $result = null;
    
    
    
    /**
     * 
     * @param type $key
     * @return type
     */
    public function isRequired($key)
    {
        return in_array($key, $this->required);
    }

    /**
     * 
     * @param type $key
     * @return type
     */
    public function isOptional($key)
    {
        return in_array($key, $this->optional);
    }

    /**
     * 
     * @param type $key
     * @return type
     */
    public function isField($key)
    {
        return ($this->isRequired($key) || $this->isOptional($key));
    }

    /**
     * 
     * @return type
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * 
     * @return type
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * 
     * @param type $collection
     */
    public function map($collection)
    {

        return array_map(function($element) {
            return $this->transform($element);
        }, $collection);
    }

    /**
     * 
     * @param type $collection
     */
    public function gard($collection) 
    {
        $requiredCollection = array();
        $resultCollection   = array();
        
        foreach ($collection as $key => $value) {
            
            // check if the field is in the expected result collection set
            if($this->isField($key)) {
                
                $resultCollection[$key] = $value;
                
                if($this->isRequired($key)) {
                    array_push($requiredCollection, $key);
                }
            }
        }
        
        $result = array_diff($this->required, $requiredCollection);
        
        if(!empty($result)) {
            $this->error = "Error field " . array_first($result) . " is required";
            return false;
        }
        else {
            $this->result = $resultCollection;
            return true;
        }
    }    

    
    /**
     * 
     */
    public abstract function transform($element);
    
}
