(function () {

    /**
     *
     * @param {*} Translation
     * @param {*} GroupModel
     * @param {*} PubSub
     */
    var Controller = FormController (function(Translation) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;


        /**
         *
         */
        model.name = "authentication";


        /**
         * @component
         */
        model.FIELD = {

            "user_email": {
                name            : "user_email",
                type            : 'textarea',
                value           : null,
                show_errors     : false,
                error           : 'Field is required'
            },

            "user_password": {
                name            : "user_password",
                type            : 'password',
                value           : null,
                show_errors     : false,
                error           : 'Field is required'
            }
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(function($provider) {

            model.LABEL = {

                INPUT_USERNAME          :  $provider({
                    'en' : 'username',
                    'bg' : 'потребителско име'
                }),

                INPUT_USERPASS       :  $provider({
                    'en' : 'password',
                    'bg' : 'парола'
                })
            };
        });
    });


    /**
     * @type
     * Component injector
     */
    Controller.$inject = ['Translation', 'PubSub', 'Form'];

    var Component = {
        templateUrl     : './form-authentication.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('formAuthentication', Component);

})();
