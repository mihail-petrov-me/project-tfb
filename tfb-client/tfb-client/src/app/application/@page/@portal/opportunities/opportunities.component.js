(function() {

  /**
   *
   */
  var Controller = function() {

    /**
     * Preserve component context. In this component model
     * every single scope property iss directly atached to
     * model variable.
     *
     * Pleace do not forget that in order
     * to apply the scope to the front end HTML structure
     * you must apply the model property directly
     */
    var model = this;

    /**
     *
     */
    model.$onInit = function() {

    };
  };

  /**
   *
   */
  var Component = {
    templateUrl : './opportunities.template.html',
    controller  : Controller
  };

  angular.module('application.components.page').component('opportunities', Component);

})();
