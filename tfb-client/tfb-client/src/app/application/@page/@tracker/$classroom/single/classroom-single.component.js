(function () {

    var Controller = function (Translation, UserModel, PubSub, BundleModel) {

        this.TITLE = null;

        /**
         * @type
         * Router event
         */
        this.$routerOnActivate = (next, previous) => {

            this.bundleId   = next.params.id;
            this.TITLE      = UserModel.state('__class_signature_classroom');
        };
    };


    var ComponentRouter = [
        { path: '/:id/attendance'  ,  component: 'attendanceClassroom'  ,    as: 'AttendanceClassroom'  , useAsDefault: true    },
        { path: '/:id/assesment'   ,  component: 'assesmentClassroom'   ,    as: 'AssesmentClassroom'                           },
        { path: '/:id/feedback'    ,  component: 'feedbackClassroom'    ,    as: 'FeedbackClassroom'                            },
        { path: '/:id/info'        ,  component: 'infoClassroom'        ,    as: 'InfoClassroom'                                },
    ];


    Controller.$inject  = ['Translation', 'UserModel', 'PubSub', 'BundleModel'];
    var Component       = {
        templateUrl     : './classroom-single.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        $routeConfig    : ComponentRouter,
        bindings        : { $router     : '<' }
    };

    angular.module('application.components.page').component('classroomSingle', Component);
})();
