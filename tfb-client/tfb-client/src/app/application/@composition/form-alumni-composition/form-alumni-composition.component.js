(function () {

    /**
     *
     * @param {*} window
     * @param {*} http
     * @param {*} PubSub
     * @param {*} UserModel
     */
    var Controller = function(PubSub) {

        /**
         *
         */
        var model = this;

        /**
         *
         */
        model.name = "alumni-form";

        /**
         *
         */
        model.FORM = {

            active      : null,
            collection  : ['alumni', 'alumni', 'employ'],

            get_active  : function() {
                return model.FORM.active;
            },

            set_active  : function($id) {
                model.FORM.active = model.FORM.collection[$id];
            }
        };

        /**
         *
         */
        model.$onInit = function() {

            PubSub.api.subscribe('event:ui:dropdown:after_select', function(collection) {

                if(collection.id == 'input:dropdown:user_type') {
                    model.FORM.set_active(collection.element.id - 1);
                }
            });


            /**
             *
             */
            PubSub.api.subscribe(PubSub.events.form.PROCESS.INIT, function(name) {
                if(name == 'alumni-form') {
                    PubSub.api.publish(PubSub.events.form.PROCESS.INIT, model.FORM.active)
                }
            });
        };


        model.$onDestroy = function() {

            PubSub.api.unsubscribe('event:ui:dropdown:after_select');
            PubSub.api.unsubscribe(PubSub.events.form.PROCESS.INIT);
        }
    };

    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['PubSub'];


  /**
   *
   */
  var Component = {
    templateUrl     : './form-alumni-composition.template.html',
    controller      : Controller,
    controllerAs    : 'model',
    bindings        : {
      $router   : '<'
    }
  };

  /**
   *
   */
  angular.module('application.components.page').component('formAlumniComposition', Component);

})();
