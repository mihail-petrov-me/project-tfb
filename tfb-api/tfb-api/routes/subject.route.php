<?php

// GET
// =====================================================================================================================
$app->get('subjects'                                       , 'Tracker\SubjectController@collection'     );
$app->get('subjects/{id}'                                  , 'Tracker\SubjectController@single'         );
$app->get('subjects/teacher/{id}/bundle/{bundle_id}'       , 'Tracker\SubjectController@mySubjects'     );

// POST
// =====================================================================================================================
$app->post('subjects'                                      , 'Tracker\SubjectController@create'         );

// PUT
// =====================================================================================================================
$app->put('subjects/{id}'                                  , 'Tracker\SubjectController@update'         );

// DELETE
// =====================================================================================================================
$app->delete('subjects/{id}'                               , 'Tracker\SubjectController@delete'         );