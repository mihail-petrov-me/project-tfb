<?php
namespace App\Http\Controllers\Api\Portal;

use App\Http\Models\Entity\Entity;

use \Illuminate\Http\Request;
use \App\Http\Controllers\Controller;

use \App\Services\TokenProvider;
use \Firebase\JWT\JWT;
use DB;

class EntityController extends Controller
{
    /**
     * Contains the service provider with witch the user can authenticate 
     * it self infront of the system
     * 
     * @var AuthenticationService 
     */
    private $tokenProvider = null;
    
    /**
     *
     * @var type 
     */
    private $model = null;

    /**
     * EntityController constructor.
     * @param Entity $user
     * @param TokenProvider $tokenProvider
     * @internal param TokenProvider $tokenProvider
     */
    public function __construct(Entity $user, TokenProvider $tokenProvider)
    {
        $this->model            = $user;
        $this->tokenProvider   = $tokenProvider;
    }


    /**
     * @param $id
     * @param Request $request
     * @return \App\Http\Controllers\type
     */
    public function showComment($id, Request $request) 
    {
        $collection = $this->model->show_comment($id, $request->query());
        return $this->responseSuccess($collection, $this->model->getMeta());
    }

    /**
     * @param $id
     * @param Request $request
     * @return \App\Http\Controllers\type
     */
    public function showUserFeed($id, Request $request) 
    {
        $collection = $this->model->fetch_user_feed($id, $request->query());
        return $this->responseSuccess($collection, $this->model->getMeta());
    }    
    
    /**
     * 
     * @param type $id
     * @param Request $request
     * @return type
     */
    public function showPostEntity($id, Request $request)
    {
        $entity = $this->model->fetch_post_entity($id, $request->query());
        return $this->responseSuccess($entity);
    }
    
    /**
     * 
     * @param type $id
     * @param Request $request
     * @return type
     */
    public function showPostCollection($id, Request $request)
    {
        $collection = $this->model->fetch_post_collection($id, $request->query());
        return $this->responseSuccess($collection, $this->model->getMeta());
    }
    
    /**
     * 
     * @param type $id
     * @param Request $request
     * @return type
     */
    public function showPollEntity($id, Request $request)
    {
        $entity = $this->model->fetch_poll_entity($id, $request->query());
        return $this->responseSuccess($entity);
    }

    /**
     * 
     * @param type $id
     * @param Request $request
     * @return type
     */
    public function showPollCollection($id, Request $request)
    {
        $collection = $this->model->fetch_poll_collection($id, $request->query());
        return $this->responseSuccess($collection, $this->model->getMeta());
    }
    
    /**
     * 
     * @param type $id
     * @param Request $request
     * @return type
     */
    public function showArticleEntity($id, Request $request)
    {
        $entity = $this->model->fetch_article_entity($id, $request->query());
        return $this->responseSuccess($entity);
    }
    
    /**
     * 
     * @param type $id
     * @param Request $request
     * @return type
     */
    public function showArticleCollection($id, Request $request)
    {
        $collection = $this->model->fetch_article_collection($id, $request->query());
        return $this->responseSuccess($collection, $this->model->getMeta());
    }

    /**
     * @param Request $request
     * @return \App\Http\Controllers\type
     */
    public function createComment(Request $request) 
    {
        $this->model->create_comment($request->json()->all());
        return $this->responseSuccess();
    }
}
