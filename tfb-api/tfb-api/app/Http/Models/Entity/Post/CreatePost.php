<?php

namespace App\Http\Models\Entity\Post;
use DB;

class CreatePost
{

    /**
     * 
     * @param type $collection
     * @return type
     */
    public function execute($collectionId, $collection) 
    {
        
       $entityCollection = $this->transform_query($collection);
       extract($entityCollection);
        
       $this->assigne_post($id, $userId, $content);
       $this->assigne_collection_entity($id, $collectionId, $userId); 
    }    
    
    /**
     * 
     * @param type $collection
     * @return type
     */
    private function transform_query($collection)
    {
        return array(
            'id'        => uniqid(),
            'content'   => $collection['content'],
            'userId'    => $collection['user_id']
        );
    }

    /**
     * 
     * @param type $id
     * @param type $userId
     * @param type $title
     * @param type $content
     */
    private function assigne_post($id, $userId, $content)
    {
        DB::table('block_post')->insertGetId(array(
            'id'        => $id,
            'content'   => $content,
            'user_id'   => $userId
        ));
    }
    
    
    /**
     * 
     * @param type $modelId
     * @param type $groupId
     */
    private function assigne_collection_entity($modelId, $groupId, $userId)
    {
        DB::table('collection_entity')->insert(array(
            'entity_id'     => $modelId,
            'collection_id' => $groupId,
            'user_id'       => $userId,
            'entity_type'   => 1
        ));
    }
}
