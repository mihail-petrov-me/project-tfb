<?php

use App\Http\Models\Campaign\CampaignTerm;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignTermTable extends Migration
{
    private $table = CampaignTerm::TABLE;

    public function up()
    {
        Schema::create($this->table, function(Blueprint $builder)
        {
            //# Primary Key
            $builder->char(CampaignTerm::ID, 13);
            $builder->char(CampaignTerm::CAMPAIGN_ID, 13);

            //# Table Entity Collection
            $builder->string(CampaignTerm::TITLE, 200);
            $builder->date(CampaignTerm::DATE_START, 200);
            $builder->date(CampaignTerm::DATE_END, 200);

            //# Meta
            $builder->timestamps();
            $builder->softDeletes();

            // Primary Key indexes
            $builder->primary([
                CampaignTerm::ID,
                CampaignTerm::CAMPAIGN_ID
            ]);
        });
    }

    public function down()
    {
        Schema::drop($this->table);
    }
}
