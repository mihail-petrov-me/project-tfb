<?php

namespace App\Http\Models\Subject\Fetch;
use App\Http\Models\Repository;
use DB;

class FetchSubjectCollection extends Repository
{
    public $filter = array(
        'teacher_id', 'bundle_id'
    );

    public $filterTransform = array(
        'teacher_id'  => 'goals.teacher_id',
        'bundle_id'   => 'goals.bundle_id'
    );

    public function execute($query)
    {
        $build = DB::table('goals');
        return $this->parseFilterQuery($build, $query)->get();
    }
}