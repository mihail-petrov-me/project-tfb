(function () {

    /**
     *
     * @param {*} UserModel
     * @param {*} PubSub
     * @param {*} Request
     */
    var Controller = function (UserModel, PubSub, Request) {

        /**
         * @type
         * State collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        this.STATE = {
            IS_SIDEBAR_VISIBLE : true,
            LIST_CAN_LOAD_MORE : false
        };

        this.$filter = {};


        this.collection = [];

        /**
         * @type
         * Request : FETCH
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        this.request_FETCH_collection = () => {

            UserModel.api.PAGINATE().fetch
            .users(this.$filter)
                .then(this.handle_INIT_FETCH_success)
                .catch(this.handler_INIT_FETCH_error);
        }

        /**
         * @type
         * Request : FETCH
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        this.request_REFRESH_collection = () => {

            UserModel.api.PAGINATE().fetch
            .users(this.$filter)
                .then(this.handle_REFRESH_COLLECTION_suscess)
                .catch(this.handle_REFRESH_COLLECTION_error);
        };

        /**
         * @type
         * Handler : FECH : SUCCESS
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        this.handle_INIT_FETCH_success =  ($response) => {

            this.LIST_CAN_LOAD_MORE = $response.pagination.has_next;
            this.collection         = this.collection.concat($response.data);
        };

        /**
         * @type
         * Handler : FECH : ERROR
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        this.handler_INIT_FETCH_error =  (response) => {
            console.error("Error in handling user behaiviour");
        };


        /**
         * @type
         * Handler : FECH : SUCCESS
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        this.handle_REFRESH_COLLECTION_suscess = ($response) => {

            this.LIST_CAN_LOAD_MORE    = $response.pagination.has_next;
            this.collection            = $response.data
        };

        /**
         * @type
         * Handler : FECH : ERROR
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        this.handle_REFRESH_COLLECTION_error = ($response) => {
            console.log("@EXCEPTION : While refreshing collection");
        };


        /**
         * @type
         * Filter : FECH : Request
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        this.filter_request = ($response) => {

            Request.flush();
            this.$filter = angular.extend(this.$filter, $response);
            this.request_REFRESH_collection();
        };

        /**
         * @type
         * Filter : FECH : Request
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        this.filter_refresh = ($response) => {

            Request.flush();
            this.$filter = $response;
            this.request_REFRESH_collection();
        };

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        this.$onInit =  () => {

            this.request_FETCH_collection();

            PubSub.api.publish(PubSub.events.PAGE.LOAD_FINISH);
            PubSub.api.publish("api:publish:user:list-view");

            // Request new pagination load from the current badge of elements
            // This request can be used for every single configuration of the
            // element collection
            PubSub.api.subscribe('ui-user-list:request:load', this.request_FETCH_collection);

            // Request filter configuration for :
            // * filter : user type
            // * filter : form alumni / teacher dropdown
            // * text search configuration
            PubSub.api.subscribe('users:filter:user_type'       ,  this.filter_refresh);
            PubSub.api.subscribe('event:ui:search_input'        ,  this.filter_request);
            PubSub.api.subscribe(PubSub.events.form.PROCESS.END ,  this.filter_request);
        };


        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        this.$onDestroy = () => {

            PubSub.api.unsubscribe('users:filter:user_type');
            PubSub.api.unsubscribe('ui-user-list:request:load');
            PubSub.api.unsubscribe(PubSub.events.form.PROCESS.END);
        };
    };


    Controller.$inject  = ['UserModel', 'PubSub', 'Request'];
    var Component       = {
        templateUrl     : './user-list.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('userList', Component);

})();
