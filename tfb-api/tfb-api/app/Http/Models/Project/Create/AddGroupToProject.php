<?php
namespace App\Http\Models\Project\Create;

use DB;
use App\Http\Models\Project\ProjectGroup;
use App\Http\Models\Project\Project;
use App\Http\Models\Repository;


/**
 * Class AddGroupToProject
 * @package App\Http\Models\Project\Create
 */
class AddGroupToProject extends Repository
{
    /**
     * @param $id
     * @param $collection
     *
     * @return array
     */
    public function execute(string $id, array $collection): array
    {
        $entity = $this->build($id, $collection);
        DB::table(Project::TABLE_GROUP)->insert($entity);

        return $entity;
    }

    /**
     * @param string $id
     * @param array $collection
     *
     * @return array
     */
    public function build(string $id, array $collection): array
    {
        return [
            ProjectGroup::ID        => uniqid(),
            Project::ID             => $id,
            ProjectGroup::TITLE     => $this->process($collection, ProjectGroup::TITLE)
        ];
    }
}