<?php

// GET routes
// =====================================================================================================================
$app->get('/groups'                      , 'Portal\GroupController@index'             );//->middleware('jwt');
$app->get('/groups/create'               , 'Portal\GroupController@create'            )->middleware('jwt');
$app->get('/groups/{id}'                 , 'Portal\GroupController@show'              )->middleware('jwt');
$app->get('/groups/{id}/edit'            , 'Portal\GroupController@edit'              )->middleware('jwt');
$app->get('/groups/{id}/members'         , 'Portal\GroupController@showParticipents'  )->middleware('jwt');
$app->get('/groups/{id}/articles'        , 'Portal\GroupController@getGroupArticle'   )->middleware('jwt');
$app->get('/groups/{id}/posts'           , 'Portal\GroupController@getGroupPosts'     )->middleware('jwt');
$app->get('/groups/{id}/videos'          , 'Portal\GroupController@getGroupVideo'     )->middleware('jwt');
$app->get('/groups/{id}/data'            , 'Portal\GroupController@getGroupData'      )->middleware('jwt');
$app->get('/groups/poll/{id}'            , 'Portal\GroupController@getPoll'           )->middleware('jwt');
$app->get('/groups/articles/{id}'        , 'Portal\GroupController@getGroupSingleArticle')->middleware('jwt');


// POST routes
// =====================================================================================================================
$app->post('/groups/test'                , 'Portal\GroupController@test'              )->middleware('jwt');
$app->post('/groups'                     , 'Portal\GroupController@store'             );
$app->post('/groups/subscribe'           , 'Portal\GroupController@subscribe'         )->middleware('jwt');
$app->post('/groups/{id}/articles'       , 'Portal\GroupController@createGroupArticle')->middleware('jwt');
$app->post('/groups/{id}/polls'          , 'Portal\GroupController@createGroupPoll'   )->middleware('jwt');
$app->post('/groups/{id}/videos'         , 'Portal\GroupController@createGroupVideo'  )->middleware('jwt');
$app->post('/groups/{id}/posts'          , 'Portal\GroupController@createGroupPost'   )->middleware('jwt');
$app->post('/groups/{id}/answer'         , 'Portal\GroupController@answerGroupPoll'   )->middleware('jwt');


// PUT routes
// =====================================================================================================================
$app->put('/groups/{id}'                 , 'Portal\GroupController@update'            )->middleware('jwt');

// DELETE routes
// =====================================================================================================================
$app->put('/groups/{id}'                 , 'Portal\GroupController@delete'            )->middleware('jwt');