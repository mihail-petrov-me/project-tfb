(function () {

    /**
     *
     * @param {*} Translation
     * @param {*} PubSub
     * @param {*} Video
     * @param {*} sce
     */
    var Controller = function (Component, Translation, PubSub, Video, $sce) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;

        /**
         * @type
         * Publish collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.PUBLISH = {
            'video'     : PubSub.events.component.VIDEO.FINISH,
            'finish'    : PubSub.events.component.FINISH
        };


        /**
         * @type
         * Publish collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.SUBSCRIBE = {
            'video'     : PubSub.events.component.VIDEO.PROCESS
        };

        /**
         * @type
         * State collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.STATE = {
            IS_FIND_ACTION_VISIBLE : false,
            IS_IFRAME_VISIBLE      : false,

            IS_MODE_INTERACTIVE   : false,
            IS_MODE_PRESENTATION  : true
        };

        /**
         * @type
         * Field collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.FIELD = {
            TITLE : null,
            URL   : null
        };


        /**
         * @type
         * Event : on
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.on_input = function() {
            model.STATE.IS_FIND_ACTION_VISIBLE = (model.FIELD.URL.length > 0);
        };

        /**
         * @type
         * Event : on
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.on_find = function() {

            model.FIELD.URL = Video.getDomainURL(model.FIELD.URL);
            model.STATE.IS_IFRAME_VISIBLE = true;
        };

        /**
         * @type
         * Event : init
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.init_process = function() {

            /**
             * Notify the information component that the
             * request neads to be processed from the component form
             */
            PubSub.api.publish(model.PUBLISH.video, {
                title   : model.FIELD.TITLE,
                content : model.FIELD.URL
            });

            /**
             * Notify the core component that the process
             * event is already finished. So it can resolve
             * the UI state of the curently active window
             */
            PubSub.api.publish(model.PUBLISH.finish);

            /**
             * Destroy the field content
             */
            model.FIELD.TITLE   = null;
            model.FIELD.URL     = null;
        };


        /**
         * @type
         * Event : init
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.$init_interactive = function () {

            Component.$interactive(model.uiMode, function () {

                model.STATE.IS_MODE_INTERACTIVE     = true;
                model.STATE.IS_MODE_PRESENTATION    = false;

                PubSub.api.subscribe(model.SUBSCRIBE.video, model.init_process);
            });
        };

        /**
         * @type
         * Event : init
         *
         * @description
         * This method is executed when the initialisation of
         * the current component ocurre. It is responsible for fetching
         * a collection of related groups.
         *
         * @return
         * void
         */
        model.$init_presentation = function () {

            Component.$presentation(model.uiMode, function () {

                model.STATE.IS_MODE_INTERACTIVE     = false;
                model.STATE.IS_MODE_PRESENTATION    = true;

                model.STATE.IS_IFRAME_VISIBLE   = true;

                model.FIELD.TITLE = model.uiTitle;
                model.FIELD.URL   = Video.getDomainURL(model.uiContent);
            });
        };


        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        model.$onInit = function() {
            model.$init_interactive();
        };


        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component change
         */
        model.$onChanges = function() {
            model.$init_presentation();
        }

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component change
         */
        model.$onDestroy = function() {
            PubSub.api.unsubscribe(model.SUBSCRIBE.video);
        }
    };

    Controller.$inject = ['Component','Translation', 'PubSub', 'Video', '$sce'];

    var Component = {
        templateUrl : './ui-block-video.template.html',
        controller  : Controller,
        controllerAs: 'model',
        bindings    : {
            uiReference : '<',
            uiMode      : '<',
            uiTitle     : '<',
            uiContent   : '<'
        }
    };

    angular.module('application.components.visual').component('uiBlockVideo', Component);

})();
