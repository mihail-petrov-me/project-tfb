(function () {

    /**
     *
     */
    var BASE_URL = "initiatives";

    /**
     *
     * @param {$http} request
     */
    var create = function(request, base, provider) {

        /**
         * @endpoint
         * POST : /groups
         *
         * @type
         * Request
         *
         * @param {*} collection
         *
         * @description
         * Create a new group
         *
         * @return
         * Promise
         */
        function group(collection) {

            return request.post(base.url([BASE_URL]), collection)
            .then(_handlerSuccessGroup).catch(_handlerErrorGroup);
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerSuccessGroup(response) {
            return response.data.success.data;
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerErrorGroup(response) {
            return provider.reject(response.status)
        }

        /**
         * @endpoint
         * POST : /groups/:model_id/articles
         *
         * @type
         * Request
         *
         * @param {*} collection
         *
         * @description
         * Create a new group
         *
         * @return
         * Promise
         */
        function article($modelId, collection) {

            if($modelId === undefined) {
                throw new Error("Model Identificator is required for this endpoint");
            }

            return request.post(base.url([BASE_URL, $modelId, 'articles']), collection)
            .then(_handlerSuccessArticle).catch(_handlerErrorArticle);
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerSuccessArticle(response) {
            return response.data.success.data;
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerErrorArticle(response) {
            return provider.reject(response.status)
        }


        /**
         * @endpoints
         * POST : /groups/:model_id/polls
         *
         * @type
         * Request
         *
         * @param {*} collection
         *
         * @description
         * Create a new group
         *
         * @return
         * Promise
         */
        function poll($modelId, collection) {

            if($modelId === undefined) {
                throw new Error("Model Identificator is required for this endpoint");
            }

            return request.post(base.url([BASE_URL, $modelId, 'polls']), collection)
            .then(_handlerSuccessPoll).catch(_handlerErrorPoll);
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerSuccessPoll(response) {
            return response.data.success.data;
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerErrorPoll(response) {
            return provider.reject(response.status)
        }


        /**
         * @endpoints
         * POST : /groups/:model_id/videos
         *
         * @type
         * Request
         *
         * @param {*} collection
         *
         * @description
         * Create a new group
         *
         * @return
         * Promise
         */
        function video($modelId, collection) {

            if($modelId === undefined) {
                throw new Error("Model Identificator is required for this endpoint");
            }

            return request.post(base.url([BASE_URL, $modelId, 'videos']), collection)
            .then(_handlerSuccessVideo).catch(_handlerErrorVideo);
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerSuccessVideo(response) {
            return response.data.success.data;
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerErrorVideo(response) {
            return provider.reject(response.status)
        }


        /**
         * @endpoints
         * POST : /groups/:model_id/polls
         *
         * @type
         * Request
         *
         * @param {*} collection
         *
         * @description
         * Create a new group
         *
         * @return
         * Promise
         */
        function post($modelId, collection) {

            if($modelId === undefined) {
                throw new Error("Model Identificator is required for this endpoint");
            }

            return request.post(base.url([BASE_URL, $modelId, 'posts']), collection)
            .then(_handlerSuccessPost).catch(_handlerErrorPost);
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerSuccessPost(response) {
            return response.data.success.data;
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerErrorPost(response) {
            return provider.reject(response.status)
        }

        /**
         * @type
         * Api
         *
         * @description
         * Expose the internal api of the create functionality regarding
         * the group comunication
         */
        return {
            group   : group,
            article : article,
            poll    : poll,
            video   : video,
            post    : post
        };
    };

    /**
     *
     * @param {*} request
     * @param {*} base
     * @param {*} provided
     */
    var fetch = function(request, base, provided) {

        /**
         * @endpoint
         * GET : /groups
         * GET : /groups/id
         *
         * @type
         * Request
         *
         * @param {*} id
         *
         * @description
         * Retreave information for ALL / SINGLE group entity based on
         * provided $id identificator
         *
         * @return
         * Promise
         */
        function groups($modelId) {

            return request.get(base.url(["groups", $modelId]))
            .then(_handlerSuccessGroup).catch(_handlerErrorGroup);
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerSuccessGroup(response) {
            return response.data.success.data;
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerErrorGroup(response) {
            return provided.reject(response.status);
        }

        /**
         * @endpoint
         * GET : /groups/id/members
         *
         * @type
         * Request
         *
         * @param {*} id
         *
         * @description
         * Every group has users that subscribe in its activities.
         * This method retreave all subscribers to a specific group
         *
         * @return
         * Promise
         */
        function members($modelId) {

            if($modelId === undefined) {
                throw new Error("Model Identificator is required for this endpoint");
            }

            return request.get(base.url([BASE_URL, $modelId, 'members']))
            .then(_handlerSuccessMembers).catch(_handlerErrorMembers);
        };

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerSuccessMembers(response) {
            return response.data.success.data;
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerErrorMembers(response) {
            return provided.reject(response.status);
        }

        /**
         * @endpoint
         * GET : /groups/:model_id/articles
         * GET : /groups/:model_id/articles/:resource_id
         * @type
         * Request
         *
         * @param {*} $modelId
         * @param {*} $resourceId
         *
         * @description
         * Every group has articles. This method will going to retreave
         * every single article
         *
         * @return
         * Promise
         */
        function articles($modelId, $resourceId) {

            if($modelId === undefined) {
                throw new Error("Model Identificator is required for this endpoint");
            }

            return request.get(base.url([BASE_URL, $modelId, 'articles', $resourceId]))
            .then(_handlerSuccessArticles).catch(_handlerErrorArticles);
        };

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerSuccessArticles(response) {
            return response.data.success.data;
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerErrorArticles(response) {
            return provided.reject(response.status);
        }


        /**
         * @endpoint
         * GET : /groups/:model_id/articles
         * GET : /groups/:model_id/articles/:resource_id
         * @type
         * Request
         *
         * @param {*} $modelId
         * @param {*} $resourceId
         *
         * @description
         * Every group has articles. This method will going to retreave
         * every single article
         *
         * @return
         * Promise
         */
        function posts($modelId, $resourceId) {

            if($modelId === undefined) {
                throw new Error("Model Identificator is required for this endpoint");
            }

            return request.get(base.url([BASE_URL, $modelId, 'posts', $resourceId]))
            .then(_handlerSuccessPosts).catch(_handlerErrorPosts);
        };

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerSuccessPosts(response) {
            return response.data.success.data;
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerErrorPosts(response) {
            return provided.reject(response.status);
        }


        /**
         * @endpoint
         * GET : /groups/:model_id/articles
         * GET : /groups/:model_id/articles/:resource_id
         * @type
         * Request
         *
         * @param {*} $modelId
         * @param {*} $resourceId
         *
         * @description
         * Every group has articles. This method will going to retreave
         * every single article
         *
         * @return
         * Promise
         */
        function data($modelId, $resourceId) {

            if($modelId === undefined) {
                throw new Error("Model Identificator is required for this endpoint");
            }

            return request.get(base.url([BASE_URL, $modelId, 'data', $resourceId]))
            .then(_handlerSuccessData).catch(_handlerErrorData);
        };

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerSuccessData(response) {
            return response.data.success.data;
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerErrorData(response) {
            return provided.reject(response.status);
        }


        /**
         * @type
         * GET : Api
         *
         * @description
         * Expose the internal api of the fetch functionality regarding
         * the group copumication
         */
        return {
            groups      : groups,
            members     : members,
            articles    : articles,
            posts       : posts,
            data        : data
        };
    }

    /**
     *
     * @param {$http} request
     */
    var update = function(request, base, provider) {

        /**
         * @endpoint
         * PUT : /groups/:model_id
         *
         * @type
         * Request
         *
         * @param {*} collection
         *
         * @description
         * Create a new group
         *
         * @return
         * Promise
         */
        function group($modelId, collection) {

            if($modelId === undefined) {
                throw new Error("Model Identificator is required for this endpoint");
            }

            return request.put(base.url([BASE_URL, $modelId]), collection)
            .then(_handlerSuccessGroup).catch(_handlerErrorGroup);
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerSuccessGroup(response) {
            return response.data.success.data;
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerErrorGroup(response) {
            return provider.reject(response.status)
        }

        /**
         * @endpoint
         * PUT : /groups/:model_id/articles/:resource_id
         *
         * @type
         * Request
         *
         * @param {*} collection
         *
         * @description
         * Update an existing group
         *
         * @return
         * Promise
         */
        function article($modelId, $resourceId, collection) {

            if($modelId === undefined) {
                throw new Error("Model Identificator is required for this endpoint");
            }

            if($resourceId === undefined) {
                throw new Error("Resource Identificator is required for this endpoint");
            }

            return request.put(base.url([BASE_URL, $modelId, 'articles', $resourceId]), collection)
            .then(_handlerSuccessArticle).catch(_handlerErrorArticle);
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerSuccessArticle(response) {
            return response.data.success.data;
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerErrorArticle(response) {
            return provider.reject(response.status)
        }

        /**
         * @type
         * Api
         *
         * @description
         * Expose the internal api of the create functionality regarding
         * the group comunication
         */
        return {
            group   : group,
            article : article
        };
    };

    /**
     *
     * @param {$http} request
     */
    var archive = function(request, base, provider) {

        /**
         * @endpoint
         * DELETE : /groups/:model_id
         *
         * @type
         * Request
         *
         * @param {*} collection
         *
         * @description
         * Create a new group
         *
         * @return
         * Promise
         */
        function group($modelId, collection) {

            if($modelId === undefined) {
                throw new Error("Model Identificator is required for this endpoint");
            }

            return request.delete(base.url([BASE_URL, $modelId]), collection)
            .then(_handlerSuccessGroup).catch(_handlerErrorGroup);
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerSuccessGroup(response) {
            return response.data.success.data;
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerErrorGroup(response) {
            return provider.reject(response.status)
        }

        /**
         * @endpoint
         * DELETE : /groups/:model_id/articles/:resource_id
         *
         * @type
         * Request
         *
         * @param {*} collection
         *
         * @description
         * Update an existing group
         *
         * @return
         * Promise
         */
        function article($modelId, $resourceId, collection) {

            if($modelId === undefined) {
                throw new Error("Model Identificator is required for this endpoint");
            }

            if($resourceId === undefined) {
                throw new Error("Resource Identificator is required for this endpoint");
            }

            return request.delete(base.url([BASE_URL, $modelId, 'articles', $resourceId]), collection)
            .then(_handlerSuccessArticle).catch(_handlerErrorArticle);
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerSuccessArticle(response) {
            return response.data.success.data;
        }

        /**
         * @type
         * Handler
         *
         * @param {*} response
         */
        function _handlerErrorArticle(response) {
            return provider.reject(response.status)
        }

        /**
         * @type
         * Api
         *
         * @description
         * Expose the internal api of the create functionality regarding
         * the group comunication
         */
        return {
            group   : group,
            article : article
        };
    };

    /**
     *
     * @param {*} request
     * @param {*} base
     * @param {*} provider
     */
    var ApiProvider = function(request, base, provider) {

        return {
            create  : create(request, base, provider),
            fetch   : fetch(request, base, provider),
            update  : update(request, base, provider),
            archive : archive(request, base, provider)
        };
    };

    /**
     *
     * @param {*} http
     * @param {*} q
     * @param {*} AppManager
     */
    var ServiceHandler = function ($http, $q, AppManager) {

        return {
            api :ApiProvider($http, AppManager, $q)
        };
    };

    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    ServiceHandler.$inject = ['$http', '$q', 'AppManager'];


    /**
     *
     */
    angular.module('application.models').factory('InitiativeModel', ServiceHandler);

})();
