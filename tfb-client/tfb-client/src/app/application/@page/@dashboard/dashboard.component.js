(function () {

    var Controller = function(PubSub) {

        this.on_init = ($argument) => {
            PubSub.api.publish('CHANGE_APP', $argument);
        }
    };

    var ComponentRoute = [

        { path: 'feeds'                 ,component: 'feeds'             ,as: 'Feeds',       useAsDefault: true},
        { path: 'groups/...'            ,component: 'groups'            ,as: 'Groups'       },
        { path: 'projects/...'          ,component: 'projects'          ,as: 'Projects'     },
        { path: 'users/...'             ,component: 'users'             ,as: 'Users'        },
        { path: 'profile/...'           ,component: 'profile'           ,as: 'Profile'      },

        { path: 'articles/:id'          ,component: 'article'           ,as: 'Article'      },
        { path: 'posts/:id'             ,component: 'post'              ,as: 'Post'         },
        { path: 'polls/:id'             ,component: 'poll'              ,as: 'Poll'         },

        // { path: 'tracker/...'           ,component: 'tracker'           ,as: 'Tracker'      },
        // { path: 'initiatives/...'       ,component: 'initiatives'       ,as: 'Initiatives'  },
        { path: 'messages'              ,component: 'messages'          ,as: 'Messages'     },
        { path: 'opportunities'         ,component: 'opportunities'     ,as: 'Opportunities'},

        // @Tracker
        { path: 'bundle/...'            ,  component: 'bundle'          , as: 'Bundle'       },
        // { path: 'bundle'                ,  component: 'bundle'          , as: 'Bundle'       },
        { path: 'schedule/...'          ,  component: 'schedule'        , as: 'Schedule'     },
        { path: 'classroom/...'         ,  component: 'classroom'       , as: 'Classroom'    },
        { path: 'assesment'             ,  component: 'assesment'       , as: 'Assesment'    },


        // @Administration
        { path: 'administration/...'    ,component: 'administration'    ,as: 'Administration'},

    ];


    var Component = {
        templateUrl     : './dashboard.template.html',
        controllerAs    : 'model',
        controller      : Controller,
        $routeConfig    : ComponentRoute,
        bindings        : { $router: '<' }
    };

    angular.module('application.components.page').component('dashboard', Component);
})();
