<?php

use App\Http\Models\Collections\CollectionMunicipality;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMunicipalityTable extends Migration
{
    private $table = CollectionMunicipality::TABLE;

    public function up()
    {
        Schema::create($this->table, function(Blueprint $builder) {

            //# Primary Key
            $builder->increments(CollectionMunicipality::ID, 10);

            //# Table Entity Collection
            $builder->integer(CollectionMunicipality::AREA_ID);
            $builder->string(CollectionMunicipality::TITLE, 200);
        });
    }

    public function down()
    {
        Schema::drop($this->table);
    }
}
