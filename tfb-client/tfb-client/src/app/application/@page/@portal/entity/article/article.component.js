(function () {

    /**
     *
     * @param {*} Translation
     * @param {*} GroupModel
     * @param {*} PubSub
     */
    var Controller = function (Translation, GroupModel, PubSub, UserModel, State, EntityModel, $sce) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;


        model.entity = null;


        /**
         * @type
         * Router event
         *
         * @description
         * This method is executed on component route init
         */
        model.$routerOnActivate = function (next, previous) {

            UserModel.state("article_id", next.params.id);

            EntityModel.api.SINGLE().fetch
                .article(UserModel.state("article_id"))
                    .then(model.handle_FETCH_article_success)
                    .catch(model.handle_FETCH_article_error);
        };


        model.handle_FETCH_article_success = function($response) {

            model.sidebar_signature     = $response.data.signature
            model.sidebar_picture       = $response.data.picture
            model.sidebar_title         = $response.data.name
            model.sidebar_position      = $response.data.position

            model.articleTitle          = $response.data.title;
            model.articleContent        = $sce.trustAsHtml($response.data.content);
        };

        model.handle_FETCH_article_error = function() {
            console.log("Errror : article");
        };
    };


    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['Translation', 'GroupModel', 'PubSub', 'UserModel', 'State', 'EntityModel', '$sce'];

    var Component = {
        templateUrl         : './article.template.html',
        controller          : Controller,
        controllerAs        : 'model'
    };

    angular.module('application.components.page').component('article', Component);
})();
