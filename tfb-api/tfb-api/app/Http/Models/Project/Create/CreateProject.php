<?php

namespace App\Http\Models\Project\Create;

use App\Http\Models\Project\Project;
use App\Http\Models\Project\ProjectGroup;
use App\Http\Models\Repository;
use DB;

/**
 * Class CreateProject
 * @package App\Http\Models\Project\Create
 */
class CreateProject extends Repository
{
    /**
     * @param $collection
     *
     * @return array
     */
    public function execute($collection): array
    {
        $projectTask  = $this->tableProjectTask($collection);
        $projectGroup = $this->tableProjectGroup($projectTask[Project::ID], $collection);

        DB::table(Project::TABLE)->insert($projectTask);
        DB::table(Project::TABLE_GROUP)->insert($projectGroup);

        return $projectTask;
    }

    /**
     * @param $collection
     *
     * @return array
     */
    public function tableProjectTask($collection): array
    {
        return [
            Project::ID             => uniqid(),
            Project::TITLE          => $this->process($collection, Project::TITLE),
            Project::DESCRIPTION    => $this->process($collection, Project::DESCRIPTION),
            Project::START_DATE     => $this->process($collection, Project::START_DATE),
            Project::END_DATE       => $this->process($collection, Project::END_DATE)
        ];
    }

    /**
     * @param $projectId
     * @param $collection
     *
     * @return array
     */
    public function tableProjectGroup($projectId, $collection): array
    {
        return [
            Project::ID             => $projectId,
            ProjectGroup::ID        => uniqid(),
            ProjectGroup::TITLE     => $this->process($collection, ProjectGroup::TITLE, 'Общи задачи')
        ];
    }
}