(function () {

    var Controller = FormController(function(Translation) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;

        /**
         *
         */
        model.name = "employ";


        /**
         * @component
         * Fields collection
         *
         * @behaivior
         * This property contains all the fields of
         * the form controller. Every single field contain value and mutator properties
         * in order to check if this field is required or not you must provide
         *
         * @properties
         *  isRequired : true | false
         *  value : String
         */
        model.FIELD = {

            "position": {
                name            : "position",
                type            : 'text',
                show_errors     : false,
                isRequired      : true,
                value           : null
            }
        };

        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(function($provider) {

            model.LABEL = {


                INPUT_POSITION : $provider({
                    "bg"    : "Позиция в организацията",
                    "en"    : "Position in the organisation"
                }),

                LABEL_POSITION :    $provider({
                    "bg"    : "Заемана длъжност в организацията",
                    "en"    : "What is your position"
                }),
            };
        });


    });




    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['Translation', 'PubSub', 'Form'];

    /**
     *
     */
    var Component = {
        templateUrl     : './form-employ.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            uiCollection: '<',
            uiIsDisabled    : '<'
        }
    };

    /**
     *
     */
    angular.module('application.components.page').component('formEmploy', Component);

})();
