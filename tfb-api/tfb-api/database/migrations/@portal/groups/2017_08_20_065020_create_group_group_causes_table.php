<?php

use \App\Http\Models\Group\Group;
use \App\Http\Models\Group\GroupCause;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupGroupCausesTable extends Migration
{
    private $table = Group::TABLE_CAUSES;

    public function up()
    {
        Schema::create($this->table, function (Blueprint $builder)
        {
            //# Primary Key
            $builder->char(Group::ID, 13);
            $builder->char(GroupCause::ID, 13);

            $builder->primary([Group::ID, GroupCause::ID]);
        });
    }

    public function down()
    {
        Schema::drop($this->table);
    }
}
