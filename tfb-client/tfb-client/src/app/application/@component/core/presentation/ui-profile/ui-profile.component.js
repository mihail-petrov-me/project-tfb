(function () {

    var Controller = function(UserModel) {

        /**
         * @type
         * State provider
         */
        this.activeTheme = ($theme) => {

            if(!this.theme) {
                this.theme = 'full';
            }

            return $theme == this.theme;
        }


        /**
         * @type
         * Event : on
         *
         * @return
         * void
         */
        // this.on_changeImage = () => {

        //     if (model['user_picture']) {

        //         var reader = new FileReader();
        //         reader.readAsDataURL(model.picture);

        //         reader.onloadend = function () {
        //             model.uiPicture = reader.result;
        //             model.request_UPDATE_picture();
        //         };
        //     }
        // };


        /**
         * @type
         * Request : UPDATE :picture
         */
        // this.request_UPDATE_picture = () => {

        //     UserModel.api.update
        //     .picture(UserModel.profile.my("user_id"), { file: model['user_picture'] })
        //         .then(model.handler_UPDATE_picture_success)
        //         .catch(model.handler_UPDATE_picture_error);
        // }

        /**
         * @type
         * Handler : UPDATE : SUCCESS
         */
        this.handler_UPDATE_picture_success = () => {
            this.uiPicture     = UserModel.profile.my('user_picture');
        };

        /**
         * @type
         * Handler : UPDATE : ERROR
         */
        this.handler_UPDATE_picture_error = () => {
            console.log("@Exception update profile picture");
        };


        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {

            if(this.serviceProvide) {

                this.uiSignature   = UserModel.profile.my('user_signature');
                this.uiWho         = UserModel.profile.my('user_name');
                this.uiPicture     = UserModel.profile.my('user_picture');
            }
        }

        /**
         * @type
         * Lifecycle event
         */
        this.$onChanges = () => {

            if(this.uiProvide) {

                if(this.uiProvide.signature) {
                    this.uiSignature   = this.uiProvide['user_signature']
                }

                if(this.uiProvide.name) {
                    model.uiWho         = this.uiProvide['user_name']
                }

                if(this.uiProvide.picture) {
                    this.uiPicture     = this.uiProvide['user_picture']
                }
            }
        }
    };


    Controller.$inject = ['UserModel']
    var Component = {
        templateUrl     : './ui-profile.template.html',
        controllerAs    : 'model',
        controller      : Controller,
        transclude      : true,
        bindings        : {

            uiSignature         : '<',
            uiSize              : '<',
            uiWho               : '<',
            uiPicture           : '<',
            uiPosition          : '<',
            uiProvide           : '<',

            theme               : '<',
            serviceProvide      : '@',
            serviceUpload       : '@',

            uiState             : '@'
        }
    };

    angular.module('application.components.visual').component('uiProfile', Component);

})();
