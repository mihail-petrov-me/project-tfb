(function () {

    var BASE_URL = "groups";

    /**
     * @type
     * Group creation services
     *
     * @description
     * Contains collection of methods for creating an Group related items
     */
    var create = function(request) {

        return {
            group       : (collection)              => request.post([BASE_URL], collection),
            article     : ($modelId, collection)    => request.post([BASE_URL, $modelId, 'articles'], collection),
            poll        : ($modelId, collection)    => request.post([BASE_URL, $modelId, 'polls'], collection),
            video       : ($modelId, collection)    => request.post([BASE_URL, $modelId, 'videos'], collection),
            post        : ($modelId, collection)    => request.post([BASE_URL, $modelId, 'posts'], collection),
            subscribe   : (collection)              => request.post([BASE_URL, 'subscribe'], collection),
            vote        : ($modelId, collection)    => request.post([BASE_URL, $modelId, 'answer'], collection)
        };
    };


    /**
     * @type
     * Group update services
     *
     * @description
     * Contains collection of methods for updating an Group related items
     */
    var update = function(request) {

        return {

            group: ($id, $body) => {

                if($id === undefined) { throw new Error("Model Identificator is required for this endpoint"); }
                return request.put([BASE_URL, $id], $body);
            },

            article: ($id, $resourceId, $body) => {

                if($id === undefined)           { throw new Error("Model Identificator is required for this endpoint"); }
                if($resourceId === undefined)   { throw new Error("Resource Identificator is required for this endpoint"); }

                return request.put([BASE_URL, $id, 'articles', $resourceId], $body);
            }
        };
    };


    /**
     * @type
     * Group fetch services
     *
     * @description
     * Contains collection of methods for fetching an Group related items
     */
    var fetch = function(request, requestType) {

        return {
            groups      : ($id)         => request.beffore(requestType).get([BASE_URL, $id]),
            members     : ($id)         => request.beffore(requestType).get([BASE_URL, $id, 'members']),
            articles    : ($id)         => request.beffore(requestType).get([BASE_URL, 'articles', $id]),
            posts       : ($id, $body)  => request.beffore(requestType).get([BASE_URL, $id, 'posts', $body]),
            videos      : ($id, $body)  => request.beffore(requestType).get([BASE_URL, $id, 'videos', $body]),
            data        : ($id, $query) => request.beffore(requestType).get([BASE_URL, $id, 'data'], $query),
            poll        : ($id)         => request.beffore(requestType).get([BASE_URL, 'poll', $id])
        };
    }


    /**
     * @type
     * Group archive services
     *
     * @description
     * Contains collection of methods for archiving an Group related items
     */
    var archive = function (request) {

        /**
         * @type
         * Api
         *
         * @description
         * Expose the internal api of the create functionality regarding
         * the group comunication
         */
        return {

            group: ($modelId, collection) => {

                if ($modelId === undefined) { throw new Error("Model Identificator is required for this endpoint"); }
                return request.delete([BASE_URL, $modelId], collection);
            },

            article: ($modelId, $resourceId, collection) => {

                if ($modelId === undefined)     { throw new Error("Model Identificator is required for this endpoint"); }
                if ($resourceId === undefined)  { throw new Error("Resource Identificator is required for this endpoint"); }

                return request.delete([BASE_URL, $modelId, 'articles', $resourceId], collection);
            }
        };
    };



    /**
     *
     * @param {*} request
     * @param {*} base
     * @param {*} provider
     */
    var ApiProvider = function(request) {

        var requestType = { type : 'collection' };

        return {

            PAGINATE : function() {
                requestType.type = 'pagination';
                return this;
            },

            REFRESH : function() {
                requestType.type = 'refresh';
                return this;
            },

            COLLECTION : function() {
                requestType.type = 'collection';
                return this;
            },

            SINGLE : function() {
                requestType.type = 'single';
                return this;
            },

            create  : create(request),
            fetch   : fetch(request, requestType),
            update  : update(request),
            archive : archive(request)
        };
    };

    /**
     *
     * @param {*} http
     * @param {*} q
     * @param {*} AppManager
     */
    var ServiceHandler = function (Request) {

        return { api :ApiProvider(Request) };
    };

    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    ServiceHandler.$inject = ['Request'];


    /**
     *
     */
    angular.module('application.models').factory('GroupModel', ServiceHandler);

})();
