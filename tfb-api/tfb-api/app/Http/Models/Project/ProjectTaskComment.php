<?php
namespace App\Http\Models\Project;

use App\Http\Models\Model;
use App\Http\Models\Project\Create\AddMemberToProject;
use App\Http\Models\Project\Create\AddTaskToProject;
use App\Http\Models\Project\Fetch\DeleteProject;
use App\Http\Models\Project\Fetch\FetchProjectCollection;
use App\Http\Models\Project\Fetch\FetchProject;
use App\Http\Models\Project\Create\CreateProject;
use App\Http\Models\Project\Fetch\FetchProjectMemberCollection;
use App\Http\Models\Project\Update\UpdateProject;
use DB;


/**
 * Class ProjectTaskComment
 * @package App\Http\Models\Project
 */
class ProjectTaskComment extends Model
{
    // Public keys
    const ID                = 'task_comment_id';
    const CONTENT           = 'task_comment_content';
}
