<?php

namespace App\Http\Models\Campaign\Delete;
use App\Http\Models\Repository;
use DB;

class DeleteCampaign extends Repository
{
    /**
     * @param $id
     * @param $query
     * @return bool
     */
    public function execute($id, $query)
    {
        DB::table('campaigns')
            ->where(array(
            'id'    => $id
        ))
            ->delete($id);

        return true;
    }
}