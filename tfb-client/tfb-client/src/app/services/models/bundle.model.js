(function () {

    var BASE_URL = "bundles";

    /**
     * @type
     * Class creation services
     *
     * @description
     * Contains collection of methods for creating an Group related items
     */
    var create = function(request) {

        return {
            goal   : ($id, $collection)   => request.post([BASE_URL, $id, 'goal'], $collection),
            // bundle   : ($id, $collection)   => request.post([BASE_URL, 'teacher', $id, 'bundle'], $collection),
            student  : ($id, $collection)   => request.post([BASE_URL, $id, 'student'], $collection),
            // goal     : ($collection)        => request.post([BASE_URL, 'goals'], $collection),
            // note     : ($teacherId, $studentId, $collection)        => request.post([BASE_URL,'teachers', $teacherId, 'student', $studentId, 'note'], $collection),
        };
    };

    /**
     * @type
     * Group fetch services
     *
     * @description
     * Contains collection of methods for fetching an Group related items
     */
    var fetch = function(request, requestType) {

        return {

            bundleSingle            : ($id)     => request.beffore(requestType).get([BASE_URL, $id]),
            bundleCollection        : ($query)  => request.beffore(requestType).get([BASE_URL], $query),
            bundleSpecialCollection : ($query)  => request.beffore(requestType).get(['bundlesspecial'], $query),

            bundles                 : ($id)     => request.beffore(requestType).get([BASE_URL, 'teacher', $id, 'bundles']),
            teachers                : ($id)     => request.beffore(requestType).get([BASE_URL, 'teacher', $id]),

            studentCollection       : ($id)     => request.beffore(requestType).get([BASE_URL, $id, 'student']),
            goalCollection          : ($id)     => request.beffore(requestType).get([BASE_URL, $id, 'goal']),
        };
    };

    /**
     * @type
     * Class update services
     *
     * @description
     * Contains collection of methods for updating an Group related items
     */
    var update = function(request) {

        return {
            assign      : ($id, $collection)                => request.put([BASE_URL, $id, 'add'], $collection),
            goal        : ($id, $goalId, $collection)       => request.put([BASE_URL, $id, 'goal', $goalId], $collection),
            student     : ($id, $studentId, $collection)    => request.put([BASE_URL, $id, 'student', $studentId], $collection)
        };
    };

    /**
     * @type
     * Group fetch services
     *
     * @description
     * Contains collection of methods for archiving an Group related items
     */
    var archive = function(request) {

        return {
            student : ($id, $studentId) => request.delete([BASE_URL, $id, 'student', $studentId]),
            goal    : ($id, $goalId) => request.delete([BASE_URL, $id, 'goal', $goalId])
        };
    };

    /**
     *
     * @param {*} request
     * @param {*} base
     * @param {*} provider
     */
    var ApiProvider = function(request) {

        var requestType = {
            type : 'collection'
        };

        return {

            PAGINATE : function() {
                requestType.type = 'pagination';
                return this;
            },

            REFRESH : function() {
                requestType.type = 'refresh';
                return this;
            },

            COLLECTION : function() {
                requestType.type = 'collection';
                return this;
            },

            SINGLE : function() {
                requestType.type = 'single';
                return this;
            },

            create  : create(request),
            fetch   : fetch(request, requestType),
            update  : update(request),
            archive : archive(request)
        };
    };

    /**
     *
     * @param {*} http
     * @param {*} q
     * @param {*} AppManager
     */
    var ServiceHandler = function (Request) {

        return { api: ApiProvider(Request) };
    };

    ServiceHandler.$inject = ['Request'];
    angular.module('application.models').factory('BundleModel', ServiceHandler);
})();
