<?php

use App\Http\Models\Collections\CollectionSchool;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolTable extends Migration
{
    private $table = CollectionSchool::TABLE;

    public function up()
    {
        Schema::create($this->table, function(Blueprint $builder) {

            //# Primary Key
            $builder->char(CollectionSchool::ID, 10)->primary();

            //# Table Entity Collection
            $builder->integer(CollectionSchool::AREA_ID);
            $builder->integer(CollectionSchool::MUNICIPALITY_ID);
            $builder->integer(CollectionSchool::CITY_ID);
            $builder->string(CollectionSchool::TITLE, 500);
        });
    }

    public function down()
    {
        Schema::drop($this->table);
    }
}
