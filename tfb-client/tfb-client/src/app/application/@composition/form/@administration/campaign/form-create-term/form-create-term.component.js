(function () {

    /**
     *
     * @param {*} Translation
     * @param {*} GroupModel
     * @param {*} PubSub
     */
    var Controller = function(Translation, PubSub) {

        this.name       = "FORM_CREATE_TERM";
        this.specialID  = null;

        /**
         * @component
         */
        this.FIELD = {

            "campaign_term_title": {
                name            : "campaign_term_title",
                type            : 'text',
                value           : null,
                show_errors     : false,
                isRequired      : true
            },

            "campaign_date_start": {
                name            : "campaign_date_start",
                type            : 'date',
                value           : null,
                show_errors     : false,
                isRequired      : true
            },

            "campaign_date_end": {
                name            : "campaign_date_end",
                type            : 'date',
                value           : null,
                show_errors     : false,
                isRequired      : true
            }
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                INPUT_TERM_TITLE:   $provider({
                    'en' : 'Term Title',
                    'bg' : 'Заглавие на срока'
                }),

                INPUT_DATE_START:   $provider({
                    'en' : 'Term Title',
                    'bg' : 'От'
                }),

                INPUT_DATE_END:   $provider({
                    'en' : 'Term Title',
                    'bg' : 'До'
                }),
            };
        });

        this.$onInit = () => {

            this.parent.FIELD  = this.FIELD;
            this.formContact   = this.parent.formContact;
            this.specialID     = this.parent.SPECIAL_ID;
        };


        this.on_create = () => {
            PubSub.form.init({ name: this.name, uid: this.specialID });
        };
    };


    /**
     * @type
     * Component injector
     */
    Controller.$inject  = ['Translation', 'PubSub', 'Form'];
    var Component       = {
        templateUrl     : './form-create-term.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        transclude      : true,

        require         : {
            parent      : '^formWrapper'
        },

        bindings        : {
            uiCollection : '<'
        }
    };

    angular.module('application.components.page').component('formCreateTerm', Component);

})();
