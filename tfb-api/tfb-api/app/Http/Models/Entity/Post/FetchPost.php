<?php

namespace App\Http\Models\Entity\Post;
use DB;

class FetchPost
{

    public function execute($id, $query) 
    {
        
        return DB::table("block_post", $query)
            ->leftJoin(
                    'collection_entity', 
                    'block_post.id', '=', 'collection_entity.entity_id')
            ->leftJoin(
                    'users', 
                    'block_post.user_id', '=', 'users.id')
            ->leftJoin(
                    'user_experience', 
                    'user_experience.user_id', '=', 'users.id')
            ->where(
                    'block_post.id', $id)
            ->first();
    }
}
