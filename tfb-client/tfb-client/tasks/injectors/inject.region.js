var fs = require('fs.extra');
var EOL = require('os').EOL;


var settings = require('../../settings');
var util = require('../helpers/util');


/**
 *
 */
exports.exec = function (source) {


    function extractSummary(iCalContent) {

        var rx = /"@INJECT:(.*)"/g;
        var arr = rx.exec(iCalContent);
        return arr[1];
    }

  util.traverse(source, function(file, directory, filePath) {


    var re = /"@INJECT:(.+)"/gi;



    var path = directory.replace(source, '');
    var file = fs.readFileSync(filePath).toString();
    var found = file.match(re);

    if(found) {

        for(var i = 0; i < found.length; i++) {
            // extract path from string
            var p = extractSummary(found[i]);
            var fff = source + path + p;
            var ddd = fs.readFileSync(fff).toString();
            var file = file.replace(found[i], ddd);
            fs.writeFileSync(filePath, file);
        }
    }
  });

};
