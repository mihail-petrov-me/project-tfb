<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitiativesTable extends Migration
{

  private $table = 'initiatives';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function(Blueprint $builder) {

          //# Primary Key
          //#
          $builder->increments('id');

          //# Foregn Key
          //#

          //# Table Entity Collection
          //#
          $builder->string('title', 100);
          $builder->string('description', 100);
          $builder->string('couses', 256);
          $builder->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop($this->table);
    }
}
