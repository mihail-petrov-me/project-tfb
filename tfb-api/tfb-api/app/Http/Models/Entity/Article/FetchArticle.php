<?php

namespace App\Http\Models\Entity\Article;
use DB;

class FetchArticle
{

    public function execute($id, $query) 
    {
        
        return DB::table("block_article", $query)
            ->leftJoin(
                    'collection_entity', 
                    'block_article.id', '=', 'collection_entity.entity_id')
            ->leftJoin(
                    'users', 
                    'block_article.user_id', '=', 'users.id')
            ->leftJoin(
                    'user_experience', 
                    'user_experience.user_id', '=', 'users.id')
                
            ->where(
                    'block_article.id', $id)
            ->first();
    }
}
