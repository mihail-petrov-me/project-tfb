(function () {

    var Controller = function(UserModel) {
        this.uiCollection = UserModel.profile.groups();
    };

    var Component = {
        templateUrl     : './group-repeater.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.visual').component('groupRepeater', Component);
})();
