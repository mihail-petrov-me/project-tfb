(function () {

    /**
     *
     * @param {*} Translation
     * @param {*} GroupModel
     * @param {*} PubSub
     */
    var Controller = function (Translation, GroupModel, PubSub, UserModel, State, EntityModel) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;


        /**
         * @type
         * Router event
         *
         * @description
         * This method is executed on component route init
         */
        model.$routerOnActivate = function (next, previous) {

            EntityModel.api.SINGLE().fetch
                .entity()
                    .then(function($response) {

                    })
                    .catch(function($response) {

                    });


            // UserModel.state("__group_id", next.urlPath);
            // $timeout(model.init_modelCollection);
            // $timeout(model.init_FETCH_data);

            // model.PERMISION = {
            //     IS_MEMBER   : UserModel.profile.isMember('groups', UserModel.state('__group_id')),
            //     IS_ADMIN    : UserModel.profile.isAdmin('groups', UserModel.state('__group_id')),
            //     IS_OWNER    : UserModel.profile.isOwner('groups', UserModel.state('__group_id')),
            // };

            // model.init_members();
        };


    };


    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['Translation', 'GroupModel', 'PubSub', 'UserModel', 'State', 'EntityModel'];

    var Component = {
        templateUrl         : './poll.template.html',
        controller          : Controller,
        controllerAs        : 'model'
    };

    angular.module('application.components.page').component('poll', Component);
})();
