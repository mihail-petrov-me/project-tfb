(function () {

    /**
     *
     * @param {*} PubSub
     * @param {*} Translation
     */
    var Controller = function (PubSub, Translation, $sce, UserModel, EntityModel) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;

        /**
         *
         */
        model.STATE = {
            'IS_DONE_VISIBLE'     : false,
            'IS_EDIT_VISIBLE'     : false,
            'IS_COMMENT_VISIBLE'  : false
        };


        model.ACTIVE_INDEX = null;


        /**
         *
         */
        model.PUBLISH = {
            'mode_interactive'  : PubSub.events.component.POST.MODE_INTERACTIVE,
            'mode_presentation' : PubSub.events.component.POST.MODE_PRESENTATION
        };


        model.FIELD = {
            comment : {
                name            : "comment",
                type            : 'textarea',
                value           : null,
                show_errors     : false,
                error           : 'Field is required'
            }
        }


        /**
         *
         */
        model.getICON = function($type) {

            if($type == 1) {
                return $sce.trustAsHtml('&#xE250;');
            }

            if($type == 2) {
                return $sce.trustAsHtml('&#xE893;');
            }

            if($type == 3) {
                return $sce.trustAsHtml('&#xE922;');
            }

            if($type == 4) {
                return $sce.trustAsHtml('&#xE037;');
            }

            if($type == 5) {
                return $sce.trustAsHtml('&#xE3B0;');
            }
        };


        /**
         *
         */
        model.isActive = function($type, element) {

            if($type == 'post' && element == 1) {
                return true;
            }

            if($type == 'article' && element == 2) {
                return true;
            }

            if($type == 'poll' && element == 3) {
                return true;
            }

            if($type == 'video' && element == 4) {
                return true;
            }
        };

        /**
         *
         */
        model.on_comment = function($entityId) {
            model.ACTIVE_INDEX = $entityId;
        }

        /**
         *
         */
        model.show_comment = function($entity) {
            return ($entity == model.ACTIVE_INDEX);
        }

        /**
         *
         */
        model.on_commentSubmit = function($element) {

            EntityModel.api.create.comment({
                'entity_id' : $element.entity_id,
                'content'   : model.commentContent,
                'user_id'   : $element.user_id
            });
        };


        model.on_load = function() {
            PubSub.api.publish('ui_block_list_load_more');
        };
    };


    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['PubSub', 'Translation', '$sce', 'UserModel', 'EntityModel'];

    /**
     *
     */
    var Component = {
        templateUrl     : './ui-list-block.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            uiCollection : '<',
            uiComments   : '<',
            uiLoadMore   : '<'
        }
    };

    angular.module('application.components.visual').component('uiListBlock', Component);

})();
