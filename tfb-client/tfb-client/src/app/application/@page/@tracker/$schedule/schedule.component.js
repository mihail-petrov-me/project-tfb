(function () {

    var ComponentRouter = [

        { path: '/table'         ,  component: 'scheduleTable',           as: 'ScheduleTable'   , useAsDefault: true      },
        { path: '/calendar'      ,  component: 'scheduleCalendar',        as: 'ScheduleCalendar' }
    ];

    var Component       = {
        templateUrl     : './schedule.template.html',
        controllerAs    : 'model',
        $routeConfig    : ComponentRouter,
        bindings        : { $router     : '<' }
    };

    angular.module('application.components.page').component('schedule', Component);
})();
