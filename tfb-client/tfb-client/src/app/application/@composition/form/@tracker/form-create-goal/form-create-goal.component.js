(function () {

    var Controller = function(Translation, ClassModel, PubSub, BundleModel, SubjectModel, UserModel) {

        this.name = "FORM_CREATE_GOAL";

        this.STATE = {
            SHOW_FORM_PRIMARY   : false,
            SHOW_HOLE_FORM      : false
        };

        /**
         * @component
         * Fields collection
         */
        this.FIELD = {

            "goal_title": {
                name            : "goal_title",
                type            : 'text',
                value           : null,
                show_errors     : false,
                isRequired      : true
            },

            "goal_rigar": {
                name            : "goal_rigar",
                type            : 'dropdown',
                value           : null,
                show_errors     : false,
                isRequired      : true
            },
            "goal_type": {
                name            : "goal_type",
                type            : 'dropdown',
                value           : null,
                show_errors     : false,
                isRequired      : true
            }
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                INPUT_GOAL_TITLE: $provider({
                    "bg"    : "Наименование на целта",
                    "en"    : "Note for this particular student"
                }),

                LABEL_DROPDOWN_RIGAR: $provider({
                    "bg"    : "Ниво на мисловните процеси",
                    "en"    : "Rigar"
                }),

                LABEL_DROPDOWN_TYPE: $provider({
                    "bg"    : "Тип",
                    "en"    : "Type"
                }),

                OPTIONS_DROPDOWN_RIGAR : $provider({
                    "bg"    : [
                        { 'id': '1' , 'title': '1' },
                        { 'id': '2' , 'title': '2' },
                        { 'id': '3' , 'title': '3' }
                    ],
                    "en"    : [
                        { 'id': '1' , 'title': '1' },
                        { 'id': '2' , 'title': '2' },
                        { 'id': '3' , 'title': '3' }
                    ]
                }),

                OPTIONS_DROPDOWN_TYPE : $provider({
                    "bg"    : [
                        { 'id': '1' , 'title': 'Умения' },
                        { 'id': '2' , 'title': 'Предмет' },
                    ],
                    "en"    : [
                        { 'id': '1' , 'title': '1' },
                        { 'id': '2' , 'title': '2' },
                    ]
                }),
            };
        });


        this.add_student = () => {
            PubSub.form.init("FORM_CREATE_GOAL")
        };

        this.update_student = () => {
            PubSub.form.update("FORM_CREATE_GOAL")
        };


        this.add_secondary_goal = (element) => {

            PubSub.api.publish('PRIMARY_ELEMENT_ID', element.id);
            this.STATE.SHOW_FORM_PRIMARY = true;
        };

        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {

            this.parent.FIELD  = this.FIELD;
            this.formContact   = this.parent.formContact;

            this.STATE.SHOW_FORM_PRIMARY = (this.uiCollection.length == 0);

            PubSub.form.end("FORM_CREATE_GOAL", () => {
                this.STATE.SHOW_FORM_PRIMARY = false;
            });

            PubSub.api.subscribe('event:ui:dropdown:after_select', () => {
                this.STATE.SHOW_HOLE_FORM = true;
            });
        };

        /**
         *
         */
        this.show_form = () => {
            this.STATE.SHOW_FORM_PRIMARY = true;
        }


        this.$onChanges = () => {

            if(this.uiUpdate) {
                this.FIELD['goal_title'].value  = this.uiCollection.title;
                this.FIELD['goal_rigar'].value  = this.uiCollection.rigar;
                this.parent.uiId = this.uiId;
            }
        }
    };


    /**
     * @type
     * Component injector
     */
    Controller.$inject = ['Translation', 'ClassModel', 'PubSub', 'BundleModel', 'SubjectModel', 'UserModel'];
    let Component = {
        templateUrl     : './form-create-goal.template.html',
        controller      : Controller,
        controllerAs    : 'model',

        transclude: {
            'rightFormSlot' : '?rightFormSlot',
            'afterFormSlot' : '?afterFormSlot'
        },

        require             : {
            parent          : '^formWrapper'
        },

        bindings        : {

            uiIsDisabled    : '<',
            uiPresentation  : '<',
            uiId            : '<',


            uiUpdate        : '@',
            uiCollection    : '<'
        }
    };

    angular.module('application.components.page').component('formCreateGoal', Component);
})();
