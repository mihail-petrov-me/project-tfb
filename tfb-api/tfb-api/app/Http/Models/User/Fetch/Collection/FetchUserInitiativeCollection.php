<?php

namespace App\Http\Models\User\Fetch\Collection;
use App\Http\Models\Repository;
use DB;

class FetchUserInitiativeCollection extends Repository
{
    public function execute($id)
    {
        return DB::table('initiatives')
            ->join(
                    'user_collection', 
                    'user_collection.collection_id', '=', 'initiatives.id'
            )
            ->select(
                    'initiatives.id', 
                    'initiatives.title', 
                    'initiatives.description', 
                    'user_collection.is_admin', 
                    'user_collection.is_owner'
            )
            ->where(
                    'user_collection.user_id', $id
            )
            ->get();
    }
}