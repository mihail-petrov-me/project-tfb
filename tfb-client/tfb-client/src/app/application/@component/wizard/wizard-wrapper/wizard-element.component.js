(function () {

    /**
     *
     * @param {*} Translation
     * @param {*} PubSub
     */
    var Controller = function(Translation, PubSub) {

        this.$onInit = () => {

            this.parent.PROPERTY.COLLECTION.push(this);
            if(this.parent.PROPERTY.COLLECTION.length == 1) {
                this.isVisible = true;
            }
        };
    };


    var Component = {
        templateUrl     : './wizard-element.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        transclude      : true,

        require             : {
            parent          : '^wizardWrapper'
        },

        bindings        : {
            uiTitle      : "@",
            uiFinish     : "@",
            isVisible    : "<"
        }
    };

    angular.module('application.components.visual').component('wizardElement', Component);

})();
