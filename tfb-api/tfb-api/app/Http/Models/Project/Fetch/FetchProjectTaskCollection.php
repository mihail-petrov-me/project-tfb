<?php
namespace App\Http\Models\Project\Fetch;

use App\Http\Models\Project\ProjectGroup;
use App\Http\Models\Project\ProjectTask;
use DB;
use App\Http\Models\Project\Project;
use \App\Http\Models\Repository;

/**
 * Class FetchProjectCollection
 */
class FetchProjectTaskCollection extends Repository
{

    /**
     * @param $id
     *
     * @return array
     */
    public function execute($id): array
    {
        return $this->build($id);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function parse($id)
    {
        $collection = DB::table(Project::TABLE_GROUP)
            ->select(
                ProjectGroup::ID,
                ProjectGroup::TITLE
            )
            ->where([Project::ID => $id])
            ->get();

        return json_decode($collection, true);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function parseTask($id)
    {
        $collection = DB::table(Project::TABLE_TASK)
            ->where([ProjectGroup::ID => $id])
            ->get();

        return json_decode($collection, true);
    }

    /**
     * @param $id
     * @param $collection
     *
     * @return array
     */
    public function parseSubTasks($id, $collection): array
    {
        return array_filter($collection, function($element)  use ($id) {

            if($element[ProjectTask::PARENT] === $id) {
                return $element;
            }
        });
    }


    /**
     * @param $id
     *
     * @return array
     */
    public function build($id): array
    {
        // fetch all groups
        $groupCollection = $this->parse($id);

        // fetch all tasks related to this project group

        $d = [];

        foreach ($groupCollection as $groupModel)
        {
            $groupId             = $groupModel[ProjectGroup::ID];
            $groupModel['tasks'] = $this->parseTask($groupId);

            $d[] = $groupModel;
        }


//        foreach ($groupCollection as $groupModel)
//        {
//            $groupId        = $groupModel[ProjectGroup::ID];
//            $taskCollection = $this->parseTask($groupId);
//
//            $ddd[]          = $groupModel;
//            $ddd[]['tasks']   = $taskCollection;
//        }
//
//        return $ddd;

//        $ddd = [];
//        foreach ($groupCollection as $d) {
//            $groupId     = $d[ProjectGroup::ID];
//            $d['groups'][] = [
//                'group_id'    => $d[ProjectGroup::ID],
//                'group_title' => $d[ProjectGroup::TITLE],
//                'tasks'       => $this->parseTask($groupId)
//            ];
//
//            $ddd[] = $d;
//        }




//        $commentModel   = new FetchProjectTaskCommentCollection();
//        $memberModel    = new FetchProjectMemberCollection();
//        $taskCollection = $this->parse($id);
//        $ddd = [];
//
//        foreach ($taskCollection as $d) {
//
//            $taskId         = $d[ProjectTask::ID];
//            $d['comments']  = $commentModel->execute($taskId);
//            $d['sub_tasks'] = $this->parseSubTasks($taskId, $taskCollection);
//            $d['members']   = $memberModel->execute($taskId);
//            $ddd[]  = $d;
//        }
//

        return $d;

        //return $groupModel;

        //return $groupCollection;
    }
}