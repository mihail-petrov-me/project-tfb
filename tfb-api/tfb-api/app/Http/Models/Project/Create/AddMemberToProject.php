<?php

namespace App\Http\Models\Project\Create;

use DB;
use App\Http\Models\Project\Project;
use App\Http\Models\Repository;
use App\Http\Models\User\User;

/**
 * Class AddMemberToProject
 * @package App\Http\Models\Project\Create
 */
class AddMemberToProject extends Repository
{
    /**
     * @param $collection
     */
    public function execute($id, $collection)
    {

        // get collection
        $c = $collection['collection'];
        $d = [];
        foreach ($c as $key => $value) {
            $d[] = [
                Project::ID         => $id,
                User::ID            => $this->process($value, User::ID),
                User::IS_ADMIN      => $this->process($value, User::IS_ADMIN, 0),
                User::IS_OWNER      => $this->process($value, User::IS_OWNER, 0),
            ];
        }

        DB::table(Project::TABLE_USER)->insert($d);
    }


    public function build($id, $collection)
    {
//        $d = [];
//        foreach ($collection as $key => $value) {
//            $d[] = array(
//                Project::ID => $id,
//                User::ID    => $value
//            )
//        }
    }
}