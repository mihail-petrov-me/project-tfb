<?php
namespace App\Http\Models\Bundle\Fetch;

use App\Http\Models\Repository;
use DB;

class FetchStudentCollection extends Repository
{
    public function execute($id)
    {
        return DB::table('students')->where('bundle_id', $id)->orderBy('name')->get();
    }
}