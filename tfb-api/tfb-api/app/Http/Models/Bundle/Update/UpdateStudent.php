<?php
namespace App\Http\Models\Bundle\Update;

use App\Http\Models\Repository;
use DB;

class UpdateStudent extends Repository
{
    public function execute($bundleId, $studentId, $collection)
    {
        $response = DB::table('students')->where(array(
            'bundle_id' => $bundleId,
            'id'        => $studentId
        ))->update($collection);

        if($response) {
            return $collection;
        }
    }
}