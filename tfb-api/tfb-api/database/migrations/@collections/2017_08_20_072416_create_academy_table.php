<?php

use \App\Http\Models\Collections\CollectionAcademy;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcademyTable extends Migration
{
    private $table = CollectionAcademy::TABLE;

    public function up()
    {
        Schema::create($this->table, function(Blueprint $builder) {

            //# Primary Key
            $builder->char(CollectionAcademy::ID, 10)->primary();

            //# Table Entity Collection
            $builder->string(CollectionAcademy::TITLE, 500);
        });
    }

    public function down()
    {
        Schema::drop($this->table);
    }
}
