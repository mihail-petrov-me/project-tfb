(function () {

    /**
     *
     * @param {*} Translation
     * @param {*} PubSub
     * @param {*} UserModel
     */
    var Controller = function (Translation, PubSub, UserModel, State) {

        this.$stateDomain = null;


        this.STATE = {
            IS_SIDEBAR_VISIBLE   : (UserModel.state("is_sidebar_visible") == 'true' ? true : false)
        };

        /**
         * @type
         * Event
         */
        this.on_hide = () => {

            this.componentUI.removeClass('red');
            UserModel.state("is_sidebar_visible", "false");
        };

        /**
         * @type
         * Event
         */
        this.on_show = () => {

            this.componentUI.addClass('red');
            UserModel.state("is_sidebar_visible", "true");
        };

        /**
         * @type
         * Event
         */
        this.on_back = () => {
            State.pop(model.$stateDomain);
        };


        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {

            this.componentUI = $("ui-sidebar");
            this.componentUI.addClass('red');

            State.listen(function($domain) {

                this.STATE_BACK = State.hasStack($domain);
                if(State.hasStack($domain)) {
                    this.$stateDomain = $domain;
                }
            });

            //
            PubSub.api.subscribe('TOGGLE_SIDEBAR', () => {

                this.STATE.IS_SIDEBAR_VISIBLE = !this.STATE.IS_SIDEBAR_VISIBLE;

                if(this.STATE.IS_SIDEBAR_VISIBLE) {
                    this.on_show();
                }
                else {
                    this.on_hide();
                }
            });
        };

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component changes
         */
        this.$onChanges = () => {

            if(this.uiIsVisible){
                this.STATE.IS_SIDEBAR_VISIBLE = this.uiIsVisible;

            }
        }
    };

    /**
     * @type
     * Component injector
     */
    Controller.$inject = ['Translation', 'PubSub', 'UserModel', 'State'];

    var Component = {
        templateUrl     : './ui-sidebar.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        transclude      : true,
        bindings        : {
            uiIsVisible : '<',
            uiTitle     : '<'
        }
    };

    angular.module('application.components.visual').component('uiSidebar', Component);

})();
