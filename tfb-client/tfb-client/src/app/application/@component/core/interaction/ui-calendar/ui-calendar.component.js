(function () {

    /**
     *
     * @param {*} PubSub
     * @param {*} Translation
     */
    var Controller = function (PubSub, Translation, $sce, UserModel, EntityModel) {

        var model = this;

        this.STATE = {
            WEEK_VIEW : false,
            MONTH_VIEW : true
        };


        this.currentWeek = null;

        this.eventCollection = [];

        /**
         *
         * @param {*} date
         */
        function _removeTime(date) {
            return date.day(0).hour(0).minute(0).second(0).millisecond(0);
        }

        /**
         *
         * @param {*} scope
         * @param {*} start
         * @param {*} month
         */
        function _buildMonth(scope, start, month) {

            scope.weeks = [];
            var done        = false,
                date        = start.clone(),
                monthIndex  = date.month(),
                count       = 0;

            while (!done) {

                scope.weeks.push({
                    days: _buildWeek(date.clone(), month)
                });

                date.add(1, "w");
                done = count++ > 2 && monthIndex !== date.month();
                monthIndex = date.month();
            }
        }

        /**
         *
         * @param {*} date
         * @param {*} month
         */
        function _buildWeek(date, month) {

            var isCurrentWeek = false;

            var days = [];
            for (var i = 0; i < 7; i++) {

                if(date.isoWeekday() == 6 || date.isoWeekday() ==7) {

                }
                else {
                    days.push({
                        name            : date.format("dd").substring(0, 1),
                        number          : date.date(),
                        isCurrentMonth  : date.month() === month.month(),
                        isToday         : date.isSame(new Date(), "day"),
                        date            : date,
                        events          : model.events[(+date)]
                    });

                    if(date.isSame(new Date(), "day")) {
                        isCurrentWeek = true;
                    }

                }

                    date = date.clone();
                    date.add(1, "d");
            }

            if(isCurrentWeek) {
                model.currentWeek = days;
            }

            return days;
        }

        /**
         *
         */
        this.select = (day) => {
            this.selected = day.date;
            PubSub.api.publish('ui-calendar:event:select', day.date);
        };

        /**
         *
         */
        this.next = () => {

            var next = this.month.clone();
            _removeTime(next.month(next.month() + 1).date(1));

            this.month.month(this.month.month() + 1);
            _buildMonth(this, next, this.month);
        };

        /**
         *
         */
        this.previous = () => {

            var previous = this.month.clone();
            _removeTime(previous.month(previous.month() - 1).date(1));

            this.month.month(this.month.month() - 1);
            _buildMonth(this, previous, this.month);
        };


        /**
         *
         */
        this.$onInit = () => {

            moment.locale('fr', {
                months : 'Януари_Февруари_Март_Април_Май_Юни_Юли_Август_Септември_Октомври_Ноември_Декември'.split('_')
            });

            this.selected   = _removeTime(this.selected || moment());
            this.month      = this.selected.clone();
            this.start       = this.selected.clone();
            this.start.date(1);

            _removeTime(this.start.day(0));
            _buildMonth(this, this.start, this.month);

            PubSub.api.subscribe('ui-calender:event:reload', () => {

                _removeTime(this.start.day(0));
                _buildMonth(this, this.start, this.month);
            });
        };


        this.changeMonthView = () => {
            this.STATE.WEEK_VIEW    = false;
            this.STATE.MONTH_VIEW   = true;
        };


        this.changeWeekView = () => {
            this.STATE.MONTH_VIEW   = false;
            this.STATE.WEEK_VIEW    = true;
        };
    };


    Controller.$inject  = ['PubSub', 'Translation', '$sce', 'UserModel', 'EntityModel'];
    var Component       = {
        templateUrl     : './ui-calendar.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            selected    : '<',
            events      : '<'
        }
    };
    angular.module('application.components.visual').component('uiCalendar', Component);

})();
