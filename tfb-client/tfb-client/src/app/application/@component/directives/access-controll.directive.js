(function () {

    angular.module('application.components.directive').directive('accessControl', function(UserModel) {

        function hide(element, num)
        {
            if(!UserModel.profile.isPermited(num)) {
                element.addClass('display-hide');
            }
        }

        return {
            restrict: "A",

            link: function (scope, element, attrs) {

                // protect from
                if(attrs.hideFrom) {
                    hide(element, scope.$eval(attrs.hideFrom));
                }

                if(attrs.visibleFor) {
                    hide(element, scope.$eval(attrs.visibleFor));
                }
            }
        }
    });

})();
