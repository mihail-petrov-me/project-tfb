var build          = require('./build/build.dev');
var watch          = require('watch');
var settings       = require('../settings');
var chalk          = require('chalk');

build.exec();

// #4 Watch for changes that ocure on a code level
// #
watch.watchTree(settings.app, {
  interval : 1
}, function(f, curr, prev) {

   if (typeof f == "object" && prev === null && curr === null) {
      // Finished walking the tree
    } else if (prev === null) {
      // f is a new file
    } else if (curr.nlink === 0) {
      // f was removed
    } else {

      var collection    = f.split('.');
      var type          = collection.slice(-1)[0];

      if(type == 'js') {
        console.log("@SCRIPT");
        build.rebuildScript(f);
      }

      if(type == 'html') {
        console.log("@TEMPLATE");
        build.rebuildTemplate(f);
      }

      if(type == 'scss') {
        console.log("@STYLE");
        build.rebuildStyle();
      }
      console.log("... ... ... ...");
    }
});
