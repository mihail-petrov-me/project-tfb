(function () {

    var EventCollection = {};

    EventCollection.form = {

        PROCESS : {
            INIT        : 'event:form:process_init',
            UPDATE      : 'event:form:process_update',
            END         : 'event:form:process_end',
            UPDATE_END  : 'event:form:process_update_end'
        },

        CHANGE   : 'event:form:change'
    };

    EventCollection.page = {

        GROUP : {
            LIST : {
                INIT    : 'event_page:group_list:init',
                LOAD    : 'event_page:group_list:load',
                DESTROY : 'event_page:group_list:destroy'
            },

            INFO : {
                INIT    : 'event_page:group_info:init',
                LOAD    : 'event_page:group_info:load',
                DESTROY : 'event_page:group_info:destroy'
            },

            CREATE : {
                INIT    : 'event_page:group_create:init',
                DESTROY : 'event_page:group_create:destroy'
            },

        },

        LOAD : 'event:ui:after_load'
    };

    EventCollection.component = {

        FINISH  : 'event_component:finish',

        UISIDEBAR : {
            STATE   : {
                DISABLE : 'event_component:sidebar:state:disable'
            }
        },

        POST : {

            PROCESS : 'event_component:post:process',
            FINISH  : 'event_component:post:finish',

            MODE_INTERACTIVE    : 'event_component:post:mode_interactive',
            MODE_PRESENTATION   : 'event_component:post:mode_presen',
        },

        ARTICLE : {
            PROCESS : 'event_component:article:process',
            FINISH  : 'event_component:article:finish',
        },

        POLL : {
            PROCESS : 'event_component:poll:process',
            FINISH  : 'event_component:poll:finish',
        },

        VIDEO : {
            PROCESS : 'event_component:video:process',
            FINISH  : 'event_component:video:finish',
        },

        IMAGE : {
            PROCESS : 'event_component:image:process',
            FINISH  : 'event_component:image:finish',
        }
    };

    EventCollection.PAGE = {

        LOAD_FINISH         : 'event:ui:after_load',

        PROFILE_INIT        : 'event:page:profile:init',
        LIST_INIT           : 'event:page:list:destroy',

        LIST_DESTROY        : 'event:page:list:destroy',
        PROFILE_DESTROY     : 'event:page:profile:destroy'
    };


    /**
     *
     * @param {*} timeout
     */
    var Service = function ($timeout) {

        var ErrorMessages = {
            invalideArgument : '@When subscribing for an event, a callback function must be defined.'
        };

        /**
         * Container for storing active subscriber references. This object is responsible for
         * tracking every single active subscriber on the application level
         */
        var PubSub = {
            topics: {},
            subUid: -1
        };

        /**
         * @description
         * Subscribe to events of interest with a specific topic name and a
         * callback function, to be executed when the topic/event is observed.
         *
         * @parameters
         * @param {string} topic
         * @param {function} callback
         * @param {boolean} once
         *
         */
        function subscribe(topic, callback, once = false) {

            var token = (PubSub.subUid += 1);
            var obj   = {};

            if (typeof callback !== 'function') {
                throw new TypeError(ErrorMessages.invalideArgument);
            }

            if (!PubSub.topics[topic]) {
                PubSub.topics[topic] = [];
            }

            obj.token    = token;
            obj.callback = callback;
            obj.once     = !!once;

            PubSub.topics[topic].push(obj);

            return token;
        }

        /**
         * @description
         * Subscribe to events of interest setting a flag
         * indicating the event will be published only one time.
         *
         * @parameters
         * @param {string} topic
         * @param {function} callback
         */
        function once(topic, callback) {
            return subscribe(topic, callback, true);
        }

        /**
         * @description
         * Publish or broadcast events of interest with a specific
         * topic name and arguments such as the data to pass along.
         *
         * @parameters
         * @param {string} topic
         * @param {*} data
         */
        function publish(topic, data) {

            var len;
            var subscribers;
            var currentSubscriber;
            var token;

            if (!PubSub.topics[topic]) {
                return false;
            }

            $timeout(function () {

                subscribers = PubSub.topics[topic];
                len = subscribers ? subscribers.length : 0;

                while (len) {
                    len -= 1;
                    token = subscribers[len].token;
                    currentSubscriber = subscribers[len];

                    currentSubscriber.callback(data, {
                        name: topic,
                        token: token
                    });

                    if (currentSubscriber.once === true) {
                        unsubscribe(token);
                    }
                }
            }, 0);

            return true;
        }

        /**
         * @description
         * Unsubscribe from a specific topic, based on the topic name,
         * or based on a tokenized reference to the subscription.
         *
         * @parameters
         * @param {String} topic
         */
        function unsubscribe(topic) {

            var tf = false;
            var prop;
            var len;

            for (prop in PubSub.topics) {

                if (Object.hasOwnProperty.call(PubSub.topics, prop)) {

                    if (PubSub.topics[prop]) {
                        len = PubSub.topics[prop].length;

                        while (len) {
                            len -= 1;

                            // If t is a tokenized reference to the subscription.
                            // Removes one subscription from the array.
                            if (PubSub.topics[prop][len].token === topic) {
                                PubSub.topics[prop].splice(len, 1);
                                return topic;
                            }

                            // If t is the event type.
                            // Removes all the subscriptions that match the event type.
                            if (prop === topic) {
                                PubSub.topics[prop].splice(len, 1);
                                tf = true;
                            }
                        }

                        if (tf === true) {
                            return topic;
                        }
                    }
                }
            }

            return false;
        }


        return {
            api : {
                subscribe,
                once,
                publish,
                unsubscribe
            },

            form : {

                init: (formName) => {
                    publish(EventCollection.form.PROCESS.INIT, formName);
                },

                update: (formName) => {
                    publish(EventCollection.form.PROCESS.UPDATE, formName);
                },

                end: (formName, callback) => {

                    subscribe(EventCollection.form.PROCESS.END, ($response) => {
                        if($response.form_id == formName) { callback($response.response); }
                    });
                },

                endUpdate: (formName, callback) => {

                    subscribe(EventCollection.form.PROCESS.UPDATE_END, ($response) => {
                        if($response.form_id == formName) { callback($response.response); }
                    });
                },


                change: (formName, callback) => {

                    subscribe(EventCollection.form.CHANGE, ($response) => {
                        if($response.form_id == formName) { callback($response); }
                    });
                },

                destroy: () => {
                    unsubscribe(EventCollection.form.PROCESS.END);
                }
            },

            events : EventCollection
        };
    };

    Service.$inject = ['$timeout'];
    angular.module('application.service').factory('PubSub', Service);
}());
