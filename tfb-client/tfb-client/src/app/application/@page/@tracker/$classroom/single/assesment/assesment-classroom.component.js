(function () {

    var Controller = function (Translation, PubSub, UserModel, BundleModel) {


        this.PROPERTY = {
            BUNDLE_ID : null
        }

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en' : '@New bundle',
                    'bg' : '@Нов клас'
                })
            };
        });


        /**
         * @type
         * Lifecycle event : INIT
         */
        this.$onInit = () => {

        };


        this.on_next = () => {
            this.$router.navigate(['/Dashboard/Classroom/ClassroomSingle', {'id' : this.PROPERTY.BUNDLE_ID}]);
        };


        /**
         * @type
         * Router event
         */
        this.$routerOnActivate = (next, previous) => {
            this.PROPERTY.BUNDLE_ID = next.params.id;
        };



        /**
         * @type
         * Lifecycle event : Destroy
         */
        this.$onDestroy = () => {

        };
    };

    Controller.$inject  = ['Translation', 'PubSub', 'UserModel', 'BundleModel'];
    var Component       = {
        templateUrl     : './assesment-classroom.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            $router     : '<'
        }
    };

    angular.module('application.components.page').component('assesmentClassroom', Component);
})();
