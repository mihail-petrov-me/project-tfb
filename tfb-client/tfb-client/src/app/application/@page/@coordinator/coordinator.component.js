(function () {

    /**
     *
     * @param {*} UserModel
     * @param {*} Translation
     * @param {*} State
     * @param {*} PubSub
     */
    var Controller = function (UserModel, Translation, State, PubSub) {

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {
            this.LABEL = {};
        });

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        this.$onInit = () => {

            if(!UserModel.profile.isPermited(['1', '4', '5'])) {
                this.$router.navigate(['/Dashboard']);
            }
        };
    };

    var ComponentRouter = [

        //
        // #
        { path: 'dashboard/...' ,  component: 'dashboard' , as: 'Dashboard' },
        { path: 'schedule/...'  ,  component: 'schedule'  , as: 'Schedule'  },
    ];

    Controller.$inject  = ['UserModel', 'Translation', 'State', 'PubSub'];

    var Component       = {
        templateUrl     : './tracker.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        $routeConfig    : ComponentRouter,
        bindings        : { $router     : '<' }
    };

    angular.module('application.components.page').component('tracker', Component);
})();
