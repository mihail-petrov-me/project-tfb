<?php

namespace App\Http\Models;

class Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const DELETED_AT = 'deleted_at';


    /**
     *
     * @var type 
     */
    public $limit = 10;
    
    /**
     *
     * @var type 
     */
    public $start = 0;
    
    
    /**
     *
     * @var type 
     */
    public $statment = null;

    /**
     *
     * @var type 
     */
    protected $pagginationCollection = array();        
    
    /**
     *
     * @var type 
     */
    protected $timestamp = null;

    /**
     *
     * @var type 
     */
    private $refreshField = null;
    
    /**
     * 
     * @param type $field
     */
    public function setRefresh($field)
    {
        $this->refreshField = $field;
    }
    
    /**
     * 
     * @return type
     */
    public function getRefresh()
    {
        return $this->refreshField;
    }

    
    /**
     * 
     * @param type $collection
     * @param type $key
     * @param type $default
     * @return type
     */
    public function process($collection, $key, $default = null)
    {
        if (is_null($collection)) {
            return $default;
        }
        
        return (isset($collection[$key]))? $collection[$key] : $default;
    }
    
    /**
     * 
     * @param type $collection
     * @return boolean
     */
    public function isRefresh($collection)
    {
        if(isset($collection['request'])) {
            return ($collection['request'] == 'refresh');
        }
        
        return false;        
    }
    
    /**
     * 
     * @param type $builderExpression
     * @param type $query
     * @return type
     */
    private function buildPaginationQuery($builderExpression, $query)
    {
        $this->setPagination('start', $this->process($query, 'start', 0));
        $this->setPagination('limit', $this->process($query, 'limit', 0));
        
        $collection = $builderExpression
            ->limit($this->getPagination()["limit"])
            ->offset($this->getPagination()["start"])
            ->get();
        
        $hasNext = (count($collection) == $this->getPagination()["limit"]);
        $this->setPagination('has_next', $hasNext);
        $this->setPagination('next', (count($collection) + 1 + $this->getPagination()["start"]));
        
        
        if(isset($collection) && count($collection) > 0) {
            $this->timestamp = $collection[0]->created_at;
        }
        
        return $collection;   
    }
    
    /**
     * 
     * @param type $builderExpression
     * @param type $query
     * @return type
     */
    public function buildRefreshQuery($builderExpression, $query)
    {
        if(isset($query['timestamp']))
        {
            $collection = $builderExpression
            ->where($this->getRefresh(), '>', $query['timestamp'])
            ->get();
        
            $this->timestamp = $collection[0]->created_at;

            return $collection;
        }
        
        $collection = $builderExpression->get();
        $this->timestamp = $collection[0]->created_at;

        return $collection;        
    }
    
    /**
     * 
     * @param type $query
     * @return type
     */
    public function select($builderExpression, $query)
    {
        if($this->isRefresh($query)) {
            return $this->buildRefreshQuery($builderExpression, $query);
        }
        
        return $this->buildPaginationQuery($builderExpression, $query);
    }
    
    
    
    public function getMeta()
    {
        return array(
            "pagination"    => $this->getPagination(),
            "timestamp"     => $this->timestamp
        );
    }

    /**
     * 
     * @return type
     */
    public function getPagination()
    {
        return $this->pagginationCollection;
    }
    
    /**
     * 
     * @param type $key
     * @param type $value
     */
    public function setPagination($key, $value)
    {
        $this->pagginationCollection[$key] = $value;
    }
}