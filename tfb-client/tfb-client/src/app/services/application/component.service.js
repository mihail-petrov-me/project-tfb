((() => {

    var Service = () => {

        /**
         *
         * @param {*} model
         * @param {*} calback
         */
        function $presentation(mode, calback) {
            if(mode == 'presentation') calback();
        }

        /**
         *
         * @param {*} model
         * @param {*} calback
         */
        function $interactive(mode, calback) {
            if(mode == 'interactive') calback();
        }

        return { $presentation, $interactive };
    };

    angular.module('application.service').factory('Component', Service);

})());
