(function () {

    /**
     *
     * @param {*} UserModel
     * @param {*} Translation
     * @param {*} State
     * @param {*} PubSub
     */
    var Controller = function (UserModel, Translation, State, PubSub, BundleModel) {

        this.STATE = {
            MESSAGE_NO_SCHOOL_AVAILABLE : false
        };


        this.on_process = () => {

        };


        /**
         * @type
         * EVENT : request
         */
        this.request_FETCH_user = () => {

            BundleModel.api.COLLECTION().fetch
                .teachers(UserModel.profile.my("user_id"))
                    .then(this.handler_experiance_success)
                    .catch(this.handler_experiance_error);

        };

        /**
         * @type
         * HANDLER :  success
         */
        this.handler_experiance_success = ($response) => {

            //
            //
            // ########################################################
            UserModel.state("tracker", JSON.stringify($response.data));
            // ########################################################
            //
            //


            if($response.data.length == 0) {
                this.STATE.MESSAGE_NO_SCHOOL_AVAILABLE = true;
            }
            else if($response.data.length > 0) {
                this.STATE.MESSAGE_NO_SCHOOL_AVAILABLE = false;
            }
        };

        /**
         * @type
         * HANDLER :  error
         */
        this.handler_experiance_error = () => {
            console.log("@Exception: When we try to get user experiance");
        };

        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {
            this.request_FETCH_user();
        };
    };

    Controller.$inject = ['UserModel', 'Translation', 'State', 'PubSub', 'BundleModel'];
    var Component = {
        templateUrl     : './init-tracker.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('initTracker', Component);

})();
