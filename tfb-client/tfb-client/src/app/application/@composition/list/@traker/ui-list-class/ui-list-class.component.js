(function () {

    var Controller = function (UserModel) {

        var model = this;


        model.hasAvailability = function($id) {
            return (UserModel.profile.my('id') != $id);
        };
    }

    /**
     *
     */
    var Component = {
        templateUrl     : './ui-list-class.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        transclude      : true,
        bindings        : {
            collection    : '<',
            uiLoadMore    : '<'
        }
    };

    /**
     *
     */
    angular.module('application.components.visual').component('uiListClass', Component);

})();
