(function () {


    /**
     * @type :
     * Base url
     */
    var BASE_URL = "entity";

    /**
     * @type
     * User creation services
     *
     * @description
     * Contains collection of methods for creating an user related items
     */
    var create = function(request) {

        return {
            comment     : (collection) => request.post([BASE_URL, 'comments'], collection)
        };
    };


    /**
     * @type
     * User update services
     *
     * @description
     * Contains collection of methods for updating an user related items
     */
    var update = function(request) {

        return {
            contact     : ($id, $body) => request.put([BASE_URL     , $id, "contact"    ],  $body),
            education   : ($id, $body) => request.put([BASE_URL     , $id, "education"  ],  $body),
            employment  : ($id, $body) => request.put([BASE_URL     , $id, "employment" ],  $body),
            language    : ($id, $body) => request.put([BASE_URL     , $id, "language"   ],  $body),
            picture     : ($id, $body) => request.upload([BASE_URL  , $id, "picture"    ],  $body)
        };
    };


    /**
     * @type
     * User fetch services
     *
     * @description
     * Contains collection of methods for fetching an user related items
     */
    var fetch = function(request, requestType) {

        return {
            comments        : ($id) => request.beffore(requestType).get([BASE_URL, $id, "comments"]),
            userfeed        : ($id) => request.beffore(requestType).get([BASE_URL, "feed", $id]),
            posts           : ($id) => request.beffore(requestType).get([BASE_URL, $id, 'posts']),
            articles        : ($id) => request.beffore(requestType).get([BASE_URL, $id, 'articles'])
        };
    }


    /**
     * @type
     * User fetch services
     *
     * @description
     * Contains collection of methods for archiving an user related items
     */
    var archive = function(request, base, provider) {

        return {};
    };


    /**
     *
     * @param {*} request
     * @param {*} base
     * @param {*} provider
     */
    var ApiProvider = function(request) {

        var requestType = {
            type : 'collection'
        };

        return {

            PAGINATE : function() {
                requestType.type = 'pagination';
                return this;
            },

            REFRESH : function() {
                requestType.type = 'refresh';
                return this;
            },

            COLLECTION : function() {
                requestType.type = 'collection';
                return this;
            },

            SINGLE : function() {
                requestType.type = 'single';
                return this;
            },

            create  : create(request),
            fetch   : fetch(request, requestType),
            update  : update(request),
            archive : archive(request)
        }
    };

    /**
     *
     * @param {*} http
     * @param {*} q
     * @param {*} AppManager
     */
    var ServiceHandler = function (Request) {
        return { api: ApiProvider(Request) };
    };

    ServiceHandler.$inject = ['Request'];
    angular.module('application.models').factory('EntityModel', ServiceHandler);
})();
