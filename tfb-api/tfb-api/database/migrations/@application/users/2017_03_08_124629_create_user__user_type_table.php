<?php

use App\Http\Models\User\User;
use \App\Http\Models\User\UserType;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserUserTypeTable extends Migration
{
    private $table = User::TABLE_USER_TYPES;

    public function up()
    {
        Schema::create($this->table, function(Blueprint $builder) {

            //# Primary Key
            $builder->char(User::ID, 13);
            $builder->integer(UserType::ID);
            $builder->primary([User::ID, UserType::ID]);
        });
    }

    public function down()
    {
        Schema::drop($this->table);
    }
}
