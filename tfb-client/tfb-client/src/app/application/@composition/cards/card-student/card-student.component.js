(function () {

    var Controller = function (BundleModel) {

        /**
         * @type
         * State Collection
         */
        this.STATE = {
            PRESENTATION    : true,
            EDIT            : false
        };


        /**
         * @type
         * Event : Click
         */
        this.on_edit = () => {
            this.STATE.PRESENTATION    = false;
            this.STATE.EDIT            = true;

            this.inputText = this.collection.name;
        };

        /**
         * @type
         * Event : Click
         */
        this.on_close = () => {

            this.STATE.PRESENTATION = true;
            this.STATE.EDIT         = false;
        };

        /**
         * @type
         * Event : Click
         */
        this.on_delete = () => {

            BundleModel.api.archive
            .student(this.uiBundle, this.uiStudent)
                .then(function($response) {
                    console.log("I have deleted an user");
                    console.log($response);
                })
                .catch(function($response) {
                    console.log("@Exception: Could not remove student from the collection");
                });
        };

        /**
         * @type
         * Event : Change
         */
        this.$onChanges = () => {

            this.collection = {
                "name"      : this.name
            };
        };
    };


    var Component = {
        templateUrl     : './card-student.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {

            name         : '<',
            uiBundle     : '<',
            uiStudent    : '<'
        }
    };
    angular.module('application.components.visual').component('cardStudent', Component);
})();
