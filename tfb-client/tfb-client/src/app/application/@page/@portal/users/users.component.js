(function () {

    /**
     * Bootstrap application component
     */
    var Controller = function (PubSub, Translation, State) {

        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en': '@Users',
                    'bg': '@Колеги'
                }),

                LABEL_ALL: $provider({
                    'en': 'All',
                    'bg': 'Всички'
                }),

                LABEL_ALUMNI: $provider({
                    'en': 'Alumnies',
                    'bg': 'Алумнита'
                }),

                LABEL_TEACHER: $provider({
                    'en': 'Teacher',
                    'bg': 'Учители'
                }),

                LABEL_EMPLOY: $provider({
                    'en': 'Employees',
                    'bg': 'Служители'
                }),

                LABEL_CONTACT: $provider({
                    'en': 'Contact',
                    'bg': 'Контакти'
                }),

                LABEL_EDUCATION: $provider({
                    'en': 'Education',
                    'bg': 'Образование'
                }),

                LABEL_EMPLOYMENT: $provider({
                    'en': 'Employment',
                    'bg': 'Работодатели'
                }),

                LABEL_MESSAGE: $provider({
                    'en': 'Send message',
                    'bg': 'Изпрати съобщение'
                }),
            };
        });


        /**
         * @type
         * Event
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        this.on_filter = ($key, $value) => {

            var $request    = {};
            $request[$key]  = $value;

            PubSub.api.publish("users:filter:user_type", $request);
            State.push("users", ($value == 3) ? "filter-employ" : "filter-alumni");
        }

        /**
         * @type
         * Event
         *
         * @description
         * This handler is colled after the authentication request is made
         * based on the result the user will be redirected to the application
         * or to regissstration form in order to compleat it's registration
         */
        this.on_filterByType = ($type) => {
            PubSub.api.publish('__filter_by_type', $type);
        };

        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        this.$onInit = () => {

            PubSub.api.subscribe('api:publish:user-profile'     , () => { State.push("users", "profile");});
            PubSub.api.subscribe('api:publish:user:list-view'   , () => { State.push("users", "filter");});
        };


        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component destroy
         */
        this.$onDestroy = () => {

            PubSub.api.unsubscribe('api:publish:user-profile');
            PubSub.api.unsubscribe('api:publish:user:list-view');
        }
    };

    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */

    var ComponentRouter  = [
        { path: '/',    component: 'userList',    as: 'UserList', useAsDefault: true},
        { path: '/:id', component: 'userProfile', as: 'UserProfile'}
    ];

    Controller.$inject  = ['PubSub', 'Translation', 'State'];
    var Component       = {
        templateUrl     : './users.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        $routeConfig    : ComponentRouter,
        bindings        : { $router: '<' }
    };

    angular.module('application.components.page').component('users', Component);

})();
