// System Modules
// #
var fs                  = require('fs-extra');

// Build Tasks
// #
var clean               = require('../common/clean');
var copyApp             = require('../common/copy.app');
var copyTransform       = require('../common/copy.transform');
var copyFont            = require('../common/copy.fonts');

//
// #
var style               = require('../common/style');
var serve               = require('../common/serve');

//
var injectTemplate      = require('../injectors/inject.template');
var injectScript        = require('../injectors/inject.script');
var injectStyle         = require('../injectors/inject.style');
var injectRegion        = require('../injectors/inject.region');
var injectComponent     = require('../injectors/inject.component');
var injectTranspile     = require('../injectors/inject.transpile');

//
// #
var lint                = require('../common/lint');

// Configuration
// #
var settings            = require('../../settings');
var scriptCollection    = require((settings.app + 'modules.js'));
var moduleReference     = scriptCollection.vendors;
var styleReference      = scriptCollection.cssVendors;
var fontsReference      = scriptCollection.fontVendors;


var INJECTOR    = require('../../injector.json');
var build_AST   = require('../helpers/build');


console.log("****")
console.log(settings.root);
console.log("****")


/**
 * Execute the build of the application
 */
exports.exec = function() {

    // #1. Clean evelopment directory build
    clean.exec(settings.dev);

    // #2. Copy working directory from src to _dev
    copyApp.exec({
        from    : [(settings.app + 'app'), (settings.app + 'assets'), (settings.app + 'fonts')],
        to      : [(settings.dev + 'app'), (settings.dev + 'assets'), (settings.dev + 'fonts')]
    });

    // #3. Copy vendor directory from src to _dev
    copyTransform.exec({
        from        : [settings.root],
        to          : [settings.dev + '/vendor/'],
        transform   : INJECTOR.transformers.script
    });

    // #4 Build and inject style
    // #
    style.exec({
        from    : (settings.app + 'styles/style.scss'),
        to      : (settings.dev + 'styles/style.css'),
        vendor  : INJECTOR.transformers.style
    });

    // #5. Copy vendor directory from src to _dev
    copyTransform.exec({
        from        : [settings.root],
        to          : [settings.dev + '/fonts/'],
        transform   : INJECTOR.transformers.font
    });

    // #5. Copy vendor directory from src to _dev
    injectScript.exec({
        from    : settings.app,
        to      : settings.dev,
        pipe    : INJECTOR.pipe
    });

    injectTemplate.exec(settings.dev);
    injectTranspile.exec(settings.dev + 'app/');
    injectStyle.exec(settings.dev, 'styles/style.css');
    injectRegion.exec(settings.dev);

    console.log("@BUILD: INITIAL APPLICATION BUILD");
};


/**
 * Rebuild only the portion containing application script
 */
exports.rebuildScript = function($from) {

    //destination element
    var $to = (settings.dev + $from.replace("/vagrant/tfb-client/src/", ""));
    copyApp.exec({ from : [$from], to : [$to] });

    injectTemplate.single($to);
    injectTranspile.single($to);
    console.log("@BUILD: SCRIPT");
};

/**
 * Rebuild only the portion containing application @TEMPLATE
 */
exports.rebuildTemplate = function($from) {

    //destination element
    var $to = (settings.dev + $from.replace("/vagrant/tfb-client/src/", ""));
    copyApp.exec({ from : [$from], to : [$to] });

    console.log("@BUILD: TEMPLATE");
};

/**
 * Rebuild only the portion containing application style.
 */
exports.rebuildStyle = function() {

    style.exec({
        from    : (settings.app + 'styles/style.scss'),
        to      : (settings.dev + 'styles/style.css'),
        vendor  : INJECTOR.transformers.style
    });

    injectStyle.exec(settings.dev, 'styles/style.css');

    console.log("@BUILD: APPLICATION STYLE");
};
