<?php

// GET
// =====================================================================================================================
$app->get('/entity/{id}/comments'      , 'Portal\EntityController@showComment'             )->middleware('jwt');
$app->get('/entity/posts/{id}'         , 'Portal\EntityController@showPostEntity'          )->middleware('jwt');
$app->get('/entity/polls/{id}'         , 'Portal\EntityController@showPollEntity'          )->middleware('jwt');
$app->get('/entity/articles/{id}'      , 'Portal\EntityController@showArticleEntity'       )->middleware('jwt');
$app->get('/entity/{id}/posts'         , 'Portal\EntityController@showPostCollection'      )->middleware('jwt');
$app->get('/entity/{id}/polls'         , 'Portal\EntityController@showPollCollection'      )->middleware('jwt');
$app->get('/entity/{id}/articles'      , 'Portal\EntityController@showArticleCollection'   )->middleware('jwt');
$app->get('/entity/feed/{id}'          , 'Portal\EntityController@showUserFeed'            )->middleware('jwt');

// POST routes
// =====================================================================================================================
$app->post('/entity/comments'          , 'Portal\EntityController@createComment'           )->middleware('jwt');

// PUT routes
// =====================================================================================================================
$app->put('/users/{id}'                , 'Portal\EntityController@update'                  )->middleware('jwt');

// DELETE routes
// =====================================================================================================================
$app->delete('/users/{id}'             , 'Portal\EntityController@delete'                  )->middleware('jwt');
