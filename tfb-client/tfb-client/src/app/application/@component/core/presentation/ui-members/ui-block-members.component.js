(function () {

    var Component = {

        templateUrl         : './ui-block-members.template.html',
        controllerAs        : 'model',
        bindings            : {
            uiCollection    : '<',
            uiPicture       : '<',
        }
    };

    angular.module('application.components.visual').component('uiBlockMembers', Component);

})();
