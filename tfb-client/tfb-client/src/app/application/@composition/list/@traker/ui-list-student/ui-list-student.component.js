(function () {

    var Controller = function (PubSub, UserModel) {

        this.PROPERTY = {
            BUNDLE_ID : null
        };


        this.collection = [];
        /**
         *
         */
        this.add_student = () => {
            PubSub.form.init('FORM_CREATE_STUDENT');
        };

        /**
         *
         */
        this.$onInit = () => {

            this.PROPERTY.BUNDLE_ID = this.uiBundle;

            PubSub.form.end('FORM_CREATE_STUDENT', ($response) => {
                console.log($response);
            });
        };

        this.count = {
            a : true
        };

        this.on_showAdd = () => {
            this.count.a = false;
        };


        this.on_add = () => {

            this.inputText             = null;
            this.count.a               = true;

            this.submit_student();
        };

        this.get_data = () => {
            return this.count.a;
        };

        /**
         *
         */
        this.submit_student = () => {
            PubSub.api.publish(PubSub.events.form.PROCESS.INIT, 'form-student');
        };


        this.$onDestroy = () => {
            PubSub.form.destroy();
        };
    };

    var Component = {
        templateUrl     : './ui-list-student.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        transclude      : true,
        bindings        : {
            collection    : '<',
            uiLoadMore    : '<',
            uiBundle      : '<'
        }
    };

    /**
     *
     */
    angular.module('application.components.visual').component('uiListStudent', Component);

})();
