(function () {

    /**
     *
     * @param {*} Translation
     * @param {*} PubSub
     */
    var Controller = function (Translation, PubSub, UserModel) {

        this.STATE = {
            IS_SIDEBAR_VISIBLE  : true,
            SHOW_DROPDOWN       : false
        };

        this.currentState = 'PORTAL';

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                ITEM_USERS : $provider({
                    'en' : 'users',
                    'bg' : 'Потребители'
                }),

                ITEM_PROJECTS : $provider({
                    'en' : 'projects',
                    'bg' : 'Проекти'
                }),

                ITEM_INITIATIVES : $provider({
                    'en' : 'initiatives',
                    'bg' : 'Инициативи'
                }),

                ITEM_GROUPS : $provider({
                    'en' : 'groups',
                    'bg' : 'Групи'
                }),

                ITEM_MESSAGES : $provider({
                    'en' : 'messages',
                    'bg' : 'Съобщения'
                }),

                ITEM_OPPORTUNITIES : $provider({
                    'en' : 'opportunities',
                    'bg' : 'Възможности'
                }),

                ITEM_PROFILE : $provider({
                    'en' : 'profile',
                    'bg' : 'Профил'
                }),

                ITEM_FEED : $provider({
                    'en' : 'feed',
                    'bg' : 'Активност'
                }),

                ITEM_CLASSROOM : $provider({
                    'en' : 'class room',
                    'bg' : 'Училище'
                }),

                ITEM_BUNDLE : $provider({
                    'en' : 'bundles',
                    'bg' : 'Добави класове'
                }),

                ITEM_ASSESMENT : $provider({
                    'en' : 'assesment',
                    'bg' : 'Тестове и оценка'
                }),

                ITEM_SCHEDULE : $provider({
                    'en' : 'schedule',
                    'bg' : 'Разписание'
                }),

                ITEM_ADMINISTRATION : $provider({
                    'en' : 'Administration panel',
                    'bg' : 'Администрация'
                }),
            };
        });


        this.on_init = ($argument) => {

            PubSub.api.publish('CHANGE_APP', $argument);
            this.showDropdown();
        }

        /**
         * @type
         * Event Callback
         */
        this.on_languageSwitch = () => {
            Translation.toggle();
        };


        /**
         *
         */
        this.state = ($state) => {
            return ($state == this.currentState);
        }

        /**
         * @type
         * Event
         */
        this.$onInit = () => {

            PubSub.api.subscribe('CHANGE_APP', ($state) => {

                UserModel.state("__navigation_id", $state);
                this.currentState = $state;

                if($state == 'PORTAL') {

                }

                if($state == 'TRACKER') {
                    window.location.href = "#!/dashboard/classroom/dashboard";
                }

                if($state == 'ADMINISTRATION') {
                    window.location.href = "#!/dashboard/administration/campaign";
                }
            });
        }

        /**
         *
         */
        this.showDropdown = () => {
            this.STATE.SHOW_DROPDOWN = !this.STATE.SHOW_DROPDOWN;
        };
    };

    /**
     * @type
     * Component injector
     */
    Controller.$inject  = ['Translation', 'PubSub', 'UserModel'];
    var Component       = {
        templateUrl     : './ui-navigation.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.visual').component('uiNavigation', Component);

})();
