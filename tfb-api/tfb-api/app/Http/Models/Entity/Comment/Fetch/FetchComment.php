<?php

namespace App\Http\Models\Entity\Comment\Fetch;
use DB;

class FetchComment
{
    public function execute($id)
    {
        return DB::table('entity_comment')
            ->join(
                'users', 
                'entity_comment.user_id', '=', 'users.id'
            )
            ->select(
                    
                'entity_comment.id',                    
                'entity_comment.content',
                'entity_comment.created_at',                    
                
                'users.name',
                'users.signature',
                'users.picture'
            )
            ->where(
                'entity_comment.entity_id', $id
            )
            ->orderBy(
                'entity_comment.created_at'
            );
    }
}