// System modules
// # =============================================
var fs  = require('fs.extra');
var EOL = require('os').EOL;

// Core components
// # =============================================
var buildCollection = require('../helpers/build');


// Application components
// # =============================================
var buildScriptCollection = function(collection) {

    var scriptCollection = [];

    for (var i = 0; i < collection.length; i++) {
        scriptCollection.push(['<script src="', collection[i], '"></script>'].join(''));
    }

    return scriptCollection;
};


// API : Interface
// # =============================================
exports.exec = function(options) {

    buildCollection.build(options.from, options.pipe, function(collection) {

        var scriptCollection;
        if(options.pipe.vendor) {
            scriptCollection = buildScriptCollection([].concat(options.pipe.vendor, collection));
        }
        else {
            scriptCollection = buildScriptCollection([].concat(options.pipe.vendor, collection));
        }


        var index   = fs.readFileSync(options.from + 'index.html').toString();
        var result  = index.replace(/@task:BUILD/, scriptCollection.join(EOL));
        fs.writeFileSync(options.to + 'index.html', result);
    });
};
