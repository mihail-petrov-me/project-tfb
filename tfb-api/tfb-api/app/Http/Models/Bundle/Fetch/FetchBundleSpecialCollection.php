<?php
namespace App\Http\Models\Bundle\Fetch;

use App\Http\Models\Bundle\Bundle;
use App\Http\Models\Repository;
use App\Http\Models\Subject\Subject;
use App\Http\Models\User\User;
use DB;

/**
 * Class FetchBundleSpecialCollection
 * @package App\Http\Models\Bundle\Fetch
 */
class FetchBundleSpecialCollection extends Repository
{
    public function execute($query)
    {
        $data = DB::table(Bundle::TABLE)
            ->select([
                $this->table(Bundle::TABLE, Bundle::ID),
                $this->table(Bundle::TABLE, Bundle::SCHOOL_ID),
                $this->table(Bundle::TABLE, Bundle::INIT_YEAR),
                $this->table(Bundle::TABLE, Bundle::SIGNATURE),

                $this->table(Subject::TABLE, User::TEACHER_ID),
                $this->table(Subject::TABLE, Subject::TITLE),
            ])
            ->leftJoin(
                Subject::TABLE,
                $this->table(Subject::TABLE, Bundle::ID),
                '=',
                $this->table(Bundle::TABLE, Bundle::ID)
            )
            ->where([
                $this->table(Subject::TABLE, User::TEACHER_ID) =>  $query['teacher_id']
            ])->get();

        $collection = [];
        foreach ($data as $element) {


                if(array_key_exists($element->bundle_id, $collection)) {
                    $collection[$element->bundle_id]["subjects"][] = $element->subject_title;
                }
                else {
                    $collection[$element->bundle_id]["id"]        = $element->bundle_id;
                    $collection[$element->bundle_id]["school_id"] = $element->school_id;
                    $collection[$element->bundle_id]["init_year"] = $element->init_year;
                    $collection[$element->bundle_id]["signature"] = $element->signature;

                    $collection[$element->bundle_id]["subjects"] = [];
                    $collection[$element->bundle_id]["subjects"][] = $element->subject_title;
                }
        }

        return array_values($collection);
    }
}