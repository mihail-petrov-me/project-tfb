<?php
namespace App\Http\Models\Bundle\Create;

use App\Http\Models\User\User;
use App\Http\Models\Bundle\Bundle;
use App\Http\Models\Repository;
use DB;

class CreateBundle extends Repository
{
    /**
     * @param $collection
     * @return array
     */
    public function execute($collection)
    {
        $entity = $this->build($collection);

        DB::table(Bundle::TABLE)->insert($entity);

        DB::table(Bundle::TABLE_TEACHERS)->insert(array(
            User::TEACHER_ID    => $this->process($collection, User::TEACHER_ID),
            Bundle::ID          => $entity[Bundle::ID]
        ));

        return $entity;
    }

    /**
     * @param $collection
     * @return array
     */
    public function build($collection)
    {
        $position   = $this->process($collection, Bundle::POSITION);
        $label      = $this->process($collection, Bundle::LABEL);
        $schoolId   = $this->process($collection, Bundle::SCHOOL_ID);

        $signature  = "{$position}{$label}";
        $bundleId   = "{$schoolId}_{$signature}_" . date("Y");

        return array(
            Bundle::ID                => uniqid(),
            Bundle::TFB_ID            => $bundleId,
            Bundle::SCHOOL_ID         => $schoolId,
            Bundle::INIT_YEAR         => date("Y"),
            Bundle::LABEL             => $label,
            Bundle::POSITION          => $position,
            Bundle::SIGNATURE         => $signature
        );
    }
}