<?php


namespace App\Http\Models\User\Create;

use App\Http\Models\Repository;
use App\Http\Models\User\UserEducation;
use DB;

class CreateUserEducation extends Repository
{
    /**
     * @param $id
     * @param $collection
     * @return mixed
     */
    public function execute($id, $collection)
    {
        return DB::table(UserEducation::TABLE)->insert(array(

            UserEducation::USER_ID          => $id,
            UserEducation::INSTITUTION      => $this->process($collection, UserEducation::INSTITUTION),
            UserEducation::DATE_START       => $this->process($collection, UserEducation::DATE_START),
            UserEducation::DATE_END         => $this->process($collection, UserEducation::DATE_END),
            UserEducation::DEGREE           => $this->process($collection, UserEducation::DEGREE),
            UserEducation::FIELD_OF_STUDY   => $this->process($collection, UserEducation::FIELD_OF_STUDY)
        ));
    }
}