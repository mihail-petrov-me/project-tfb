<?php

use App\Http\Models\Collections\CollectionSchool;
use App\Http\Models\User\User;
use App\Http\Models\Campaign\Campaign;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignTeacherTable extends Migration
{
    private $table = Campaign::TABLE_TEACHER;

    public function up()
    {
        Schema::create($this->table, function(Blueprint $builder)
        {
            //# Primary Key
            $builder->char(Campaign::ID, 13);
            $builder->char(User::TEACHER_ID, 13);
            $builder->char(User::COORDINATOR_ID, 13);
            $builder->char(CollectionSchool::ID, 13);

            //# Primary Key index
            $builder->primary([
                Campaign::ID,
                User::TEACHER_ID,
                User::COORDINATOR_ID,
                CollectionSchool::ID
            ], "campaign_teacher");
        });
    }

    public function down()
    {
        Schema::drop($this->table);
    }
}
