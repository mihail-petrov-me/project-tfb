<?php

namespace App\Http\Models\Campaign\Update;
use App\Http\Models\Repository;
use DB;

class UpdateCampaign extends Repository
{
    /**
     * @param $id
     * @param $collection
     * @param $query
     * @return array
     */
    public function execute($id, $collection, $query)
    {
        $entity = $this->build($collection);
        DB::table('campaigns')
            ->where(array(
            'id'    => $id
        ))
            ->update($entity);

        return $entity;
    }

    /**
     * @param $collection
     * @return array
     */
    public function build($collection)
    {
        return array(
            'title' => $this->process($collection, 'title')
        );
    }
}