(function () {

    var Controller = function (Translation, PubSub, UserModel, CampaignModel) {

        /**
         * @type
         * Event Publish collection
         */
        this.PUBLISH = {
            FORM_PROCESS_INIT: PubSub.events.form.PROCESS.INIT,
        };

        /**
         * @type
         * Event Subscribe collection
         */
        this.SUBSCRIBE = {
            FORM_PROCESS_END: PubSub.events.form.PROCESS.END
        };

        /**
         * @type
         * Event Subscribe collection
         */
        this.PROPERTY = {
            COLLECTION : []
        };

        this.STATE = {
            IS_SIDEBAR_VISIBLE : false
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en' : '@Education campaign',
                    'bg' : '@Образователна кампания'
                })
            };
        });

        /**
         * @type
         * Event: ON
         */
        this.on_select = () => {
            this.STATE.IS_SIDEBAR_VISIBLE = true;
            console.log("Select ele,emty")
        };

        /**
         * @type
         * Request : FETCH : userfeed
         */
        this.request_FETCH_CampaignCollection = () => {

            let handler = CampaignModel.api.fetch.campaignCollection();

            handler.then(($response) => {
                this.PROPERTY.COLLECTION = $response.data;
            });

            handler.catch(() => {
                console.log("@Exception : Could not load any campaign collection");
            });
        };

        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {
            this.request_FETCH_CampaignCollection();


            PubSub.api.subscribe('PANEL_CREATE_CAMPAIGN:END', ($response) => {
                this.request_CREATE_campaign($response);
            });
         };

        /**
         * @type
         * Lifecycle event
         */
        this.$onDestroy = function() {

        };
    };

    Controller.$inject  = ['Translation', 'PubSub', 'UserModel', 'CampaignModel'];
    var Component       = {
        templateUrl     : './campaign-list.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('campaignList', Component);
})();
