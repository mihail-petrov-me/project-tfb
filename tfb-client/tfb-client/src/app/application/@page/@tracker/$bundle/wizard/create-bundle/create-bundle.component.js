(function () {

    var Controller = function (Translation, PubSub, UserModel, BundleModel) {

        this.PROPERTY = {
            COLLECTION : []
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en' : 'Assign existent class',
                    'bg' : 'Припознай клас'
                })
            };
        });

        /**
         * @type
         * Lifecycle event : INIT
         */
        this.$onInit = () => {
            this.request_FETCH_bundleCollection();
        };
    };

    Controller.$inject  = ['Translation', 'PubSub', 'UserModel', 'BundleModel'];
    let Component       = {
        templateUrl     : './choose-bundle.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            $router     : '<'
        }
    };

//    angular.module('application.components.page').component('chooseBundle', Component);
})();
