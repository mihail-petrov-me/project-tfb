<?php

namespace App\Http\Models\Entity\Comment\Create;
use DB;

class CreateComment
{
    public function execute($collection)
    {
        DB::table('entity_comment')->insert(array(
            'id'        => uniqid(),
            'entity_id' => $collection['entity_id'],
            'content'   => $collection['content'],
            'user_id'   => $collection['user_id']
        ));        
    }
}