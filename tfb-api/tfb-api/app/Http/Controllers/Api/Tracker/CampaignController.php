<?php
namespace App\Http\Controllers\Api\Tracker;

use App\Http\Models\Campaign\Campaign;
use \Illuminate\Http\Request;
use \App\Http\Controllers\Controller;
use \Illuminate\Http\JsonResponse;

/**
 * Class CampaignController
 * @package App\Http\Controllers\Api\Tracker
 */
class CampaignController extends Controller
{
    private $API_ERROR_MESSAGES = [
        "CAMPAIGN_CREATED" => "User created successfully",
        "CAMPAIGN_UPDATED" => "The requested resource could not be updated"
    ];

    /**
     * @var Campaign|null
     */
    private $model;

    /**
     * CampaignController constructor.
     *
     * @param Campaign $model
     */
    public function __construct(Campaign $model)
    {
        $this->model = $model;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function showCampaignCollection(Request $request): JsonResponse
    {
        $entity = $this->model->collection($request->query());
        return $this->responseSuccess($entity);
    }


    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function showCampaignSingle($id): JsonResponse
    {
        $entity = $this->model->single(['id' => $id]);
        return $this->responseSuccess($entity);
    }

    /**
     * @param $id
     * @param $termId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showCampaignTermCollection($id, $termId, Request  $request): JsonResponse
    {
        return $this->responseSuccess();
    }

    /**
     * @param $id
     * @param $termId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showCampaignTermSingle($id, $termId, Request  $request): JsonResponse
    {
        return $this->responseSuccess();
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createCampaign(Request $request): JsonResponse
    {
        $entity = $this->model->store($request->json()->all());
        return $this->responseSuccess($entity);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createCampaignTerm(Request $request): JsonResponse
    {
        $entity = $this->model->store($request->json()->all());
        return $this->responseSuccess($entity);
    }


    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createCampaignTeacher($id, Request $request): JsonResponse
    {
        $entity = $this->model->create_campaign_teacher($id, $request->json()->all());
        return $this->responseSuccess($entity);
    }

    /**
     * @param $id
     * @param Request $request
     */
    public function updateCampaign($id, Request  $request)
    {

    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateCampaignTerm($id, $termId, Request $request): JsonResponse
    {
        $entity = $this->model->store($request->json()->all());
        return $this->responseSuccess($entity);
    }

    /**
     * @param $id
     * @param Request $request
     */
    public function deleteCampaign($id, Request  $request)
    {

    }

    /**
     * @param $id
     * @param Request $request
     */
    public function deleteCampaignTerm($id, $termId, Request  $request)
    {

    }
}