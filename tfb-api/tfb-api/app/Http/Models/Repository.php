<?php
namespace App\Http\Models;
use DB;

/**
 * Class Repository
 * @package App\Http\Models
 */
class Repository
{
    /**
     * @var array
     */
    public $filter = [];

    /**
     * @var array
     */
    public $filterTransform = [];

    /**
     * @param $table
     * @return mixed
     */
    public function db($table)
    {
        return DB::table($table);
    }

    /**
     * @param $table
     * @param $collection
     */
    public function delete($table, $collection)
    {
        DB::table($table)->where($collection)->delete();
    }

    /**
     * @param $table
     * @param $collection
     * @param $select
     *
     * @return mixed
     */
    public function fetch($table, $collection, array $select = ['*'])
    {
        return json_decode(DB::table($table)->select($select)->where($collection)->get(), true);
    }

    /**
     * @param $table
     * @param $collection
     * @param $select
     *
     * @return mixed
     */
    public function fetchSingle($table, $collection, array $select = ['*'])
    {
        return (array)DB::table($table)->select($select)->where($collection)->first();
    }

    /**
     * @param $table
     * @param $field
     * @return string
     */
    public function table($table, $field): string
    {
        return "{$table}.{$field}";
    }


    /**
     * @param $collection
     * @param $key
     * @param null $default
     * @return null
     */
    public function process($collection, $key, $default = null)
    {
        if (null === $collection) {
            return $default;
        }
        
        return $collection[$key] ?? $default;
    }

    /**
     * @param $expression
     * @param $query
     * @return mixed
     */
    public function parseFilterQuery($expression, $query)
    {
        foreach ($query as $key => $value) {

            if(in_array($key, $this->filter, true)) {

                $mkey = $this->filterTransform[$key];
                $expression = $expression->where($mkey, $value);
            }
        }

        return $expression;
    }


    public function extractBundle($bundle)
    {
        return null;
    }

    /**
     * @param   DB   $data
     *
     * @param bool $fetchSingle
     *
     * @return mixed
     */
    public function response ($data, $fetchSingle = false)
    {
        if($fetchSingle) {
            return (array)$data->first();
        }

        return json_decode($data->get(), true);
    }
}