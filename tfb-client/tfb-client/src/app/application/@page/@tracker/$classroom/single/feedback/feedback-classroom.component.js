(function () {

    var Controller = function (Translation, PubSub, UserModel, BundleModel) {


        this.PROPERTY = {
            BUNDLE_ID : null,
            SELECTED_STUDENT : null
        }

        this.collection = [];



        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en' : '@New bundle',
                    'bg' : '@Нов клас'
                })
            };
        });

        /**
         * @type
         * Request : FETCH : userfeed
         */
        this.request_FETCH_students = () => {

            BundleModel.api.COLLECTION().fetch
            .students(UserModel.profile.my("user_id"), this.PROPERTY.BUNDLE_ID)
                .then(this.handler_FETCH_students_success)
                .catch(this.handler_FETCH_students_error);
        };

        /**
         * @type
         * Handler : FECH : SUCCESS
         */
        this.handler_FETCH_students_success = ($response) => {
            this.collection = $response.data;
        };

        /**
         * @type
         * Handler : FECH : ERROR
         */
        this.handler_FETCH_students_error = ($response) => {
            console.log("@Exception: Cannot fetch current active teachers");
        };


        /**
         * @type
         * Lifecycle event : INIT
         */
        this.$onInit = () => {

        };

        /**
         * @type
         * Router event
         */
        this.$routerOnActivate = (next, previous) => {

            this.PROPERTY.BUNDLE_ID = next.params.id;
            this.request_FETCH_students();
        };



        this.on_select = ($element) => {
            this.PROPERTY.SELECTED_STUDENT = $element.name;
        };

        this.on_select_bundle = ($element) => {
            this.PROPERTY.SELECTED_STUDENT = "Целият клас";
        };





        /**
         * @type
         * Lifecycle event : Destroy
         */
        this.$onDestroy = () => {

        };
    };

    Controller.$inject  = ['Translation', 'PubSub', 'UserModel', 'BundleModel'];
    var Component       = {
        templateUrl     : './feedback-classroom.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            $router     : '<'
        }
    };

    angular.module('application.components.page').component('feedbackClassroom', Component);
})();
