<?php

namespace App\Http\Models\User\Update;

use App\Http\Models\Repository;
use App\Http\Models\User\UserEmployment;
use DB;

class UpdateUserEmployment extends Repository
{
    public function execute($id, $collection)
    {
        return DB::table(UserEmployment::TABLE)
            ->where(array(
                UserEmployment::USER_ID => $id
            ))
            ->update(array(
                UserEmployment::ORGANISATION    => $this->process($collection, UserEmployment::ORGANISATION),
                UserEmployment::POSITION        => $this->process($collection, UserEmployment::POSITION),
                UserEmployment::WEBSITE         => $this->process($collection, UserEmployment::WEBSITE),
                UserEmployment::DATE_START      => $this->process($collection, UserEmployment::DATE_START),
                UserEmployment::DATE_END        => $this->process($collection, UserEmployment::DATE_END),
                UserEmployment::DESCRIPTION     => $this->process($collection, UserEmployment::DESCRIPTION),
            ));
    }
}