<?php
namespace App\Http\Models\User;

use App\Http\Models\Model;
use DB;

// # Fetch Collection
// # ===========================================================================
use App\Http\Models\User\Fetch\Collection\FetchUserCollection;
use App\Http\Models\User\Fetch\Collection\FetchUserGroupCollection;
use App\Http\Models\User\Fetch\Collection\FetchUserProjectCollection;
use App\Http\Models\User\Fetch\Collection\FetchUserInitiativeCollection;
use App\Http\Models\User\Fetch\Collection\FetchUserCommentCollection;

// # Fetch Single
// # ===========================================================================
use App\Http\Models\User\Fetch\Single\FetchUserSignature;
use App\Http\Models\User\Fetch\Single\FetchUserContact;
use App\Http\Models\User\Fetch\Single\FetchUserEducation;
use App\Http\Models\User\Fetch\Single\FetchUserEmployment;
use App\Http\Models\User\Fetch\Single\FetchUserLanguage;
use App\Http\Models\User\Fetch\Single\FetchUserExperiance;

// # Update
// # ===========================================================================
use App\Http\Models\User\Update\UpdateUserContact;
use App\Http\Models\User\Update\UpdateUserEducation;
use App\Http\Models\User\Update\UpdateUserEmployment;
use App\Http\Models\User\Update\UpdateUserExperince;
use App\Http\Models\User\Update\UpdateUserLanguage;

// # Create
// # ===========================================================================
use App\Http\Models\User\Create\CreateUser;

/**
 * Class UserContact
 * @package App\Http\Models\User
 */
class UserContact extends Model
{
    // Public fields
    const ID            = "user_contact_id";
    const PHONE         = "user_contact_phone";
    const BIRTH_DATE    = "user_contact_birth_date";
    const BIRTH_PLACE   = "user_contact_birth_place";
    const ADDRESS       = "user_contact_address";

    // FK
    const USER_ID       = "user_id";

    // Meta fields
    const CREATED_AT    = "created_at";
    const UPDATED_AT    = "updated_at";
    const DELETED_AT    = "deleted_at";

    // Tables
    const TABLE         = "user__contact";
}