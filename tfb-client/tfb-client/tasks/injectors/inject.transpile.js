// Core components
// # =============================================
var fs        = require('fs-extra');
var util      = require('../helpers/util');
var babel     = require('babel-core');


// API
// # =============================================
exports.exec = function(source) {

  util.traverse(source, function(file, directory, filePath) {

    var f = fs.readFileSync(filePath).toString();
    var res = babel.transform(f, { filename :filePath });

    fs.writeFileSync(filePath, res.code);
  });
};


exports.single = function(source) {

    var collection  = source.split('/');
    var last        = collection.slice(-1)[0];
    var path        = source.replace(last, "");

    var f        = fs.readFileSync(source).toString();

    var res = babel.transform(f, { filename: source });
    fs.writeFileSync(source, res.code);
};
