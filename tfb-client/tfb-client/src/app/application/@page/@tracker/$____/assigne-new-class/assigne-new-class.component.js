(function () {

    var ComponentRouter = [
        { path: '/'     , component: 'classNewListComponent'    , as: 'ClassNewListComponent'  , useAsDefault: true },
        { path: '/:id'  , component: 'studentListComponent'     , as: 'StudentListComponent' },
    ];

    var Component       = {
        templateUrl     : './assigne-new-class.template.html',
        $routeConfig    : ComponentRouter,
        bindings        : { $router     : '<' }
    };

    angular.module('application.components.page').component('assigneNewClass', Component);

})();
