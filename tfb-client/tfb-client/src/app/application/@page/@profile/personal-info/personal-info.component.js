(function () {

    /**
     *
     * @param {*} PubSub
     * @param {*} UserModel
     */
    var Controller = function(Translation, PubSub, UserModel) {

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                BUTTON_ACTION : $provider({
                    'en' : 'Update',
                    'bg' : 'Актуализирай'
                })
            };
        });


        this.$onInit = () => {

            var $result = UserModel.api.SINGLE().fetch.contact(UserModel.profile.my("user_id"));

            $result.then(($r) => {
                this.fieldCollection = $r.data;
                console.log(this.fieldCollection);
            });

            $result.catch(($r) => {

                console.log("Error");
                console.log($r);
            });

        }
    };

    /**
     * @type
     * Component injector
     */
    Controller.$inject = ['Translation','PubSub', 'UserModel'];

    var Component = {
        templateUrl     : './personal-info.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.page').component('personalInfo', Component);

})();
