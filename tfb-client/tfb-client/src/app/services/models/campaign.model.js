(function () {

    var BASE_URL = "campaigns";

    /**
     * @type
     * Class creation services
     *
     * @description
     * Contains collection of methods for creating an Group related items
     */
    var create = function(request) {

        return {
            campaign: ($collection)         => request.post([BASE_URL], $collection),
            teacher : ($id, $collection)    => request.post([BASE_URL, $id, 'teacher'], $collection),
        };
    };

    /**
     * @type
     * Group fetch services
     *
     * @description
     * Contains collection of methods for fetching an Group related items
     */
    var fetch = function(request, requestType) {

        return {
            campaignCollection  : ($query) => request.beffore(requestType).get([BASE_URL], $query),
            campaignSingle      : ($id) => request.beffore(requestType).get([BASE_URL, $id]),


            // all         : ($id) => request.beffore(requestType).get([BASE_URL]),
            // single      : ($id) => request.beffore(requestType).get([BASE_URL, $id]),
            // teachers    : ($id) => request.beffore(requestType).get([BASE_URL, 'teachers']),
        };
    }

    /**
     * @type
     * Class update services
     *
     * @description
     * Contains collection of methods for updating an Group related items
     */
    var update = function(request) {

        return {};
    };

    /**
     * @type
     * Group fetch services
     *
     * @description
     * Contains collection of methods for archiving an Group related items
     */
    var archive = function(request) {

        return {
            student : ($id) => request.delete([BASE_URL,'student', $id])
        };
    };

    /**
     *
     * @param {*} request
     * @param {*} base
     * @param {*} provider
     */
    var ApiProvider = function(request) {

        var requestType = {
            type : 'collection'
        };

        return {

            PAGINATE : function() {
                requestType.type = 'pagination';
                return this;
            },

            REFRESH : function() {
                requestType.type = 'refresh';
                return this;
            },

            COLLECTION : function() {
                requestType.type = 'collection';
                return this;
            },

            SINGLE : function() {
                requestType.type = 'single';
                return this;
            },

            create  : create(request),
            fetch   : fetch(request, requestType),
            update  : update(request),
            archive : archive(request)
        };
    };

    /**
     *
     * @param {*} http
     * @param {*} q
     * @param {*} AppManager
     */
    var ServiceHandler = function (Request) {
        return { api: ApiProvider(Request) };
    };

    ServiceHandler.$inject = ['Request'];
    angular.module('application.models').factory('CampaignModel', ServiceHandler);
})();
