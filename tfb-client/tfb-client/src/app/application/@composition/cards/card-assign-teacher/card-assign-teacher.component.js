(function () {

    var Controller = function (PubSub) {

        /**
         * @type
         * State Repository
         */
        this.STATE = {
            IS_EXPANDED : false
        };

        /**
         * @type
         * Event : @on click
         */
        this.on_toggle = () => {

            this.STATE.IS_EXPANDED = !this.STATE.IS_EXPANDED


            if(this.STATE.IS_EXPANDED) {
                PubSub.api.publish('LIST_ITEM_SELECTED', {
                    'item_id'       : this.id,
                    'item_slug'     : 'card-assign-teacher',
                    'item_command'  : 'open'
                });
            }

            if(!this.STATE.IS_EXPANDED) {

                PubSub.api.publish('LIST_ITEM_SELECTED', {
                    'item_id'       : this.id,
                    'item_slug'     : 'card-assign-teacher',
                    'item_command'  : 'close'
                });
            }
        }

        /**
         *
         */
        this.on_submit = () => {
            PubSub.form.init('FORM_ASSIGNE_TEACHER_SCHOOL');
        }
    }

    var Component = {
        templateUrl     : './card-assign-teacher.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            element     : '<',
            id          : '<'
        }
    };
    angular.module('application.components.visual').component('cardAssignTeacher', Component);

})();
