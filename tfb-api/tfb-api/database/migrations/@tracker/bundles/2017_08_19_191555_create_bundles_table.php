<?php

use \App\Http\Models\Bundle\Bundle;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBundlesTable extends Migration
{
    private $table = Bundle::TABLE;

    public function up()
    {
        Schema::create($this->table, function(Blueprint $builder)
        {
            //# Primary Key
            $builder->char(Bundle::ID, 13)->primary();

            //# Table Entity Collection
            $builder->string(Bundle::TFB_ID, 25);
            $builder->char(Bundle::SCHOOL_ID, 13);
            $builder->char(Bundle::INIT_YEAR, 4);
            $builder->char(Bundle::LABEL, 1);
            $builder->tinyInteger(Bundle::POSITION);
            $builder->char(Bundle::SIGNATURE, 4);

            //# Meta
            $builder->timestamps();
            $builder->softDeletes();

            // Indexes
            $builder->unique([Bundle::SCHOOL_ID, Bundle::LABEL, Bundle::POSITION]);
        });
    }

    public function down()
    {
        Schema::drop($this->table);
    }
}
