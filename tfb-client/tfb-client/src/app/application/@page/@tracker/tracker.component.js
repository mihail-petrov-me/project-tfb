(function () {

    /**
     *
     * @param {*} UserModel
     * @param {*} Translation
     * @param {*} State
     * @param {*} PubSub
     */
    var Controller = function (UserModel, Translation, State, PubSub) {

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {
            this.LABEL = {};
        });


        /**
         * @type
         * EVENT : request
         */
        this.request_FETCH_user = () => {

            let $requestQuery = {
                'teacher_id': UserModel.profile.my("user_id")
            };

            CampaignModel.api.COLLECTION().fetch
                .campaignCollection($requestQuery)
                    .then(this.handler_experiance_success)
                    .catch(this.handler_experiance_error);
        };

        /**
         * @type
         * HANDLER :  success
         */
        this.handler_experiance_success = ($response) => {
            UserModel.state("tracker", JSON.stringify($response.data[0]));
        };

        /**
         * @type
         * HANDLER :  error
         */
        this.handler_experiance_error = () => {
            console.log("@Exception: When we try to get user experiance");
        };



        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        this.$onInit = () => {

            if(!UserModel.profile.isPermited(['1', '4', '5'])) {
                this.$router.navigate(['/Dashboard']);
            }

            this.request_FETCH_user();
            console.log("TRACKER COMPONENT");

        };

    };

    var ComponentRouter = [

        //
        // #
        { path: 'init-tracker',             component: 'initTracker',           as: 'InitTracker', useAsDefault: true   },
        { path: 'assigne-class/...',        component: 'assigneClass',          as: 'AssigneClass'                      },
        { path: 'assigne-goal/...',         component: 'assigneGoal',           as: 'AssigneGoal'                       },
        { path: 'assigne-new-class/...',    component: 'assigneNewClass',       as: 'AssigneNewClass'                   },
        { path: 'assigne-student/:id',      component: 'assigneStudent',        as: 'AssigneStudent'                    },
        { path: 'program-block',            component: 'programBlock',          as: 'ProgramBlock'                      },
        { path: 'target-block',             component: 'targetBlock',           as: 'TargetBlock'                       },
        { path: 'assigne-target',           component: 'assigneTarget',         as: 'AssigneTarget'                     },
        { path: 'assigne-program',          component: 'assigneProgram',        as: 'AssigneProgram'                    },
        { path: 'class-calendar',           component: 'classCalendar',         as: 'ClassCalendar'                     },
        { path: 'class-calendar-bar',       component: 'classCalendarBar',      as: 'ClassCalendarBar'                  },
        { path: 'class-chart',              component: 'classChart',            as: 'ClassChart'                        },
        { path: 'class-chart-active',       component: 'classChartActive',      as: 'ClassChartActive'                  },
        { path: 'class-chart-avg',          component: 'classChartAvg',         as: 'ClassChartAvg'                     },
        { path: 'assigne-student-program',  component: 'assigneStudentProgram', as: 'AssigneStudentProgram'             },

        //
        // #
        { path: 'bundle/...'                ,  component: 'bundle'                      , as: 'Bundle'                  },
        { path: 'schedule/...'              ,  component: 'schedule'                    , as: 'Schedule'                },
        { path: 'classroom/...'             ,  component: 'classroom'                   , as: 'Classroom'               },
    ];

    Controller.$inject  = ['UserModel', 'Translation', 'State', 'PubSub'];
    var Component       = {
        templateUrl     : './tracker.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        $routeConfig    : ComponentRouter,
        bindings        : { $router     : '<' }
    };

    angular.module('application.components.page').component('tracker', Component);
})();
