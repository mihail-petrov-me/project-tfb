(function () {

    var Controller = function(Translation, PubSub, Form, UserModel, BundleModel, SubjectModel) {

        this.name = "FORM_ASSIGNE_EVENT";

        /**
         * @component
         * Fields collection
         *
         */
        this.FIELD = {

            "bundle"      : {
                name            : "bundle",
                type            : 'dropdown',
                title           : 'Клас',

                provider        : function($id) {

                    return BundleModel.api.COLLECTION().fetch.bundleSpecialCollection({
                        "teacher_id" : UserModel.profile.my('id')
                    });
                },

                reducer         : {
                    'id'    : 'id',
                    'title' : 'signature'
                },

                value           : null,
                isRequired      : true,
                show_errors     : false
            },

            "subject"      : {
                name            : "subject",
                type            : 'dropdown',
                title           : 'Предмет',

                provider        : function($id) {
                    return SubjectModel.api.COLLECTION().fetch.mySubjects(UserModel.profile.my('id'), $id);
                },

                reducer         : {
                    'id'    : 'subject_id',
                    'title' : 'subject_title'
                },

                value           : null,
                isRequired      : true,
                show_errors     : false
            },
        };

        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                OPTIONS_BUNDLE_PLACEHOLDER : $provider({
                    "bg"    : "Клас",
                    "en"    : "Class"
                }),

                OPTIONS_SUBJECT_PLACEHOLDER : $provider({
                    "bg"    : "Предмет",
                    "en"    : "E-mail"
                }),
            };
        });


        this.on_submit = () => {
            PubSub.form.init(this.name);
        }


        /**
         * @type
         * Lifecycle event
         *
         * @description
         * This method is executed on component init
         */
        this.$onInit = () => {
            this.parent.FIELD  = this.FIELD;
            this.formContact   = this.parent.formContact;
        };


    };

    var Component = {
        templateUrl     : './form-assigne-event.template.html',
        controller      : Controller,
        controllerAs    : 'model',

        require             : {
            parent          : '^formWrapper'
        },

        bindings        : {
            uiCollection    : '<',
            uiIsDisabled    : '<'
        }
    };

    angular.module('application.components.page').component('formAssigneEvent', Component);

})();
