<?php
namespace App\Http\Controllers\Api\Tracker;

use App\Http\Models\Goal\Goal;
use Illuminate\Database\QueryException;
use \Illuminate\Http\Request;
use \App\Http\Controllers\Controller;
use \Illuminate\Http\JsonResponse;


/**
 * Class GoalController
 * @package App\Http\Controllers\Api\Tracker
 */
class GoalController extends Controller
{
    /**
     * @var array
     */
    private $API_ERROR_MESSAGES = [
        'FETCH' => 'Requested entity was not found'
    ];

    private $model;

    /**
     * GoalController constructor.
     *
     * @param Goal $model
     */
    public function __construct(Goal $model)
    {
        $this->model           = $model;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchBundleCollection(Request $request): JsonResponse
    {
        $collection = $this->model->collection($request->query());
        return $this->responseSuccess($collection);
    }

    /**
     * @param $id
     *
     * @return JsonResponse
     */
    public function fetchBundleSingle($id): JsonResponse
    {
        $collection = $this->model->single($id);
        return $this->responseSuccess($collection);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createBundle(Request $request)
    {
        try {
            $collection = $this->model->create($request->json()->all());
            return $this->responseSuccess($collection);
        }
        catch (QueryException $exception)
        {
            return $this->responseNotFound("Entity already exists");
        }
    }

    /**
     * @param         $id
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function updateBundle($id, Request $request): JsonResponse
    {
        $collection = $this->model->update($id, $request->json()->all());
        return $this->responseSuccess( $collection);
    }

    /**
     * @param $id
     *
     * @return JsonResponse
     */
    public function deleteBundle($id): JsonResponse
    {
        $collection = $this->model->delete($id);
        return $this->responseSuccess($collection);
    }
}