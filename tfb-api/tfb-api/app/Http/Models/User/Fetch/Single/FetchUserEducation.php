<?php
namespace App\Http\Models\User\Fetch\Single;

use App\Http\Models\Repository;
use App\Http\Models\User\User;
use App\Http\Models\User\UserEducation;
use DB;

/**
 * Class FetchUserEducation
 * @package App\Http\Models\User\Fetch\Single
 */
class FetchUserEducation extends Repository
{
    /**
     * @param $id
     * @return mixed
     */
    public function execute($id)
    {
        $data = DB::table(UserEducation::TABLE)
            ->select([
                 UserEducation::ID,
                 UserEducation::INSTITUTION,
                 UserEducation::DATE_START,
                 UserEducation::DATE_END,
                 UserEducation::DEGREE,
                 UserEducation::FIELD_OF_STUDY
            ])
            ->where([
                 UserEducation::ID => $id
            ]);

        return $this->response($data);
    }
}