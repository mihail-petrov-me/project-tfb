(function () {

    var Directive = function () {

        return function(text, limit) {

            var changedString   = String(text).replace(/<[^>]+>/gm, '');
            var length          = changedString.length;
            var suffix          = '<span class="next"> ... </span>';

            return length > limit ? changedString.substr(0, limit - 1) + suffix : changedString;
        }
    };

    angular.module('application.components.directive').filter('limitHtml', Directive);
})();
