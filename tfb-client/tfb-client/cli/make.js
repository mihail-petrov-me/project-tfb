var fs = require('fs.extra');
var settings  = require('../settings');
var arguments = process.argv.slice(2);

//
var moduleName = arguments[0];
var componentName = arguments[1];
var componentDirectory = arguments[2];

var templateString = componentName + '.template.html';

//
var componentBlueprint = (settings.root + 'cli/blueprint/component.blueprint.js');
var componentReference = fs.readFileSync(componentBlueprint).toString();
var componentTemplate = componentReference.
  replace(/{{MODULE_NAME}}/, moduleName).
  replace(/{{COMPONENT_NAME}}/, componentName).
  replace(/{{TEMPLATE_URL}}/, templateString);

// Create a new folder
var componentDirectory = (settings.root + componentDirectory + '/' + componentName );
fs.mkdirSync(componentDirectory);


var componentFile = componentDirectory + '/' + componentName + '.component.js';
fs.writeFileSync(componentFile, componentTemplate);

var templateFile = componentDirectory + '/' + componentName + '.template.html';
fs.writeFileSync(templateFile, "");
