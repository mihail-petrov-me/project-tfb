(function() {

  /**
   * Bootstrap application component
   */
  var Controller = function() {

    var model = this;

    /**
     *
     */
    model.$onInit = function() {

    };

    /**
     *
     */
    model.$onDestroy = function() {

    };

  }

  /**
   *
   */
  var Component = {
    templateUrl: './{{TEMPLATE_URL}}',
    controller: Controller,
    controllerAs: 'model'
  };

  angular.module('{{MODULE_NAME}}', []).component('{{COMPONENT_NAME}}', Component);

})();
