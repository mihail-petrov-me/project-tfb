<?php

use \App\Http\Models\User\User;
use \App\Http\Models\User\UserEducation;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserEducationTable extends Migration
{
    private $table = UserEducation::TABLE;

    public function up()
    {
        Schema::create($this->table, function(Blueprint $builder) {

            //# Primary Key
            $builder->char(UserEducation::ID, 13)->primary();

            //# Foreign Key
//            $builder->char(UserEducation::USER_ID, 13);
//            $builder->index(UserEducation::USER_ID);
//            $builder->foreign(UserEducation::USER_ID)->references(User::ID)->on(User::TABLE);

            //# Table Entity Collection
            $builder->text(UserEducation::INSTITUTION)->nullable();
            $builder->date(UserEducation::DATE_START)->nullable();
            $builder->date(UserEducation::DATE_END)->nullable();
            $builder->string(UserEducation::DEGREE, 250)->nullable();
            $builder->text(UserEducation::FIELD_OF_STUDY)->nullable();

            //# Meta
            $builder->timestamps();
            $builder->softDeletes();
        });
    }

    public function down()
    {
      Schema::drop($this->table);
    }
}
