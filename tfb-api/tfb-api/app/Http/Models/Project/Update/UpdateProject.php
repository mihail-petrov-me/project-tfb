<?php
namespace App\Http\Models\Project\Update;

use Carbon\Carbon;
use DB;
use App\Http\Models\Project\Project;
use App\Http\Models\Repository;

/**
 * Class FetchProjectCollection
 */
class UpdateProject extends Repository
{
    /**
     * @param $id
     *
     * @param $collection
     *
     * @return mixed
     */
    public function execute($id, $collection)
    {
        return DB::table(Project::TABLE)->where([
            Project::ID             => $id
        ])->update([
            Project::TITLE          => $this->process($collection, Project::TITLE),
            Project::DESCRIPTION    => $this->process($collection, Project::DESCRIPTION),
            Project::START_DATE     => $this->process($collection, Project::START_DATE),
            Project::END_DATE       => $this->process($collection, Project::END_DATE),
            Project::UPDATED_AT     => Carbon::now()
        ]);
    }
}