(function () {

    var Controller = function(Translation) {

        this.name          = "FORM_CREATE_USER";

        /**
         * @component
         */
        this.FIELD = {

            "fname": {
                name            : "title",
                type            : 'text',
                value           : null,
                show_errors     : false,
                isRequired      : true
            },

            "lname": {
                name            : "lname",
                type            : 'text',
                value           : null,
                show_errors     : false,
                isRequired      : true
            },

            "email": {
                name            : "email",
                type            : 'text',
                value           : null,
                show_errors     : false,
                isRequired      : true
            }
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                INPUT_FNAME:  $provider({
                    'en' : 'Title',
                    'bg' : 'Име'
                }),

                INPUT_LNAME:  $provider({
                    'en' : 'Title',
                    'bg' : 'Фамилия'
                }),

                INPUT_EMAIL:  $provider({
                    'en' : 'Title',
                    'bg' : 'E-mail'
                })
            };
        });

        this.$onInit = () => {

            this.parent.FIELD  = this.FIELD;
            this.formContact   = this.parent.formContact;
        }
    };

    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject  = ['Translation', 'PubSub', 'Form'];
    var Component       = {
        templateUrl     : './form-create-user.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        require         : {
            parent      : '^formWrapper'
        },
    };

    angular.module('application.components.page').component('formCreateUser', Component);

})();
