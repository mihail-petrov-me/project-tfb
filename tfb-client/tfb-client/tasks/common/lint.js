var chalk = require('chalk');
var setting = require('../../settings');
var linter = new require('eslint');
var util = require('../helpers/util');

/**
 *
 */
var errorFormater = function(file, results) {

  console.log("");
  console.log(chalk.green(("# " + file)));
  console.log(chalk.green(("#")));

  for(var i = 0; i < results.length; i++) {

    var messageItem = results[i];
    var color = (messageItem.severity == 1) ? 'yellow' : 'red';
    console.log("> " + chalk[color](messageItem.line + " > " + messageItem.message));
  }
};

/**
 *
 */
var format = function(collection) {

  for(var i = 0; i < collection.length; i++) {
    errorFormater(collection[i].filePath, collection[i].messages);
  }
};

/**
 *
 */
exports.exec = function(base) {

  var fileCollection = [];
  util.traverse(base, function(fileName, directory, filePath) {
    if(util.isJs(fileName)) fileCollection.push(filePath);
  });

  var e = new linter.CLIEngine({
    configFile : (setting.root + '.eslintrc.json')
  });

  var report  = e.executeOnFiles(fileCollection);
  format(report.results);

  if(report.results[0].errorCount > 0) {
    process.exit();
  }

};
