(function () {

    var Controller = function (Translation, PubSub, UserModel, CampaignModel) {

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en' : '@Education campaign',
                    'bg' : '@Образователна кампания'
                })
            };
        });

        /**
         * @type
         * Request : FETCH : userfeed
         */
        this.request_CREATE_campaign = ($collection) => {

            CampaignModel.api.create.campaign($collection)
                .then(this.handle_CREATE_campaign_success)
                .catch(this.handle_CREATE_campaign_error);
        };

        /**
         * @type
         * Handler : FECH : SUCCESS
         */
        this.handle_CREATE_campaign_success = ($response) => {
            this.$router.navigate(['/Dashboard/Administration/CampaignSingle', {'id' : $response.data.id}]);
        };

        /**
         * @type
         * Handler : FECH : ERROR
         */
        this.handle_CREATE_campaign_error = () => {
            console.log("@Exception: Cannot create new campaign");
        };

        /**
         * @type
         * Lifecicle Event : $INIT
         */
        this.$onInit = () => {

            PubSub.api.subscribe('PANEL_CREATE_CAMPAIGN:END', ($response) => {
                this.request_CREATE_campaign($response);
            });
        }
    };

    Controller.$inject  = ['Translation', 'PubSub', 'UserModel', 'CampaignModel'];
    var Component       = {
        templateUrl     : './campaign-create.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            $router     : '<'
        }
    };

    angular.module('application.components.page').component('campaignCreate', Component);
})();
