<?php
namespace App\Http\Models\Bundle\Fetch;

use App\Http\Models\Repository;
use DB;

class FetchPrimaryGoals extends Repository
{
    public function execute()
    {
        return DB::table('tt_goal')->get();
    }
}