(function () {

    var Component = {
        templateUrl     : './ui-mobile-profile.template.html'
    };

    angular.module('application.components.visual').component('uiMobileProfile', Component);
})();
