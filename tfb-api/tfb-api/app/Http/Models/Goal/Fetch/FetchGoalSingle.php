<?php

namespace App\Http\Models\Goal\Fetch;
use App\Http\Models\Repository;
use DB;

class FetchGoalSingle extends Repository
{
    public function execute($id)
    {
        return DB::table('goals')->where(array(
            'id'    => $id
        ))->get();
    }
}