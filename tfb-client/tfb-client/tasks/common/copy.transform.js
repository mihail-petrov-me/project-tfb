// Core components
// # =============================================
var fs = require('fs-extra');

// Task interface
// # =============================================
exports.exec = function (options) {

    for (var i = 0; i < options.transform.length; i++) {

        var moduleReference = options.transform[i];
        if (moduleReference.path) {

            var source      = [options.from, moduleReference.path].join('');
            var destination = [options.to, moduleReference.script].join('');
            fs.copySync(source, destination);
        }
    }
};
