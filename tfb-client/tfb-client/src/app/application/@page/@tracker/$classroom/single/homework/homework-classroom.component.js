(function () {

    var Controller = function (Translation, PubSub, UserModel, BundleModel) {


        this.collection = [];

        this.itemSelected = null;

        this.bundleId = null;


        this.BLOCK = {
            VIEW_STUDENT_COLLECTION : true,
            VIEW_STUDENT_NOTE       : false
        }

        this.ATTENDANCE_ENUM = {
            'present'   : 1,
            'late'      : 2,
            'absent'    : 3
        };


        this.STATE = {
            VIEW_TIMER  : true,
            VIEW_INFO   : false,
            VIEW_NOTE   : false
        };

        this.VIEW = {
            NAVIGATION : true
        };



        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en' : '@Education campaign',
                    'bg' : '@Образователна кампания'
                })
            };
        });

        /**
         *
         */
        this.on_state = ($state) => {

            for(var index in this.STATE) {
                this.STATE[index] = false;
            }

            this.STATE[$state] = true;
        };

        /**
         *
         */
        this.is_state = ($state) => {
            return this.STATE[$state];
        };



        this.on_view = () => {

            this.VIEW.NAVIGATION                = true;
            this.BLOCK.VIEW_STUDENT_COLLECTION  = true;
            this.BLOCK.VIEW_STUDENT_NOTE        = false;
        };


        /**
         *
         */
        this.on_note = ($element) => {

            this.itemSelected = $element;

            // #
            this.VIEW.NAVIGATION                = false;

            // #
            this.BLOCK.VIEW_STUDENT_COLLECTION  = false;
            this.BLOCK.VIEW_STUDENT_NOTE        = true;
        };


        /**
         * @type
         * Event
         */
        this.on_close = () => {
            this.itemSelected = null;
        }

        /**
         *
         */
        this.on_create = () => {
            PubSub.form.init("FORM_CREATE_NOTE");
        };


        /**
         *
         */
        this.on_attendance = (element, $state) => {
            element['attendance'] = this.ATTENDANCE_ENUM[$state];
        };


        this.is_attendance = (element, $state) => {
            return (element['attendance'] == this.ATTENDANCE_ENUM[$state]);
        }

        /**
         *
         */
        this.on_attendance_process = () => {
            console.log(" ** Call atendancy ** ");
            console.log(this.collection);
        };

        /**
         * @type
         * Request : FETCH : userfeed
         */
        this.request_FETCH_students = () => {

            BundleModel.api.COLLECTION().fetch
            .studentCollection(this.bundleId)
                .then(this.handler_FETCH_students_success)
                .catch(this.handler_FETCH_students_error);
        };

        /**
         * @type
         * Handler : FECH : SUCCESS
         */
        this.handler_FETCH_students_success = ($response) => {

            this.collection = $response.data;

            for(var i = 0; i < this.collection.length; i++) {
                this.collection[i]['attendance'] = 1;
            }

            PubSub.api.publish('event:ui:after_load');
        };

        /**
         * @type
         * Handler : FECH : ERROR
         */
        this.handler_FETCH_students_error = ($response) => {
            console.log("@Exception: Cannot fetch current active teachers");
        };


        /**
         * @type
         * Lifecycle event
         */
        this.$onInit = () => {

        };

        /**
         * @type
         * Router event
         */
        this.$routerOnActivate = (next, previous) => {

            this.bundleId = next.params.id;
            this.request_FETCH_students();
        };
    };


    Controller.$inject  = ['Translation', 'PubSub', 'UserModel', 'BundleModel'];
    var Component       = {
        templateUrl     : './homework-classroom.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            $router     : '<'
        }
    };

    angular.module('application.components.page').component('homeworkClassroom', Component);
})();
