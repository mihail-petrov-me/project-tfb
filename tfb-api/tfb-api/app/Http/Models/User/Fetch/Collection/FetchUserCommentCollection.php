<?php

namespace App\Http\Models\User\Fetch\Collection;
use App\Http\Models\Repository;
use DB;

/**
 * Class FetchUserCommentCollection
 * @package App\Http\Models\User\Fetch\Collection
 */
class FetchUserCommentCollection extends Repository
{
    public function execute($id)
    {
       return DB::table('block_post_comment')
            ->join(
                'users',
                'block_post_comment.user_id', '=', 'users.id'
            )
            ->where(
                'block_post_comment.entity_id', $id
            )
            ->orderBy('created_at', 'desc');
    }
}