(function () {

    var Controller = function (UserModel) {

    }

    /**
     *
     */
    var Component = {
        templateUrl     : './ui-list-class-line.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            collection    : '<',
            uiLoadMore    : '<'
        }
    };

    /**
     *
     */
    angular.module('application.components.visual').component('uiListClassLine', Component);

})();
