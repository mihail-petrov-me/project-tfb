(function () {

    /**
     *
     * @param {*} Translation
     * @param {*} PubSub
     */
    var Controller = function (Translation, PubSub) {

        /**
         * @type
         * State provider
         *
         * @description
         * Preserve the component state insithe the component and component scope
         * it is eqvivalent of @scope in non component environment
         */
        var model = this;

        /**
         * @type
         * State collection
         *
         * @description
         * The state collection contain every single state thro out the current component
         */
        model.STATE = {
            SHOW_OPTIONS_LIST : false
        };


        /**
         * @type
         * Translation collection
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        Translation.$translate(function($provider) {

            model.LABEL = {

                PLACEHOLDER : $provider({
                    'en' : 'Search for something',
                    'bg' : 'Намери нещо интересно'
                })
            };
        });

        /**
         * @type
         * Event : click
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        model.on_search = function() {
            PubSub.api.publish('event:ui:search_input', { 'q' : model.search });
        };


        /**
         * @type
         * Event : click
         *
         * @description
         * This method contains collection of every single label that neads to be translated for
         * the current component, in orderfor the localisation to work properly
         */
        model.on_clear = function() {

            model.search = null;
            PubSub.api.publish('event:ui:search_input', { 'q' : model.search });
        }
    }

    /**
     * @type
     * Component injector
     *
     * @description
     * In order to prevent the minification tool to invalidate the
     * application component we must inject there srtring literal values directly
     */
    Controller.$inject = ['Translation', 'PubSub'];

    var Component = {
        templateUrl     : './ui-input-search.template.html',
        controller      : Controller,
        controllerAs    : 'model'
    };

    angular.module('application.components.visual').component('uiInputSearch', Component);

})();
