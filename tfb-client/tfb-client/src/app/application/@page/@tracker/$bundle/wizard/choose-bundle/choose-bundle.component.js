(function () {

    var Controller = function (Translation, PubSub, UserModel, BundleModel) {

        this.PROPERTY = {
            COLLECTION : []
        };

        /**
         * @type
         * Translation collection
         */
        Translation.$translate(($provider) => {

            this.LABEL = {

                SIDEBAR_TITLE : $provider({
                    'en' : 'Assign existent class',
                    'bg' : 'Припознай клас'
                })
            };
        });

        /**
         *
         * @param id
         */
        this.request_UPDATE_teacherSubject = (id) => {

            let teacherId = {
                'teacher_id' : UserModel.profile.my("user_id")
            };

            BundleModel.api.update.assign(id, teacherId )
                .then(this.handle_UPDATE_teacherSubject_success(id))
                .catch(this.handle_UPDATE_teacherSubject_error);
        };

        /**
         *
         * @param id
         */
        this.handle_UPDATE_teacherSubject_success = (id) => {

            PubSub.api.publish("WIZARD::PROCESS");
            UserModel.state("__bundle_id__", id);
        };

        /**
         *
         */
        this.handle_UPDATE_teacherSubject_error = () => {
            console.log("@Exception: Cannot apply this bundle to this teacher because the record already exists");
        };

        /**
         *
         */
        this.request_FETCH_bundleCollection = () => {

            let $queryRequest = {
                'school_id'     : JSON.parse(UserModel.state('tracker'))['school_id'],
                'teacher_id'    : UserModel.profile.my("user_id")
            };

            BundleModel.api.COLLECTION().fetch.bundleCollection($queryRequest)
                .then(this.handle_FETCH_bundleCollection_success)
                .catch(this.handle_FETCH_bundleCollection_error);
        };

        /**
         *
         */
        this.handle_FETCH_bundleCollection_success = ($request) => {
            this.PROPERTY.COLLECTION = $request.data;
        };

        /**
         *
         */
        this.handle_FETCH_bundleCollection_error = () => {
            console.log("@Exception: Cant fetch bundle collection");
        };

        /**
         *
         * @param element
         */
        this.on_process = (element) => {
            this.request_UPDATE_teacherSubject(element.id);
        };

        /**
         * @type
         * Lifecycle event : INIT
         */
        this.$onInit = () => {
            this.request_FETCH_bundleCollection();
        };
    };

    Controller.$inject  = ['Translation', 'PubSub', 'UserModel', 'BundleModel'];
    let Component       = {
        templateUrl     : './choose-bundle.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : { $router     : '<' }
    };

    angular.module('application.components.page').component('chooseBundle', Component);
})();
