((() => {

    /**
     * Base URL is the main provider of the external servicess content.
     */
    const BASE_URL = "http://api.zaednovchas.bg"

    /**
     * Parse input query collection in order to produce fully applicapeb web adress
     * Every queryCollection starts with ? and contains N numbers of key value pairs
     *
     * @example
     * Input data {filter : "data", page : "10"} will produce ?filter=data&page=10
     *
     * @param {*} queryCollection
     */
    const __parseQuery = (queryCollection = {}) => {

        if(Object.keys(queryCollection).length === 0) return "";

        let queryBuilder = ['?'];
        for(var index in queryCollection) {
            if(queryCollection[index]) queryBuilder.push([index, '=', queryCollection[index]].join(''));
        }

        return (queryBuilder.join('&'));
    }

    var Service = () => {

        /**
         * @description
         * This function build and return a new url adress based
         * on the specific based url and the specifid nby the user path
         *
         * @param {string} path
         *
         * @returns
         * string - build path
         */
        function url(collection, queryCollection) {

            let urlBuilder = [BASE_URL];
            for(var index = 0; index < collection.length; index++) {
                if(collection[index]) urlBuilder.push(collection[index]);
            }

            return [urlBuilder.join('/'), __parseQuery(queryCollection)].join('');
        }

        return { url };
    };

    angular.module('application.service').factory('AppManager', Service);

})());
