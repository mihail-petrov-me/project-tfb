(function () {

    var AuthenticationProviders = {

        'GOOGLE'    : 'google',
        'FACEBOOK'  : 'facebook',
        'APP'       : 'app'
    };

    /**
     * Configuration of every singe OAuthe provider
     * This configuration if foing to be apply to the autehntication uri generator
     */
    var AuthenticationProviderConfig = {

        "app"    : {
            'type'      : 'custom',
            'handler'   : function(__handler, collection) {
                return __handler.auth('app', collection);
            },
        },


        "google" : {

            'type'      : 'oauth',
            'handler'   : function(__handler, tokken) {
                return __handler.auth("google", { "code": decodeURIComponent(tokken) });
            },

            'uri'               : 'https://accounts.google.com/o/oauth2/auth',
            'properties'        : {
                'client_id'     : '152370577815-1575o6o4shgl2rvmhic0e98cpdd24l9i.apps.googleusercontent.com',
                'response_type' : 'code',
                'scope'         : 'email profile',
                'prompt'        : 'select_account',
                'access_type'   : 'offline'
            }

        },

        'facebook' : {
            'type'      : 'oauth',
            'handler'   : function(AppHandler, collection) {
                return AppHandler.profile.auth('facebook', collection);
            },

            'uri'   : 'https://www.facebook.com/v2.8/dialog/oauth',
            'properties'        : {
                'client_id'     : '120358461835764',
                'response_type' : 'code',
                'scope'         : 'email public_profile'
            }
        }
    };


    /**
     *
     * @param {*} http
     * @param {*} window
     * @param {*} q
     */
    var Service = function($http, $window, $q, UserModel) {

        var HandlerAuthentication = {

            'oauth' : function(configuration, collection) {

                var userState   = $q.defer();
                var handler     = oAuthAuthenticateFactory(configuration);


                handler.then(function(data) {
                    userState.resolve(configuration.handler(UserModel, data));
                });


                handler.catch(function(data) {
                    userState.reject(data.status);
                });

                return userState.promise;
            },

            'custom' : function(configuration, collection) {
                return configuration.handler(UserModel, collection);
            }
        };

        /**
         * @description
         * This is a specific configuration for the current application
         * the authentication URI is diferent based on the provider.
         *
         * @return
         * Web URL containing the provider authentication string
         */
        function getAuthenticationURI(providerCollection) {

            var collection = [];

            if(!providerCollection.properties['redirect_uri']) {
                providerCollection.properties['redirect_uri'] = $window.location.origin;
            }

            for(var index in providerCollection.properties) {
                collection.push([index, '=', providerCollection.properties[index]].join(''));
            }

            return [providerCollection.uri, collection.join('&')].join('?');
        };

        /**
         * @description
         * When the user try to authenticate usinmg the specific provider,
         * an authentication tokken is generated.
         * This tokken must be retreved and provided in order for the authentication process to move forward
         * @param {*} authenticationPanel
         *
         * @return
         * authentication tokken of the specific provider agent
         */
        function getAuthenticationToken(authenticationPanel) {

            var authenticationPanelURI = authenticationPanel.document.URL;
            return authenticationPanelURI.substring(authenticationPanelURI.lastIndexOf("=") + 1, authenticationPanelURI.lastIndexOf("#"));
        };


        /**
         * @description
         * This function is going to process the authentication process
         * funcrder only if the tokken is valid.
         *
         * @param {*} tokken
         * @param {*} callback
         *
         * @return
         * void call callback function
         */
        function checkAuthenticationToken(tokken, callback) {

            if (tokken != undefined && tokken != "") {
                callback(tokken);
            }
        };

        /**
         * @description
         * Open a modal pop up window for the specific application
         *
         * @return
         * instance of window object
         *
         */
        function openAuthenticationWindow(uri) {
            return $window.open(uri, 'Authentication Window', "width=500, height=500, left=100, top=100");
        };


        /**
         * Fasade for authenticating OAuth based connections
         *
         * @param {*} provider
         *
         * @return
         * Promise - containing autehntication token
         */
        function oAuthAuthenticateFactory(configCollection) {

            var auhenticationState  = $q.defer();
            var authenticationUri   = getAuthenticationURI(configCollection);
            var authenticationPanel = openAuthenticationWindow(authenticationUri);


            var authenticationInterval = setInterval(function () {

                try {

                    var authenticationPanelCode = getAuthenticationToken(authenticationPanel);

                    checkAuthenticationToken(authenticationPanelCode, function (authenticationCode) {

                        authenticationPanel.close();
                        clearInterval(authenticationInterval);

                        auhenticationState.resolve(authenticationCode);
                    });
                }
                catch (e) { }

            }, 500);

            return auhenticationState.promise;
        };


        /**
         * Fasade for authenticating user based on specific provider
         *
         * @param {*} provider
         *
         * @return
         * Promise - containing autehntication token
         */
        function authenticate(provider, collection) {

            let configuration = AuthenticationProviderConfig[provider];
            return HandlerAuthentication[configuration.type](configuration, collection);
        }

        return {
            authenticate    : authenticate,
            providers       : AuthenticationProviders
        };
    };


    Service.$inject = ['$http', '$window', '$q', 'UserModel'];
    angular.module('application.service').factory('Authentication', Service);

})();
