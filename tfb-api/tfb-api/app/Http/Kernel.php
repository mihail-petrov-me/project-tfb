<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{

    protected $middleware = [
        \Barryvdh\Cors\HandleCors::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [],
        'api' => [ 'throttle:60,1' ],
    ];

    protected $routeMiddleware = [
        'throttle'  => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'jwt'       =>  \App\Http\Middleware\JwtMiddleware::class
    ];
}
