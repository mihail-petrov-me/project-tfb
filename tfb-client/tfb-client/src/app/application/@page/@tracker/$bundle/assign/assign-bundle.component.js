(function () {

    var Controller = function (Translation, PubSub, UserModel, BundleModel) {

        this.$onInit = () => {
            PubSub.api.publish('event:ui:after_load');
        }
    };

    Controller.$inject  = ['Translation', 'PubSub', 'UserModel', 'BundleModel'];
    let Component       = {
        templateUrl     : './assign-bundle.template.html',
        controller      : Controller,
        controllerAs    : 'model',
        bindings        : {
            $router     : '<'
        }
    };

    angular.module('application.components.page').component('assignBundle', Component);
})();
