<?php
namespace App\Services\Authentication\providers;

use App\Http\Models\User\User;

/**
 * Class ApplicationAuthenticationProvider
 * @package App\Services\Authentication\providers
 */
class ApplicationAuthenticationProvider
{
    /**
     * @var array
     */
    private $EXCEPTION_COLLECTION = [
        'HANDLE'    => 'NO valid credentials available for the current user'
    ];

    /**
     *
     * @param array $collection
     *
     * @return array
     * @throws \Exception
     * @internal param array $request
     */
    public function process(array $collection) : array
    {
        $profileCollection = $this->handle($collection);
        if(empty($profileCollection)) {
            throw new \RuntimeException($this->EXCEPTION_COLLECTION['HANDLE']);
        }

        //$profileCollection['permissions'] = $this->getPermissions($profileCollection[User::ID]);
        return $profileCollection;
    }

    /**
     * @param $collection
     *
     * @return mixed
     */
    private function handle($collection)
    {
        return (new User())->fetch_user($collection);
    }

    /**
     * @param string $id
     * @return array
     */
    private function getPermissions(string $id): array
    {
        return (new User())->fetch_userPermission($id);
    }
}