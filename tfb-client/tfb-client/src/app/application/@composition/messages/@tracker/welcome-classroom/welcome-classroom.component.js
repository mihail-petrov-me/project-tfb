(function () {

    var Component = {
        templateUrl     : './welcome-classroom.template.html',
        controllerAs    : 'model',
        bindings        : {
            showIf      : '<'
        }
    };

    angular.module('application.components.visual').component('welcomeClassroom', Component);

})();
